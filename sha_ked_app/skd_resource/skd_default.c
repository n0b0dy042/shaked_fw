#include "sha_ked_common.h"
#include <string.h>

#define DISABLE  NULL
#define ENABLE   !DISABLE
 

#define SKD_DEFAULT_BATTERY_LEVEL_VOLT               3200  
#define SKD_DEFAULT_RECORD_EVENT_TYPE                1

#define SKD_BLE_DEFAULT_GAIN                         2
#define SKD_BLE_DEFAULT_WAKEUP_TIMEOUT_IN_SEC        60

#define SKD_DEFAULT_GNSS_ON_TIME_INTERVAL            60
#define SKD_DEFAULT_GNSS_OFF_TIME_INTERVAL           30
#define SKD_DEFAULT_BLE_ON_TIME_INTERVAL             60
#define SKD_DEFAULT_BLE_OFF_TIME_INTERVAL            30

#define SKD_GNSS_DEFAULT_DISTANCE_THERSHOLD_SEC      10 
#define SKD_GNSS_DEFAULT_TIME_THRESHOLD              30
#define SKD_GNSS_DEFAULT_ANGLE_THRESHOLD             40 
#define SKD_GNSS_DEFAULT_DISTANCE_FILTER             50
#define SKD_GNSS_DEFAULT_SPEED_FILTER                70
#define SKD_GNNS_DEFAULT_SEARCH_TIMEOUT              100 

#define SKD_BUTTONS_DEFAULT_TIMEOUT                  60

const skd_data_t skd_default_data  = {

    .year   = ( (CURRENT_YEAR) - 2000), 
    .month  = CURRENT_MONTH,
    .day    = CURRENT_DAY, 

};  

const skd_time_t skd_default_time  = {
    
    .hours        = (CURRENT_HOUR-3),
    .minutes      = CURRENT_MIN,
    .seconds      = CURRENT_SEC,
    .microseconds = 700

};  

const skd_gnss_data_t skd_default_gnns_data = {
    
    .angle            = 172, 
    .fix_type         = 2, 
    .hdop             = 12,
    .latitude         = 54.687157,
    .longitude        = 25.279652,
    .satellites_count = 10,
    .speed            = 2,
    .reliability_flag = 0,
     
}; 


const skd_device_info_t skd_default_devInfo = {
      
      .deviceType  = 10,
      .hw0         = 1, 
      .hw1         = 2, 
      .hw2         = 3,
      .fw0         = CURRENT_YEAR-2000, 
      .fw1         = CURRENT_MONTH, 
      .fw2         = CURRENT_DAY
  
};  


const skd_ble_settings_t skd_default_bleSettings = {
  
  .mode                     = { 
                                .AO    = ENABLE,
                                .EXANTEN = DISABLE,
  }, 
  
  .defaultWakeUpTimeoutSec  = SKD_BLE_DEFAULT_WAKEUP_TIMEOUT_IN_SEC, 
  .gain                     = SKD_BLE_DEFAULT_GAIN, 
  
  //.deviceName    = "GPS TRACKER_DEBUG",
  //.deviceNameLen = 17
}; 

const skd_ms_settings_t skd_default_msSettings = {
  
    .mode                     = {
          .DSM = ENABLE,
          .DEM = ENABLE,
          .DLS = ENABLE
     },
    .sensitivity              = 8,
    .movmentStartTimeoutSec   = 1, 
    .movmentStopTimeoutSec    = 30,//2*60,
    .longStopTimeoutSec       = 15*60,  

};  

const skd_timer_settings_t skd_default_timerSettings = {

  .mode                       = {
                  .DBTI = ENABLE,
                  .DGTI = ENABLE, 
                  .DBTZ = DISABLE, 
  },             
  .gnssOnTimeIntervalSec      = SKD_DEFAULT_GNSS_ON_TIME_INTERVAL,
  .gnssOffTimeIntervalSec     = SKD_DEFAULT_GNSS_OFF_TIME_INTERVAL,
  .bleOnTimeIntervalSec       = SKD_DEFAULT_BLE_ON_TIME_INTERVAL,
  .bleOffTimeIntervalSec      = SKD_DEFAULT_BLE_OFF_TIME_INTERVAL,

};  

const skd_gnss_settings_t skd_default_gnssSettings = {
    
    .powerMode                      = {
        .AO    = ENABLE,
        .WFTI  = DISABLE,
        .WFMT  = DISABLE,
        .SSIE  = DISABLE, 
        .EXANTEN = ENABLE,
     },

    .DSPmode                        = {

         .DG   = DISABLE,
         .DDT  = DISABLE,
         .DTT  = ENABLE,
         .DAT  = DISABLE,

    },                             
    
    .saveFilterMode                 = {

        .DFEN = DISABLE,
        .SFEN = DISABLE,

    }, 

    .distanceThresholdMeter         = SKD_GNSS_DEFAULT_DISTANCE_THERSHOLD_SEC,
    .timeThresholdSeconds           = SKD_GNSS_DEFAULT_TIME_THRESHOLD, 
    .angleThresholdDegree           = SKD_GNSS_DEFAULT_ANGLE_THRESHOLD, 
    .speedFilterThresholdKmOverHour = SKD_GNSS_DEFAULT_DISTANCE_FILTER,
    .distanceFilterThresholdMeter   = SKD_GNSS_DEFAULT_DISTANCE_FILTER,
    .searchTimeoutSec               = SKD_GNNS_DEFAULT_SEARCH_TIMEOUT

};  

const skd_button_settings_t skd_default_buttonSettings = {
    
    .mode                 = DETECT_SINGLE_PRESS, 
    .detectionTimeoutSec  = SKD_BUTTONS_DEFAULT_TIMEOUT

}; 


const skd_led_settings_t skd_default_ledSettings = {

    .mode               = LED_ALLWAYS_OFF, //LED_ALLWAYS_ON, //LED_ON_ONLY_AFTER_START,
    .defaultTimeoutSec  = 59

};   


const skd_db_settings_t  skd_default_dbSettings   = {
  
  .mode      = OVER_WRITE_DISABLE, 
  .cashSize  = 10 

};  



void skd_set_default_status(skd_device_status_t* pDevStatus ){

   pDevStatus->batteryLevelVolt = SKD_DEFAULT_BATTERY_LEVEL_VOLT;
   pDevStatus->chargeStatus     = NULL; 
   pDevStatus->motionStatus     = 1; 

   memcpy(&pDevStatus->gnssData, &skd_default_gnns_data, sizeof( skd_gnss_data_t ) ); 

}   


void skd_set_default_record(skd_snapshot_t* pRecord){

  pRecord->evtId = SKD_DEFAULT_RECORD_EVENT_TYPE; 
 
  skd_set_default_status(&pRecord->devStatus); 
  
  skd_time_t* pTime = &pRecord->timeStamp.time; 
  skd_data_t* pData = &pRecord->timeStamp.date;  
  
  memcpy(pData, &skd_default_data, sizeof(skd_data_t)); 
  memcpy(pTime, &skd_default_time, sizeof(skd_time_t));  


}  

void skd_set_defualt_ts_config(skd_timer_settings_t* pSettings){
  
  memcpy( pSettings, &skd_default_timerSettings, sizeof(skd_timer_settings_t)); 

} 
void skd_set_defualt_settings(skd_settings_t* pSettings){

  memcpy(&pSettings->bleSettings,    &skd_default_bleSettings,     sizeof(skd_default_bleSettings)    );  
  memcpy(&pSettings->gnnsSettings,   &skd_default_gnssSettings,    sizeof(skd_default_gnssSettings)   );
  memcpy(&pSettings->msSettings,     &skd_default_msSettings,      sizeof(skd_default_msSettings)     );
  memcpy(&pSettings->timerSettings,  &skd_default_timerSettings,   sizeof(skd_default_timerSettings)  );
  memcpy(&pSettings->butSettings,    &skd_default_buttonSettings,  sizeof(skd_default_buttonSettings) );
  memcpy(&pSettings->ledSettings,    &skd_default_ledSettings,     sizeof(skd_default_ledSettings)    );
  memcpy(&pSettings->dbSettings,     &skd_default_dbSettings,      sizeof(skd_default_ledSettings)    );

} 

void skd_set_default_time(skd_timestamp_t* pTime){

  memcpy(&pTime->time, &skd_default_time, sizeof(skd_time_t));  
  memcpy(&pTime->date, &skd_default_data, sizeof(skd_data_t));
      
}   
void skd_set_default_bleSettings(skd_ble_settings_t* pSettings){

   memcpy(pSettings, &skd_default_bleSettings, sizeof(skd_ble_settings_t)); 

} 

void skd_set_safe_bleSettings(skd_ble_settings_t* pSettings){

   memcpy(pSettings, &skd_default_bleSettings, sizeof(skd_ble_settings_t)); 

} 