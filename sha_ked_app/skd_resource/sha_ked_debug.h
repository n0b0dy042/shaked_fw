#ifndef __DEBUG_SETTINGS_H__
#define __DEBUG_SETTINGS_H__


// <e> SHA_KED_MAIN_LOG_ENABLED - Enables logging in the module.
//==========================================================

#ifndef SHA_KED_MAIN_LOG_ENABLED
#define SHA_KED_MAIN_LOG_ENABLED      1
#endif

// <0=> Off 
// <1=> Error 
// <2=> Warning 
// <3=> Info 
// <4=> Debug 

#ifndef SHA_KED_MAIN_LOG_LEVEL
#define SHA_KED_MAIN_LOG_LEVEL        4
#endif

// <o> NRF_LOG_WARNING_COLOR  - ANSI escape code prefix.
 
// <0=> Default 
// <1=> Black 
// <2=> Red 
// <3=> Green 
// <4=> Yellow 
// <5=> Blue 
// <6=> Magenta 
// <7=> Cyan 
// <8=> White 

#ifndef SHA_KED_MAIN_INFO_LOG_COLOR
#define SHA_KED_MAIN_INFO_LOG_COLOR        1
#endif
// <o> NRF_LOG_WARNING_COLOR  - ANSI escape code prefix.
 
// <0=> Default 
// <1=> Black 
// <2=> Red 
// <3=> Green 
// <4=> Yellow 
// <5=> Blue 
// <6=> Magenta 
// <7=> Cyan 
// <8=> White 

#ifndef SHA_KED_MAIN_DEBUG_LOG_COLOR
#define SHA_KED_MAIN_DEBUG_LOG_COLOR        1
#endif

// <e> SHA_KED_FDS_LOG_ENABLED - Enables logging in the module.
//==========================================================

#ifndef SHA_KED_FDS_LOG_ENABLED
#define SHA_KED_FDS_LOG_ENABLED      0
#endif

// <0=> Off 
// <1=> Error 
// <2=> Warning 
// <3=> Info 
// <4=> Debug 

#ifndef SHA_KED_FDS_LOG_LEVEL
#define SHA_KED_FDS_LOG_LEVEL        4
#endif

// <o> NRF_LOG_WARNING_COLOR  - ANSI escape code prefix.
 
// <0=> Default 
// <1=> Black 
// <2=> Red 
// <3=> Green 
// <4=> Yellow 
// <5=> Blue 
// <6=> Magenta 
// <7=> Cyan 
// <8=> White 

#ifndef SHA_KED_FDS_INFO_LOG_COLOR
#define SHA_KED_FDS_INFO_LOG_COLOR        1
#endif
// <o> NRF_LOG_WARNING_COLOR  - ANSI escape code prefix.
 
// <0=> Default 
// <1=> Black 
// <2=> Red 
// <3=> Green 
// <4=> Yellow 
// <5=> Blue 
// <6=> Magenta 
// <7=> Cyan 
// <8=> White 

#ifndef SHA_KED_FDS_DEBUG_LOG_COLOR
#define SHA_KED_FDS_DEBUG_LOG_COLOR        1
#endif


// <e> SHA_KED_MACHINE_LOG_ENABLED - Enables logging in the module.
//==========================================================

#ifndef SHA_KED_MACHINE_LOG_ENABLED
#define SHA_KED_MACHINE_LOG_ENABLED      1
#endif

// <0=> Off 
// <1=> Error 
// <2=> Warning 
// <3=> Info 
// <4=> Debug 

#ifndef SHA_KED_MACHINE_LOG_LEVEL
#define SHA_KED_MACHINE_LOG_LEVEL        4
#endif

// <o> NRF_LOG_WARNING_COLOR  - ANSI escape code prefix.
 
// <0=> Default 
// <1=> Black 
// <2=> Red 
// <3=> Green 
// <4=> Yellow 
// <5=> Blue 
// <6=> Magenta 
// <7=> Cyan 
// <8=> White 

#ifndef SHA_KED_MACHINE_INFO_LOG_COLOR
#define SHA_KED_MACHINE_INFO_LOG_COLOR        3
#endif
// <o> NRF_LOG_WARNING_COLOR  - ANSI escape code prefix.
 
// <0=> Default 
// <1=> Black 
// <2=> Red 
// <3=> Green 
// <4=> Yellow 
// <5=> Blue 
// <6=> Magenta 
// <7=> Cyan 
// <8=> White 

#ifndef SHA_KED_MACHINE_DEBUG_LOG_COLOR
#define SHA_KED_MACHINE_DEBUG_LOG_COLOR        1
#endif







// <e> SHA_KED_PA_LNA_LOG_ENABLED - Enables logging in the module.
//==========================================================

#ifndef SHA_KED_PA_LNA_LOG_ENABLED
#define SHA_KED_PA_LNA_LOG_ENABLED      0
#endif

// <0=> Off 
// <1=> Error 
// <2=> Warning 
// <3=> Info 
// <4=> Debug 

#ifndef SHA_KED_PA_LNA_LOG_LEVEL
#define SHA_KED_PA_LNA_LOG_LEVEL        4
#endif

// <o> NRF_LOG_WARNING_COLOR  - ANSI escape code prefix.
 
// <0=> Default 
// <1=> Black 
// <2=> Red 
// <3=> Green 
// <4=> Yellow 
// <5=> Blue 
// <6=> Magenta 
// <7=> Cyan 
// <8=> White 

#ifndef SHA_KED_PA_LNA_INFO_LOG_COLOR
#define SHA_KED_PA_LNA_INFO_LOG_COLOR        1
#endif
// <o> NRF_LOG_WARNING_COLOR  - ANSI escape code prefix.
 
// <0=> Default 
// <1=> Black 
// <2=> Red 
// <3=> Green 
// <4=> Yellow 
// <5=> Blue 
// <6=> Magenta 
// <7=> Cyan 
// <8=> White 

#ifndef SHA_KED_PA_LNA_DEBUG_LOG_COLOR
#define SHA_KED_PA_LNA_DEBUG_LOG_COLOR        1
#endif



// <e> SHA_KED_EVENT_SERVICE_LOG_ENABLED - Enables logging in the module.
//==========================================================

#ifndef SHA_KED_EVENT_SERVICE_LOG_ENABLED
#define SHA_KED_EVENT_SERVICE_LOG_ENABLED      0
#endif

// <0=> Off 
// <1=> Error 
// <2=> Warning 
// <3=> Info 
// <4=> Debug 

#ifndef SHA_KED_EVENT_SERVICE_LOG_LEVEL
#define SHA_KED_EVENT_SERVICE_LOG_LEVEL        4
#endif

// <o> NRF_LOG_WARNING_COLOR  - ANSI escape code prefix.
 
// <0=> Default 
// <1=> Black 
// <2=> Red 
// <3=> Green 
// <4=> Yellow 
// <5=> Blue 
// <6=> Magenta 
// <7=> Cyan 
// <8=> White 

#ifndef SHA_KED_EVENT_SERVICE_INFO_LOG_COLOR
#define SHA_KED_EVENT_SERVICE_INFO_LOG_COLOR        1
#endif
// <o> NRF_LOG_WARNING_COLOR  - ANSI escape code prefix.
 
// <0=> Default 
// <1=> Black 
// <2=> Red 
// <3=> Green 
// <4=> Yellow 
// <5=> Blue 
// <6=> Magenta 
// <7=> Cyan 
// <8=> White 

#ifndef SHA_KED_EVENT_SERVICE_DEBUG_LOG_COLOR
#define SHA_KED_EVENT_SERVICE_DEBUG_LOG_COLOR        1
#endif



// <e> SHA_KED_ADC_SREVICE_LOG_ENABLED - Enables logging in the module.
//==========================================================

#ifndef SHA_KED_ADC_SREVICE_LOG_ENABLED
#define SHA_KED_ADC_SREVICE_LOG_ENABLED      0
#endif

// <0=> Off 
// <1=> Error 
// <2=> Warning 
// <3=> Info 
// <4=> Debug 

#ifndef SHA_KED_ADC_SREVICE_LOG_LEVEL
#define SHA_KED_ADC_SREVICE_LOG_LEVEL        4
#endif

// <o> NRF_LOG_WARNING_COLOR  - ANSI escape code prefix.
 
// <0=> Default 
// <1=> Black 
// <2=> Red 
// <3=> Green 
// <4=> Yellow 
// <5=> Blue 
// <6=> Magenta 
// <7=> Cyan 
// <8=> White 

#ifndef SHA_KED_ADC_SREVICE_INFO_LOG_COLOR
#define SHA_KED_ADC_SREVICE_INFO_LOG_COLOR        1
#endif
// <o> NRF_LOG_WARNING_COLOR  - ANSI escape code prefix.
 
// <0=> Default 
// <1=> Black 
// <2=> Red 
// <3=> Green 
// <4=> Yellow 
// <5=> Blue 
// <6=> Magenta 
// <7=> Cyan 
// <8=> White 

#ifndef SHA_KED_ADC_SREVICE_DEBUG_LOG_COLOR
#define SHA_KED_ADC_SREVICE_DEBUG_LOG_COLOR        1
#endif



// <e> SHA_KED_IND_SERVICE_LOG_ENABLED - Enables logging in the module.
//==========================================================

#ifndef SHA_KED_IND_SERVICE_LOG_ENABLED
#define SHA_KED_IND_SERVICE_LOG_ENABLED      0
#endif

// <0=> Off 
// <1=> Error 
// <2=> Warning 
// <3=> Info 
// <4=> Debug 

#ifndef SHA_KED_IND_SERVICE_LOG_LEVEL
#define SHA_KED_IND_SERVICE_LOG_LEVEL        4
#endif

// <o> NRF_LOG_WARNING_COLOR  - ANSI escape code prefix.
 
// <0=> Default 
// <1=> Black 
// <2=> Red 
// <3=> Green 
// <4=> Yellow 
// <5=> Blue 
// <6=> Magenta 
// <7=> Cyan 
// <8=> White 

#ifndef SHA_KED_IND_SERVICE_INFO_LOG_COLOR
#define SHA_KED_IND_SERVICE_INFO_LOG_COLOR        1
#endif
// <o> NRF_LOG_WARNING_COLOR  - ANSI escape code prefix.
 
// <0=> Default 
// <1=> Black 
// <2=> Red 
// <3=> Green 
// <4=> Yellow 
// <5=> Blue 
// <6=> Magenta 
// <7=> Cyan 
// <8=> White 

#ifndef SHA_KED_IND_SERVICE_DEBUG_LOG_COLOR
#define SHA_KED_IND_SERVICE_DEBUG_LOG_COLOR        1
#endif



// <e> SHA_KED_MD_SERVICE_LOG_ENABLED - Enables logging in the module.(Motion Detector)
//==========================================================

#ifndef SHA_KED_MD_SERVICE_LOG_ENABLED
#define SHA_KED_MD_SERVICE_LOG_ENABLED      0
#endif

// <0=> Off 
// <1=> Error 
// <2=> Warning 
// <3=> Info 
// <4=> Debug 

#ifndef SHA_KED_MD_SERVICE_LOG_LEVEL
#define SHA_KED_MD_SERVICE_LOG_LEVEL        4
#endif

// <o> NRF_LOG_WARNING_COLOR  - ANSI escape code prefix.
 
// <0=> Default 
// <1=> Black 
// <2=> Red 
// <3=> Green 
// <4=> Yellow 
// <5=> Blue 
// <6=> Magenta 
// <7=> Cyan 
// <8=> White 

#ifndef SHA_KED_MD_SERVICE_INFO_LOG_COLOR
#define SHA_KED_MD_SERVICE_INFO_LOG_COLOR        1
#endif
// <o> NRF_LOG_WARNING_COLOR  - ANSI escape code prefix.
 
// <0=> Default 
// <1=> Black 
// <2=> Red 
// <3=> Green 
// <4=> Yellow 
// <5=> Blue 
// <6=> Magenta 
// <7=> Cyan 
// <8=> White 

#ifndef SHA_KED_MD_SERVICE_DEBUG_LOG_COLOR
#define SHA_KED_MD_SERVICE_DEBUG_LOG_COLOR        1
#endif


// <e> SHA_KED_PW_DETECT_SERVICE_LOG_ENABLED - Enables logging in the module.(Power Detector)
//==========================================================

#ifndef SHA_KED_PW_DETECT_SERVICE_LOG_ENABLED
#define SHA_KED_PW_DETECT_SERVICE_LOG_ENABLED      0
#endif

// <0=> Off 
// <1=> Error 
// <2=> Warning 
// <3=> Info 
// <4=> Debug 

#ifndef SHA_KED_PW_DETECT_SERVICE_LOG_LEVEL
#define SHA_KED_PW_DETECT_SERVICE_LOG_LEVEL        4
#endif

// <o> NRF_LOG_WARNING_COLOR  - ANSI escape code prefix.
 
// <0=> Default 
// <1=> Black 
// <2=> Red 
// <3=> Green 
// <4=> Yellow 
// <5=> Blue 
// <6=> Magenta 
// <7=> Cyan 
// <8=> White 

#ifndef SHA_KED_PW_DETECT_SERVICE_INFO_LOG_COLOR
#define SHA_KED_PW_DETECT_SERVICE_INFO_LOG_COLOR        1
#endif
// <o> NRF_LOG_WARNING_COLOR  - ANSI escape code prefix.
 
// <0=> Default 
// <1=> Black 
// <2=> Red 
// <3=> Green 
// <4=> Yellow 
// <5=> Blue 
// <6=> Magenta 
// <7=> Cyan 
// <8=> White 

#ifndef SHA_KED_PW_DETECT_SERVICE_DEBUG_LOG_COLOR
#define SHA_KED_PW_DETECT_SERVICE_DEBUG_LOG_COLOR        1
#endif


// <e> SHA_KED_BUT_SERVICE_LOG_ENABLED - Enables logging in the module.
//==========================================================

#ifndef SHA_KED_BUT_SERVICE_LOG_ENABLED
#define SHA_KED_BUT_SERVICE_LOG_ENABLED      0
#endif

// <0=> Off 
// <1=> Error 
// <2=> Warning 
// <3=> Info 
// <4=> Debug 

#ifndef SHA_KED_BUT_SERVICE_LOG_LEVEL
#define SHA_KED_BUT_SERVICE_LOG_LEVEL        4
#endif

// <o> NRF_LOG_WARNING_COLOR  - ANSI escape code prefix.
 
// <0=> Default 
// <1=> Black 
// <2=> Red 
// <3=> Green 
// <4=> Yellow 
// <5=> Blue 
// <6=> Magenta 
// <7=> Cyan 
// <8=> White 

#ifndef SHA_KED_BUT_SERVICE_INFO_LOG_COLOR
#define SHA_KED_BUT_SERVICE_INFO_LOG_COLOR        1
#endif
// <o> NRF_LOG_WARNING_COLOR  - ANSI escape code prefix.
 
// <0=> Default 
// <1=> Black 
// <2=> Red 
// <3=> Green 
// <4=> Yellow 
// <5=> Blue 
// <6=> Magenta 
// <7=> Cyan 
// <8=> White 

#ifndef SHA_KED_BUT_SERVICE_DEBUG_LOG_COLOR
#define SHA_KED_BUT_SERVICE_DEBUG_LOG_COLOR        1
#endif


// <e> SHA_KED_GNSS_SERVICE_LOG_ENABLED - Enables logging in the module.
//==========================================================

#ifndef SHA_KED_GNSS_SERVICE_LOG_ENABLED
#define SHA_KED_GNSS_SERVICE_LOG_ENABLED      0
#endif

// <0=> Off 
// <1=> Error 
// <2=> Warning 
// <3=> Info 
// <4=> Debug 

#ifndef SHA_KED_GNSS_SERVICE_LOG_LEVEL
#define SHA_KED_GNSS_SERVICE_LOG_LEVEL        4
#endif

// <o> NRF_LOG_WARNING_COLOR  - ANSI escape code prefix.
 
// <0=> Default 
// <1=> Black 
// <2=> Red 
// <3=> Green 
// <4=> Yellow 
// <5=> Blue 
// <6=> Magenta 
// <7=> Cyan 
// <8=> White 

#ifndef SHA_KED_GNSS_SERVICE_INFO_LOG_COLOR
#define SHA_KED_GNSS_SERVICE_INFO_LOG_COLOR        1
#endif
// <o> NRF_LOG_WARNING_COLOR  - ANSI escape code prefix.
 
// <0=> Default 
// <1=> Black 
// <2=> Red 
// <3=> Green 
// <4=> Yellow 
// <5=> Blue 
// <6=> Magenta 
// <7=> Cyan 
// <8=> White 

#ifndef SHA_KED_GNSS_SERVICE_DEBUG_LOG_COLOR
#define SHA_KED_GNSS_SERVICE_DEBUG_LOG_COLOR        1
#endif


// <e> SHA_KED_CUS_SREVICE_LOG_ENABLED - Enables logging in the module.
//==========================================================

#ifndef SHA_KED_CUS_SREVICE_LOG_ENABLED
#define SHA_KED_CUS_SREVICE_LOG_ENABLED      0
#endif

// <0=> Off 
// <1=> Error 
// <2=> Warning 
// <3=> Info 
// <4=> Debug 

#ifndef SHA_KED_CUS_SERVICE_LOG_LEVEL
#define SHA_KED_CUS_SERVICE_LOG_LEVEL        4
#endif

// <o> NRF_LOG_WARNING_COLOR  - ANSI escape code prefix.
 
// <0=> Default 
// <1=> Black 
// <2=> Red 
// <3=> Green 
// <4=> Yellow 
// <5=> Blue 
// <6=> Magenta 
// <7=> Cyan 
// <8=> White 

#ifndef SHA_KED_CUS_SERVICE_INFO_LOG_COLOR
#define SHA_KED_CUS_SERVICE_INFO_LOG_COLOR        1
#endif
// <o> NRF_LOG_WARNING_COLOR  - ANSI escape code prefix.
 
// <0=> Default 
// <1=> Black 
// <2=> Red 
// <3=> Green 
// <4=> Yellow 
// <5=> Blue 
// <6=> Magenta 
// <7=> Cyan 
// <8=> White 

#ifndef SHA_KED_CUS_SERVICE_DEBUG_LOG_COLOR
#define SHA_KED_CUS_SERVICE_DEBUG_LOG_COLOR        1
#endif



// <e> SHA_KED_LIVE_SREVICE_LOG_ENABLED - Enables logging in the module.
//==========================================================

#ifndef SHA_KED_LIVE_SREVICE_LOG_ENABLED
#define SHA_KED_LIVE_SREVICE_LOG_ENABLED      0
#endif

// <0=> Off 
// <1=> Error 
// <2=> Warning 
// <3=> Info 
// <4=> Debug 

#ifndef SHA_KED_LIVE_SERVICE_LOG_LEVEL
#define SHA_KED_LIVE_SERVICE_LOG_LEVEL        4
#endif

// <o> NRF_LOG_WARNING_COLOR  - ANSI escape code prefix.
 
// <0=> Default 
// <1=> Black 
// <2=> Red 
// <3=> Green 
// <4=> Yellow 
// <5=> Blue 
// <6=> Magenta 
// <7=> Cyan 
// <8=> White 

#ifndef SHA_KED_LIVE_SERVICE_INFO_LOG_COLOR
#define SHA_KED_LIVE_SERVICE_INFO_LOG_COLOR        1
#endif
// <o> NRF_LOG_WARNING_COLOR  - ANSI escape code prefix.
 
// <0=> Default 
// <1=> Black 
// <2=> Red 
// <3=> Green 
// <4=> Yellow 
// <5=> Blue 
// <6=> Magenta 
// <7=> Cyan 
// <8=> White 

#ifndef SHA_KED_LIVE_SERVICE_DEBUG_LOG_COLOR
#define SHA_KED_LIVE_SERVICE_DEBUG_LOG_COLOR        1
#endif


// <e> SHA_KED_RTC_SREVICE_LOG_ENABLED - Enables logging in the module.
//==========================================================

#ifndef SHA_KED_RTC_SREVICE_LOG_ENABLED
#define SHA_KED_RTC_SREVICE_LOG_ENABLED      0
#endif

// <0=> Off 
// <1=> Error 
// <2=> Warning 
// <3=> Info 
// <4=> Debug 

#ifndef SHA_KED_RTC_SERVICE_LOG_LEVEL
#define SHA_KED_RTC_SERVICE_LOG_LEVEL        4
#endif

// <o> NRF_LOG_WARNING_COLOR  - ANSI escape code prefix.
 
// <0=> Default 
// <1=> Black 
// <2=> Red 
// <3=> Green 
// <4=> Yellow 
// <5=> Blue 
// <6=> Magenta 
// <7=> Cyan 
// <8=> White 

#ifndef SHA_KED_RTC_SERVICE_INFO_LOG_COLOR
#define SHA_KED_RTC_SERVICE_INFO_LOG_COLOR        1
#endif
// <o> NRF_LOG_WARNING_COLOR  - ANSI escape code prefix.
 
// <0=> Default 
// <1=> Black 
// <2=> Red 
// <3=> Green 
// <4=> Yellow 
// <5=> Blue 
// <6=> Magenta 
// <7=> Cyan 
// <8=> White 

#ifndef SHA_KED_RTC_SERVICE_DEBUG_LOG_COLOR
#define SHA_KED_RTC_SERVICE_DEBUG_LOG_COLOR        1
#endif


// <e> SHA_KED_NRF_HAL_LOG_ENABLED - Enables logging in the module.
//==========================================================

#ifndef SHA_KED_NRF_HAL_LOG_ENABLED
#define SHA_KED_NRF_HAL_LOG_ENABLED      0
#endif

// <0=> Off 
// <1=> Error 
// <2=> Warning 
// <3=> Info 
// <4=> Debug 

#ifndef SHA_KED_NRF_HAL_LOG_LEVEL
#define SHA_KED_NRF_HAL_LOG_LEVEL        4
#endif

// <o> NRF_LOG_WARNING_COLOR  - ANSI escape code prefix.
 
// <0=> Default 
// <1=> Black 
// <2=> Red 
// <3=> Green 
// <4=> Yellow 
// <5=> Blue 
// <6=> Magenta 
// <7=> Cyan 
// <8=> White 

#ifndef SHA_KED_NRF_HAL_INFO_LOG_COLOR
#define SHA_KED_NRF_HAL_INFO_LOG_COLOR        3
#endif
// <o> NRF_LOG_WARNING_COLOR  - ANSI escape code prefix.
 
// <0=> Default 
// <1=> Black 
// <2=> Red 
// <3=> Green 
// <4=> Yellow 
// <5=> Blue 
// <6=> Magenta 
// <7=> Cyan 
// <8=> White 

#ifndef SHA_KED_NRF_HAL_DEBUG_LOG_COLOR
#define SHA_KED_NRF_HAL_DEBUG_LOG_COLOR        1
#endif


// <e> SHA_KED_BLE_STACK_LOG_ENABLED - Enables logging in the module.
//==========================================================

#ifndef SHA_KED_BLE_STACK_LOG_ENABLED
#define SHA_KED_BLE_STACK_LOG_ENABLED      1
#endif

// <0=> Off 
// <1=> Error 
// <2=> Warning 
// <3=> Info 
// <4=> Debug 

#ifndef SHA_KED_BLE_STACK_LOG_LEVEL
#define SHA_KED_BLE_STACK_LOG_LEVEL        2
#endif

// <o> NRF_LOG_WARNING_COLOR  - ANSI escape code prefix.
 
// <0=> Default 
// <1=> Black 
// <2=> Red 
// <3=> Green 
// <4=> Yellow 
// <5=> Blue 
// <6=> Magenta 
// <7=> Cyan 
// <8=> White 

#ifndef SHA_KED_BLE_STACK_INFO_LOG_COLOR
#define SHA_KED_BLE_STACK_INFO_LOG_COLOR        1
#endif
// <o> NRF_LOG_WARNING_COLOR  - ANSI escape code prefix.
 
// <0=> Default 
// <1=> Black 
// <2=> Red 
// <3=> Green 
// <4=> Yellow 
// <5=> Blue 
// <6=> Magenta 
// <7=> Cyan 
// <8=> White 

#ifndef SHA_KED_BLE_STACK_DEBUG_LOG_COLOR
#define SHA_KED_BLE_STACK_DEBUG_LOG_COLOR        1
#endif




// <e> SHA_KED_BLE_API_LOG_ENABLED - Enables logging in the module.
//==========================================================

#ifndef SHA_KED_BLE_API_LOG_ENABLED
#define SHA_KED_BLE_API_LOG_ENABLED      0
#endif

// <0=> Off 
// <1=> Error 
// <2=> Warning 
// <3=> Info 
// <4=> Debug 

#ifndef SHA_KED_BLE_API_LOG_LEVEL
#define SHA_KED_BLE_API_LOG_LEVEL        4
#endif

// <o> NRF_LOG_WARNING_COLOR  - ANSI escape code prefix.
 
// <0=> Default 
// <1=> Black 
// <2=> Red 
// <3=> Green 
// <4=> Yellow 
// <5=> Blue 
// <6=> Magenta 
// <7=> Cyan 
// <8=> White 

#ifndef SHA_KED_BLE_API_INFO_LOG_COLOR
#define SHA_KED_BLE_API_INFO_LOG_COLOR        3
#endif
// <o> NRF_LOG_WARNING_COLOR  - ANSI escape code prefix.
 
// <0=> Default 
// <1=> Black 
// <2=> Red 
// <3=> Green 
// <4=> Yellow 
// <5=> Blue 
// <6=> Magenta 
// <7=> Cyan 
// <8=> White 

#ifndef SHA_KED_BLE_API_DEBUG_LOG_COLOR
#define SHA_KED_BLE_API_DEBUG_LOG_COLOR        1
#endif



// <e> SHA_KED_BLE_SKD_SERVICE_LOG_ENABLED - Enables logging in the module.
//==========================================================

#ifndef SHA_KED_BLE_SKD_SERVICE_LOG_ENABLED
#define SHA_KED_BLE_SKD_SERVICE_LOG_ENABLED      0
#endif

// <0=> Off 
// <1=> Error 
// <2=> Warning 
// <3=> Info 
// <4=> Debug 

#ifndef SHA_KED_BLE_SKD_SERVICE_LOG_LEVEL
#define SHA_KED_BLE_SKD_SERVICE_LOG_LEVEL        4
#endif

// <o> NRF_LOG_WARNING_COLOR  - ANSI escape code prefix.
 
// <0=> Default 
// <1=> Black 
// <2=> Red 
// <3=> Green 
// <4=> Yellow 
// <5=> Blue 
// <6=> Magenta 
// <7=> Cyan 
// <8=> White 

#ifndef SHA_KED_BLE_SKD_SERVICE_INFO_LOG_COLOR
#define SHA_KED_BLE_SKD_SERVICE_INFO_LOG_COLOR        3
#endif
// <o> NRF_LOG_WARNING_COLOR  - ANSI escape code prefix.
 
// <0=> Default 
// <1=> Black 
// <2=> Red 
// <3=> Green 
// <4=> Yellow 
// <5=> Blue 
// <6=> Magenta 
// <7=> Cyan 
// <8=> White 

#ifndef SHA_KED_BLE_SKD_SERVICE_DEBUG_LOG_COLOR
#define SHA_KED_BLE_SKD_SERVICE_DEBUG_LOG_COLOR        1
#endif




// <e> SHA_KED_CMD_SRV_LOG_ENABLED - Enables logging in the module.
//==========================================================

#ifndef SHA_KED_CMD_SRV_LOG_ENABLED
#define SHA_KED_CMD_SRV_LOG_ENABLED     0
#endif

// <0=> Off 
// <1=> Error 
// <2=> Warning 
// <3=> Info 
// <4=> Debug 

#ifndef SHA_KED_CMD_SRV_LOG_LEVEL
#define SHA_KED_CMD_SRV_LOG_LEVEL        4
#endif

// <o> NRF_LOG_WARNING_COLOR  - ANSI escape code prefix.
 
// <0=> Default 
// <1=> Black 
// <2=> Red 
// <3=> Green 
// <4=> Yellow 
// <5=> Blue 
// <6=> Magenta 
// <7=> Cyan 
// <8=> White 

#ifndef SHA_KED_CMD_SRV_INFO_LOG_COLOR
#define SHA_KED_CMD_SRV_INFO_LOG_COLOR        3
#endif
// <o> NRF_LOG_WARNING_COLOR  - ANSI escape code prefix.
 
// <0=> Default 
// <1=> Black 
// <2=> Red 
// <3=> Green 
// <4=> Yellow 
// <5=> Blue 
// <6=> Magenta 
// <7=> Cyan 
// <8=> White 

#ifndef SHA_KED_CMD_SRV_DEBUG_LOG_COLOR
#define SHA_KED_CMD_SRV_DEBUG_LOG_COLOR        1
#endif

// <e> SHA_KED_CMD_SRV_LOG_ENABLED - Enables logging in the module.
//==========================================================

#ifndef SHA_KED_APP_LOG_ENABLED
#define SHA_KED_APP_LOG_ENABLED     1
#endif

// <0=> Off 
// <1=> Error 
// <2=> Warning 
// <3=> Info 
// <4=> Debug 

#ifndef SHA_KED_APP_LOG_LEVEL
#define SHA_KED_APP_LOG_LEVEL        4
#endif

// <o> NRF_LOG_WARNING_COLOR  - ANSI escape code prefix.
 
// <0=> Default 
// <1=> Black 
// <2=> Red 
// <3=> Green 
// <4=> Yellow 
// <5=> Blue 
// <6=> Magenta 
// <7=> Cyan 
// <8=> White 

#ifndef SHA_KED_APP_INFO_LOG_COLOR
#define SHA_KED_APP_INFO_LOG_COLOR        3
#endif
// <o> NRF_LOG_WARNING_COLOR  - ANSI escape code prefix.
 
// <0=> Default 
// <1=> Black 
// <2=> Red 
// <3=> Green 
// <4=> Yellow 
// <5=> Blue 
// <6=> Magenta 
// <7=> Cyan 
// <8=> White 

#ifndef SHA_KED_APP_DEBUG_LOG_COLOR
#define SHA_KED_APP_DEBUG_LOG_COLOR        1
#endif



// <e> SHA_KED_GEO_SRV_LOG_ENABLED - Enables logging in the module.
//==========================================================

#ifndef SHA_KED_GEO_SRV_LOG_ENABLED
#define SHA_KED_GEO_SRV_LOG_ENABLED     0
#endif

// <0=> Off 
// <1=> Error 
// <2=> Warning 
// <3=> Info 
// <4=> Debug 

#ifndef SHA_KED_GEO_SRV_LOG_LEVEL
#define SHA_KED_GEO_SRV_LOG_LEVEL        2
#endif

// <o> NRF_LOG_WARNING_COLOR  - ANSI escape code prefix.
 
// <0=> Default 
// <1=> Black 
// <2=> Red 
// <3=> Green 
// <4=> Yellow 
// <5=> Blue 
// <6=> Magenta 
// <7=> Cyan 
// <8=> White 

#ifndef SHA_KED_GEO_SRV_INFO_LOG_COLOR
#define SHA_KED_GEO_SRV_INFO_LOG_COLOR        3
#endif
// <o> NRF_LOG_WARNING_COLOR  - ANSI escape code prefix.
 
// <0=> Default 
// <1=> Black 
// <2=> Red 
// <3=> Green 
// <4=> Yellow 
// <5=> Blue 
// <6=> Magenta 
// <7=> Cyan 
// <8=> White 

#ifndef SHA_KED_GEO_SRV_DEBUG_LOG_COLOR
#define SHA_KED_GEO_SRV_DEBUG_LOG_COLOR        1
#endif


// <e> SKD_ZOE_M8B_DRV_LOG_ENABLED - Enables logging in the module.
//==========================================================

#ifndef SKD_ZOE_M8B_DRV_LOG_ENABLED
#define SKD_ZOE_M8B_DRV_LOG_ENABLED     0
#endif

// <0=> Off 
// <1=> Error 
// <2=> Warning 
// <3=> Info 
// <4=> Debug 

#ifndef SKD_ZOE_M8B_DRV_LOG_LEVEL
#define SKD_ZOE_M8B_DRV_LOG_LEVEL        4
#endif

// <o> NRF_LOG_WARNING_COLOR  - ANSI escape code prefix.
 
// <0=> Default 
// <1=> Black 
// <2=> Red 
// <3=> Green 
// <4=> Yellow 
// <5=> Blue 
// <6=> Magenta 
// <7=> Cyan 
// <8=> White 

#ifndef SKD_ZOE_M8B_DRV_INFO_LOG_COLOR
#define SKD_ZOE_M8B_DRV_INFO_LOG_COLOR        3
#endif
// <o> NRF_LOG_WARNING_COLOR  - ANSI escape code prefix.
 
// <0=> Default 
// <1=> Black 
// <2=> Red 
// <3=> Green 
// <4=> Yellow 
// <5=> Blue 
// <6=> Magenta 
// <7=> Cyan 
// <8=> White 

#ifndef SKD_ZOE_M8B_DRV_DEBUG_LOG_COLOR
#define SKD_ZOE_M8B_DRV_DEBUG_LOG_COLOR        1
#endif



// <e> SHA_KED_IS25_DRV_LOG_ENABLED - Enables logging in the module.
//==========================================================

#ifndef SHA_KED_IS25_DRV_LOG_ENABLED
#define SHA_KED_IS25_DRV_LOG_ENABLED     1
#endif

// <0=> Off 
// <1=> Error 
// <2=> Warning 
// <3=> Info 
// <4=> Debug 

#ifndef SHA_KED_IS25_DRV_LOG_LEVEL
#define SHA_KED_IS25_DRV_LOG_LEVEL        4
#endif

// <o> NRF_LOG_WARNING_COLOR  - ANSI escape code prefix.
 
// <0=> Default 
// <1=> Black 
// <2=> Red 
// <3=> Green 
// <4=> Yellow 
// <5=> Blue 
// <6=> Magenta 
// <7=> Cyan 
// <8=> White 

#ifndef SHA_KED_IS25_DRV_INFO_LOG_COLOR
#define SHA_KED_IS25_DRV_INFO_LOG_COLOR        3
#endif
// <o> NRF_LOG_WARNING_COLOR  - ANSI escape code prefix.
 
// <0=> Default 
// <1=> Black 
// <2=> Red 
// <3=> Green 
// <4=> Yellow 
// <5=> Blue 
// <6=> Magenta 
// <7=> Cyan 
// <8=> White 

#ifndef SHA_KED_IS25_DRV_DEBUG_LOG_COLOR
#define SHA_KED_IS25_DRV_DEBUG_LOG_COLOR        1
#endif


// <e> SHA_KED_MD_SRV_LOG_ENABLED - Enables logging in the module.
//==========================================================

#ifndef SHA_KED_MD_SRV_LOG_ENABLED
#define SHA_KED_MD_SRV_LOG_ENABLED    1
#endif

// <0=> Off 
// <1=> Error 
// <2=> Warning 
// <3=> Info 
// <4=> Debug 

#ifndef SHA_KED_MD_SRV_LOG_LEVEL
#define SHA_KED_MD_SRV_LOG_LEVEL        2
#endif

// <o> NRF_LOG_WARNING_COLOR  - ANSI escape code prefix.
 
// <0=> Default 
// <1=> Black 
// <2=> Red 
// <3=> Green 
// <4=> Yellow 
// <5=> Blue 
// <6=> Magenta 
// <7=> Cyan 
// <8=> White 

#ifndef SHA_KED_MD_SRV_INFO_LOG_COLOR
#define SHA_KED_MD_SRV_INFO_LOG_COLOR        3
#endif
// <o> NRF_LOG_WARNING_COLOR  - ANSI escape code prefix.
 
// <0=> Default 
// <1=> Black 
// <2=> Red 
// <3=> Green 
// <4=> Yellow 
// <5=> Blue 
// <6=> Magenta 
// <7=> Cyan 
// <8=> White 

#ifndef SHA_KED_MD_SRV_DEBUG_LOG_COLOR
#define SHA_KED_MD_SRV_DEBUG_LOG_COLOR        1
#endif

// <e> SHA_KED_BUT_SRV_LOG_ENABLED - Enables logging in the module.
//==========================================================

#ifndef SHA_KED_BUT_SRV_LOG_ENABLED
#define SHA_KED_BUT_SRV_LOG_ENABLED     1
#endif

// <0=> Off 
// <1=> Error 
// <2=> Warning 
// <3=> Info 
// <4=> Debug 

#ifndef SHA_KED_BUT_SRV_LOG_LEVEL
#define SHA_KED_BUT_SRV_LOG_LEVEL        2
#endif

// <o> NRF_LOG_WARNING_COLOR  - ANSI escape code prefix.
 
// <0=> Default 
// <1=> Black 
// <2=> Red 
// <3=> Green 
// <4=> Yellow 
// <5=> Blue 
// <6=> Magenta 
// <7=> Cyan 
// <8=> White 

#ifndef SHA_KED_BUT_SRV_INFO_LOG_COLOR
#define SHA_KED_BUT_SRV_INFO_LOG_COLOR        3
#endif
// <o> NRF_LOG_WARNING_COLOR  - ANSI escape code prefix.
 
// <0=> Default 
// <1=> Black 
// <2=> Red 
// <3=> Green 
// <4=> Yellow 
// <5=> Blue 
// <6=> Magenta 
// <7=> Cyan 
// <8=> White 

#ifndef SHA_KED_BUT_SRV_DEBUG_LOG_COLOR
#define SHA_KED_BUT_SRV_DEBUG_LOG_COLOR        1
#endif

// <e> SHA_KED_DB_SRV_LOG_ENABLED - Enables logging in the module.
//==========================================================

#ifndef SHA_KED_DB_LOG_ENABLED
#define SHA_KED_DB_LOG_ENABLED    1
#endif

// <0=> Off 
// <1=> Error 
// <2=> Warning 
// <3=> Info 
// <4=> Debug 

#ifndef SHA_KED_DB_LOG_LEVEL
#define SHA_KED_DB_LOG_LEVEL        2
#endif

// <o> NRF_LOG_WARNING_COLOR  - ANSI escape code prefix.
 
// <0=> Default 
// <1=> Black 
// <2=> Red 
// <3=> Green 
// <4=> Yellow 
// <5=> Blue 
// <6=> Magenta 
// <7=> Cyan 
// <8=> White 

#ifndef SHA_KED_DB_INFO_LOG_COLOR
#define SHA_KED_DB_INFO_LOG_COLOR        3
#endif
// <o> NRF_LOG_WARNING_COLOR  - ANSI escape code prefix.
 
// <0=> Default 
// <1=> Black 
// <2=> Red 
// <3=> Green 
// <4=> Yellow 
// <5=> Blue 
// <6=> Magenta 
// <7=> Cyan 
// <8=> White 

#ifndef SHA_KED_DB_DEBUG_LOG_COLOR
#define SHA_KED_DB_DEBUG_LOG_COLOR        1
#endif


// <e> SHA_KED_TIME_SRV_LOG_ENABLED - Enables logging in the module.
//==========================================================

#ifndef SHA_KED_TIME_SRV_LOG_ENABLED
#define SHA_KED_TIME_SRV_LOG_ENABLED    1
#endif

// <0=> Off 
// <1=> Error 
// <2=> Warning 
// <3=> Info 
// <4=> Debug 

#ifndef SHA_KED_TIME_SRV_LOG_LEVEL
#define SHA_KED_TIME_SRV_LOG_LEVEL        2
#endif

// <o> NRF_LOG_WARNING_COLOR  - ANSI escape code prefix.
 
// <0=> Default 
// <1=> Black 
// <2=> Red 
// <3=> Green 
// <4=> Yellow 
// <5=> Blue 
// <6=> Magenta 
// <7=> Cyan 
// <8=> White 

#ifndef SHA_KED_TIME_SRV_INFO_LOG_COLOR
#define SHA_KED_TIME_SRV_INFO_LOG_COLOR        3
#endif
// <o> NRF_LOG_WARNING_COLOR  - ANSI escape code prefix.
 
// <0=> Default 
// <1=> Black 
// <2=> Red 
// <3=> Green 
// <4=> Yellow 
// <5=> Blue 
// <6=> Magenta 
// <7=> Cyan 
// <8=> White 

#ifndef SHA_KED_TIME_SRV_DEBUG_LOG_COLOR
#define SHA_KED_TIME_SRV_DEBUG_LOG_COLOR        1
#endif

// <e> SHA_KED_TIME_SRV_LOG_ENABLED - Enables logging in the module.
//==========================================================

#ifndef SHA_KED_LED_SRV_LOG_ENABLED
#define SHA_KED_LED_SRV_LOG_ENABLED    0
#endif

// <0=> Off 
// <1=> Error 
// <2=> Warning 
// <3=> Info 
// <4=> Debug 

#ifndef SHA_KED_LED_SRV_LOG_LEVEL
#define SHA_KED_LED_SRV_LOG_LEVEL        4
#endif

// <o> NRF_LOG_WARNING_COLOR  - ANSI escape code prefix.
 
// <0=> Default 
// <1=> Black 
// <2=> Red 
// <3=> Green 
// <4=> Yellow 
// <5=> Blue 
// <6=> Magenta 
// <7=> Cyan 
// <8=> White 

#ifndef SHA_KED_LED_SRV_INFO_LOG_COLOR
#define SHA_KED_LED_SRV_INFO_LOG_COLOR        3
#endif
// <o> NRF_LOG_WARNING_COLOR  - ANSI escape code prefix.
 
// <0=> Default 
// <1=> Black 
// <2=> Red 
// <3=> Green 
// <4=> Yellow 
// <5=> Blue 
// <6=> Magenta 
// <7=> Cyan 
// <8=> White 

#ifndef SHA_KED_LED_SRV_DEBUG_LOG_COLOR
#define SHA_KED_LED_SRV_DEBUG_LOG_COLOR        1
#endif


// <e> SHA_KED_TIME_SRV_LOG_ENABLED - Enables logging in the module.
//==========================================================

#ifndef SHA_KED_PW_SRV_LOG_ENABLED
#define SHA_KED_PW_SRV_LOG_ENABLED    0
#endif

// <0=> Off 
// <1=> Error 
// <2=> Warning 
// <3=> Info 
// <4=> Debug 

#ifndef SHA_KED_PW_SRV_LOG_LEVEL
#define SHA_KED_PW_SRV_LOG_LEVEL        4
#endif

// <o> NRF_LOG_WARNING_COLOR  - ANSI escape code prefix.
 
// <0=> Default 
// <1=> Black 
// <2=> Red 
// <3=> Green 
// <4=> Yellow 
// <5=> Blue 
// <6=> Magenta 
// <7=> Cyan 
// <8=> White 

#ifndef SHA_KED_PW_SRV_INFO_LOG_COLOR
#define SHA_KED_PW_SRV_INFO_LOG_COLOR        3
#endif
// <o> NRF_LOG_WARNING_COLOR  - ANSI escape code prefix.
 
// <0=> Default 
// <1=> Black 
// <2=> Red 
// <3=> Green 
// <4=> Yellow 
// <5=> Blue 
// <6=> Magenta 
// <7=> Cyan 
// <8=> White 

#ifndef SHA_KED_PW_SRV_DEBUG_LOG_COLOR
#define SHA_KED_PW_SRV_DEBUG_LOG_COLOR        1
#endif


// <e> SHA_KED_TIME_SRV_LOG_ENABLED - Enables logging in the module.
//==========================================================

#ifndef SKD_NRF_SERIAL_DRV_LOG_ENABLED
#define SKD_NRF_SERIAL_DRV_LOG_ENABLED    1
#endif

// <0=> Off 
// <1=> Error 
// <2=> Warning 
// <3=> Info 
// <4=> Debug 

#ifndef SKD_NRF_SERIAL_DRV_LOG_LEVEL
#define SKD_NRF_SERIAL_DRV_LOG_LEVEL        4
#endif

// <o> NRF_LOG_WARNING_COLOR  - ANSI escape code prefix.
 
// <0=> Default 
// <1=> Black 
// <2=> Red 
// <3=> Green 
// <4=> Yellow 
// <5=> Blue 
// <6=> Magenta 
// <7=> Cyan 
// <8=> White 

#ifndef SKD_NRF_SERIAL_DRV_INFO_LOG_COLOR
#define SKD_NRF_SERIAL_DRV_INFO_LOG_COLOR        3
#endif
// <o> NRF_LOG_WARNING_COLOR  - ANSI escape code prefix.
 
// <0=> Default 
// <1=> Black 
// <2=> Red 
// <3=> Green 
// <4=> Yellow 
// <5=> Blue 
// <6=> Magenta 
// <7=> Cyan 
// <8=> White 

#ifndef SKD_NRF_SERIAL_DRV_DEBUG_LOG_COLOR
#define SKD_NRF_SERIAL_DRV_DEBUG_LOG_COLOR        1
#endif


// <e> SHA_KED_FDS_LOG_ENABLED - Enables logging in the module.
//==========================================================

#ifndef SHA_KED_BUT_LOG_ENABLED
#define SHA_KED_BUT_LOG_ENABLED      0
#endif

// <0=> Off 
// <1=> Error 
// <2=> Warning 
// <3=> Info 
// <4=> Debug 

#ifndef SHA_KED_BUT_LOG_LEVEL
#define SHA_KED_BUT_LOG_LEVEL        3
#endif

// <o> NRF_LOG_WARNING_COLOR  - ANSI escape code prefix.
 
// <0=> Default 
// <1=> Black 
// <2=> Red 
// <3=> Green 
// <4=> Yellow 
// <5=> Blue 
// <6=> Magenta 
// <7=> Cyan 
// <8=> White 

#ifndef SHA_KED_BUT_INFO_LOG_COLOR
#define SHA_KED_BUT_INFO_LOG_COLOR        1
#endif
// <o> NRF_LOG_WARNING_COLOR  - ANSI escape code prefix.
 
// <0=> Default 
// <1=> Black 
// <2=> Red 
// <3=> Green 
// <4=> Yellow 
// <5=> Blue 
// <6=> Magenta 
// <7=> Cyan 
// <8=> White 

#ifndef SHA_KED_BUT_DEBUG_LOG_COLOR
#define SHA_KED_BUT_DEBUG_LOG_COLOR        1
#endif



#endif
