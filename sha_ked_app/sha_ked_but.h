#ifndef SHA_KED_BUT_H
#define SHA_KED_BUT_H

#include <stdint.h>
#include "app_timer.h"
#include "nrf_gpio.h"

typedef enum {
  
  BUTTON_RELEASED = 0,
  BUTTON_PUSHED

} buttonEvnt_t;  


typedef enum{
  NO_PROBLEM              = 0,
  NO_BUTTON_INITIALIZED,
  BUTTON_ALREADY_INITIALIZED,
  BUTTON_ALREADY_DEINITIALIZED

} buttonErrorCode_t;  

typedef void (*isrHandler_t) (buttonEvnt_t evntType) ;


typedef struct {

  uint8_t                 pin_no;                /**< Pin to be used as a button. */
  uint8_t                 active_state;          /**< APP_BUTTON_ACTIVE_HIGH or APP_BUTTON_ACTIVE_LOW. */
  isrHandler_t            isr_handler;           /**< Handler to be called when button is pushed. */
  nrf_gpio_pin_pull_t     pull_cfg;              /**< Pull-up or -down configuration. */     

  uint32_t                debounce_timeMs; 
  app_timer_id_t          debounce_timerId; 
  bool                    state;                 /* HIGH or LOW  */

} skd_button_cfg_t;


buttonErrorCode_t skd_but_init    ( skd_button_cfg_t* pConfig ); 
buttonErrorCode_t skd_but_deInit  ( skd_button_cfg_t* pConfig );  


#endif