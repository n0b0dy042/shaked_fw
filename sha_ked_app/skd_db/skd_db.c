#include "sha_ked_debug.h"
#include "skd_db.h"
#include "lfs.h"
#include "is25_drv.h" 


#define DEFAULT_MAX_CASH_SIZE           1*sizeof( skd_snapshot_t )


#if SHA_KED_DB_LOG_ENABLED == 1 
    #define NRF_LOG_LEVEL               SHA_KED_DB_LOG_LEVEL
    #define NRF_LOG_INFO_COLOR          SHA_KED_DB_INFO_LOG_COLOR
    #define NRF_LOG_DEBUG_COLOR         SHA_KED_DB_DEBUG_LOG_COLOR
#else 
    #define NRF_LOG_LEVEL       0
#endif 



#define NRF_LOG_MODULE_NAME     skd_db 
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
NRF_LOG_MODULE_REGISTER();



#define TRANSFER_ACTIVE     true
#define TRANSFER_DEACTIVE   false



//Static defined 8
static uint8_t lfs_read_buf[IS25_WRITABLE_PAGE_SIZE];
static uint8_t lfs_prog_buf[IS25_WRITABLE_PAGE_SIZE];
static uint8_t lfs_look_buf[IS25_WRITABLE_PAGE_SIZE];
static uint8_t lfs_file_buf[IS25_WRITABLE_PAGE_SIZE];


// variables used by the filesystem
static lfs_t lfs;
static lfs_file_t  db_file;
static lfs_off_t   db_file_rd_cnt    = NULL;    

static lfs_file_t  file;


static max_cashed_data_size      =  DEFAULT_MAX_CASH_SIZE; 


/*--------------------------DB-FLOW-PARAMETERS--------------------------------------*/
static bool                 isDBTransferActive     = TRANSFER_DEACTIVE; 
static uint32_t              readRecordId           = NULL;
static uint32_t              lastRecordId           = NULL;
static uint32_t              recordsLen             =  NULL; 
static skd_snapshot_t       pRecords[ MAX_SET_LEN ];   

static skd_db_transfer_api_t crntApi = {NULL};

int user_provided_block_device_read(const struct lfs_config *c, lfs_block_t block, lfs_off_t off, void *buffer, lfs_size_t size)
{
  uint32_t addr    =  block * IS25_ERASABLE_SECTOR_SIZE + off; 
  uint8_t* pData    = (uint8_t* )buffer;   
  uint32_t dataLen = size;  

  NRF_LOG_DEBUG("LFS:Proccsing block read operation with block %d to read %d bytes ", block, size);  
  NRF_LOG_DEBUG("LPS:Reading %d bytes for block number %d with offest %d", size, block, off );  
  NRF_LOG_FLUSH();
  
  if( (off + size) >  IS25_ERASABLE_SECTOR_SIZE ) {
    
    NRF_LOG_WARNING("Reading out of a current block boader");
    uint32_t tail = IS25_ERASABLE_SECTOR_SIZE - off; 
    IS25_read_page(addr, pData, tail);
    NRF_LOG_DEBUG("%d byte read :", tail); 
    NRF_LOG_HEXDUMP_DEBUG(pData, tail);    
    NRF_LOG_FLUSH();

    addr    += tail;
    pData   += tail;
    dataLen -= tail; 

  }
  
  if( dataLen <= IS25_MAX_WRITABLE_NUMBER_OF_BYTES) {
    
    IS25_read_page(addr, pData, dataLen);
    NRF_LOG_DEBUG("%d byte read :", dataLen); 
    NRF_LOG_HEXDUMP_DEBUG(pData, dataLen);     
    NRF_LOG_FLUSH();

    return LFS_ERR_OK; 
  }   

  do{

       IS25_read_page(addr, pData, IS25_MAX_WRITABLE_NUMBER_OF_BYTES);
        
       addr     +=  IS25_MAX_WRITABLE_NUMBER_OF_BYTES;
       pData    +=  IS25_MAX_WRITABLE_NUMBER_OF_BYTES;
       dataLen  -=  IS25_MAX_WRITABLE_NUMBER_OF_BYTES; 
       
  }while(dataLen > IS25_MAX_WRITABLE_NUMBER_OF_BYTES);  
  
  if(dataLen > 0) IS25_read_page(addr, pData, dataLen);    


  return LFS_ERR_OK;

} 


int user_provided_block_device_prog(const struct lfs_config *c, lfs_block_t block, lfs_off_t off, const void *buffer, lfs_size_t size)
{
  uint32_t dataLen       = size;
  uint32_t addr          = block * IS25_ERASABLE_SECTOR_SIZE + off; 
  uint32_t str_page_numb = (uint32_t)(addr/IS25_WRITABLE_PAGE_SIZE);
  uint32_t end_page_num  = (uint32_t) ( (addr + dataLen - 1) / IS25_WRITABLE_PAGE_SIZE );
  uint8_t* pData         = (uint8_t* )buffer;   

  NRF_LOG_DEBUG("LFS:Proccsing block write operation with block %d to write %d bytes with offset %d", block, size, off);  
  NRF_LOG_FLUSH();

  if( (off + size) >  IS25_ERASABLE_SECTOR_SIZE ) {
    
    NRF_LOG_WARNING("Writing out of current block boader");
  
  }


  if( str_page_numb != end_page_num ){
      
      uint32_t r = (str_page_numb + 1) *IS25_WRITABLE_PAGE_SIZE - addr; 
      

      IS25_write_page(addr, pData,  r );
      NRF_LOG_DEBUG("Add %d bytes to current page :",  r ); 
      NRF_LOG_HEXDUMP_DEBUG(pData,  r ); 
      NRF_LOG_FLUSH();

      addr      += r;
      pData     += r; 
      dataLen   -= r;
      
      if(dataLen == 0) return LFS_ERR_OK;
  }



  if( dataLen <= IS25_MAX_WRITABLE_NUMBER_OF_BYTES) {
    
    IS25_write_page(addr, pData, dataLen);
    NRF_LOG_DEBUG("%d bytes written :", dataLen); 
    NRF_LOG_HEXDUMP_DEBUG(pData, dataLen); 
    NRF_LOG_FLUSH();
       
    return LFS_ERR_OK; 
  }   

  do{

       IS25_write_page(addr, pData, IS25_MAX_WRITABLE_NUMBER_OF_BYTES );
        
       addr     +=  IS25_MAX_WRITABLE_NUMBER_OF_BYTES;
       pData    +=  IS25_MAX_WRITABLE_NUMBER_OF_BYTES;
       dataLen  -=  IS25_MAX_WRITABLE_NUMBER_OF_BYTES; 
       
  }while(dataLen > IS25_WRITABLE_PAGE_SIZE);  
  
  if(dataLen > 0) IS25_write_page(addr, pData, dataLen);    

  return LFS_ERR_OK;
}

int user_provided_block_device_erase(const struct lfs_config *c, lfs_block_t block)
{

  NRF_LOG_DEBUG("LFS:Proccsing block errase operation with block %d", block );
  NRF_LOG_FLUSH();
    
  uint32_t addr    =  block * IS25_ERASABLE_SECTOR_SIZE + 1; 

  
  IS25_erase_4k_sector(addr);

  return LFS_ERR_OK;  
}

int user_provided_block_device_sync(const struct lfs_config *c)
{
  
  NRF_LOG_DEBUG("LFS:Proccsing synchronization operation"); 
  return LFS_ERR_OK;

}


// configuration of the filesystem is provided by this struct
const struct lfs_config lfs_is25_cfg =
{
	//block device operations
	.read 				        = user_provided_block_device_read,
	.prog 				        = user_provided_block_device_prog,
	.erase				        = user_provided_block_device_erase,
	.sync 				        = user_provided_block_device_sync,

	//block device configuration
	.read_size  				= IS25_WRITABLE_PAGE_SIZE,                    	
	.prog_size  				= IS25_WRITABLE_PAGE_SIZE,                    	
	.block_size 				= IS25_ERASABLE_SECTOR_SIZE,  
	.block_count				= IS25_ERASABLE_SECTOR_COUNT,                                    	
	.lookahead  				= sizeof(lfs_look_buf),                    	
		
	//static defined
	.read_buffer                            = lfs_read_buf,
	.prog_buffer                            = lfs_prog_buf,
	.lookahead_buffer                       = lfs_look_buf,
	.file_buffer                            = lfs_file_buf,

};




skd_db_error_code_t skd_db_init( skd_db_config_t* pConfig){

    NRF_LOG_INFO("Initailizing sha_ked database");   
    
    IS25_hw_init();
    IS25_sw_init();
  
    uint32_t chipID = NULL;
    uint16_t unique_chipID = NULL;  
   
    NRF_LOG_INFO("Test IS25 chipset"); 

    chipID         =  IS25_readID();
    unique_chipID  =  IS25_read_unique_ID();
    NRF_LOG_INFO("Chip ID : 0x%08x", chipID);
    NRF_LOG_INFO("Unique chip ID : 0x%04x", unique_chipID); 
    NRF_LOG_FLUSH(); 
    

    IS25_sw_reset( );

    // mount the filesystem
   NRF_LOG_INFO("Trying to mount a flash driver."); 
   int ret  = lfs_mount(&lfs, &lfs_is25_cfg);
   
   if(ret != LFS_ERR_OK) {
      
      NRF_LOG_INFO("No little FS found on flash driver. Trying to format flash driver.");
       
      ret = lfs_format(&lfs, &lfs_is25_cfg);
      
      if( ret != LFS_ERR_OK ) {
        
        NRF_LOG_ERROR("Fail to format flash drive with error %d", ret); 
        return DB_NO_FLASH_DRIVE_FOUND;
         
       }
      
       NRF_LOG_DEBUG("Formating flash driver success");
       
       ret = lfs_mount(&lfs, &lfs_is25_cfg);
        
      if(ret != LFS_ERR_OK) {
       
        NRF_LOG_ERROR("Fail%d", ret);
        return DB_FLASH_DRVIE_CORRUPTED; 

      }
   }

   NRF_LOG_INFO("IS25 mounted successfully");
   NRF_LOG_INFO("Trying to open sha_ked database");
        
   ret = lfs_file_open(&lfs, &db_file, "sha_ked_db", LFS_O_RDWR | LFS_O_APPEND); 
   if(ret == LFS_ERR_NOENT) {
       
        NRF_LOG_WARNING("No sha_ked_database found on flash drive");
        NRF_LOG_INFO("Creating new database");
        ret  = lfs_file_open(&lfs, &db_file, "sha_ked_db", LFS_O_RDWR | LFS_O_APPEND | LFS_O_CREAT);

        if(ret != LFS_ERR_OK) {

          NRF_LOG_ERROR("Fail to creat a new data base with error %d",  ret);
          return DB_UNKNOWN_ERROR;
        
        }
         
   } 
   
   NRF_LOG_INFO("Data base ready");

   lfs_soff_t db_file_size    = lfs_file_size(&lfs,&db_file); 

   if(db_file_size % sizeof(skd_snapshot_t)){
      
      NRF_LOG_ERROR("Data base is currupted!!!!"); 
      skd_db_deInit();
      return DB_DATA_BASED_CORRUPTED; 

   }
    
   NRF_LOG_INFO("%d records avalible in database",  db_file_size/ sizeof( skd_snapshot_t ));

   NRF_LOG_FLUSH();
   return DB_NO_ERROR; 
}



skd_db_error_code_t skd_db_deInit(){
   
   skd_db_trf_deInit();

   lfs_unmount(&lfs);
   
   IS25_sw_deInit(); 
   IS25_hw_deInit(); 

   NRF_LOG_INFO("Shut  down sha_ked database"); 


}


skd_db_error_code_t skd_db_clean_soft(){

  int ret; 
  NRF_LOG_INFO("Deleting db file");
  NRF_LOG_PROCESS(); 
  
  ret = lfs_file_close(&lfs, &db_file); 
  if(ret != LFS_ERR_OK){
  
    NRF_LOG_ERROR("Fail to close db_file with error %d", ret); 
  
  } 

  ret = lfs_remove(&lfs,"sha_ked_db");
  if(ret != LFS_ERR_OK){
  
    NRF_LOG_ERROR("Fail to remove db_file with error %d", ret); 
  
  } 
    
  ret = lfs_file_open(&lfs, &db_file, "sha_ked_db", LFS_O_RDWR | LFS_O_APPEND | LFS_O_CREAT ); 
  
  if(ret != LFS_ERR_OK){
  
    NRF_LOG_ERROR("Fail to open db_file with error", ret); 
  
  } 

  NRF_LOG_INFO("Cleanning finished");
  NRF_LOG_PROCESS(); 
    
  return DB_NO_ERROR; 

}


skd_db_error_code_t skd_db_clean_hard(){

    NRF_LOG_INFO("Clean flash drive");
    NRF_LOG_PROCESS(); 

    IS25_erase_chip(); 
    skd_db_init(NULL);  

    NRF_LOG_INFO("Cleanning finished");
    NRF_LOG_PROCESS(); 
    
    return DB_NO_ERROR; 
}


skd_db_error_code_t    skd_db_write_record  (skd_snapshot_t* pRecord){
  
  int ret = lfs_file_write( &lfs, &db_file, (uint8_t*)pRecord, sizeof( skd_snapshot_t ));   
  
  if (ret < LFS_ERR_OK){
    
    NRF_LOG_ERROR("Fail to write sample with error %d", ret);
    return DB_UNKNOWN_ERROR;
  
  } 

  NRF_LOG_INFO("The current file postition is %d and file size %d", db_file.pos, db_file.size); 
  NRF_LOG_INFO(" %d byte cashed and  and max cash size is %d", db_file.pos- db_file.size, max_cashed_data_size); 

  if(db_file.pos - db_file.size > max_cashed_data_size ) {
   
    NRF_LOG_INFO("Sync file"); 
    ret = lfs_file_sync(&lfs, &db_file);
    if(ret < LFS_ERR_OK ) {

      NRF_LOG_ERROR("Fail to sync file with error %d", ret);
      return ret;

    }  
    
  } 

  return DB_NO_ERROR; 
}


skd_db_error_code_t   skd_db_read_record   (uint32_t sampleId, skd_snapshot_t* pRecord){
  
  int ret = lfs_file_sync(&lfs, &db_file);
  
  if(ret < LFS_ERR_OK ) {

      NRF_LOG_ERROR("Fail to sync file with error %d", ret);
      return ret;

  }  

  lfs_soff_t db_size = lfs_file_size(&lfs,&db_file)/ sizeof( skd_snapshot_t ); 
  
  //check if parameters legit
  if(sampleId > db_size)  return DB_INVALIDE_PARAMETERS;
  
  //set cursor
  lfs_file_seek(&lfs, &db_file, sampleId*sizeof(skd_snapshot_t) , LFS_SEEK_SET); 
   
  //read 
  ret = lfs_file_read(&lfs, &db_file, pRecord, sizeof(skd_snapshot_t));

  if( ret < LFS_ERR_OK) {
  
    NRF_LOG_ERROR("Fail to read sample %d with error %d", sampleId, ret);
    
    return DB_UNKNOWN_ERROR;
  }  
  
  //set cursor back to the end of file
  lfs_file_seek(&lfs,&db_file, 0, LFS_SEEK_END); 

  return DB_NO_ERROR; 
} 



skd_db_error_code_t      skd_db_read_records  (uint32_t strId, uint32_t endId, void* pRecords){

  int ret = lfs_file_sync(&lfs, &db_file);
  
  if( ret < LFS_ERR_OK ) {

      NRF_LOG_ERROR("Fail to sync file with error %d", ret);
      return ret;

  }  

  lfs_soff_t db_size = lfs_file_size(&lfs, &db_file)/ sizeof( skd_snapshot_t ); 

  //check if parameters legit
  if(strId > endId )   return DB_INVALIDE_PARAMETERS;
  if(endId > db_size)  return DB_INVALIDE_PARAMETERS; 
  
  //set cursor
  lfs_file_seek(&lfs, &db_file, strId*sizeof(skd_snapshot_t) , LFS_SEEK_SET); 
  
  //read 
   ret = lfs_file_read(&lfs, &db_file, pRecords, (endId - strId)*sizeof(skd_snapshot_t));

  if( ret < LFS_ERR_OK) {
  
    NRF_LOG_ERROR("Fail to read set with error %d",  ret);
    
    return DB_UNKNOWN_ERROR;
  }  
  
  //set cursor back to the end of file
  lfs_file_seek(&lfs, &db_file, 0, LFS_SEEK_END); 

  return DB_NO_ERROR; 

}

uint32_t skd_db_read_number_of_records (void){

  return lfs_file_size(&lfs,&db_file)/ sizeof( skd_snapshot_t ); 
 
} 


// Data sample transfer 
skd_db_transfer_status_t skd_db_trf_init(uint32_t strId, uint32_t len, skd_db_transfer_api_t* pApi){

  readRecordId  = strId; 
  lastRecordId  = strId + len;
  
  NRF_LOG_DEBUG("Initializing records transfer"); 
  NRF_LOG_PROCESS();

  if(isDBTransferActive) return TRANSFER_BUSY;


  isDBTransferActive = TRANSFER_ACTIVE; 

  if(pApi != NULL) memcpy(&crntApi, pApi, sizeof(skd_db_transfer_api_t));

  if( lastRecordId - readRecordId <  MAX_SET_LEN)    recordsLen = lastRecordId - readRecordId; 
  else                                               recordsLen = MAX_SET_LEN;  

  skd_db_read_records( readRecordId, 
                       readRecordId + recordsLen,  
                       pRecords);
                     
  readRecordId += recordsLen; 

} 



skd_db_error_code_t skd_db_trf_deInit(){
   
    if(crntApi.transferCompleteCb) crntApi.transferCompleteCb(TRANSFER_ABORTED_BY_USER);
    
    readRecordId       = NULL;
    lastRecordId       = NULL; 
    recordsLen         = NULL; 
    isDBTransferActive = TRANSFER_DEACTIVE; 
    
    memset(&crntApi,NULL,sizeof(skd_db_transfer_api_t));
}


void skd_db_trf_process(){
   
  if(isDBTransferActive == TRANSFER_DEACTIVE) return; 
  
  skd_db_transfer_status_t ret =  TRANSFER_NO_ERROR;  

  do{
  
    if(crntApi.sendDataCb) ret = crntApi.sendDataCb( (uint8_t*)pRecords , recordsLen * sizeof(skd_snapshot_t)) ;


    if(ret == TRANSFER_BUSY)  return; // try next time

    NRF_LOG_INFO("%d records were sent over BLE channle", recordsLen );  
    NRF_LOG_PROCESS();
     
    if( ret == TRANSFER_ABORTED ) {
     
      NRF_LOG_ERROR("Transfer aborted with error %d", ret );
      NRF_LOG_PROCESS(); 
      
      if(crntApi.transferCompleteCb) crntApi.transferCompleteCb( TRANSFER_ABORTED );

      skd_db_trf_deInit(); 
    
      return; 
    } 
    
    if( lastRecordId == readRecordId ){
  
      NRF_LOG_INFO("Data Transfer completed. %d records transfered.", readRecordId );

      if(crntApi.transferCompleteCb) crntApi.transferCompleteCb( TRANSFER_COMPLITED_SUCCESS );

      skd_db_trf_deInit();
     
      return; 

    }  
    
    if ( lastRecordId - readRecordId <  MAX_SET_LEN)    recordsLen = lastRecordId - readRecordId; 
    else                                                recordsLen = MAX_SET_LEN;  
    
    skd_db_read_records( readRecordId, 
                         readRecordId + recordsLen,  
                         pRecords);
    //*/                  
    readRecordId += recordsLen;    
  
  }while(1);


} 










/* Legacy  Testing&Debuging

skd_db_error_code_t skd_db_test(){

    skd_snapshot_t testRecord = {NULL}; 
    
    NRF_LOG_INFO("Initializing sha_ked database");

    uint32_t  db_size  = NULL; 
    uint32_t  ret      = NULL; 
    
    ret = skd_db_init(NULL);
    
    if(ret != DB_NO_ERROR ){

      NRF_LOG_ERROR(" Fail to initializing sha ked database with error %d", ret); 
      return DB_UNKNOWN_ERROR; 

    }  
    
    //NRF_LOG_INFO("Soft cleaning database"); 
    //skd_db_clean_soft();
    
    //NRF_LOG_INFO("Hard cleaning database"); 
    //skd_db_clean_hard();

    NRF_LOG_INFO("Initializing success");
    NRF_LOG_INFO("Reading sha_ked database size");
    db_size = skd_db_read_number_of_records();
    
    NRF_LOG_INFO("%d sample avalible in the database", db_size); 
    
    NRF_LOG_INFO("Writing sample to sha_ked database");
    ret = skd_db_write_record(&testRecord);
    
    if(ret != DB_NO_ERROR ){

      NRF_LOG_ERROR("Fail to initializing sha ked database with error %d", ret); 
      return DB_UNKNOWN_ERROR;

    }  
    
    NRF_LOG_INFO("Writing success");

    NRF_LOG_INFO("Reading sample from sha_ked database");
    ret = skd_db_read_record( db_size, &testRecord );  
    
    if( ret != DB_NO_ERROR  ) {
    
      NRF_LOG_ERROR("Fail to read from sha ked database with error %d", ret); 
      return DB_UNKNOWN_ERROR; 
    
    } 
    

    NRF_LOG_INFO("Reading success");
    NRF_LOG_INFO("Writing extra sample to sha_ked database");

    ret = skd_db_write_record(&testRecord);

    if(ret != DB_NO_ERROR ){

      NRF_LOG_ERROR(" Fail to initializing sha ked database with error %d", ret); 
      return DB_UNKNOWN_ERROR; 
    
    }  
    
    NRF_LOG_INFO("Reading sha_ked database size");
    db_size = skd_db_read_number_of_records();
    
    NRF_LOG_INFO("%d sample avalible into database", db_size); 

    NRF_LOG_FLUSH(); 


}












void skd_fs_test(){
   
    NRF_LOG_INFO("Start Littel Fs test");   
    
    IS25_hw_init();
    IS25_sw_init();

    // mount the filesystem
    NRF_LOG_INFO("Trying to mount a flash driver."); 
    int ret  = lfs_mount(&lfs, &lfs_is25_cfg);

    if(ret != LFS_ERR_OK) {
      
      NRF_LOG_INFO("No little FS found on flash driver. Trying to format flash driver.");
       
      ret = lfs_format(&lfs, &lfs_is25_cfg);
      
      if( ret != LFS_ERR_OK ) {
        
        NRF_LOG_ERROR("Fail to format flash drive with error %d", ret);
        NRF_LOG_ERROR("END OF TEST");  
        return;
         
      }
      
      NRF_LOG_INFO("Formating flash driver: success");
       
      ret = lfs_mount(&lfs, &lfs_is25_cfg);
      
      if(ret != LFS_ERR_OK) {
       
        NRF_LOG_ERROR("Fail to mount flash drive with error %d", ret);
        NRF_LOG_ERROR("END OF TEST");  
        return; 

      } 
       
    }  
    
    NRF_LOG_INFO("IS25 mounted successfully");
    NRF_LOG_INFO("Reading boot counter"); 

    // read current count
    uint32_t boot_count = 0;  
    lfs_file_open(&lfs, &file, "boot_count", LFS_O_RDWR | LFS_O_CREAT);
    lfs_file_read(&lfs, &file, &boot_count, sizeof(boot_count));
    
    // update boot count
    boot_count += 1;
    lfs_file_rewind(&lfs, &file);
    lfs_file_write(&lfs, &file, &boot_count, sizeof(boot_count));

    // remember the storage is not updated until the file is closed successfully
    lfs_file_close(&lfs, &file);

    // release any resources we were using
    lfs_unmount(&lfs);

    // print the boot count
    NRF_LOG_INFO("boot_count: %d\n", boot_count);
    NRF_LOG_FLUSH(); 
  
    uint32_t addr = NULL;       
    char mantra[]   = "Give me a green cucumber"; 
    uint8_t mantra_len = strlen(mantra);
    
    //IS25_chip_erase(); take more than 70 sec
    NRF_LOG_DEBUG("Erasing 4k sector"); 
    IS25_erase_4k_sector(addr); // Erase the sector for mantra
    
    NRF_LOG_DEBUG("Wrting mantry '%s' to block", mantra);
    IS25_write_page(addr, (uint8_t* )mantra, mantra_len ); //Save the mantra to flash
    
    NRF_LOG_DEBUG("Delet mantry '%s'  from ram ", mantra);
    memset(mantra, NULL, mantra_len); // Forget the mantra
    
    NRF_LOG_DEBUG("Reading mantry from block");
    IS25_read_page(addr, (uint8_t* )mantra, mantra_len ); // Read the mantra from flash
    
    NRF_LOG_DEBUG("Read mantra: %s", mantra);   // Show the mantra
    
    NRF_LOG_FLUSH();


} 





skd_db_error_code_t    skd_db_write_sample  (skd_data_sample_t* pSample){
  
  int ret = lfs_file_write( &lfs, &db_file, (uint8_t*)pSample, sizeof( skd_data_sample_t ));   
  
  if (ret < LFS_ERR_OK){
    
    NRF_LOG_ERROR("Fail to write sample with error %d", ret);
    return DB_UNKNOWN_ERROR;
  
  } 

  NRF_LOG_INFO("The current file postition is %d and file size %d", db_file.pos, db_file.size); 
  NRF_LOG_INFO(" %d byte cashed and  and max cash size is %d", db_file.pos- db_file.size, max_cashed_data_size); 

  if(db_file.pos - db_file.size > max_cashed_data_size ) {

    NRF_LOG_INFO("Sync file"); 
    lfs_file_sync(&lfs, &db_file);
  } 

  return DB_NO_ERROR; 
}

skd_db_error_code_t   skd_db_read_sample(uint32_t sampleId, skd_data_sample_t* pSample){
  
  lfs_file_sync(&lfs, &db_file);
  
  lfs_soff_t db_size = lfs_file_size(&lfs,&db_file)/ sizeof( skd_data_sample_t ); 
  
  //check if parameters legit
  if(sampleId > db_size)  return DB_INVALIDE_PARAMETERS;
  
  //set cursor
  lfs_file_seek(&lfs, &db_file, sampleId*sizeof(skd_data_sample_t) , LFS_SEEK_SET); 
   
  //read 
  int ret = lfs_file_read(&lfs, &db_file, pSample, sizeof(skd_data_sample_t));

  if( ret < LFS_ERR_OK) {
  
    NRF_LOG_ERROR("Fail to read sample %d with error %d", sampleId, ret);
    
    return DB_UNKNOWN_ERROR;
  }  
  
  //set cursor back to the end of file
  lfs_file_seek(&lfs,&db_file, 0, LFS_SEEK_END); 

  return DB_NO_ERROR; 
} 


skd_db_error_code_t  skd_db_read_set (uint32_t strId, uint32_t endId, void* pSet){
  
  lfs_file_sync(&lfs, &db_file);

  lfs_soff_t db_size = lfs_file_size(&lfs, &db_file)/ sizeof( skd_data_sample_t ); 

  //check if parameters legit
  if(strId > endId )   return DB_INVALIDE_PARAMETERS;
  if(endId > db_size)  return DB_INVALIDE_PARAMETERS; 
  
  //set cursor
  lfs_file_seek(&lfs, &db_file, strId*sizeof(skd_data_sample_t) , LFS_SEEK_SET); 
  
  //read 
  int ret = lfs_file_read(&lfs, &db_file, pSet, (endId - strId)*sizeof(skd_data_sample_t));

  if( ret < LFS_ERR_OK) {
  
    NRF_LOG_ERROR("Fail to read set with error %d",  ret);
    
    return DB_UNKNOWN_ERROR;
  }  
  
  //set cursor back to the end of file
  lfs_file_seek(&lfs, &db_file, 0, LFS_SEEK_END); 

  return DB_NO_ERROR; 
 
}

uint32_t skd_db_read_size (void){

  return lfs_file_size(&lfs,&db_file)/ sizeof( skd_data_sample_t ); 
 
} 



static uint8_t buf[280] = {NULL}; 

void skd_db_runTest() {

    uint32_t chipID = NULL;
    uint16_t unique_chipID = NULL;  
    
    IS25_hw_init();
    IS25_sw_init();
    IS25_sw_reset(); 
    


    uint32_t sector5     = 5;
    uint32_t sector5Addr = sector5 * IS25_ERASABLE_SECTOR_SIZE; 
    uint32_t offset      = 255; 
    


    IS25_erase_4k_sector(sector5Addr);
    
    memset(buf, 1, sizeof(buf)); 
    buf[IS25_WRITABLE_PAGE_SIZE -1] = 0x05;
    IS25_write_page(sector5Addr  , buf, IS25_WRITABLE_PAGE_SIZE -1 );
    IS25_read_page(sector5Addr, buf, IS25_WRITABLE_PAGE_SIZE -1);
    NRF_LOG_ERROR("The first page with addr %d ", sector5Addr )
    NRF_LOG_HEXDUMP_ERROR(buf, 50 );
    
    NRF_LOG_ERROR("the last byte is %d", buf[IS25_WRITABLE_PAGE_SIZE -1] );   

    memset(buf, 2, sizeof(buf));
    buf[IS25_WRITABLE_PAGE_SIZE -1] = 0x07;
    IS25_write_page(sector5Addr + IS25_WRITABLE_PAGE_SIZE, buf, IS25_WRITABLE_PAGE_SIZE -1);
    IS25_read_page(sector5Addr + IS25_WRITABLE_PAGE_SIZE, buf, IS25_WRITABLE_PAGE_SIZE - 1);
    NRF_LOG_ERROR("The second page with addr %d ", sector5Addr + IS25_WRITABLE_PAGE_SIZE )
    NRF_LOG_HEXDUMP_ERROR(buf, 50 );


   NRF_LOG_ERROR("the last byte is %d", buf[IS25_WRITABLE_PAGE_SIZE -1] );   

  user_provided_block_device_prog(NULL, 2, 0, buf, 200);

  user_provided_block_device_read(NULL, 0, 0, buf, 250);
  user_provided_block_device_read(NULL, 1, 0, buf, 250);
  
  user_provided_block_device_read(NULL, 2, 0, buf, 250);

  user_provided_block_device_erase(NULL,  2);

  user_provided_block_device_read(NULL, 0, 0, buf, 250);
  user_provided_block_device_read(NULL, 1, 0, buf, 250);
  user_provided_block_device_read(NULL, 2, 0, buf, 250);


    chipID         =  IS25_readID();
    unique_chipID  =  IS25_read_unique_ID();
    NRF_LOG_INFO("Chip ID : 0x%08x", chipID);
    NRF_LOG_INFO("Unique chip ID : 0x%04x", unique_chipID); 
    NRF_LOG_FLUSH(); 

    uint32_t sector2     = 2;
    uint32_t sector2Addr = sector2 * IS25_ERASABLE_SECTOR_SIZE; 

    uint32_t sector16     = 16;
    uint32_t sector16Addr = sector16 * IS25_ERASABLE_SECTOR_SIZE; 
    
    uint32_t sector17     = 17;
    uint32_t sector17Addr = sector17 * IS25_ERASABLE_SECTOR_SIZE; 


    uint32_t sector32     = 32;
    uint32_t sector32Addr = sector32 * IS25_ERASABLE_SECTOR_SIZE; 


    //IS25_erase_chip();
    //IS25_erase_4k_sector(0);
    IS25_erase_4k_sector(0);
    IS25_erase_4k_sector(sector2Addr);
    IS25_erase_4k_sector(sector16Addr); 
    IS25_erase_4k_sector(sector17Addr);
    IS25_erase_4k_sector(sector32Addr);
    //IS25_erase_4k_sector(1);
      
     memset(buf, 0, 8); 
     IS25_write_page(0, buf, 8 ); 
     
     memset(buf, 1, 8); 
     IS25_write_page(8, buf, 8 ); 


     memset(buf, 1, 8); 
     IS25_write_page(sector2Addr, buf, 8); 
     
     memset(buf, 0x16, 8); 
     IS25_write_page(sector16Addr, buf, 8);  

     memset(buf, 0x17, 8); 
     IS25_write_page(sector17Addr, buf, 8); 


     memset(buf, 0x32, 8); 
     IS25_write_page(sector32Addr, buf, 8); 

    IS25_erase_4k_sector(sector2Addr + 1000);
    //IS25_erase_4k_sector(  sector2Addr );
    //IS25_erase_32k_block(sector16Addr);
    //IS25_erase_64k_block(0);

    IS25_read_page(0, buf, 50); 
    NRF_LOG_ERROR("The first sector")
    NRF_LOG_HEXDUMP_ERROR(buf, 50 );
   


    NRF_LOG_ERROR("----------------------------------------");
    NRF_LOG_ERROR("Second sector");
    IS25_read_page(sector2Addr, buf, 50); 
    NRF_LOG_HEXDUMP_ERROR(buf, 50 );


    NRF_LOG_ERROR("----------------------------------------");
    NRF_LOG_ERROR("16 sector");
    IS25_read_page(sector16Addr, buf, 50); 
    NRF_LOG_HEXDUMP_ERROR(buf, 50 );

    NRF_LOG_ERROR("----------------------------------------");
    NRF_LOG_ERROR("17 sector");
    IS25_read_page(sector17Addr, buf, 50); 
    NRF_LOG_HEXDUMP_ERROR(buf, 50 );


    NRF_LOG_ERROR("----------------------------------------");
    NRF_LOG_ERROR("32 sector");
    IS25_read_page(sector32Addr, buf, 50); 
    NRF_LOG_HEXDUMP_ERROR(buf, 50 );



} 

*/