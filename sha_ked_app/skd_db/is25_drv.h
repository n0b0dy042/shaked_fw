 #ifndef __IS25_DRV_H__
#define __IS25_DRV_H__

#include <stdio.h>
#include <stdint.h>
#include <string.h>


#define BYTES_IN_4_KB         4*1024 
#define BYTES_IN_32_KB        8*BYTES_IN_4_KB
#define BYTES_IN_64_KB        2*BYTES_IN_32_KB


#define IS25_MAX_WRITABLE_NUMBER_OF_BYTES        255

#define IS25_WRITABLE_PAGE_SIZE                  256
#define IS25_ERASABLE_SECTOR_SIZE                BYTES_IN_4_KB 
#define IS25_ERASABLE_BLOCK_SIZE                 BYTES_IN_16_KB 
#define IS25_ERASABLE_EXT_BLOCK_SIZE             BYTES_IN_32_KB 


#define IS25_ERASABLE_SECTOR_COUNT                8192
#define IS25_ERASABLE_BLOCK_COUNT                 1024
#define IS25_ERASABLE_EXT_BLOCK_COUNT             512


typedef enum {

  IS25_NO_ERROR = 0,
  IS25_INVALID_PARAMETERS,
  IS25_INVALID_PAGE_ADDR, 
  IS25_DEVICE_BUSY, 
  IS25_UNKNOWN_ERROR

} IS25_error_code_t; 



typedef struct{
  uint8_t WIP:1;  //Write In Progress Bit. Indicates if a write cycle is in progress and the device is busy 
  uint8_t WEL:1;  //Write Enable Latch. Indicates If the device is write enabled
  uint8_t BP:4;   // Block Protection Bit. Indicates, if the specific blocks are write-protected 
  uint8_t QE:1;   // Quad Enable bit. Indicates the Quad output function enable
  uint8_t SRWD:1; // Status Register Write Disable. Indicates, if the Status Register is write-protected 
     
}IS25_status_reg_t;  

typedef struct{

  uint8_t RE:1;     //Indicates if reset Enable/Disable 
  uint8_t TBS:1;    //Top/Bottom Selection, Indicates Top/Botton area.
  uint8_t PSUS:1;   //Program suspend bit. Indicates if program is suspend or not
  uint8_t ESUS:1;   // Erase suspend bit. indicates  if erase is suspend or not
  uint8_t IRL:4;    //Indicates if the Information Rows can be programmed 

}IS25_fucntion_reg_t;  


typedef struct{

  uint8_t burstLen:2;         //Burst Length. For detail check ref.
  uint8_t brustLenEnable:1;   //
  uint8_t dummyCycle:4;       // Number of Dummy Cycles
  uint8_t holdReset:1;

}IS25_read_reg_t;

typedef struct{

  uint8_t WIP:1;       // Same as WIP in IS25_status_reg_t
  uint8_t PROT_E:1;    // Protection Error Bit. Indicates protection error in an Erase or a Program operation
  uint8_t P_ERR:1;     // Program Error Bit. Indicates an Program operation failure or protection error
  uint8_t RESERVED:2; 
  uint8_t OSD:3;       // Output Driver Strength:

}IS25_ext_read_reg_t;


typedef struct{

  uint32_t ABE:1;
  uint32_t ABSD:4;   
  uint32_t ABSA:27;
   
}IS25_autoboot_reg_t;

typedef struct{

  uint8_t BA24:1;       //Indicates  if lower/upper 128Mb segment is selected.  
  uint8_t RESERVED:6;
  uint8_t EXTADD:1;     // Indicates if 3-byte or 4-byte addressing selection

}IS25_bank_address_reg_t;

typedef struct{

  uint16_t RESERVED1:1;
  uint16_t PSTMLB:1;   // Persistent Protection Mode Lock Bit
  uint16_t PWDMLB:1;   // Password Protection Mode Lock Bit
  uint16_t RESERVED2:12;
  uint16_t TBPARM:1;   // Configures Parameter Sectors location

}IS25_asp_reg_t;        



// Basic Operation
IS25_error_code_t         IS25_hw_init              (void); 
IS25_error_code_t         IS25_sw_init              (void);
IS25_error_code_t         IS25_hw_deInit            (void); 
IS25_error_code_t         IS25_sw_deInit            (void);

uint32_t                  IS25_readID               (void); 
uint16_t                  IS25_read_unique_ID       (void);

void                      IS25_sw_reset             (void);                    
IS25_error_code_t         IS25_write_page           (uint32_t addr, uint8_t* pData, uint8_t dataLen);
IS25_error_code_t         IS25_read_page            (uint32_t addr, uint8_t* pData, uint8_t dataLen);
IS25_error_code_t         IS25_erase_4k_sector      (uint32_t addr); 
IS25_error_code_t         IS25_erase_32k_block      (uint32_t addr);
IS25_error_code_t         IS25_erase_64k_block      (uint32_t addr);
IS25_error_code_t         IS25_erase_chip           (void); 


//Debug function 
IS25_error_code_t         IS25_test(void);
IS25_status_reg_t         IS25_read_status          (void);
void                      IS25_wrtie_status         (IS25_status_reg_t value);  
IS25_bank_address_reg_t   IS25_read_bank_reg        (void);
IS25_ext_read_reg_t       IS25_read_ext_reg         (void);


#endif