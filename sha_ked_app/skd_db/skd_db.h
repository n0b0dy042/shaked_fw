#ifndef SKD_DB_SRV_H
#define SKD_DB_SRV_H


#include <stdint.h>

#include "sha_ked_common.h"



#define  MAX_SET_LEN              (243/sizeof(skd_snapshot_t) ) - 1

typedef skd_db_settings_t skd_db_config_t;

typedef enum{

  DB_NO_ERROR,
  DB_NO_FLASH_DRIVE_FOUND,
  DB_FLASH_DRVIE_CORRUPTED,
  DB_DATA_BASED_CORRUPTED,
  DB_INVALIDE_PARAMETERS,
  DB_UNKNOWN_ERROR
  
}skd_db_error_code_t; 


typedef enum{
  
  TRANSFER_NO_ERROR = 0,
  TRANSFER_COMPLITED_SUCCESS,
  TRANSFER_ABORTED,
  TRANSFER_ABORTED_BY_USER,
  TRANSFER_BUSY,

} skd_db_transfer_status_t;  


typedef void                      (*skd_db_transferCompleteCb_t)    (skd_db_transfer_status_t status);  
typedef skd_db_transfer_status_t  (*skd_db_sendDateCb_t)            (uint8_t *pData,  uint16_t dataLen);  


typedef struct {
 
  skd_db_transferCompleteCb_t      transferCompleteCb;
  skd_db_sendDateCb_t              sendDataCb;
  
}skd_db_transfer_api_t; 


skd_db_error_code_t           skd_db_init                     (skd_db_config_t* pConfig);
skd_db_error_code_t           skd_db_deInit                   (void); 


skd_db_error_code_t           skd_db_write_record             (skd_snapshot_t* pRecord);
uint32_t                      skd_db_read_number_of_records   (void);  
skd_db_error_code_t           skd_db_read_record              (uint32_t sampleId, skd_snapshot_t* pRecord);
skd_db_error_code_t           skd_db_read_records             (uint32_t strId, uint32_t endId, void* pRecords);  


skd_db_error_code_t           skd_db_clean_soft               (void);
skd_db_error_code_t           skd_db_clean_hard               (void);


skd_db_transfer_status_t      skd_db_trf_init                 ( uint32_t strId, 
                                                                uint32_t len,  
                                                                skd_db_transfer_api_t* pApi);


skd_db_error_code_t      skd_db_trf_deInit               ( void );
void                     skd_db_trf_process              ( void );


/* Legacy

void                     skd_fs_test                     (void );   // check little fs
skd_db_error_code_t      skd_db_test                     (void );   // check db based on little fs
void                     skd_db_runTest                  (void);
skd_db_error_code_t      skd_db_write_sample  (skd_data_sample_t* pSample);
uint32_t                 skd_db_read_size     (void);  
skd_db_error_code_t      skd_db_read_sample   (uint32_t sampleId, skd_data_sample_t* pSample);
skd_db_error_code_t      skd_db_read_set      (uint32_t strId, uint32_t endId, void* pSet);  

*/ 

#endif