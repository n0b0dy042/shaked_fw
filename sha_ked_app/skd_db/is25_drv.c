#include "IS25_drv.h"
#include "nrf_drv_spi.h"
#include "nrf_gpio.h"
#include "nrf_delay.h"
#include "sha_ked_debug.h"

#define IS25_READ_CHIP_ID_INST_ID            0x9f    /* Read JEDEC ID */
#define IS25_REALSE_POWER_DOWN_INST_ID       0xAB    /* RELEASE DEEP POWER DOWN */
#define IS25_READ_STATUS_REG_INST_ID         0x05    /* Read status register */
#define IS25_WRITE_STATUS_REG_INST_ID        0x01    /* Write status register */  
#define IS25_READ_UNIQUE_ID_INST_ID          0x4B    /* Read a a factory-set read-only 16-byte number that is unique to the device*/
#define IS25_ENTER_DP_PWR_DWN_INST_ID        0xB9    /* Setting the device on the minimizing the power consumption*/ 
#define IS25_RELEASE_DP_PWR_DWN_INST_ID      0xAB    /* Release the device from the deep power-down mode*/
#define IS25_RESET_ENABLE_INST_ID            0x66    /* Enable reset command ID*/ 
#define IS25_RESET_START_INST_ID             0x99    /* Start reset command ID*/
#define IS25_WRITE_ENABLE_INST_ID            0x06    /* Enable write mode */
#define IS25_WRITE_DISABLE_INST_ID           0x04    /* Disable write mode */
#define IS25_READ_BANK_REG_INST_ID           0x16
#define IS25_ENTER_4B_MODE_INST_ID           0xB7    /* ENTER 4-BYTE ADDRESS MODE OPERATION */
#define IS25_EXIT_4B_MODE_INST_ID            0x29    /* EXIT 4-BYTE ADDRESS MODE OPERATION */
#define IS25_PROGRAM_PAGE_4B_INST_ID         0x12    /* 4PP PAGE PROGRAM OPERATION*/
#define IS25_READ_EXT_READ_REG_INST_ID       0x81    /* READ EXTENDED READ PARAMETERS OPERATION */
#define IS25_READ_PAGE_NORM_4B_INST_ID       0x13    /* The Normal Read (NORD) instruction is used to read memory contents at a maximum frequency of 80MHz. */
#define IS25_CHIP_ERASE_OPERATION_INST_ID    0x60    /* Erases the entire memory array. */ 
#define IS25_SECTOR_ERASE_INST_ID            0X21    /* Erases a 4 Kbyte sector */
#define IS25_32K_BLOCK_ERASE_INST_ID         0X5C    /* Erases a 32 Kbyte block for 4b addressing*/ 
#define IS25_64K_BLOCK_ERASE_INST_ID         0XDC    /* Erases a 64 Kbyte block for 4b addressing*/ 


#define IS25_SPI_SS_PIN       6 
#define IS25_SPI_MOSI_PIN     3
#define IS25_SPI_MISO_PIN     5
#define IS25_SPI_SCK_PIN      2



#define IS25_CS_INIT()           nrf_gpio_cfg_output(IS25_SPI_SS_PIN);
#define IS25_CS_HIGH()           nrf_gpio_pin_set(IS25_SPI_SS_PIN)
#define IS25_CS_LOW()            nrf_gpio_pin_clear(IS25_SPI_SS_PIN)



#define S_125_kHz	33554432
#define S_250_kHz	67108864 
#define S_500_kHz	134217728
#define S_1_MHz 	268435456 
#define S_2_MHz 	536870912
#define S_4_MHz 	1073741824 
#define S_8_MHz 	2147483648




#if SHA_KED_IS25_DRV_LOG_ENABLED == 1 
    #define NRF_LOG_LEVEL               SHA_KED_IS25_DRV_LOG_LEVEL
    #define NRF_LOG_INFO_COLOR          SHA_KED_IS25_DRV_INFO_LOG_COLOR
    #define NRF_LOG_DEBUG_COLOR         SHA_KED_IS25_DRV_DEBUG_LOG_COLOR
#else 
    #define NRF_LOG_LEVEL       0
#endif 


#define NRF_LOG_MODULE_NAME     is25_drv
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
NRF_LOG_MODULE_REGISTER();

#define SPI_INSTANCE  0 /**< SPI instance index. */
static const nrf_drv_spi_t spi = NRF_DRV_SPI_INSTANCE(SPI_INSTANCE);  /**< SPI instance. */
static volatile bool spi_xfer_done;  /**< Flag used to indicate that SPI instance completed the transfer. */



void IS25_delay_us(uint32_t delayUs){
  nrf_delay_us(delayUs); 
} 

void IS25_delay_ms(uint32_t delayMs){
  nrf_delay_ms(delayMs);
} 
/**
 * @brief SPI user event handler.
 * @param event
 */
void spi_event_handler(nrf_drv_spi_evt_t const * p_event,
                       void *                    p_context)
{
    spi_xfer_done = true;
    NRF_LOG_DEBUG("Transfer completed.");


}
IS25_error_code_t  IS25_hw_init(){
    
  nrf_drv_spi_config_t spi_config = {
      .sck_pin		= IS25_SPI_SCK_PIN,
      .mosi_pin		= IS25_SPI_MOSI_PIN,
      .miso_pin		= IS25_SPI_MISO_PIN,
      .ss_pin		= NRF_DRV_SPI_PIN_NOT_USED,
      .irq_priority	= SPI_DEFAULT_CONFIG_IRQ_PRIORITY,
      .orc		= 0xFF,
      .frequency	= (nrf_drv_spi_frequency_t) S_8_MHz,
      .mode		= NRF_DRV_SPI_MODE_0,
      .bit_order	= NRF_DRV_SPI_BIT_ORDER_MSB_FIRST,
  };

  IS25_CS_INIT();
  IS25_CS_HIGH(); 
  
  nrf_drv_spi_init(&spi, &spi_config, NULL, NULL); // spi_event_handler, NULL);
  
  return IS25_NO_ERROR;

}

IS25_error_code_t  IS25_sw_init(){
  
  IS25_sw_reset(); 
  return IS25_NO_ERROR;

}

static IS25_error_code_t  IS25_write_instruction(uint8_t inst_id, uint8_t* pData, uint32_t dataLen){
  
  ret_code_t ret    =   NULL; 

  IS25_CS_LOW();

  ret = nrf_drv_spi_transfer(&spi, &inst_id, sizeof(inst_id), NULL, NULL);

  if (ret != NRF_SUCCESS ) {
    NRF_LOG_ERROR("SPI FAIL TO TRANSFER with error %d", ret);
    IS25_CS_HIGH();
    return  IS25_UNKNOWN_ERROR;
  }

  if (dataLen != NULL) nrf_drv_spi_transfer(&spi, pData,  dataLen,  NULL, NULL);
  
  IS25_CS_HIGH();
  
  return IS25_NO_ERROR;
}

static IS25_error_code_t  IS25_write_instruction_ext(uint8_t inst_id, uint32_t addr,  uint8_t* pData, uint32_t dataLen){
 
  uint8_t buf[5] = {NULL}; 
 
  buf[0] =  inst_id; 
  buf[1] =  (addr >> 24);
  buf[2] =  (addr >> 16);
  buf[3] =  (addr >> 8);
  buf[4] =  addr; 

  IS25_CS_LOW();

  nrf_drv_spi_transfer(&spi, buf, 5, NULL, NULL);

  if (dataLen != NULL) {

    nrf_drv_spi_transfer(&spi, pData,  dataLen,  NULL, NULL);
    //nrf_drv_spi_transfer(&spi, &pData[dataLen],  1,  NULL, NULL);

  }

  IS25_CS_HIGH();
  
  return IS25_NO_ERROR;
}

static IS25_error_code_t  IS25_read_instruction(uint8_t inst_id, uint8_t* pData, uint32_t dataLen){
  
  ret_code_t ret    =   NULL; 
  
  IS25_CS_LOW();
  ret = nrf_drv_spi_transfer(&spi, &inst_id, sizeof(inst_id), NULL, NULL);

  if (ret != NRF_SUCCESS ) {
    NRF_LOG_ERROR("SPI FAIL TO TRANSFER with error %d", ret);
    IS25_CS_HIGH();
    return  IS25_UNKNOWN_ERROR; 
  }

  IS25_delay_us(30); // small to avoid missing bytes(NRF spi driver problem)
  
  ret = nrf_drv_spi_transfer(&spi, NULL,  NULL,  pData, dataLen);
  if (ret != NRF_SUCCESS ) {
    NRF_LOG_ERROR("SPI FAIL TO RECEIVE with error %d", ret);
    IS25_CS_HIGH(); 
    return  IS25_UNKNOWN_ERROR; 
  }
  IS25_CS_HIGH();


  return IS25_NO_ERROR;

}


static IS25_error_code_t  IS25_read_instruction_ext(uint8_t inst_id, uint32_t addr,  uint8_t* pData, uint32_t dataLen){
  
  ret_code_t ret    =   NULL; 
  
  uint8_t buf[5] = {NULL};
  
  buf[0] =  inst_id; 
  buf[1] =  (addr >> 24);
  buf[2] =  (addr >> 16);
  buf[3] =  (addr >> 8);
  buf[4] =  addr; 




  IS25_CS_LOW();

   ret = nrf_drv_spi_transfer(&spi, buf, 5, NULL, NULL);
  if (ret != NRF_SUCCESS ) {
    NRF_LOG_ERROR("SPI FAIL TO TRANSFER instruction with error %d", ret);
    IS25_CS_HIGH();
    return  IS25_UNKNOWN_ERROR; 
  }
  
  IS25_delay_us(30); // small to avoid missing bytes(NRF spi driver problem)
  
  ret = nrf_drv_spi_transfer(&spi, NULL,  NULL,  pData, dataLen);
  if (ret != NRF_SUCCESS ) {
    NRF_LOG_ERROR("SPI FAIL TO RECEIVE with error %d", ret);
    IS25_CS_HIGH(); 
    return  IS25_UNKNOWN_ERROR; 
  }

/*  
  ret = nrf_drv_spi_transfer(&spi, NULL,  NULL,  &pData[dataLen], 1);
  if (ret != NRF_SUCCESS ) {
    NRF_LOG_ERROR("SPI FAIL TO RECEIVE with error %d", ret);
    IS25_CS_HIGH(); 
    return  IS25_UNKNOWN_ERROR; 
  }
*/
  IS25_CS_HIGH();


  return IS25_NO_ERROR;

}

IS25_status_reg_t IS25_read_status(void){

  IS25_status_reg_t status;

  IS25_read_instruction( IS25_READ_STATUS_REG_INST_ID, (uint8_t*)&status, sizeof(status) ); 
  
  return status; 

}


void IS25_wrtie_status(IS25_status_reg_t value){
  
  IS25_write_instruction(IS25_WRITE_STATUS_REG_INST_ID, (uint8_t*)&value, sizeof(value));

}


IS25_bank_address_reg_t IS25_read_bank_reg(void){

  IS25_bank_address_reg_t reg; 

  IS25_read_instruction( IS25_READ_BANK_REG_INST_ID, (uint8_t *)&reg, sizeof(reg) );
  
  return reg; 

} 

static IS25_error_code_t IS25_wait(uint32_t delayUs){

  IS25_status_reg_t status;   
  
  do{
    
    IS25_delay_us( 5 );

    status = IS25_read_status(); 

    if(!status.WIP)   return IS25_NO_ERROR;
    if(delayUs <= 5)  return IS25_DEVICE_BUSY; 
    
    delayUs -= 5; 
      
  }while(1); 

   
}  

static IS25_error_code_t IS25_enterDeepPowerDown(void){ 
  
   IS25_write_instruction(IS25_ENTER_DP_PWR_DWN_INST_ID, NULL, NULL); 
  
}

static IS25_error_code_t IS25_releaseDeepPowerDown(void){

  IS25_write_instruction(IS25_RELEASE_DP_PWR_DWN_INST_ID, NULL, NULL); 

} 

uint16_t IS25_read_unique_ID(void){

  ret_code_t ret         =  NULL;
  uint16_t   unique_Id   =  NULL; 

  IS25_read_instruction_ext(  IS25_READ_UNIQUE_ID_INST_ID, 
                              NULL, 
                              (uint8_t* )&unique_Id, 
                              sizeof(unique_Id));
                               
  return unique_Id; 
}

uint32_t IS25_readID(){

  uint32_t   chipId = NULL;
  
  IS25_read_instruction( IS25_READ_CHIP_ID_INST_ID, (uint8_t* )&chipId, 3 );
    
  return chipId;
   
}; 

static void IS25_enableWriteMode(void){

  IS25_write_instruction(IS25_WRITE_ENABLE_INST_ID, NULL, NULL);

};


static void IS25_disableWriteMode(void){

  IS25_write_instruction(IS25_WRITE_DISABLE_INST_ID, NULL, NULL);

};

static void IS25_enter_4b_mode(void){

  IS25_write_instruction(IS25_ENTER_4B_MODE_INST_ID, NULL, NULL);

} 

static void IS25_exit_4b_mode(void){

  IS25_write_instruction(IS25_EXIT_4B_MODE_INST_ID, NULL, NULL);

}

void IS25_sw_reset(void){
  
  IS25_write_instruction(IS25_RESET_ENABLE_INST_ID, NULL, NULL); 
  IS25_write_instruction(IS25_RESET_START_INST_ID, NULL, NULL);  

}

IS25_error_code_t IS25_write_page(uint32_t addr, uint8_t* pData, uint8_t dataLen){

  uint32_t str_page_numb = (uint32_t)(addr/IS25_WRITABLE_PAGE_SIZE);
  uint32_t end_page_num  = (uint32_t) ( (addr + dataLen - 1) / IS25_WRITABLE_PAGE_SIZE );

  IS25_error_code_t ret = IS25_NO_ERROR; 
  IS25_status_reg_t status; 

    
  if(str_page_numb != end_page_num ){
  
    uint32_t numberOfLostBytes = (addr + dataLen) % IS25_WRITABLE_PAGE_SIZE; 

    NRF_LOG_WARNING("Wrong buffer size. %d bytes in the tail will be lost.", numberOfLostBytes);

  }
  
  //check if data lenght is not to long
  if( dataLen >  IS25_MAX_WRITABLE_NUMBER_OF_BYTES) return IS25_INVALID_PARAMETERS;
  
  //check if 4 byte addressing enable
  IS25_bank_address_reg_t bank_reg = IS25_read_bank_reg();

  if(!bank_reg.EXTADD) {
  
    NRF_LOG_INFO("No 4-byte addressing disable. Setting 4-byte addressing");  
    IS25_enter_4b_mode(); 
  
  }
  
  //check if device is ready
  status = IS25_read_status(); 
  
  if(status.WIP)  ret = IS25_wait(200);
  
  if(ret != IS25_NO_ERROR){

   NRF_LOG_ERROR("Fail to write page. Device not ready after timeout."); 
   return ret;  

  } 
  
  IS25_enableWriteMode(); 
  
  ret = IS25_write_instruction_ext(IS25_PROGRAM_PAGE_4B_INST_ID, addr,  pData, dataLen); 
  
  if(ret != IS25_NO_ERROR) {
    
    NRF_LOG_ERROR("Fail to write page with error 0x%02x", ret); 
    return ret; 

  }
  
  // Wait from 200 us to 800 us for page programming to finish
  nrf_delay_us(200);  
  ret = IS25_wait(600); 
  
  if(ret != IS25_NO_ERROR){

   NRF_LOG_ERROR("The sector erassing still in progress."); 
   return ret;  

  } 
   
  return IS25_NO_ERROR;
} 

IS25_error_code_t IS25_read_page(uint32_t addr, uint8_t* pData, uint8_t dataLen){

  IS25_error_code_t ret = IS25_NO_ERROR; 
  IS25_status_reg_t status; 
  
  //check if data lenght is not to long
  if( dataLen >  IS25_MAX_WRITABLE_NUMBER_OF_BYTES) return IS25_INVALID_PARAMETERS;
  
  //check if 4 byte addressing enable
  IS25_bank_address_reg_t bank_reg = IS25_read_bank_reg();

  if(!bank_reg.EXTADD) {
  
    NRF_LOG_INFO("No 4-byte addressing disable. Setting 4-byte addressing");  
    IS25_enter_4b_mode(); 
  
  }
  
  //check if device is ready
  status = IS25_read_status(); 
  
  if(status.WIP)  ret = IS25_wait(250);
  
  if(ret != IS25_NO_ERROR){

   NRF_LOG_ERROR("Fail to read page. Device not ready after timeout.") 
   return ret;  

  } 
  
  ret = IS25_read_instruction_ext(IS25_READ_PAGE_NORM_4B_INST_ID, addr,  pData, dataLen);
  
  if ( ret!= IS25_NO_ERROR){
    NRF_LOG_ERROR("Fail to read page with error 0x%02x", ret);  
  }

  return IS25_NO_ERROR; 
  
}


IS25_error_code_t IS25_erase_4k_sector(uint32_t addr){
  
  IS25_error_code_t ret = IS25_NO_ERROR; 
  IS25_status_reg_t status; 

  //check if 4 byte addressing enable
  IS25_bank_address_reg_t bank_reg = IS25_read_bank_reg();

  if(!bank_reg.EXTADD) {
      
    NRF_LOG_INFO("No 4-byte addressing disable. Setting 4-byte addressing");  
    IS25_enter_4b_mode(); 
    
  }
  
  //check if device is ready
  status = IS25_read_status(); 
  

  if(status.WIP)  ret = IS25_wait(250);
  
  if(ret != IS25_NO_ERROR){

   NRF_LOG_ERROR("Fail to erase sector. Device not ready after timeout.") 
   return ret;  

  } 

  IS25_enableWriteMode();

  IS25_write_instruction_ext( IS25_SECTOR_ERASE_INST_ID, addr, NULL, NULL); 
  
  // Wait from 0.100 sec to 0.500 sec for errasing to finish
  nrf_delay_ms(100);  
  ret = IS25_wait(200*1000); 
  
  if(ret != IS25_NO_ERROR){

   NRF_LOG_ERROR("The sector erassing still in progress.") 
   return ret;  

  } 
  
  return IS25_NO_ERROR;
}

IS25_error_code_t IS25_erase_32k_block(uint32_t addr){
  
  IS25_error_code_t ret = IS25_NO_ERROR;  
  IS25_status_reg_t status; 
  
  //check if 4 byte addressing enable
  IS25_bank_address_reg_t bank_reg = IS25_read_bank_reg();

  if(!bank_reg.EXTADD) {
      
    NRF_LOG_INFO("No 4-byte addressing disable. Setting 4-byte addressing");  
    IS25_enter_4b_mode(); 
    
  }
  
  //check if device is ready
  status = IS25_read_status(); 
  
  if(status.WIP)  ret = IS25_wait(250);
  
  if(ret != IS25_NO_ERROR){

   NRF_LOG_ERROR("Fail to erase balock. Device not ready after timeout.") 
   return ret;  

  } 

  IS25_enableWriteMode();
  IS25_write_instruction_ext( IS25_32K_BLOCK_ERASE_INST_ID, addr, NULL, NULL); 
  
  // Wait from 0.14 sec to 0.5 sec for errasing to finish
  nrf_delay_ms(140);  
  ret = IS25_wait(330*1000); 

  if(ret != IS25_NO_ERROR){

   NRF_LOG_ERROR("The block erassing still in progress.") 
   return ret;  

  } 
  

  return IS25_NO_ERROR;
}

IS25_error_code_t IS25_erase_64k_block(uint32_t addr){

  IS25_error_code_t ret = IS25_NO_ERROR;  
  IS25_status_reg_t status; 
  
  //check if 4 byte addressing enable
  IS25_bank_address_reg_t bank_reg = IS25_read_bank_reg();

  if(!bank_reg.EXTADD) {
      
    NRF_LOG_INFO("No 4-byte addressing disable. Setting 4-byte addressing");  
    IS25_enter_4b_mode(); 
    
  }
  
  //check if device is ready
  status = IS25_read_status(); 
  
  if(status.WIP)  ret = IS25_wait(250);
  
  if(ret != IS25_NO_ERROR){

   NRF_LOG_ERROR("Fail to erase balock. Device not ready after timeout.") 
   return ret;  

  } 

  IS25_enableWriteMode();
  IS25_write_instruction_ext( IS25_64K_BLOCK_ERASE_INST_ID, addr, NULL, NULL); 
  
  // Wait from 0.14 sec to 0.5 sec for errasing to finish
  nrf_delay_ms(170);  
  ret = IS25_wait(830*1000); 

  if(ret != IS25_NO_ERROR){

   NRF_LOG_ERROR("The block erassing still in progress.") 
   return ret;  

  } 

  return IS25_NO_ERROR;

}

IS25_error_code_t IS25_erase_chip(){
  
  IS25_error_code_t ret = IS25_NO_ERROR; 
  IS25_status_reg_t status; 
    
  //check if device is ready
  status = IS25_read_status(); 
  
  if(status.WIP)  ret = IS25_wait(250);
  
  if(ret != IS25_NO_ERROR){

   NRF_LOG_ERROR("Fail to erase chip. Device not ready after timeout.") 
   return ret;  

  } 
  IS25_enableWriteMode();
  IS25_write_instruction( IS25_CHIP_ERASE_OPERATION_INST_ID, NULL, NULL); 
  
  // Wait from 70 sec to 0.500 sec for errasing to finish
  nrf_delay_ms(70*1000);  
  ret = IS25_wait(120*1000*1000); 
  
  if(ret != IS25_NO_ERROR){

   NRF_LOG_ERROR("The sector erassing still in progress.") 
   return ret;  

  } 

  return IS25_NO_ERROR;
}


IS25_error_code_t IS25_test(){

    IS25_hw_init(); 
    IS25_sw_init(); 
    
    uint32_t chipID = NULL;
    uint16_t unique_chipID = NULL;  
   
    NRF_LOG_INFO("Test IS25 chipset"); 
     
    chipID         =  IS25_readID();
    unique_chipID  =  IS25_read_unique_ID();
    NRF_LOG_DEBUG("Chip ID : 0x%08x", chipID);
    NRF_LOG_DEBUG("Unique chip ID : 0x%04x", unique_chipID); 
    NRF_LOG_FLUSH(); 
    

    IS25_sw_reset( );
    uint32_t addr = NULL;       
    char mantra[]   = "Give me a green cucumber"; 
    uint8_t mantra_len = strlen(mantra);
    
    //IS25_chip_erase(); take more than 70 sec
    NRF_LOG_DEBUG("Erasing 4k sector"); 
    IS25_erase_4k_sector(addr); // Erase the sector for mantra
    
    NRF_LOG_DEBUG("Wrting mantry '%s' to block", mantra);
    IS25_write_page(addr, (uint8_t* )mantra, mantra_len ); //Save the mantra to flash
    
    NRF_LOG_DEBUG("Delet mantry '%s'  from ram ", mantra);
    memset(mantra, NULL, mantra_len); // Forget the mantra
    
    NRF_LOG_DEBUG("Reading mantry from block");
    IS25_read_page(addr, (uint8_t* )mantra, mantra_len ); // Read the mantra from flash
    
    NRF_LOG_DEBUG("Read mantra: %s", mantra);   // Show the mantra
    
    NRF_LOG_FLUSH();


} 


IS25_error_code_t         IS25_hw_deInit(void){


 return IS25_NO_ERROR;

} 
IS25_error_code_t         IS25_sw_deInit(void){


 return IS25_NO_ERROR;

}