#include "sha_ked_but.h"

#include "sdk_common.h"
#include "app_error.h"
#include "nrf_drv_gpiote.h"
#include "nrf_assert.h"

#include "sha_ked_debug.h"

#if SHA_KED_BUT_LOG_ENABLED == 1 
    #define NRF_LOG_LEVEL               SHA_KED_BUT_LOG_LEVEL
    #define NRF_LOG_INFO_COLOR          SHA_KED_BUT_INFO_LOG_COLOR
    #define NRF_LOG_DEBUG_COLOR         SHA_KED_BUT_DEBUG_LOG_COLOR
#else 
    #define NRF_LOG_LEVEL       0
#endif 

#define NRF_LOG_MODULE_NAME     sha_ked_but
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"
NRF_LOG_MODULE_REGISTER();


#define EMPTY_SLOT_PIN_NUMBER     0 
#define MAX_NUMBER_OF_BUTTONS     3 


APP_TIMER_DEF(m_detection_delay_timer_1_id);  /**< Polling timer id. */
APP_TIMER_DEF(m_detection_delay_timer_2_id);  /**< Polling timer id. */
APP_TIMER_DEF(m_detection_delay_timer_3_id);  /**< Polling timer id. */



static buttonErrorCode_t skd_but_initAll             (void);
static buttonErrorCode_t skd_but_deInitAll           (void);
static uint8_t           skd_but_findButtonId        (uint32_t          pin_no); 
static buttonErrorCode_t skd_but_addButtonToList     (skd_button_cfg_t* pConfig);
static buttonErrorCode_t skd_but_deletButtonFromList (skd_button_cfg_t* pConfig);
static buttonErrorCode_t skd_but_enable              (void);
static buttonErrorCode_t skd_but_disable             (void);

static skd_button_cfg_t  buttonsList    [ MAX_NUMBER_OF_BUTTONS ] =  { NULL };
static app_timer_id_t    buttonsTimers  [ MAX_NUMBER_OF_BUTTONS ] =  { m_detection_delay_timer_1_id, 
                                                                       m_detection_delay_timer_2_id, 
                                                                       m_detection_delay_timer_3_id  };  

static uint8_t          buttonCount = NULL;              /**< Number of configured buttons. */



static uint64_t m_pin_state;
static uint64_t m_pin_transition;

static void debounceTimeout_handler(void * p_context)
{
  
  uint8_t              pin_no        = *(uint32_t*)p_context;   
  
  NRF_LOG_DEBUG("Debounce timeout for button wiht pin number %d", pin_no);
  NRF_LOG_FLUSH(); 
  
  uint8_t  id        =  skd_but_findButtonId(pin_no);   
  bool     crntState =  nrf_drv_gpiote_in_is_set( pin_no );  

  if( buttonsList[id].state != crntState) {
    
     buttonsList[id].state = crntState; 
     if( buttonsList[id].isr_handler ) buttonsList[id].isr_handler(crntState);   

   }

}

static void gpiote_event_handler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action)
{
    uint32_t        err_code;
    uint8_t         id;
    bool            crntState;
    app_timer_id_t  timerId; 
   
    NRF_LOG_DEBUG("Pin %d was triggerd with action %d", pin, action);
    NRF_LOG_FLUSH();  
    
    id = skd_but_findButtonId( pin );
    
    err_code = app_timer_stop( buttonsList[id].debounce_timerId);
    
    crntState = nrf_drv_gpiote_in_is_set( pin );
     
    if( buttonsList[id].state == crntState) return; 
      
    err_code = app_timer_start( buttonsList[id].debounce_timerId, 
                                APP_TIMER_TICKS(buttonsList[id].debounce_timeMs), 
                                &buttonsList[id].pin_no );
    
    VERIFY_SUCCESS(err_code); 
}


static buttonErrorCode_t skd_but_initAll(){
  
  uint32_t err_code;
  
  NRF_LOG_DEBUG("Initializing all buttons");
   NRF_LOG_FLUSH();  

  if (!nrf_drv_gpiote_is_init())
  {
    err_code = nrf_drv_gpiote_init();
    VERIFY_SUCCESS(err_code);
  }
 
  for (int i = 0; i < buttonCount; i++){
       
       skd_button_cfg_t* p_btn = &buttonsList[i];
       
#if defined(BUTTON_HIGH_ACCURACY_ENABLED) && (BUTTON_HIGH_ACCURACY_ENABLED == 1)
        nrf_drv_gpiote_in_config_t config = GPIOTE_CONFIG_IN_SENSE_TOGGLE(p_btn->hi_accuracy);
#else
        nrf_drv_gpiote_in_config_t config = GPIOTE_CONFIG_IN_SENSE_TOGGLE(false);
#endif
        config.pull = p_btn->pull_cfg;
        
        err_code = nrf_drv_gpiote_in_init(p_btn->pin_no, &config, gpiote_event_handler);
        VERIFY_SUCCESS(err_code);  
        
        p_btn->state =  nrf_drv_gpiote_in_is_set( p_btn->pin_no );  
        
        err_code = app_timer_create( &buttonsTimers[i], 
                                     APP_TIMER_MODE_SINGLE_SHOT, 
                                     debounceTimeout_handler ); 
        
        p_btn->debounce_timerId = buttonsTimers[i]; 
        if(err_code != NRF_ERROR_INVALID_PARAM) APP_ERROR_CHECK(err_code); 

        NRF_LOG_INFO("Initializing the button with pinNumber %d with debuonce time %d ms",  
                                 p_btn->pin_no, 
                                 p_btn->debounce_timeMs );

        NRF_LOG_FLUSH();

  } 

  skd_but_enable();
}



static buttonErrorCode_t skd_but_deInitAll(){

  ret_code_t        ret;
 
  NRF_LOG_INFO("Deinitializing all buttons");  
  NRF_LOG_FLUSH();
  
  skd_but_disable();

  for( int i = 0; i < buttonCount; i++ ) app_timer_stop(buttonsList[i].debounce_timerId);
 
  nrf_drv_gpiote_uninit();
  
  return NO_PROBLEM;
}


buttonErrorCode_t skd_but_init( skd_button_cfg_t* pConfig ){
 
  buttonErrorCode_t ret;

  NRF_LOG_INFO("Initializing the button with pinNumber %d with debuonce time %d ms",  
                                 pConfig->pin_no, 
                                 pConfig->debounce_timeMs );
  
  NRF_LOG_FLUSH();

  skd_but_deInitAll();
  
  ret = skd_but_addButtonToList(pConfig);  
  
  skd_but_initAll(); 

  return NO_PROBLEM; 
 
}


buttonErrorCode_t skd_but_deInit(skd_button_cfg_t* pConfig){
  
  buttonErrorCode_t ret; 
  
  NRF_LOG_INFO("Deinitializing the button with pinNumber %d with debuonce time %d ms",  
                                 pConfig->pin_no, 
                                 pConfig->debounce_timeMs );
  NRF_LOG_FLUSH();

  ret = skd_but_deInitAll();

  ret = skd_but_deletButtonFromList(pConfig);
  
  ret = skd_but_initAll();
  if( ret ) return BUTTON_ALREADY_DEINITIALIZED; 

  return NO_PROBLEM; 

}

static buttonErrorCode_t skd_but_addButtonToList(skd_button_cfg_t* pConfig){
  uint8_t pin_no = pConfig->pin_no; 

  for( int i = 0; i < MAX_NUMBER_OF_BUTTONS; i++ )  
    if( pin_no == buttonsList[i].pin_no) return BUTTON_ALREADY_INITIALIZED;  

  for( int i = 0; i < MAX_NUMBER_OF_BUTTONS; i++) 
   if(buttonsList[i].pin_no ==  0 )  {
      
      memcpy(&buttonsList[i], pConfig, sizeof(skd_button_cfg_t));
      buttonCount++;
       
      break;

   }
      
  return NO_PROBLEM;

} 


static buttonErrorCode_t skd_but_deletButtonFromList(skd_button_cfg_t* pConfig){

  for(int i = 0; i < MAX_NUMBER_OF_BUTTONS; i++) 
    if(buttonsList[i].pin_no == pConfig->pin_no){
          
          memset(&buttonsList[i], NULL, sizeof(skd_button_cfg_t));
          buttonCount--;
           
          return NO_PROBLEM; 
    }
       
  return BUTTON_ALREADY_DEINITIALIZED;
} 


static uint8_t skd_but_findButtonId(uint32_t pin_no){
  
  for( int i = 0; i < MAX_NUMBER_OF_BUTTONS; i++ )  
    if( pin_no == buttonsList[i].pin_no) return i;  

}



buttonErrorCode_t skd_but_enable(void){


    uint32_t i;
    for (i = 0; i < buttonCount; i++)
    {
        nrf_drv_gpiote_in_event_enable(buttonsList[i].pin_no, true);
    }

    return NO_PROBLEM;
}


buttonErrorCode_t skd_but_disable(void){

    uint32_t i;
    for (i = 0; i < buttonCount; i++)
    {
       nrf_drv_gpiote_in_event_disable(buttonsList[i].pin_no);
       app_timer_stop(buttonsList[i].debounce_timerId); 
    }
    
    // Make sure polling timer is not running.
    return NO_PROBLEM;
}


  
/*

static skd_buttonConfig buttonsList[ MAX_NUMBER_OF_BUTTONS ] = {NULL};  

static app_button_cfg_t buttonConfigs [MAX_NUMBER_OF_BUTTONS] = {NULL}; 
static uint8_t          buttonCount = NULL;

static void buttonHandler (uint8_t pin_no, uint8_t button_action){

 for( int i = 0; i < MAX_NUMBER_OF_BUTTONS; i++ )  
    if( pin_no == buttonsList[i].pinNumber) {
      buttonsList[i].handlerISR(pin_no); 
      return; 
    } 
  
  NRF_LOG_ERROR("The unknown button was triggered."); 

}

static buttonErrorCode_t skd_but_initButtonsList(){
  uint32_t debounceMs = NULL; 
  
  NRF_LOG_INFO("Initializing button list");  
  NRF_LOG_FLUSH();

  ret_code_t        ret; 

  for(int i = 0; i < MAX_NUMBER_OF_BUTTONS; i++)
    if( buttonsList[i].pinNumber != NULL){
        
        buttonConfigs[buttonCount].pin_no         = buttonsList[i].pinNumber; 
        buttonConfigs[buttonCount].button_handler = buttonHandler;
        buttonConfigs[buttonCount].active_state   = false;
        buttonConfigs[buttonCount].pull_cfg       = NRF_GPIO_PIN_PULLUP; 
  
        buttonCount++;
    } 
  
  if(buttonCount == NULL) return NO_BUTTON_INITIALIZED; 
   
  debounceMs = skd_getMinimumDebounce(); 

  ret = app_button_init(buttonConfigs, buttonCount, APP_TIMER_TICKS( debounceMs) ); 
  APP_ERROR_CHECK(ret); 
   
  ret = app_button_enable();
  APP_ERROR_CHECK(ret);  
 
  return NO_PROBLEM;
  
} 


static uint32_t skd_getMinimumDebounce(){
  
  uint32_t minVal = 0xFFFFFFFF;  
  
  for(int i = 0; i < MAX_NUMBER_OF_BUTTONS; i++){
    
    if( buttonsList[i].pinNumber != 0  &&
        buttonsList[i].debounceTimeMs <  minVal){
        
        minVal = buttonsList[i].debounceTimeMs; 
    }
  }

}

*/