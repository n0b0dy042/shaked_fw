#ifndef SHA_KED_APP_H
#define SHA_KED_APP_H


#include <stdint.h>
#include <stdio.h> 

#include "sha_ked_common.h"

typedef enum{

   DFU_GRANTED  =  NULL,
   DFU_RJECTED

}skd_app_requestStatus_t ; 

#pragma pack(push, 1)

typedef struct {
  
  skd_settings_t   settings; 
  uint32_t         SystemBootCounter;
  skd_timestamp_t  SystemBootTime; 

} sha_ked_app_config_t;  

#pragma pack(pop)

typedef skd_app_requestStatus_t       (*skd_requestDFUCB_t)            (skd_timestamp_t*        pNextBootTime); 
typedef skd_app_requestStatus_t       (*skd_updateBLESettingsCB_t)     (skd_ble_settings_t*     pSettings); 
typedef skd_app_requestStatus_t       (*skd_updateGNSSSettingsCB_t)    (skd_gnss_settings_t*    pSettings); 
typedef skd_app_requestStatus_t       (*skd_updateMSSettingsCB_t)      (skd_ms_settings_t*      pSettings); 
typedef skd_app_requestStatus_t       (*skd_updateLEDSettingsCB_t)     (skd_led_settings_t*     pSettings); 
typedef skd_app_requestStatus_t       (*skd_updateButSettingsCB_t)     (skd_button_settings_t*  pSettings);
typedef skd_app_requestStatus_t       (*skd_updateDBSettingsCB_t)      (skd_db_settings_t*      pSettings);
typedef skd_app_requestStatus_t       (*skd_updateTimeSettingsCB_t)    (skd_timer_settings_t*   pSettings);

typedef struct{

  skd_requestDFUCB_t            requestDFUCb;
  skd_updateBLESettingsCB_t     updateBLESettingsCb;
  skd_updateGNSSSettingsCB_t    updateGNSSSettingsCb;
  skd_updateMSSettingsCB_t      updateMSSettingsCb;
  skd_updateLEDSettingsCB_t     updateLEDSettingsCb;
  skd_updateButSettingsCB_t     updateButSettingsCb; 
  skd_updateDBSettingsCB_t      updateDBSettingsCb; 
  skd_updateTimeSettingsCB_t    updateTimeSettingsCb; 

} sha_ked_app_api_t; 



void sha_ked_app_init     (sha_ked_app_config_t* pConfig, sha_ked_app_api_t* pApi);
void sha_ked_app_deInit   (); 
void sha_ked_app_process  (); 
void sha_ked_app_goDFU    ();

#endif