#ifndef SHA_KED_FDS_H
#define SHA_KED_FDS_H

#include "sha_ked_common.h" 
#include "fds.h" 

typedef enum{
  
  NO_DATA_WRITTEN = FDS_ERR_NOT_FOUND
  
}skd_fds_error_t;  

ret_code_t  skd_fds_init                 (void); 
ret_code_t  skd_fds_deInit               (void);
ret_code_t  skd_fds_readBleSettings      (skd_ble_settings_t*    pSettings);
ret_code_t  skd_fds_writeBleSettings     (skd_ble_settings_t*    pSettings);
ret_code_t  skd_fds_readGNSSSettings     (skd_gnss_settings_t*   pSettings);
ret_code_t  skd_fds_writeGNSSSettings    (skd_gnss_settings_t*   pSettings);
ret_code_t  skd_fds_readMSSettings       (skd_ms_settings_t*     pSettings);
ret_code_t  skd_fds_writeMSSettings      (skd_ms_settings_t*     pSettings);
ret_code_t  skd_fds_readTimerSettings    (skd_timer_settings_t*  pSettings);
ret_code_t  skd_fds_writeTimerSettings   (skd_timer_settings_t*  pSettings);
ret_code_t  skd_fds_readDBSettings       (skd_db_settings_t*     pSettings);
ret_code_t  skd_fds_writeDBSettings      (skd_db_settings_t*     pSettings);
ret_code_t  skd_fds_readLEDSettings      (skd_led_settings_t*    pSettings);
ret_code_t  skd_fds_writeLEDSettings     (skd_led_settings_t*    pSettings); 
ret_code_t  skd_fds_readButSettings      (skd_button_settings_t* pSettings);
ret_code_t  skd_fds_writeButSettings     (skd_button_settings_t* pSettings);
ret_code_t  skd_fds_readSKDSettings      (skd_settings_t*        pSettings);
ret_code_t  skd_fds_writeSKDSettings     (skd_settings_t*        pSettings);
ret_code_t  skd_fds_readBootCnt          (uint32_t*              bootCnt);
ret_code_t  skd_fds_writeBootCnt         (uint32_t*              bootCnt);
ret_code_t  skd_fds_readBootTime         (skd_timestamp_t*       pTimeStamp);  
ret_code_t  skd_fds_writeBootTime        (skd_timestamp_t*       pTimeStamp);  

#endif