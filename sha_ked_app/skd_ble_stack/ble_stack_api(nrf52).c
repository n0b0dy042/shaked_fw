#include "sha_ked_debug.h"
#include "ble_stack_api(nrf52).h"
#include "app_timer.h"


#if SHA_KED_BLE_API_LOG_ENABLED == 1 
    #define NRF_LOG_LEVEL               SHA_KED_BLE_API_LOG_LEVEL
    #define NRF_LOG_INFO_COLOR          SHA_KED_BLE_API_INFO_LOG_COLOR
    #define NRF_LOG_DEBUG_COLOR         SHA_KED_BLE_API_DEBUG_LOG_COLOR
#else 
    #define NRF_LOG_LEVEL       0
#endif 

#define NRF_LOG_MODULE_NAME     ble_stack_api
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
NRF_LOG_MODULE_REGISTER();






#define APP_BLE_OBSERVER_PRIO                           3                                           /**< Application's BLE observer priority. You shouldn't need to modify this value. */
#define APP_BLE_CONN_CFG_TAG                            1                                           /**< A tag identifying the SoftDevice BLE configuration. */


#define DEFAULT_FIRST_CONN_PARAMS_UPDATE_DELAY           APP_TIMER_TICKS(5000)                       /**< Time from initiating event (connect or start of notification) to first time sd_ble_gap_conn_param_update is called (5 seconds). */
#define DEFAULT_NEXT_CONN_PARAMS_UPDATE_DELAY            APP_TIMER_TICKS(30000)                      /**< Time between each call to sd_ble_gap_conn_param_update after the first call (30 seconds). */
#define DEFAULT_MAX_CONN_PARAMS_UPDATE_COUNT             3                                           /**< Number of attempts before giving up the connection parameter negotiation. */


BLE_ADVERTISING_DEF(m_advertising);    
NRF_BLE_GATT_DEF(m_gatt); 




nrf_sdh_ble_evt_handler_t ble_buff_evt_handler = NULL; 

/**@brief Function for dispatching a SoftDevice event to all modules with a SoftDevice
 *        event handler.
 *
 * @details This function is called from the SoftDevice event interrupt handler after a
 *          SoftDevice event has been received.
 *
 * @param[in] p_ble_evt  SoftDevice event.
 */
static void ble_evt_handler (ble_evt_t const * p_ble_evt, void * p_context){
  
  if(ble_buff_evt_handler) ble_buff_evt_handler(p_ble_evt, p_context);   

}
/**@brief Function for the SoftDevice initialization.
 *
 * @details This function initializes the SoftDevice and the BLE event interrupt.
 */
void ble_nrf52_stack_init(nrf_sdh_ble_evt_handler_t handler)
{
    ret_code_t err_code;

    NRF_LOG_INFO("Setting up BLE stack config");
    
    err_code = nrf_sdh_enable_request();  
    APP_ERROR_CHECK(err_code);
   
    // Configure the BLE stack using the default settings.
    // Fetch the start address of the application RAM.
    uint32_t ram_start = 0;
    err_code = nrf_sdh_ble_default_cfg_set(APP_BLE_CONN_CFG_TAG, &ram_start);
    APP_ERROR_CHECK(err_code);

    // Enable BLE stack.
    err_code = nrf_sdh_ble_enable(&ram_start);
    APP_ERROR_CHECK(err_code);

    // Register a handler for BLE events.
    ble_buff_evt_handler = handler;
    NRF_SDH_BLE_OBSERVER(m_ble_observer, APP_BLE_OBSERVER_PRIO, ble_evt_handler, NULL); 

}

/**@brief Function for the GAP initialization.
 *
 * @details This function will set up all the necessary GAP (Generic Access Profile) parameters of
 *          the device. It also sets the permissions and appearance.
 */
void ble_nrf52_gap_init(gap_config_t* pConfig)
{
    uint32_t                err_code;
    ble_gap_conn_params_t   gap_conn_params = {0};
    ble_gap_conn_sec_mode_t sec_mode;

    NRF_LOG_INFO("Setting up BLE stack GAP level");
    NRF_LOG_FLUSH();

    switch(pConfig->securityConfig){
      case OPEN_LINK:
 
          BLE_GAP_CONN_SEC_MODE_SET_OPEN(&sec_mode);
        
          break;

    }

    err_code = sd_ble_gap_device_name_set(&sec_mode,
                                          (const uint8_t *) pConfig->DeviceName,
                                          pConfig->DeviceNameLen);
    
    APP_ERROR_CHECK(err_code);

    gap_conn_params.min_conn_interval = MSEC_TO_UNITS(pConfig->minConIntervalMs, UNIT_1_25_MS);
    gap_conn_params.max_conn_interval = MSEC_TO_UNITS(pConfig->maxConIntervalMs, UNIT_1_25_MS);
    gap_conn_params.conn_sup_timeout  = MSEC_TO_UNITS(pConfig->conSupTimeoutMs,  UNIT_10_MS) ;
    gap_conn_params.slave_latency     = pConfig->slaveLatency;
    

    err_code = sd_ble_gap_ppcp_set(&gap_conn_params);
    APP_ERROR_CHECK(err_code);
}

/**@brief Function for initializing the GATT library. */
void ble_nrf52_gatt_init(gatt_config_t* pConfig)
{
    ret_code_t err_code;
    
    NRF_LOG_INFO("Setting up BLE GATT level");

    err_code = nrf_ble_gatt_init(&m_gatt, pConfig->gatt_evt_handler);
    APP_ERROR_CHECK(err_code);

    err_code = nrf_ble_gatt_att_mtu_periph_set(&m_gatt, NRF_SDH_BLE_GATT_MAX_MTU_SIZE);
    APP_ERROR_CHECK(err_code);
}



/**@brief Function for initializing the Advertising functionality.
 */
void ble_nrf52_adv_init(adv_config_t* pConfig)
{
    uint32_t               err_code;
    ble_advertising_init_t init;
    
    NRF_LOG_INFO("Setting up advertising confings");

    memset(&init, 0, sizeof(init));
    
    init.advdata.name_type          = BLE_ADVDATA_FULL_NAME;
    init.advdata.include_appearance = false;
    init.advdata.flags              = BLE_GAP_ADV_FLAGS_LE_ONLY_LIMITED_DISC_MODE;

    init.srdata.uuids_complete.uuid_cnt = sizeof(pConfig->m_adv_uuids) / sizeof(pConfig->m_adv_uuids);
    init.srdata.uuids_complete.p_uuids  = &pConfig->m_adv_uuids;

    init.config.ble_adv_fast_enabled  = true;
    init.config.ble_adv_fast_interval = MSEC_TO_UNITS(pConfig->advIntervalMs, UNIT_0_625_MS);
    init.config.ble_adv_fast_timeout  = MSEC_TO_UNITS(pConfig->advDurationMs, UNIT_10_MS);
    init.error_handler                = pConfig->errorHandler; 
    init.evt_handler                  = pConfig->evtHandler;

    err_code = ble_advertising_init(&m_advertising, &init);
    APP_ERROR_CHECK(err_code);

    ble_advertising_conn_cfg_tag_set(&m_advertising, APP_BLE_CONN_CFG_TAG);
    
}

/**@brief Function for initializing the Connection Parameters module.
 */
void ble_nrf52_con_prms_init(conn_config_t* pConfig)
{
    uint32_t               err_code;
    ble_conn_params_init_t cp_init;

    NRF_LOG_INFO("Setting up connection paramters");

    memset(&cp_init, 0, sizeof(cp_init));

    cp_init.p_conn_params                  = NULL;
    cp_init.first_conn_params_update_delay = DEFAULT_FIRST_CONN_PARAMS_UPDATE_DELAY;
    cp_init.next_conn_params_update_delay  = DEFAULT_NEXT_CONN_PARAMS_UPDATE_DELAY;
    cp_init.max_conn_params_update_count   = DEFAULT_MAX_CONN_PARAMS_UPDATE_COUNT;
    cp_init.start_on_notify_cccd_handle    = BLE_GATT_HANDLE_INVALID;
    cp_init.disconnect_on_fail             = false;
    cp_init.evt_handler                    = pConfig->evtHandler; 
    cp_init.error_handler                  = pConfig->errorHandler;
    

    err_code = ble_conn_params_init(&cp_init);
    
    if(err_code != NRF_SUCCESS) {
      NRF_LOG_DEBUG("error happend while initilazing nrf connect %d", err_code);
      NRF_LOG_FLUSH();
      //APP_ERROR_CHECK(err_code);
      
    }
}

/**@brief Function for starting advertising.
 */
void ble_nrf52_adv_start(void)
{
    uint32_t err_code = ble_advertising_start(&m_advertising, BLE_ADV_MODE_FAST);
    APP_ERROR_CHECK(err_code);
}

