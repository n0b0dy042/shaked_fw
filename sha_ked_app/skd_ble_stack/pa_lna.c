#include <stdint.h>
#include <string.h>

#include "pa_lna.h"
#include "ble.h"
#include "app_error.h"
#include "nrf_drv_gpiote.h"
#include "nrf_drv_ppi.h"

#include "nrf_drv_gpiote.h"  	        //doug added to support FEM
#include "nrf_drv_ppi.h" 		//doug added to support FEM

#include "sha_ked_debug.h"

#if SHA_KED_PA_LNA_LOG_ENABLED == 1 
    #define NRF_LOG_LEVEL               SHA_KED_PA_LNA_LOG_LEVEL
    #define NRF_LOG_INFO_COLOR          SHA_KED_PA_LNA_INFO_LOG_COLOR
    #define NRF_LOG_DEBUG_COLOR         SHA_KED_PA_LNA_DEBUG_LOG_COLOR
#else 
    #define NRF_LOG_LEVEL               0
#endif 

#define NRF_LOG_MODULE_NAME             sha_ked_pa_lna
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
NRF_LOG_MODULE_REGISTER();

//Define Sky66112 Interface
#define SKY_CTX			29
#define SKY_CRX			12
#define SKY_CHL			28
#define SKY_CPS			30
#define SKY_CSD			13
#define SKY_ANSEL               14//0 - ext ant, 1 - int ant



typedef enum
{

	PA_LNA_BYPASS_MODE = 0,
	PA_LNA_LOW_POWER,
	PA_LNA_HIGH_POWER

} pa_lna_mode_e;

typedef enum
{
	BT_INTERNAL_ANT = 0,
	BT_EXTERNAL_ANT 

} bt_ant_select_e;

static void pa_lna_ant_mode(bt_ant_select_e bt_ant_select); 
static void pa_lna_mode(pa_lna_mode_e sky_mode); 



static void pa_lna_ant_mode(bt_ant_select_e bt_ant_select)
{
	switch(bt_ant_select)
	{
		case BT_EXTERNAL_ANT:
			nrf_gpio_pin_clear(SKY_ANSEL);
                        NRF_LOG_INFO("EXTERNAL ANTENNA SELECTED");	
		break;
		
		case BT_INTERNAL_ANT:
			nrf_gpio_pin_set(SKY_ANSEL);
                        NRF_LOG_INFO("INTERNAL ANTENNA SELECTED");	
		break;
	}
}


void pa_lna_mode(pa_lna_mode_e sky_mode)
{
	switch(sky_mode)
	{
		case PA_LNA_BYPASS_MODE:
			//This options sets the Sky66112 into bypass mode
			nrf_gpio_pin_set(SKY_CHL);  	//Set CHL pin high, really a "dont care" in bypass mode
			nrf_gpio_pin_set(SKY_CPS);  	//Set CPS pin high
                        NRF_LOG_INFO("PA LNA bypass");	

		break;
		
		case PA_LNA_LOW_POWER:
			//This options sets the Sky66112 into low power mode
			nrf_gpio_pin_clear(SKY_CHL);  	//Set CHL pin high for now, really a "dont care" in bypass mode
			nrf_gpio_pin_set(SKY_CPS);  		//CPS pin control - clear to take out of bypass mode
                        NRF_LOG_INFO("PA LNA low power");         

		break;
		
		case PA_LNA_HIGH_POWER:
			//This options sets the Sky66112 into high power mode
			nrf_gpio_pin_set(SKY_CHL);  		//Set CHL pin high
			nrf_gpio_pin_clear(SKY_CPS); 		//Set CPS pin low
                        NRF_LOG_INFO("PA LNA high power");	


		break;
	}
}


void pa_lna_init(pa_lna_config_t* pConfig){
        
        ble_opt_t opt;
	uint32_t gpiote_ch = 1; 
	ret_code_t err_code;       

	
	NRF_LOG_INFO("PA LNA Init Function Start");	
        NRF_LOG_PROCESS();
	

	err_code = nrf_drv_gpiote_init();

	if(err_code != NRF_ERROR_INVALID_STATE)
	APP_ERROR_CHECK(err_code);

	err_code = nrf_drv_ppi_init();
	if(err_code != NRF_ERROR_MODULE_ALREADY_INITIALIZED)
	{
		APP_ERROR_CHECK(err_code);
	}
        
        NRF_LOG_DEBUG("Done with ppi init");	
        NRF_LOG_PROCESS();

	nrf_ppi_channel_t ppi_set_ch;
	nrf_ppi_channel_t ppi_clr_ch;

	err_code = nrf_drv_ppi_channel_alloc(&ppi_set_ch);
	APP_ERROR_CHECK(err_code);

        NRF_LOG_DEBUG("Done with ppi channel alloc");	
        NRF_LOG_PROCESS();

	err_code = nrf_drv_ppi_channel_alloc(&ppi_clr_ch);
	APP_ERROR_CHECK(err_code);

        NRF_LOG_DEBUG("Done with ppi channel alloc");	
        NRF_LOG_PROCESS();

	nrf_drv_gpiote_out_config_t config = GPIOTE_CONFIG_OUT_TASK_TOGGLE(false);


	// PA config
	if(gpiote_ch == NULL)
	{
		err_code = nrf_drv_gpiote_out_init(SKY_CTX, &config);
		APP_ERROR_CHECK(err_code);

		gpiote_ch = nrf_drv_gpiote_out_task_addr_get(SKY_CTX); 
	}

	opt.common_opt.pa_lna.pa_cfg.active_high = 1;   				// Set the pin to be active high
	opt.common_opt.pa_lna.pa_cfg.enable      = 1;   				// Enable toggling
	opt.common_opt.pa_lna.pa_cfg.gpio_pin    = SKY_CTX; 		// The GPIO pin to toggle tx  

        NRF_LOG_INFO("PA Control Pin Initialized");	
        NRF_LOG_PROCESS();
	

	// LNA config
	if(gpiote_ch == NULL)
	{
		err_code = nrf_drv_gpiote_out_init(SKY_CRX, &config);
		APP_ERROR_CHECK(err_code);        

		gpiote_ch = nrf_drv_gpiote_out_task_addr_get(SKY_CRX); 
	}

	opt.common_opt.pa_lna.lna_cfg.active_high  = 1; 				// Set the pin to be active high
	opt.common_opt.pa_lna.lna_cfg.enable       = 1; 			        // Enable toggling
	opt.common_opt.pa_lna.lna_cfg.gpio_pin     = SKY_CRX;  				// The GPIO pin to toggle rx

        NRF_LOG_INFO("LNA Control Pin Initialized");	
        NRF_LOG_PROCESS();
	

	// Common PA/LNA config
	opt.common_opt.pa_lna.gpiote_ch_id  = (gpiote_ch - NRF_GPIOTE_BASE) >> 2;   // GPIOTE channel used for radio pin toggling
	opt.common_opt.pa_lna.ppi_ch_id_clr = ppi_clr_ch;   												// PPI channel used for radio pin clearing
	opt.common_opt.pa_lna.ppi_ch_id_set = ppi_set_ch;   												// PPI channel used for radio pin setting

	err_code = sd_ble_opt_set(BLE_COMMON_OPT_PA_LNA, &opt);
	APP_ERROR_CHECK(err_code);
	


	NRF_LOG_INFO("PA LNA Init Function Complete");	
	NRF_LOG_INFO("ERR CODE: %d",err_code);	
        NRF_LOG_FLUSH();
        
        //Mode select pin init
	nrf_gpio_cfg_output(SKY_CPS); //this is the CPS pin to the SKY66112
	nrf_gpio_cfg_output(SKY_CHL); //this is the CHL pin to the SKY66112
	
        nrf_gpio_pin_clear(SKY_CHL);  	
	nrf_gpio_pin_clear(SKY_CPS);  	
		
        
        nrf_gpio_cfg_output(SKY_CSD);
	nrf_gpio_pin_set(SKY_CSD);

        pa_lna_mode(pConfig->GAIN); 
        pa_lna_ant_mode(pConfig->EXTANTEN); 


}  


void pa_lna_deinit(){
    
    nrf_gpio_pin_clear(SKY_CSD);
    nrf_gpio_pin_clear(SKY_CHL);  	
    nrf_gpio_pin_clear(SKY_CPS); 
    nrf_gpio_pin_clear(SKY_ANSEL);
    nrf_drv_ppi_uninit();
    
}


/* Alex's lagacy.

static bool status_pa_lna;

void pa_lna_init_legacy(void)
{
	ble_opt_t opt;
	uint32_t gpiote_ch = 1; 
	ret_code_t err_code;       

	status_pa_lna = false;	
	
	NRF_LOG_INFO("PA LNA Init Function Start");	
        NRF_LOG_PROCESS();
	

	memset(&opt, 0, sizeof(ble_opt_t));

	err_code = nrf_drv_gpiote_init();

	if(err_code != NRF_ERROR_INVALID_STATE)
	APP_ERROR_CHECK(err_code);

	err_code = nrf_drv_ppi_init();
	if(err_code != NRF_ERROR_MODULE_ALREADY_INITIALIZED)
	{
		APP_ERROR_CHECK(err_code);
	}
        
        NRF_LOG_DEBUG("Done with ppi init");	
        NRF_LOG_PROCESS();

	nrf_ppi_channel_t ppi_set_ch;
	nrf_ppi_channel_t ppi_clr_ch;

	err_code = nrf_drv_ppi_channel_alloc(&ppi_set_ch);
	APP_ERROR_CHECK(err_code);

        NRF_LOG_DEBUG("Done with ppi channel alloc");	
        NRF_LOG_PROCESS();

	err_code = nrf_drv_ppi_channel_alloc(&ppi_clr_ch);
	APP_ERROR_CHECK(err_code);

        NRF_LOG_DEBUG("Done with ppi channel alloc");	
        NRF_LOG_PROCESS();

	nrf_drv_gpiote_out_config_t config = GPIOTE_CONFIG_OUT_TASK_TOGGLE(false);


	// PA config
	if(gpiote_ch == NULL)
	{
		err_code = nrf_drv_gpiote_out_init(SKY_CTX, &config);
		APP_ERROR_CHECK(err_code);

		gpiote_ch = nrf_drv_gpiote_out_task_addr_get(SKY_CTX); 
	}

	opt.common_opt.pa_lna.pa_cfg.active_high = 1;   				// Set the pin to be active high
	opt.common_opt.pa_lna.pa_cfg.enable      = 1;   				// Enable toggling
	opt.common_opt.pa_lna.pa_cfg.gpio_pin    = SKY_CTX; 		// The GPIO pin to toggle tx  

        NRF_LOG_INFO("PA Control Pin Initialized");	
        NRF_LOG_PROCESS();
	

	// LNA config
	if(gpiote_ch == NULL)
	{
		err_code = nrf_drv_gpiote_out_init(SKY_CRX, &config);
		APP_ERROR_CHECK(err_code);        

		gpiote_ch = nrf_drv_gpiote_out_task_addr_get(SKY_CRX); 
	}

	opt.common_opt.pa_lna.lna_cfg.active_high  = 1; 							// Set the pin to be active high
	opt.common_opt.pa_lna.lna_cfg.enable       = 1; 							// Enable toggling
	opt.common_opt.pa_lna.lna_cfg.gpio_pin     = SKY_CRX;  				// The GPIO pin to toggle rx

        NRF_LOG_INFO("LNA Control Pin Initialized");	
        NRF_LOG_PROCESS();
	

	// Common PA/LNA config
	opt.common_opt.pa_lna.gpiote_ch_id  = (gpiote_ch - NRF_GPIOTE_BASE) >> 2;   // GPIOTE channel used for radio pin toggling
	opt.common_opt.pa_lna.ppi_ch_id_clr = ppi_clr_ch;   												// PPI channel used for radio pin clearing
	opt.common_opt.pa_lna.ppi_ch_id_set = ppi_set_ch;   												// PPI channel used for radio pin setting

	err_code = sd_ble_opt_set(BLE_COMMON_OPT_PA_LNA, &opt);
	APP_ERROR_CHECK(err_code);
	
	
	//Mode select pin init
	nrf_gpio_cfg_output(SKY_CPS); //this is the CPS pin to the SKY66112
	nrf_gpio_cfg_output(SKY_CHL); //this is the CHL pin to the SKY66112
	
	nrf_gpio_pin_clear(SKY_CHL);  	
	nrf_gpio_pin_clear(SKY_CPS);  	
	
	//En pin init
	nrf_gpio_cfg_output(SKY_CSD);
	nrf_gpio_pin_clear(SKY_CSD);
		
	//Ant select pin init
	nrf_gpio_cfg_output(SKY_ANSEL);
	nrf_gpio_pin_clear(SKY_ANSEL);


	NRF_LOG_INFO("PA LNA Init Function Complete");	
	NRF_LOG_INFO("ERR CODE: %d",err_code);	
        NRF_LOG_FLUSH();

        
}

bool check_pa_lna_status(void)
{
	return status_pa_lna;
}

void pa_lna_enable(pa_lna_mode_e sky_mode, bt_ant_select_e bt_ant_select)
{
	pa_lna_mode(sky_mode);
	pa_lna_ant_mode(bt_ant_select);
	

	
	status_pa_lna = true;
}

void pa_lna_disable(void)
{
	nrf_gpio_pin_clear(SKY_CSD);
	nrf_gpio_pin_clear(SKY_ANSEL);
	
	status_pa_lna = false;
}

*/