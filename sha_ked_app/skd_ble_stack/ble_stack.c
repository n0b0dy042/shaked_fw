#include "ble_stack.h"
#include "ble_stack_api(nrf52).h"

#include "sha_ked_debug.h"

#include "nrf_delay.h"

#include "ble_nus.h"
#include "ble_transfer_service.h"

#include "ble_skd_service.h"

#include "pa_lna.h"
#include "nrf_sdm.h"

#include "nrf_queue.h"

#define DEFAULT_MIN_CONN_INTERVAL_MS                    20       
#define DEFAULT_MAX_CONN_INTERVAL_MS                    70 
#define DEFUALT_CON_SUP_TIMEOUT_MS                      4000
#define DEFUALT_SLAVE_LATENCY                           0 
#define DEFUALT_SECURITY_CONFIG                         OPEN_LINK

#define DEFUAL_APP_ADV_INTERVAL_MS                      64                                  /**< The advertising interval (40 ms) for units of units of 0.625 ms. */
#define DEFUAL_APP_ADV_DURATION_MS                      60000                               /**< The advertising duration (60 seconds) for units of 10 milliseconds. */


#define NUS_SERVICE_UUID_TYPE           BLE_UUID_TYPE_VENDOR_BEGIN                  /**< UUID type for the Nordic UART Service (vendor specific). */


                       /**< BLE NUS service instance. */
BLE_SKD_DEF(m_skd, NRF_SDH_BLE_TOTAL_LINK_COUNT); 

BLE_ADVERTISING_DEF(m_advertising);                                                 /**< Advertising module instance. */




NRF_QUEUE_DEF(  ble_stack_status_evt_t, 
                m_ble_evt_queue, 
                5, 
                NRF_QUEUE_MODE_OVERFLOW );

NRF_QUEUE_INTERFACE_DEC(ble_stack_status_evt_t, 
                    ble_evt_queue);

NRF_QUEUE_INTERFACE_DEF( ble_stack_status_evt_t, 
                         ble_evt_queue, 
                         &m_ble_evt_queue);


#if SHA_KED_BLE_STACK_LOG_ENABLED == 1 
    #define NRF_LOG_LEVEL               SHA_KED_BLE_STACK_LOG_LEVEL
    #define NRF_LOG_INFO_COLOR          SHA_KED_BLE_STACK_INFO_LOG_COLOR
    #define NRF_LOG_DEBUG_COLOR         SHA_KED_BLE_STACK_DEBUG_LOG_COLOR
#else 
    #define NRF_LOG_LEVEL       0
#endif 


#define NRF_LOG_MODULE_NAME     ble_stack
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
NRF_LOG_MODULE_REGISTER();

#define CONNECTION_ESTABLISHED        1
#define NO_CONNECTION_ESTABLISHED     !CONNECTION_ESTABLISHED

#define POWER_ON          1
#define POWER_OFF         0

static uint16_t   m_conn_handle          = BLE_CONN_HANDLE_INVALID;                 /**< Handle of the current connection. */
static uint16_t   m_ble_nus_max_data_len = BLE_GATT_ATT_MTU_DEFAULT - 3;            /**< Maximum length of data (in bytes) that can be transmitted to the peer by the Nordic UART service module. */

static ble_stack_app_api_t ble_app_api = { NULL }; 


static bool connectionStatus = NO_CONNECTION_ESTABLISHED; 
static bool powerStatus      = POWER_OFF;

static void nus_data_handler              (ble_nus_evt_t*          p_evt); 
static void ble_evt_handler               (ble_evt_t const *       p_ble_evt,    void * p_context); 
static void gatt_evt_handler              (nrf_ble_gatt_t *        p_gatt,       nrf_ble_gatt_evt_t const * p_evt); 
static void on_adv_evt                    (ble_adv_evt_t           ble_adv_evt);
static void on_conn_params_evt            (ble_conn_params_evt_t * p_evt);
static void conn_params_error_handler     (uint32_t nrf_error);   



static void ble_stack_updateStatus(ble_stack_status_evt_t* evt){ 

  if(ble_app_api.statusUpdated_handler)  ble_app_api.statusUpdated_handler(evt);

}

static void ble_stack_dataReceived(uint8_t* p_data, uint8_t dataLen){ 

  if(ble_app_api.dataReceived_handler) ble_app_api.dataReceived_handler(p_data, dataLen);

}



/**@brief Function for handling the data from the Nordic UART Service.
 *
 * @details This function will process the data received from the Nordic UART BLE Service and send
 *          it to the UART module.
 *
 * @param[in] p_evt       Nordic UART Service event.
 */
/**@snippet [Handling the data received over BLE] */
static void skd_data_handler(ble_skd_evt_t * p_evt)
{
 
   NRF_LOG_INFO("skd_data_handler: Handling skd service event  %d", p_evt->type ); 

   switch(p_evt->type){
   
    case BLE_SKD_EVT_RX_DATA:
    { 
        NRF_LOG_INFO("skd_data_handler: %d bytes received. ",  p_evt->params.rx_data.length);
        NRF_LOG_HEXDUMP_DEBUG(p_evt->params.rx_data.p_data, p_evt->params.rx_data.length);
        
        //Send data to application 
        ble_stack_dataReceived(p_evt->params.rx_data.p_data,   p_evt->params.rx_data.length); 

    }
    break;  
    case BLE_SKD_EVT_TX_RDY:
    {
        ble_stack_status_evt_t status_evt = {NULL}; 

        NRF_LOG_INFO("skd_data_handler: Data transmission complited.");
        
        //Send new status to application
        status_evt.type = DATA_TRANSFER_COMPLETE; 
        ble_stack_updateStatus(&status_evt); 

    }    
    break; 
    case BLE_SKD_EVT_COMM_STARTED:
    {

       ble_stack_status_evt_t status_evt = {NULL}; 
       NRF_LOG_INFO("skd_data_handler:  Data transmission enabled."); 
        
       //Send new status to application
       status_evt.type = DATA_TRANSFER_ENABLE; 
       ble_stack_updateStatus(&status_evt); 
    }
    break; 
    case BLE_SKD_EVT_COMM_STOPPED: 
    {
       ble_stack_status_evt_t status_evt = {NULL}; 
       NRF_LOG_INFO("skd_data_handler: Data transmission  disabled.");
      
       //Send new status to application
       status_evt.type = NOTIFICATION_DISABLE; 
       ble_stack_updateStatus(&status_evt); 
    }
    break; 
   } 
   
   NRF_LOG_FLUSH();
}


/**@brief Function for handling the data from the TS Service.
 *
 * @details This function will process the data received from the TS BLE Service.
 *
 * @param[in] p_ts    TS Service structure.
 * @param[in] p_data   Data received.
 * @param[in] length   Length of the data.
 */
/**@snippet [Handling the data received over BLE] */
static void ts_data_handler(ble_ts_t * p_ts, uint8_t const * p_data, uint16_t length)
{
	
        NRF_LOG_DEBUG("Incoming data from the host");  
        //if(bt_transfer_service_rx_l){bt_transfer_service_rx_l(p_data, length);}
}

/**@brief Function for initializing services that will be used by the application.
 */
void services_init(service_config_t* config)
{
    uint32_t           err_code;
    ble_nus_init_t     nus_init = {0};
    ble_ts_init_t      ts_init;
    ble_skd_init_t     skd_init = {NULL};  
    nrf_ble_qwr_init_t qwr_init = {0};
    
    NRF_LOG_INFO("Setting up GATT servicies");
        
    //err_code = nrf_ble_qwr_init(&m_qwr, &qwr_init);
    //APP_ERROR_CHECK(err_code);

    // Initialize SKD service.
    memset(&skd_init, 0, sizeof(skd_init));

    skd_init.data_handler = skd_data_handler;
    err_code = ble_skd_init(&m_skd, &skd_init);
    APP_ERROR_CHECK(err_code);


}

ble_service_error_code  ble_stack_init( ble_stack_config_t* pConfig,  ble_stack_app_api_t*        pApi){
   
   gap_config_t         gap_config     = {NULL}; 
   gatt_config_t        gatt_config    = {NULL};    
   service_config_t     service_config = {NULL};
   adv_config_t         adv_config     = {NULL};   
   conn_config_t        conn_config    = {NULL};
   ble_uuid_t           m_adv_uuids    = {BLE_UUID_NUS_SERVICE, NUS_SERVICE_UUID_TYPE};
   skd_ble_settings_t*  pSettings      = &pConfig;  
   
   if(ble_stack_isOn()){
    
    NRF_LOG_WARNING("BLE stack is already online. Do deInit first!!");
    
    return BS_BUSY;
     
   }

   //Setting application call back
   ble_app_api.statusUpdated_handler  = pApi->statusUpdated_handler; 
   ble_app_api.dataReceived_handler   = pApi->dataReceived_handler;  
    
   // Init basic ble stack. 
   ble_nrf52_stack_init(ble_evt_handler);
   
   //Fill the gap structure to config BLE GAP level
   //NRF_LOG_DEBUG("deviceName: %s", pConfig->deviceName);  
   //NRF_LOG_DEBUG("deviceNameLen: %d", pConfig->deviceNameLen); 
   NRF_LOG_PROCESS(); 

   gap_config.DeviceName        = "GPS TRACKER_DEBUG"; //pConfig->deviceName;
   gap_config.DeviceNameLen     = 17;                  //pConfig->deviceNameLen;
   gap_config.maxConIntervalMs  = DEFAULT_MAX_CONN_INTERVAL_MS;
   gap_config.minConIntervalMs  = DEFAULT_MIN_CONN_INTERVAL_MS;
   gap_config.slaveLatency      = DEFUALT_SLAVE_LATENCY; 
   gap_config.conSupTimeoutMs   = DEFUALT_CON_SUP_TIMEOUT_MS;
   gap_config.securityConfig    = DEFUALT_SECURITY_CONFIG; 
  
   ble_nrf52_gap_init(&gap_config);

   //Fill the gatt structure to config BLE GATT level 
   gatt_config.att_mtu          = NULL;
   gatt_config.gatt_evt_handler = gatt_evt_handler;

   ble_nrf52_gatt_init(&gatt_config);


   //Fill the service structure  to config service seen by external BLE device
   service_config.futureField = NULL;
   
   services_init(&service_config);


   /* Fill the advertising structure  to specify the way device send advertasing
      packets around. */

   adv_config.advDurationMs  = DEFUAL_APP_ADV_DURATION_MS;
   adv_config.advIntervalMs  = DEFUAL_APP_ADV_INTERVAL_MS; 
   adv_config.errorHandler   = NULL;
   adv_config.evtHandler     = on_adv_evt;
   adv_config.m_adv_uuids    = m_adv_uuids;

   ble_nrf52_adv_init(&adv_config);


   //Fill the connection structure to specify the way device connection with host
   conn_config.futureField  = NULL;
   conn_config.evtHandler   = on_conn_params_evt; 
   conn_config.errorHandler = conn_params_error_handler;
     
   ble_nrf52_con_prms_init(&conn_config);
  
   //Fill config for pa_lna

   pa_lna_config_t pa_lna_config = {NULL};
   pa_lna_config.GAIN     = pSettings->gain; 
   pa_lna_config.EXTANTEN = pSettings->mode.EXANTEN;  
   pa_lna_init(&pa_lna_config);

   // Lanching BLE stack. 
   ble_nrf52_adv_start();
   
   powerStatus = POWER_ON; 

   return BS_NO_ERROR; 
}



static void ble_evt_handler(ble_evt_t const * p_ble_evt, void * p_context){

   
   uint32_t err_code;

   NRF_LOG_INFO("ble_evt_handler: Handling the ble event %d", p_ble_evt->header.evt_id);
   NRF_LOG_FLUSH();  
   switch (p_ble_evt->header.evt_id)
    {
        case BLE_GAP_EVT_CONNECTED:{

            NRF_LOG_INFO("Connection with an external BLE device established");

            m_conn_handle = p_ble_evt->evt.gap_evt.conn_handle;

            ble_gap_conn_params_t* conn_params = &p_ble_evt->evt.gap_evt.params.connected.conn_params
            NRF_LOG_INFO("Connection parameter");
            NRF_LOG_INFO("min_conn_interval: %d ms", conn_params->min_conn_interval*1.25); 
            NRF_LOG_INFO("max_conn_interval: %d ms", conn_params->max_conn_interval*1.25); 
            NRF_LOG_INFO("slave_latency:     %d ms", conn_params->slave_latency*1.25); 
            NRF_LOG_INFO("conn_sup_timeout:  %d ms", conn_params->conn_sup_timeout*10); 

            //APP_ERROR_CHECK(err_code);
            //err_code = nrf_ble_qwr_conn_handle_assign(&m_qwr, m_conn_handle); 
            //APP_ERROR_CHECK(err_code);
 
            connectionStatus =  CONNECTION_ESTABLISHED;
            ble_stack_status_evt_t status_evt = { .type = DEVICE_CONNECTED }; 
            ble_evt_queue_push( &status_evt); 

            }
            break;

        case BLE_GAP_EVT_DISCONNECTED:{

            NRF_LOG_INFO("Disconnected with an external BLE device lost.");
            m_conn_handle    = BLE_CONN_HANDLE_INVALID;
            connectionStatus = NO_CONNECTION_ESTABLISHED;
            
            ble_stack_status_evt_t status_evt = {.type = DEVICE_DISCONNECTED}; 
            ble_evt_queue_push(&status_evt); 

            }
            break;

        case BLE_GAP_EVT_PHY_UPDATE_REQUEST:
        {
            NRF_LOG_DEBUG("PHY update request.");
            ble_gap_phys_t const phys =
            {
                .rx_phys = BLE_GAP_PHY_AUTO,
                .tx_phys = BLE_GAP_PHY_AUTO,
            };
            err_code = sd_ble_gap_phy_update(p_ble_evt->evt.gap_evt.conn_handle, &phys);
            APP_ERROR_CHECK(err_code);
        } break;

        case BLE_GAP_EVT_SEC_PARAMS_REQUEST:
            // Pairing not supported
            NRF_LOG_DEBUG("Proccesing pairing request event.") 
            err_code = sd_ble_gap_sec_params_reply(m_conn_handle, BLE_GAP_SEC_STATUS_PAIRING_NOT_SUPP, NULL, NULL);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GATTS_EVT_SYS_ATTR_MISSING:
            // No system attributes have been stored.
            NRF_LOG_DEBUG("Proccesing system attribute access pending event.") 
            err_code = sd_ble_gatts_sys_attr_set(m_conn_handle, NULL, 0, 0);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GATTC_EVT_TIMEOUT:
            // Disconnect on GATT Client timeout event.
            NRF_LOG_DEBUG("Proccesing disconnect on GATT Client timeout event.") 
            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gattc_evt.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GATTS_EVT_TIMEOUT:
            // Disconnect on GATT Server timeout event.
            NRF_LOG_DEBUG("Processing disconnect on GATT Server timeout event. ");  
            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gatts_evt.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            APP_ERROR_CHECK(err_code);
            break;
        case BLE_GAP_EVT_CONN_PARAM_UPDATE_REQUEST: 
        {
            NRF_LOG_INFO("Connection parameters updated.") 
            ble_gap_conn_params_t* conn_params = &p_ble_evt->evt.gap_evt.params.conn_param_update.conn_params
            NRF_LOG_INFO("Connection parameter");
            NRF_LOG_INFO("min_conn_interval: %d ms", conn_params->min_conn_interval*1.25); 
            NRF_LOG_INFO("max_conn_interval: %d ms", conn_params->max_conn_interval*1.25); 
            NRF_LOG_INFO("slave_latency:     %d ms", conn_params->slave_latency*1.25); 
            NRF_LOG_INFO("conn_sup_timeout:  %d ms", conn_params->conn_sup_timeout*10); 

        } break; 
        default:
            // No implementation needed.
            NRF_LOG_INFO("Unprocessing event");               
            break;
    }
  
  NRF_LOG_FLUSH(); 

} 

static void gatt_evt_handler(nrf_ble_gatt_t * p_gatt, nrf_ble_gatt_evt_t const * p_evt){
    
    NRF_LOG_INFO("gatt_evt_handler: Handling gatt event %d", p_evt->evt_id );
    NRF_LOG_FLUSH();
      
    if ((m_conn_handle == p_evt->conn_handle) && (p_evt->evt_id == NRF_BLE_GATT_EVT_ATT_MTU_UPDATED))
        {
            m_ble_nus_max_data_len = p_evt->params.att_mtu_effective - OPCODE_LENGTH - HANDLE_LENGTH;
            NRF_LOG_DEBUG("Data len is set to 0x%X(%d)", m_ble_nus_max_data_len, m_ble_nus_max_data_len);
        }

    NRF_LOG_INFO("ATT MTU exchange completed. central 0x%x peripheral 0x%x",
                  p_gatt->att_mtu_desired_central,
                  p_gatt->att_mtu_desired_periph);
                  
    NRF_LOG_FLUSH();

} 

/**@brief Function for handling the data from the Nordic UART Service.
 *
 * @details This function will process the data received from the Nordic UART BLE Service and send
 *          it to the UART module.
 *
 * @param[in] p_evt       Nordic UART Service event.
 */
/**@snippet [Handling the data received over BLE] */
static void nus_data_handler(ble_nus_evt_t * p_evt)
{
   
   NRF_LOG_INFO("nus_data_handler: Handling incoming data for Nordic USART %d", p_evt->type ); 
  
   NRF_LOG_FLUSH();
}


/**@brief Function for handling advertising events.
 *
 * @details This function will be called for advertising events which are passed to the application.
 *
 * @param[in] ble_adv_evt  Advertising event.
 */
static void on_adv_evt(ble_adv_evt_t ble_adv_evt)
{
    uint32_t err_code;
    
    NRF_LOG_INFO("on_adv_evt: Handeling advertising events %d ", ble_adv_evt);
    NRF_LOG_FLUSH();   
    switch (ble_adv_evt)
    {
        case BLE_ADV_EVT_FAST:
            NRF_LOG_DEBUG("Fast advertising mode has started.");  
            break;
        case BLE_ADV_EVT_IDLE:
              NRF_LOG_DEBUG("IDLE: no connectable advertising is ongoing");
              
              //infinity advertising
              ble_nrf52_adv_start();
            break;
        default:
              NRF_LOG_INFO("Unprocessing event ");  
            break;
    }

    NRF_LOG_FLUSH();
} 

/**@brief Function for handling an event from the Connection Parameters Module.
 *
 * @details This function will be called for all events in the Connection Parameters Module
 *          which are passed to the application.
 *
 * @note All this function does is to disconnect. This could have been done by simply setting
 *       the disconnect_on_fail config parameter, but instead we use the event handler
 *       mechanism to demonstrate its use.
 *
 * @param[in] p_evt  Event received from the Connection Parameters Module.
 */
static void on_conn_params_evt(ble_conn_params_evt_t * p_evt)
{
    uint32_t err_code;
    
    NRF_LOG_INFO("on_conn_params_evt :Handeling connection parameters events %d", p_evt->evt_type);
     
    if (p_evt->evt_type == BLE_CONN_PARAMS_EVT_FAILED )
    {  
        NRF_LOG_ERROR("CONNECTION FAILD. Negotiation procedure failed.");  
        err_code = sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_CONN_INTERVAL_UNACCEPTABLE);
        APP_ERROR_CHECK(err_code);
    }

    if (p_evt->evt_type == BLE_CONN_PARAMS_EVT_SUCCEEDED )
    {  
        NRF_LOG_DEBUG("CONNECTION SUCCESS. Negotiation procedure succeeded.");  

    }
  
    NRF_LOG_FLUSH();
}


/**@brief Function for handling errors from the Connection Parameters module.
 *
 * @param[in] nrf_error  Error code containing information about what went wrong.
 */
static void conn_params_error_handler(uint32_t nrf_error)
{   
    
    NRF_LOG_INFO("conn_params_error_handler: Handling connection error event %d", nrf_error);
    NRF_LOG_INFO("Unprocessing event ");  
    NRF_LOG_FLUSH(); 
    APP_ERROR_HANDLER(nrf_error);

}


void  ble_stack_sendInfo(uint8_t* pInfo,  uint16_t infoLen){
    
    uint32_t ret = NULL; 
    uint16_t dataToSendLen ;  
    
    do{ 


      if(infoLen > BLE_SKD_MAX_DATA_LEN)  dataToSendLen = BLE_SKD_MAX_DATA_LEN; 
      else                                dataToSendLen = infoLen; 

        
     ret = ble_skd_send_info(&m_skd, pInfo, &dataToSendLen, m_conn_handle);
    
     if(ret == NRF_SUCCESS) {

        infoLen -=dataToSendLen;
        continue; 

      }
    
      if( ret == NRF_ERROR_BUSY) { 

          nrf_delay_ms(5); 
          continue; 

      } 

    
    }while(infoLen > 0);  



}


ble_service_error_code  ble_stack_sendData(uint8_t* pData, uint16_t dataLen){
   
   uint32_t ret = NULL; 

   if(dataLen > BLE_SKD_MAX_DATA_LEN) return BS_INVALID_PARAMETRES; 
   
   ret = ble_skd_send_data(&m_skd, pData, &dataLen, m_conn_handle);

   if(ret == NRF_ERROR_BUSY)      return BS_BUSY; 
   if(ret == NRF_ERROR_RESOURCES) return BS_BUSY;
     
   return ret; 
   
}  


ble_service_error_code  ble_read_MACaddr(uint8_t* addr){
  
  ble_gap_addr_t mac_addr = {NULL}; 
  
  sd_ble_gap_addr_get(&mac_addr);
  

  //invert copy
  for(uint8_t i; i < BLE_GAP_ADDR_LEN; i++ ){

    addr[i] = mac_addr.addr[ BLE_GAP_ADDR_LEN - 1 - i];  
  
  } 
  //memcpy(addr, mac_addr.addr, BLE_GAP_ADDR_LEN); 
  
  return BS_NO_ERROR; 

}; 

ble_service_error_code   ble_stack_deInit(){
    
   ret_code_t err_code;
    
   if(!ble_stack_isOn()){
    
      NRF_LOG_WARNING("BLE stack is already offline. Do init first!!");
    
      return BS_BUSY;
     
   }

    err_code = nrf_sdh_disable_request();
    APP_ERROR_CHECK(err_code);
     
    while (nrf_sdh_is_enabled());  //todo - wrap with timer

    pa_lna_deinit();
     
    memset(&ble_app_api, NULL, sizeof(ble_stack_app_api_t));
            
    connectionStatus =  NO_CONNECTION_ESTABLISHED;
    
    powerStatus = POWER_OFF; 

    return BS_NO_ERROR;
}

bool ble_stack_isConnected(){

  return connectionStatus; 

} 

bool ble_stack_isOn(){
  
  return powerStatus; 

}

void ble_stack_process(){

     ble_stack_status_evt_t new_evt = {NULL}; 
     
    //check if new command is  avalible
    if(ble_evt_queue_is_empty()) return;

    //processing new commnad
    ret_code_t err_code = ble_evt_queue_pop(&new_evt);
    APP_ERROR_CHECK(err_code);

    if(ble_app_api.statusUpdated_handler) ble_app_api.statusUpdated_handler(&new_evt); 

} 