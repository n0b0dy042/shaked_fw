#ifndef BLE_STACK_API_NRF52_H__
#define BLE_STACK_API_NRF52_H__ 

#include "ble_hci.h"
#include "ble_advdata.h"
#include "ble_advertising.h"
#include "ble_conn_params.h"

#include "nrf_sdh.h"
#include "nrf_sdh_soc.h"
#include "nrf_sdh_ble.h"
#include "nrf_ble_gatt.h"
#include "nrf_ble_qwr.h"


typedef enum{

  OPEN_LINK = 0,
  
}gap_sec_config_e;  


typedef struct{
  
  uint16_t            minConIntervalMs;
  uint16_t            maxConIntervalMs;
  uint16_t            conSupTimeoutMs;
  uint16_t            slaveLatency;
  uint8_t             DeviceNameLen;
  char*               DeviceName;
  gap_sec_config_e    securityConfig;
     
} gap_config_t;  


typedef struct{

  uint16_t                      att_mtu;
  nrf_ble_gatt_evt_handler_t    gatt_evt_handler; 

}gatt_config_t;  

typedef struct{

  uint8_t futureField;
    
}service_config_t; 

typedef struct{
  
  uint8_t futureField;
  ble_conn_params_evt_handler_t evtHandler;                      //!< Event handler to be called for handling events in the Connection Parameters.
  ble_srv_error_handler_t       errorHandler;                    //!< Function to be called in case of an error. 
   
} conn_config_t; 


typedef struct{

  uint16_t                  advIntervalMs;
  uint16_t                  advDurationMs;
  ble_adv_evt_handler_t     evtHandler; 
  ble_adv_error_handler_t   errorHandler;     
  ble_uuid_t                m_adv_uuids;  

}adv_config_t; 
  


void ble_nrf52_stack_init        (nrf_sdh_ble_evt_handler_t   handler);
void ble_nrf52_gap_init          (gap_config_t*               pConfig);
void ble_nrf52_gatt_init         (gatt_config_t*              pConfig);
void ble_nrf52_adv_init          (adv_config_t*               pConfig);
void ble_nrf52_con_prms_init     (conn_config_t*              pConfig); 
void ble_nrf52_adv_start         (void); 


#endif