#ifndef BLE_SERVICE_H__
#define BLE_SERVICE_H__ 



#include <stdint.h>
#include <stdbool.h>
#include "sha_ked_common.h"

#define MAX_DATA_UINT_LEN             (NRF_SDH_BLE_GATT_MAX_MTU_SIZE - 4)    
#define BLE_MAC_ADDR_LEN              6
#define MAX_DEVICE_NAME_LEN           29

typedef enum {
  
  BS_NO_ERROR = 0, 
  BS_WRONG_CONFIG_VALUE,
  BS_NO_DEVICE_CONNECTED,
  BS_NO_NOTIFCATION_ENABLED,
  BS_BUSY,
  BS_INVALID_PARAMETRES,
  
} ble_service_error_code; 

typedef enum  {

  HW_ALWAYS_ON = (uint8_t)0, 
  HW_ALLWAY_OFF,
  HW_ONLINE_ONCE, 
  HW_WORK_IN_TIME_INTERVAL, 
  HW_WORK_BY_SCHEDUEL

} ble_service_mode_e;  

#pragma pack(push, 1)


typedef struct {
    
    uint8_t msgType; 

} ble_stack_msg_t;  

/*
typedef struct{
    
    uint8_t                deviceNameLen;                    // Length of device name in bytes.  
    char                   deviceName[ MAX_DEVICE_NAME_LEN]; // Device name seen by other users over BLE.  
    skd_ble_settings_t     settings; 

} ble_stack_config_t;  
*/

typedef skd_ble_settings_t ble_stack_config_t; 

#pragma pack(pop)

typedef enum {

  DEVICE_CONNECTED       =   0,
  DEVICE_DISCONNECTED,
  HW_ONLINE,
  HW_OFFLINE, 
  NEW_DATA_ARRIVED,
  DATA_TRANSFER_COMPLETE, 
  DATA_TRANSFER_ENABLE,
  NOTIFICATION_DISABLE

}ble_service_evt_type;  

typedef struct{

  ble_service_evt_type type;

} ble_service_evt_t;  


typedef  struct{

  uint8_t type; 

} ble_stack_status_evt_t;  

typedef void (*ble_service_evt_handler) (ble_service_evt_t evt);   

typedef void (*ble_statuUpdateCB_t) (ble_stack_status_evt_t* pStatus);
typedef void (*ble_dataReceivedCb_t)  (uint8_t* p_data, uint8_t dataLen);      

typedef  struct {

  ble_statuUpdateCB_t   statusUpdated_handler; 
  ble_dataReceivedCb_t  dataReceived_handler; 

} ble_stack_app_api_t;  

ble_service_error_code  ble_stack_init(      ble_stack_config_t*      pConfig, 
                                             ble_stack_app_api_t*     pApi);

ble_service_error_code  ble_stack_deInit();                                                                                
void                    ble_stack_sendInfo( uint8_t* pInfo,  uint16_t infoLen); 


ble_service_error_code  ble_stack_sendData    (  uint8_t*                  pData, 
                                                 uint16_t                  dataLen); 


ble_service_error_code ble_read_MACaddr(uint8_t* addr); 

void ble_stack_process();
bool ble_stack_isConnected(); 
bool ble_stack_isOn();
  
#endif