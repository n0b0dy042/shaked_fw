#ifndef __PA_LNA_H
#define __PA_LNA_H



typedef struct{
  
  uint8_t EXTANTEN:1;   //Enable an external antenna 
  uint8_t GAIN:2;       //Gain
   
}pa_lna_config_t;  


void pa_lna_init(pa_lna_config_t* pConfig); 
void pa_lna_deinit(); 




#endif	//__PA_LNA_H
