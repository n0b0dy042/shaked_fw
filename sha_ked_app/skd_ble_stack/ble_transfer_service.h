#include "sdk_config.h"
#include "ble.h"
#include "ble_srv_common.h"
#include "nrf_sdh_ble.h"
#include "ble_link_ctx_manager.h"
#include "app_util_platform.h"


/**@brief   Macro for defining a ble_ts instance.
 *
 * @param     _name            Name of the instance.
 * @param[in] _nus_max_clients Maximum number of NUS clients connected at a time.
 * @hideinitializer
 */
#define BLE_TS_DEF(_name, _ts_max_clients)                      \
    BLE_LINK_CTX_MANAGER_DEF(CONCAT_2(_name, _link_ctx_storage),  \
                             (_ts_max_clients),                  \
                             sizeof(ble_ts_client_context_t));   \
    static ble_ts_t _name = {0, 0, {0,0,0,0},{0,0,0,0},{0,0,0,0}, \
                              0, 0, 0,                            \
                              &CONCAT_2(_name, _link_ctx_storage)}; \
    NRF_SDH_BLE_OBSERVER(_name ## _obs,                           \
                         BLE_NUS_BLE_OBSERVER_PRIO,               \
                         ble_ts_on_ble_evt,                      \
                         &_name)
    
#define BLE_UUID_TS_SERVICE 0x0001                      /**< The UUID of the Nordic UART Service. */

#define OPCODE_LENGTH 1
#define HANDLE_LENGTH 2

#if defined(NRF_SDH_BLE_GATT_MAX_MTU_SIZE) && (NRF_SDH_BLE_GATT_MAX_MTU_SIZE != 0)
    #define BLE_TS_MAX_DATA_LEN (NRF_SDH_BLE_GATT_MAX_MTU_SIZE - OPCODE_LENGTH - HANDLE_LENGTH) /**< Maximum length of data (in bytes) that can be transmitted to the peer by the Nordic UART service module. */
#else
    #define BLE_TS_MAX_DATA_LEN (NRF_SDH_BLE_GATT_MAX_MTU_SIZE_DEFAULT - OPCODE_LENGTH - HANDLE_LENGTH) /**< Maximum length of data (in bytes) that can be transmitted to the peer by the Nordic UART service module. */
    #warning NRF_BLE_GATT_MAX_MTU_SIZE is not defined.
#endif


/**@brief   Nordic UART Service event types. */
typedef enum
{
    BLE_TS_EVT_RX_DATA,      /**< Data received. */
    BLE_TS_EVT_TX_RDY,       /**< Service is ready to accept new data to be transmitted. */
    BLE_TS_EVT_COMM_STARTED, /**< Notification has been enabled. */
    BLE_TS_EVT_COMM_STOPPED, /**< Notification has been disabled. */

} ble_ts_evt_type_t;


/* Forward declaration of the ble_ts_t type. */
typedef struct ble_ts_s ble_ts_t;

/**@brief Nordic UART Service event handler type. */
//typedef void (*ble_ts_data_handler_t)(ble_ts_evt_type_t * p_evt);
typedef void (*ble_ts_data_handler_t) (ble_ts_t * p_ts, uint8_t const * p_data, uint16_t length);


/**@brief Nordic UART Service client context structure.
 *
 * @details This structure contains state context related to hosts.
 */
typedef struct
{
    bool is_notification_enabled; /**< Variable to indicate if the peer has enabled notification of the RX characteristic.*/
} ble_ts_client_context_t;

/**@brief Nordic UART Service initialization structure.
 *
 * @details This structure contains the initialization information for the service. The application
 * must fill this structure and pass it to the service using the @ref ble_ts_init
 *          function.
 */
typedef struct
{
    ble_ts_data_handler_t data_handler; /**< Event handler to be called for handling received data. */
} ble_ts_init_t;

/**@brief Nordic UART Service structure.
 *
 * @details This structure contains status information related to the service.
 */
struct ble_ts_s
{
    uint8_t                  uuid_type;               /**< UUID type for Nordic UART Service Base UUID. */
    uint16_t                 service_handle;          /**< Handle of Nordic UART Service (as provided by the SoftDevice). */
    ble_gatts_char_handles_t tx_handles;              /**< Handles related to the TX characteristic (as provided by the SoftDevice). */
    ble_gatts_char_handles_t rx_handles;              /**< Handles related to the RX characteristic (as provided by the SoftDevice). */
    ble_gatts_char_handles_t info_handles;      
    uint16_t                 conn_handle;             /**< Handle of the current connection (as provided by the SoftDevice). BLE_CONN_HANDLE_INVALID if not in a connection. */
    bool                     is_notification_enabled; /**< Variable to indicate if the peer has enabled notification of the RX characteristic.*/
    bool                     is_info_char_notification_enabled;
    blcm_link_ctx_storage_t * const p_link_ctx_storage;
    ble_ts_data_handler_t   data_handler;            /**< Event handler to be called for handling received data. */
};


typedef PACKED_STRUCT
{
    uint16_t mtu;
    uint16_t con_interval;
    uint8_t  tx_phy;
    uint8_t  rx_phy;
}ble_ts_ble_params_info_t;

/**@brief Function for initializing the Nordic UART Service.
 *
 * @param[out] p_ts      Nordic UART Service structure. This structure must be supplied
 *                        by the application. It is initialized by this function and will
 *                        later be used to identify this particular service instance.
 * @param[in] p_ts_init  Information needed to initialize the service.
 *
 * @retval NRF_SUCCESS If the service was successfully initialized. Otherwise, an error code is returned.
 * @retval NRF_ERROR_NULL If either of the pointers p_ts or p_ts_init is NULL.
 */
uint32_t ble_ts_init(ble_ts_t * p_ts, const ble_ts_init_t * p_ts_init);

/**@brief Function for handling the Nordic UART Service's BLE events.
 *
 * @details The Nordic UART Service expects the application to call this function each time an
 * event is received from the SoftDevice. This function processes the event if it
 * is relevant and calls the Nordic UART Service event handler of the
 * application if necessary.
 *
 * @param[in] p_ts       Nordic UART Service structure.
 * @param[in] p_ble_evt   Event received from the SoftDevice.
 */
void ble_ts_on_ble_evt(ble_evt_t const * p_ble_evt, void * p_context);

uint32_t ble_ts_info_send(ble_ts_t * p_ts, uint8_t * p_string, uint16_t length);
uint32_t ble_ts_string_send(ble_ts_t * p_ts, uint8_t * p_string, uint16_t length);
static uint32_t push_data_packets();
uint32_t ble_its_send_file(ble_ts_t * p_ts, uint8_t * p_data, uint32_t data_length, uint32_t max_packet_length);
uint32_t ble_ts_send_file_fragment(ble_ts_t * p_ts, uint8_t * p_data, uint32_t data_length);
