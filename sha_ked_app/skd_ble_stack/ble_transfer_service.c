#include "ble_transfer_service.h"
#include <stdio.h>
#include "sdk_common.h"
#include "ble_srv_common.h"
#include "ble.h"
#include "nrf_gpio.h"
   
#define  BT_TRANSFER_SERVICE_DEBUG          1


#include "sha_ked_debug.h"

#define NRF_LOG_MODULE_NAME     bt_transfer
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
NRF_LOG_MODULE_REGISTER();


#if SHA_KED_BLE_SKD_SERVICE_LOG_ENABLED == 1 
    #define NRF_LOG_LEVEL               SHA_KED_BLE_SKD_SERVICE_LOG_LEVEL
    #define NRF_LOG_INFO_COLOR          SHA_KED_BLE_SKD_SERVICE_INFO_LOG_COLOR
    #define NRF_LOG_DEBUG_COLOR         SHA_KED_BLE_SKD_SERVICE_DEBUG_LOG_COLOR
#else 
    #define NRF_LOG_LEVEL               0
#endif 




#define BLE_UUID_TS_RX_CHARACTERISTIC 	0x0002                      /**< The UUID of the RX Characteristic. */
#define BLE_UUID_TS_TX_CHARACTERISTIC 	0x0003                      /**< The UUID of the TX Characteristic. */
#define BLE_UUID_TS_INFO_CHARACTERISTIC 0x0004

#define BLE_TS_MAX_RX_CHAR_LEN        BLE_TS_MAX_DATA_LEN        /**< Maximum length of the RX Characteristic (in bytes). */
#define BLE_TS_MAX_TX_CHAR_LEN        BLE_TS_MAX_DATA_LEN        /**< Maximum length of the TX Characteristic (in bytes). */

#define TS_BASE_UUID                  {{0x87, 0x52, 0x77, 0x2E, 0x1B, 0x5B, 0x48, 0x18, 0xA9, 0xDB, 0xC9, 0xB4, 0x26, 0x00, 0x00, 0x00}} /**< Used vendor specific UUID. */

ble_ts_t * m_ts;

volatile int file_size = 0, file_pos = 0, m_max_data_length = 220;
uint8_t * file_data;

static volatile bool nrf_error_resources = false;



/**@brief Function for handling the @ref BLE_GAP_EVT_CONNECTED event from the S110 SoftDevice.
 *
 * @param[in] p_is     Nordic UART Service structure.
 * @param[in] p_ble_evt Pointer to the event received from BLE stack.
 */
static void on_connect(ble_ts_t * p_ts, ble_evt_t const * p_ble_evt)
{
    p_ts->conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
}


/**@brief Function for handling the @ref BLE_GAP_EVT_DISCONNECTED event from the S110 SoftDevice.
 *
 * @param[in] p_ts     Nordic UART Service structure.
 * @param[in] p_ble_evt Pointer to the event received from BLE stack.
 */
static void on_disconnect(ble_ts_t * p_ts, ble_evt_t const * p_ble_evt)
{
    UNUSED_PARAMETER(p_ble_evt);
    p_ts->conn_handle = BLE_CONN_HANDLE_INVALID;
}


/**@brief Function for handling the @ref BLE_GATTS_EVT_WRITE event from the S110 SoftDevice.
 *
 * @param[in] p_ts     Nordic UART Service structure.
 * @param[in] p_ble_evt Pointer to the event received from BLE stack.
 */
static void on_write(ble_ts_t * p_ts, ble_evt_t const * p_ble_evt)
{
	ble_gatts_evt_write_t const * p_evt_write = &p_ble_evt->evt.gatts_evt.params.write;
	
	
		NRF_LOG_INFO( "on_write");
		NRF_LOG_FLUSH();
	

	if ((p_evt_write->handle == p_ts->tx_handles.cccd_handle)&&(p_evt_write->len == 2))
	{
		if (ble_srv_is_notification_enabled(p_evt_write->data))
		{
			p_ts->is_notification_enabled = true;
			
			
				NRF_LOG_INFO( "tx notification enabled");
				NRF_LOG_FLUSH();
			
		}
		else
		{
			p_ts->is_notification_enabled = false;
			
			
				NRF_LOG_INFO( "tx notification disable");
				NRF_LOG_FLUSH();
			
		}
	}
	else if ((p_evt_write->handle == p_ts->info_handles.cccd_handle)&&(p_evt_write->len == 2))
	{
		if (ble_srv_is_notification_enabled(p_evt_write->data))
		{
			p_ts->is_info_char_notification_enabled = true;
			
			
				NRF_LOG_INFO( "info notification enabled");
				NRF_LOG_FLUSH();
			
		}
		else
		{
			p_ts->is_info_char_notification_enabled = false;
			
			
				NRF_LOG_INFO( "info notification disable");
				NRF_LOG_FLUSH();
			
		}
	}
	else if ((p_evt_write->handle == p_ts->rx_handles.value_handle)&&(p_ts->data_handler != NULL))
	{
		
			NRF_LOG_INFO( "rx data inp %d", p_evt_write->len);
			NRF_LOG_FLUSH();
		
		p_ts->data_handler(p_ts, p_evt_write->data, p_evt_write->len);
	}
	else
	{
		// Do Nothing. This event is not relevant for this service.
		
			NRF_LOG_INFO( "Do Nothing");
			NRF_LOG_FLUSH();
		
	}
}


/**@brief Function for adding TX characteristic.
 *
 * @param[in] p_ts       Nordic UART Service structure.
 * @param[in] p_ts_init  Information needed to initialize the service.
 *
 * @return NRF_SUCCESS on success, otherwise an error code.
 */
static uint32_t tx_char_add(ble_ts_t * p_ts, const ble_ts_init_t * p_ts_init)
{
	/**@snippet [Adding proprietary characteristic to S110 SoftDevice] */
	ble_gatts_char_md_t char_md;
	ble_gatts_attr_md_t cccd_md;
	ble_gatts_attr_t    attr_char_value;
	ble_uuid_t          ble_uuid;
	ble_gatts_attr_md_t attr_md;

	memset(&cccd_md, 0, sizeof(cccd_md));

	BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.read_perm);
	BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.write_perm);

	cccd_md.vloc = BLE_GATTS_VLOC_STACK;

	memset(&char_md, 0, sizeof(char_md));

	char_md.char_props.notify = 1;
	char_md.p_char_user_desc  = NULL;
	char_md.p_char_pf         = NULL;
	char_md.p_user_desc_md    = NULL;
	char_md.p_cccd_md         = &cccd_md;
	char_md.p_sccd_md         = NULL;

	ble_uuid.type = p_ts->uuid_type;
	ble_uuid.uuid = BLE_UUID_TS_TX_CHARACTERISTIC;

	memset(&attr_md, 0, sizeof(attr_md));

	BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.read_perm);
	BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.write_perm);

	attr_md.vloc    = BLE_GATTS_VLOC_STACK;
	attr_md.rd_auth = 0;
	attr_md.wr_auth = 0;
	attr_md.vlen    = 1;

	memset(&attr_char_value, 0, sizeof(attr_char_value));

	attr_char_value.p_uuid    = &ble_uuid;
	attr_char_value.p_attr_md = &attr_md;
	attr_char_value.init_len  = sizeof(uint8_t);
	attr_char_value.init_offs = 0;
	attr_char_value.max_len   = BLE_TS_MAX_TX_CHAR_LEN;

	return sd_ble_gatts_characteristic_add(p_ts->service_handle,
																		 &char_md,
																		 &attr_char_value,
																		 &p_ts->tx_handles);
	/**@snippet [Adding proprietary characteristic to S110 SoftDevice] */
}

/**@brief Function for adding TX characteristic.
 *
 * @param[in] p_ts       Nordic UART Service structure.
 * @param[in] p_ts_init  Information needed to initialize the service.
 *
 * @return NRF_SUCCESS on success, otherwise an error code.
 */
static uint32_t info_char_add(ble_ts_t * p_ts, const ble_ts_init_t * p_ts_init)
{
	/**@snippet [Adding proprietary characteristic to S110 SoftDevice] */
	ble_gatts_char_md_t char_md;
	ble_gatts_attr_md_t cccd_md;
	ble_gatts_attr_t    attr_char_value;
	ble_uuid_t          ble_uuid;
	ble_gatts_attr_md_t attr_md;

	memset(&cccd_md, 0, sizeof(cccd_md));

	BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.read_perm);
	BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.write_perm);

	cccd_md.vloc = BLE_GATTS_VLOC_STACK;

	memset(&char_md, 0, sizeof(char_md));

	char_md.char_props.notify = 1;
	char_md.p_char_user_desc  = NULL;
	char_md.p_char_pf         = NULL;
	char_md.p_user_desc_md    = NULL;
	char_md.p_cccd_md         = &cccd_md;
	char_md.p_sccd_md         = NULL;

	ble_uuid.type = p_ts->uuid_type;
	ble_uuid.uuid = BLE_UUID_TS_INFO_CHARACTERISTIC;

	memset(&attr_md, 0, sizeof(attr_md));

	BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.read_perm);
	BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.write_perm);

	attr_md.vloc    = BLE_GATTS_VLOC_STACK;
	attr_md.rd_auth = 0;
	attr_md.wr_auth = 0;
	attr_md.vlen    = 1;

	memset(&attr_char_value, 0, sizeof(attr_char_value));

	attr_char_value.p_uuid    = &ble_uuid;
	attr_char_value.p_attr_md = &attr_md;
	attr_char_value.init_len  = sizeof(uint8_t);
	attr_char_value.init_offs = 0;
	attr_char_value.max_len   = BLE_TS_MAX_TX_CHAR_LEN;

	return sd_ble_gatts_characteristic_add(p_ts->service_handle,
																		 &char_md,
																		 &attr_char_value,
																		 &p_ts->info_handles);
	/**@snippet [Adding proprietary characteristic to S110 SoftDevice] */
}


/**@brief Function for adding RX characteristic.
 *
 * @param[in] p_ts       Nordic UART Service structure.
 * @param[in] p_ts_init  Information needed to initialize the service.
 *
 * @return NRF_SUCCESS on success, otherwise an error code.
 */
static uint32_t rx_char_add(ble_ts_t * p_ts, const ble_ts_init_t * p_ts_init)
{
	ble_gatts_char_md_t char_md;
	ble_gatts_attr_t    attr_char_value;
	ble_uuid_t          ble_uuid;
	ble_gatts_attr_md_t attr_md;

	memset(&char_md, 0, sizeof(char_md));

	char_md.char_props.write         = 1;
	char_md.char_props.write_wo_resp = 1;
	char_md.p_char_user_desc         = NULL;
	char_md.p_char_pf                = NULL;
	char_md.p_user_desc_md           = NULL;
	char_md.p_cccd_md                = NULL;
	char_md.p_sccd_md                = NULL;

	ble_uuid.type = p_ts->uuid_type;
	ble_uuid.uuid = BLE_UUID_TS_RX_CHARACTERISTIC;

	memset(&attr_md, 0, sizeof(attr_md));

	BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.read_perm);
	BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.write_perm);

	attr_md.vloc    = BLE_GATTS_VLOC_STACK;
	attr_md.rd_auth = 0;
	attr_md.wr_auth = 0;
	attr_md.vlen    = 1;

	memset(&attr_char_value, 0, sizeof(attr_char_value));

	attr_char_value.p_uuid    = &ble_uuid;
	attr_char_value.p_attr_md = &attr_md;
	attr_char_value.init_len  = 1;
	attr_char_value.init_offs = 0;
	attr_char_value.max_len   = BLE_TS_MAX_RX_CHAR_LEN;

	return sd_ble_gatts_characteristic_add(p_ts->service_handle,
																				 &char_md,
																				 &attr_char_value,
																				 &p_ts->rx_handles);
}


void ble_ts_on_ble_evt(ble_evt_t const * p_ble_evt, void * p_context)
{ 
	NRF_LOG_DEBUG("Processing ble evt  0x%02x", p_ble_evt->header.evt_id); 
        
        if (p_ble_evt->header.evt_id == BLE_GAP_EVT_CONN_PARAM_UPDATE) 
            NRF_LOG_DEBUG("Processing BLE_GAP_EVT_CONN_PARAM_UPDATE event");  
        
        
        NRF_LOG_FLUSH(); 

        if ((p_context == NULL) || (p_ble_evt == NULL))
	{
		return;
	}

	ble_ts_t * p_ts = (ble_ts_t*)p_context;

	switch (p_ble_evt->header.evt_id)
	{
		case BLE_GAP_EVT_CONNECTED:
			on_connect(p_ts, p_ble_evt);
                        NRF_LOG_DEBUG("Processing on_connect event 0x%02x", p_ble_evt->header.evt_id); 
		break;

		case BLE_GAP_EVT_DISCONNECTED:
			on_disconnect(p_ts, p_ble_evt);
                        NRF_LOG_DEBUG("Processing on_disconnect event 0x%02x", p_ble_evt->header.evt_id); 
		break;

		case BLE_GATTS_EVT_WRITE:
                        NRF_LOG_DEBUG("Processing on_write event 0x%02x", p_ble_evt->header.evt_id); 
			on_write(p_ts, p_ble_evt);
		break;

		case BLE_GATTS_EVT_HVN_TX_COMPLETE:
		{       
                        NRF_LOG_DEBUG("Processing BLE_GATTS_EVT_HVN_TX_COMPLETE event 0x%02x", p_ble_evt->header.evt_id); 
			uint32_t count = p_ble_evt->evt.gatts_evt.params.hvn_tx_complete.count;
			if(file_size > 0)
			{
				push_data_packets();
			}
			nrf_error_resources = false;
		}
		break;

		default:
			// No implementation needed.
		break;
	}
}


uint32_t ble_ts_init(ble_ts_t * p_ts, const ble_ts_init_t * p_ts_init)
{
	uint32_t      err_code;
	ble_uuid_t    ble_uuid;
	ble_uuid128_t ts_base_uuid = TS_BASE_UUID;
      


      	NRF_LOG_INFO( "Register the serivce");
        NRF_LOG_FLUSH();


	VERIFY_PARAM_NOT_NULL(p_ts);
	VERIFY_PARAM_NOT_NULL(p_ts_init);

	// Initialize the service structure.
	p_ts->conn_handle             = BLE_CONN_HANDLE_INVALID;
	p_ts->data_handler            = p_ts_init->data_handler;
	p_ts->is_notification_enabled = false;

	/**@snippet [Adding proprietary Service to S110 SoftDevice] */
	// Add a custom base UUID.
	err_code = sd_ble_uuid_vs_add(&ts_base_uuid, &p_ts->uuid_type);
	VERIFY_SUCCESS(err_code);

	ble_uuid.type = p_ts->uuid_type;
	ble_uuid.uuid = BLE_UUID_TS_SERVICE;

	// Add the service.
	err_code = sd_ble_gatts_service_add(BLE_GATTS_SRVC_TYPE_PRIMARY,
																			&ble_uuid,
																			&p_ts->service_handle);
	/**@snippet [Adding proprietary Service to S110 SoftDevice] */
	VERIFY_SUCCESS(err_code);

	// Add the RX Characteristic.
	err_code = rx_char_add(p_ts, p_ts_init);
	VERIFY_SUCCESS(err_code);

	// Add the TX Characteristic.
	err_code = tx_char_add(p_ts, p_ts_init);
	VERIFY_SUCCESS(err_code);

	// Add the Image Info Characteristic.
	err_code = info_char_add(p_ts, p_ts_init);
	VERIFY_SUCCESS(err_code);

	return NRF_SUCCESS;
}


uint32_t ble_ts_string_send(ble_ts_t * p_ts, uint8_t * p_string, uint16_t length)
{
	ble_gatts_hvx_params_t hvx_params;
	uint32_t err_code;

	if(nrf_error_resources)
	{
		return NRF_ERROR_RESOURCES;
	}

	VERIFY_PARAM_NOT_NULL(p_ts);

	if ((p_ts->conn_handle == BLE_CONN_HANDLE_INVALID) || (!p_ts->is_notification_enabled))
	{
		return NRF_ERROR_INVALID_STATE;
	}

	if (length > BLE_TS_MAX_DATA_LEN)
	{
		return NRF_ERROR_INVALID_PARAM;
	}

	memset(&hvx_params, 0, sizeof(hvx_params));
	hvx_params.handle = p_ts->tx_handles.value_handle;
	hvx_params.p_data = p_string;
	hvx_params.p_len  = &length;
	hvx_params.type   = BLE_GATT_HVX_NOTIFICATION;

	err_code = sd_ble_gatts_hvx(p_ts->conn_handle, &hvx_params);

	if(err_code == NRF_ERROR_RESOURCES)
	{
		nrf_error_resources = true;
	}

	//NRF_LOG_INFO("NOT - Len: %i, err_code: %i", length, err_code);
	return err_code;
}


uint32_t ble_ts_info_send(ble_ts_t * p_ts, uint8_t * p_string, uint16_t length)
{
	ble_gatts_hvx_params_t hvx_params;

	VERIFY_PARAM_NOT_NULL(p_ts);

	if ((p_ts->conn_handle == BLE_CONN_HANDLE_INVALID) || (!p_ts->is_info_char_notification_enabled))
	{
		return NRF_ERROR_INVALID_STATE;
	}
	
	if (length > BLE_TS_MAX_DATA_LEN)
	{
		return NRF_ERROR_INVALID_PARAM;
	}
	
	memset(&hvx_params, 0, sizeof(hvx_params));
	hvx_params.handle = p_ts->info_handles.value_handle;
	hvx_params.p_data = p_string;
	hvx_params.p_len  = &length;
	hvx_params.type   = BLE_GATT_HVX_NOTIFICATION;

	return sd_ble_gatts_hvx(p_ts->conn_handle, &hvx_params);    
}

static uint32_t push_data_packets()
{
	int return_code = NRF_SUCCESS;
	int packet_length = m_max_data_length;
	int packet_size = 0;
	while(return_code == NRF_SUCCESS)
	{
		if((file_size - file_pos) > packet_length)
		{
			packet_size = packet_length;
		}
		else if((file_size - file_pos) > 0)
		{
			packet_size = file_size - file_pos;
		}

		if(packet_size > 0)
		{
			return_code = ble_ts_string_send(m_ts, file_data +file_pos, packet_size);
			if(return_code == NRF_SUCCESS)
			{
				file_pos += packet_size;
			}   
		}
		else
		{
			file_size = 0;
			break;
		}
	}
	return return_code;
	}

uint32_t ble_its_send_file(ble_ts_t * p_ts, uint8_t * p_data, uint32_t data_length, uint32_t max_packet_length)
{
	uint32_t err_code; 

	if ((p_ts->conn_handle == BLE_CONN_HANDLE_INVALID) || (!p_ts->is_notification_enabled))
	{
		return NRF_ERROR_INVALID_STATE;
	} 

	if(file_size != 0)
	{
		return NRF_ERROR_BUSY;
	}

	file_size = data_length;
	file_pos = 0;
	file_data = p_data;
	m_max_data_length = max_packet_length;
	m_ts = p_ts;

	err_code = push_data_packets();
	
	if(err_code == NRF_ERROR_RESOURCES) return NRF_SUCCESS;
	
	return err_code;
}



uint32_t ble_ts_send_file_fragment(ble_ts_t * p_ts, uint8_t * p_data, uint32_t data_length)
{
    uint32_t err_code; 
    
    if ((p_ts->conn_handle == BLE_CONN_HANDLE_INVALID) || (!p_ts->is_notification_enabled))
    {
        return NRF_ERROR_INVALID_STATE;
    } 
    
    if(file_size != 0)
    {
        return NRF_ERROR_BUSY;
    }
    
    err_code = ble_ts_string_send(p_ts, p_data, data_length);           
    return err_code;
}