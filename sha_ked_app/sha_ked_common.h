#ifndef SHA_KED_COMMON_H
#define SHA_KED_COMMON_H 


#include <stdio.h>
#include <stdint.h>


#include "nrf_log.h"
#include "nrf_log_ctrl.h"


#define MAX_NUMBER_OF_TIME_ZONE     5
#define MAX_NUMBER_OF_GEO_ZONE      5

#define MAX_DEVICE_NAME_LEN           29

#define NUMBER_OF_TEST_RECORDS_IN_DB  500

typedef enum{

  NO_ERROR = 0

} skd_error_code_t;  

typedef enum{

  TIME_THRESHOLD_DETECED_EVNT_ID       = 1,
  DISTANCE_THRESHOLD_DETECED_EVNT_ID,
  ANGLE_THRESHOLD_DETECED_EVNT_ID,  
  TRIGGER_DETECTED_EVNT_ID, 
  GEOZONE_DETECED_EVNT_ID, 
  GPS_JAMMING_DETECTED_EVNT_ID,
  GNSS_TURN_OFF_EVNT_ID,
  GNSS_TURN_ON_EVNT_ID,
  TIME_OUT_FOR_SATTELITE_SEARCHING_EVNT_ID,
  START_OF_MOVEMENT_DETECTED_EVNT_ID,
  END_OF_MOVEMENT_DETECTED_EVNT_ID,
  EXTERNAL_POWER_SUPPORT_CONNECTED_EVNT_ID,
  EXTERNAL_POWER_SUPPORT_DISCONNECTED_EVNT_ID,
  CHARGHING_CONNECTED_EVNT_ID,
  CHARGHING_DISCONNECTED_EVNT_ID,
  LOW_BATTERY_LEVEL_DETECTED_EVNT_ID, 
  LOW_MEMORY_LEVEL_DETECTED_EVNT_ID,
  BT_JAMMING_DETECTED_EVNT_ID,
  BT_POWER_ON_PACK_RECIEVED_EVNT_ID,
  BT_DETECTION_TURNED_ON_EVNT_ID,
  BT_TURNED_ON_EVNT_ID,
  BT_TURNED_OFF_EVNT_ID,
  MOBILE_APP_CONNTECTED_OVER_BT_EVNT_ID,
  MOBILE_APP_DISCONNTECTED_OVER_BT_EVNT_ID,
  BT_TURN_ON_BY_BUTTON_EVNT_ID,
  BT_TURN_OFF_BY_BUTTON_EVNT_ID,
  BT_TURN_ON_BY_TIMEZONE_EVNT_ID,
  BT_TURN_OFF_BY_TIMEZONE_EVNT_ID,
  SATELITES_SEARCHING_EVNT_ID,
  SATELITTES_FOUND_EVNT_ID,
  SHA_KED_POWER_ON

} skd_evnt_type_t;  

#pragma pack(push, 1)


typedef struct  {
    
    uint8_t year;
    uint8_t month;
    uint8_t day;

}skd_data_t;


typedef struct {

    uint8_t  hours;
    uint8_t  minutes;
    uint8_t  seconds;
    uint16_t microseconds;

}skd_time_t; 



typedef struct{

    skd_data_t date;
    skd_time_t time; 

}skd_timestamp_t; 



typedef struct{

     float latitude;
     float longitude;
  
} skd_geolocation_t; 


typedef struct{

    uint16_t angle; 
    uint8_t  fix_type;
    uint32_t hdop; 
    float    latitude;
    float    longitude;
    uint8_t  satellites_count;
    uint32_t speed;
    uint8_t  reliability_flag;

} skd_gnss_data_t;  


typedef struct{

    uint8_t deviceType; 
    uint8_t hw0; 
    uint8_t hw1; 
    uint8_t hw2;
    uint8_t fw0; 
    uint8_t fw1; 
    uint8_t fw2;
    uint8_t macAddr[6]; 

}skd_device_info_t;  



typedef struct{

   uint16_t            batteryLevelVolt;
   skd_gnss_data_t     gnssData;
   uint8_t             chargeStatus;
   uint8_t             motionStatus;

}skd_device_status_t;  

typedef struct{

    uint8_t               evtId;
    skd_device_status_t   devStatus; 
    skd_timestamp_t       timeStamp; 

}skd_snapshot_t; 

////////////////  Device settings        ////////////////////////

typedef enum{
  
  BLE_OFF                = ( 0 << 0 ),  
  ALWAYS_ON              = ( 1 << 0 ), 
  WAIT_FOR_BUTTON        = ( 1 << 1 ),
  WAIT_FOR_TIME_INTERVAL = ( 1 << 2 ), 
  WAIT_FOR_SCHEDULE      = ( 1 << 3 ),
  WAIT_FOR_GEOZONE       = ( 1 << 4 ), 
  WAIT_FOR_LONG_STOP     = ( 1 << 5 ),
  CONNECTED              = ( 1 << 6 ),

}skd_ble_mode_e; 


typedef struct{
  
  uint8_t AO:1;       // Always on
  uint8_t WFBT:1;     // Wait for button to turn on bluetooth for default timeout
  uint8_t WFTI:1;     // Wait for time interval to turn on/off
  uint8_t WFTZ:1;     // Wait for timezone  to turn on/off
  uint8_t WFGZ:1;     // Wait for geozone to turn on/off bluetooth
  uint8_t WFLS:1;     // Wait for long stop to turn bluetooth on until next movement detected
  uint8_t EXANTEN:1;  // Enable external antenna
  uint8_t RESERVED: 1; 

}skd_ble_mode_t; 

typedef struct{

  skd_ble_mode_t  mode;
  uint16_t        defaultWakeUpTimeoutSec;  
  uint8_t         gain;  
  //uint8_t         deviceNameLen;                    // Length of device name in bytes.
  //char            deviceName[ MAX_DEVICE_NAME_LEN];         
} skd_ble_settings_t;


typedef enum{

  MOTION_SENSOR_OFF        = (0 << 0),
  DETECT_START_OF_MOVEMENT = (1 << 0),
  DETECT_END_OF_MOVEMENT   = (1 << 1),
  DETECT_LONG_STOP         = (1 << 2)
   
}skd_ms_mode_e;     //motion sensor mode.

typedef struct{

   uint8_t DSM:1;      //detect start of movement.(DSM)
   uint8_t DEM:1;      //detect end of movement.(DEM)
   uint8_t DLS:1;      //detect long stop.(DLS)
   uint8_t RESERVED:5; //reserved for futre use 
    
}skd_ms_mode_t;     //motion sensor mode.


#define MAX_SENSITIVY    9 

typedef struct{

  skd_ms_mode_t  mode;
  uint8_t        sensitivity;
  uint16_t       movmentStartTimeoutSec; 
  uint16_t       movmentStopTimeoutSec;
  uint16_t       longStopTimeoutSec; 
    
}skd_ms_settings_t;    //motion sensor settings.

 
typedef enum{
  
  TIME_OFF                 = ( 0 << 0 ),  
  TRACK_BLE_TIME_INTERVAL  = ( 1 << 0 ),
  TRACK_GNSS_TIME_INTERVAL = ( 1 << 1 ),
  TRACK_BLE_SCHEDULE       = ( 1 << 2 )

}timer_mode_e; 

typedef struct{

  uint8_t DBTI:1;  //detect bluetooth time interval.
  uint8_t DGTI:1;  //detect gnss time interval.   
  uint8_t DBTZ:1;  //detect bluetooth timezone. 
  uint8_t RESERVED:5; 

}skd_time_mode_t; 

typedef struct{

  uint8_t          id;
  uint8_t          timezoneState;
  uint8_t          timezoneType; 
  skd_timestamp_t  startTime; 
  skd_timestamp_t  endTime; 

} skd_timezone_t;

 
typedef struct{

  skd_time_mode_t     mode;
  uint16_t            gnssOnTimeIntervalSec;
  uint16_t            gnssOffTimeIntervalSec;
  uint16_t            bleOnTimeIntervalSec;
  uint16_t            bleOffTimeIntervalSec;
  skd_timezone_t      timezones[ MAX_NUMBER_OF_TIME_ZONE ]; 
  
}skd_timer_settings_t;  



typedef struct{

  uint8_t  AO:1;        // always on. 
  uint8_t  WFTI:1;      // wait for time interval
  uint8_t  WFMT:1;      // wait for motion
  uint8_t  SSIE:1;      // satellite search interval enabled
  uint8_t  EXANTEN:1;     // external antenna enabled. 
  uint8_t  RESERVED:3;  // reserved for future use

} skd_gnss_pwr_mode_t;



typedef struct{
  
  uint8_t DG:1;         // detect geozone.
  uint8_t DDT:1;        // detect distance threshold detection  
  uint8_t DTT:1;        // detect time threshold detection
  uint8_t DAT:1;        // detect angle threshold detection
  uint8_t RESERVED:4;   // reserved for future use

} skd_data_processing_mode_t; 


typedef struct{

   uint8_t DFEN:1;      // distance filter enabled.
   uint8_t SFEN:1;      // speed filter enabled
   uint8_t RESERVED:6;  // reserved for future use

}skd_save_filter_mode_t;

typedef struct{

  uint8_t   id;
  uint8_t   geozoneState;    
  float     latitude;
  float     longitude; 
  uint32_t  radiusMeters; 

} skd_geozone_t;


typedef struct{
  
  skd_gnss_pwr_mode_t          powerMode;
  skd_data_processing_mode_t   DSPmode;
  skd_save_filter_mode_t       saveFilterMode;      
  uint16_t                     distanceThresholdMeter; 
  uint16_t                     timeThresholdSeconds;
  uint16_t                     angleThresholdDegree; 
  uint16_t                     speedFilterThresholdKmOverHour;
  uint16_t                     distanceFilterThresholdMeter; 
  uint16_t                     searchTimeoutSec; 
  skd_geozone_t                geozone[ MAX_NUMBER_OF_GEO_ZONE ];
    
} skd_gnss_settings_t;


typedef enum{

 BUTTON_OFFLINE           = (0 << 0 ),  
 DETECT_SINGLE_PRESS      = (1 << 0 ),
 DETECT_DOUBLE_PRESS      = (1 << 1 ), 
 DETECT_TRIPLE_PRESS      = (1 << 2 ), 
 DETECT_QUADRUPLE_PRESS   = (1 << 3 ) 

} skd_button_mode_e; 

typedef struct{

   uint8_t DSP:1;   // detect single press
   uint8_t DDP:1;   // detect double press
   uint8_t DTP:1;   // detect triple press
   uint8_t DQP:1;   // detect quadruple_press

} skd_button_mode_t; 

typedef struct{

  skd_button_mode_t   mode; 
  uint16_t            detectionTimeoutSec; 

} skd_button_settings_t;  


typedef enum {

  LED_ALLWAYS_OFF = 0,
  LED_ALLWAYS_ON,
  LED_ON_ONLY_AFTER_START, 

} skd_led_mode_t;  


typedef struct{
  
  uint8_t  mode;
  uint16_t defaultTimeoutSec;  

} skd_led_settings_t;  


typedef enum {

  OVER_WRITE_DISABLE = 0,
  OVER_WRITE_ENABLED

} skd_db_mode_t; 
 

typedef struct{
  
  uint8_t   mode;
  uint16_t  cashSize;  

} skd_db_settings_t; 


typedef struct{
  
  skd_ble_settings_t      bleSettings;
  skd_gnss_settings_t     gnnsSettings;
  skd_ms_settings_t       msSettings; 
  skd_timer_settings_t    timerSettings; 
  skd_button_settings_t   butSettings;
  skd_led_settings_t      ledSettings; 
  skd_db_settings_t       dbSettings;
   
} skd_settings_t; 


typedef struct{ 

  uint8_t futureField; 

}skd_setting_legacy_t; 


#pragma pack(pop)


extern const skd_gnss_data_t        skd_default_gnns_data; 
extern const skd_data_t             skd_default_data; 
extern const skd_time_t             skd_default_time; 
extern const skd_device_info_t      skd_default_devInfo; 
extern const skd_ble_settings_t     skd_default_bleSettings;
extern const skd_ms_settings_t      skd_default_msSettings; 
extern const skd_timer_settings_t   skd_default_timerSettings;
extern const skd_gnss_settings_t    skd_default_gnssSettings;
extern const skd_button_settings_t  skd_default_buttonSettings;
extern const skd_led_settings_t     skd_default_ledSettings;
extern const skd_db_settings_t      skd_default_dbSettings;
 

void skd_set_default_status       (skd_device_status_t* pDevStatus ); 
void skd_set_default_record       (skd_snapshot_t*      pRecord); 
void skd_set_defualt_settings     (skd_settings_t*      pSettings);
void skd_set_default_time         (skd_timestamp_t*     pTime);
void skd_set_default_bleSettings  (skd_ble_settings_t* pSettings);
void skd_set_safe_bleSettings     (skd_ble_settings_t* pSettings);
void skd_set_defualt_ts_config    (skd_timer_settings_t* pSettings); 


static inline void skd_debug_printTimeStampInLine(skd_timestamp_t* pTimeStamp){
 
 skd_data_t* pData = &pTimeStamp->date;
 skd_time_t* pTime = &pTimeStamp->time; 

 NRF_LOG_DEBUG("%d:%d:%d:%d:%d:%d",(pData->year +2000), pData->month,   pData->day, \
                                    pTime->hours,       pTime->minutes, pTime->seconds);  

}  

static inline void skd_debug_printTime(skd_time_t*   pTime){

  NRF_LOG_FLUSH(); 

  NRF_LOG_DEBUG("houres: %d",       pTime->hours); 
  NRF_LOG_DEBUG("minutes: %d",      pTime->minutes);  
  NRF_LOG_DEBUG("seconds: %d",      pTime->seconds); 
  NRF_LOG_DEBUG("microseconds: %d", pTime->microseconds); 

  NRF_LOG_FLUSH(); 

} 

static inline  void skd_debug_printDate(skd_data_t*   pDate){

  NRF_LOG_FLUSH(); 

  NRF_LOG_DEBUG("year: %d",       pDate->year); 
  NRF_LOG_DEBUG("month: %d",      pDate->month);  
  NRF_LOG_DEBUG("day: %d",        pDate->day); 

  NRF_LOG_FLUSH(); 

}
 

static inline  void skd_debug_printGeolocation(skd_geolocation_t* pGeolocation){

  NRF_LOG_FLUSH(); 


  NRF_LOG_DEBUG("The position latitude: " NRF_LOG_FLOAT_MARKER,  NRF_LOG_FLOAT(pGeolocation->latitude));
  NRF_LOG_DEBUG("The position longitude: " NRF_LOG_FLOAT_MARKER, NRF_LOG_FLOAT(pGeolocation->longitude)); 

  NRF_LOG_FLUSH();

}

static inline void skd_debug_printGnssData(skd_gnss_data_t* pData){
  
  NRF_LOG_FLUSH(); 
  
  NRF_LOG_DEBUG("angle: %d",                        pData->angle);
  NRF_LOG_DEBUG("fix_type %d",                      pData->fix_type );
  NRF_LOG_DEBUG("hdop: %d",                         pData->hdop);
  NRF_LOG_DEBUG("latitude: " NRF_LOG_FLOAT_MARKER , NRF_LOG_FLOAT(pData->latitude)); 
  NRF_LOG_DEBUG("longitude: " NRF_LOG_FLOAT_MARKER, NRF_LOG_FLOAT(pData->longitude));
  NRF_LOG_DEBUG("satellites_count:%d",              pData->satellites_count); 
  NRF_LOG_DEBUG("speed:%d",                         pData->speed);  //NRF_LOG_DEBUG("speed:",NRF_LOG_FLOAT_MARKER,  pData->speed);  
  NRF_LOG_DEBUG("reliability_flag:%d",              pData->reliability_flag); 
 
  NRF_LOG_FLUSH(); 

}  

static inline void skd_debug_printDevStatus(skd_device_status_t* pDevStatus){

  NRF_LOG_FLUSH(); 
  
   
  NRF_LOG_DEBUG("batteryLevelVolt: %d",pDevStatus->batteryLevelVolt); 
  skd_debug_printGnssData(&pDevStatus->gnssData);
  NRF_LOG_DEBUG("chargeStatus: %d", pDevStatus->chargeStatus);
  NRF_LOG_DEBUG("motionStatus: %d", pDevStatus->motionStatus); 

  NRF_LOG_FLUSH(); 


} 

static inline void skd_debug_printSnapShot(skd_snapshot_t* pSnapshot){

  NRF_LOG_FLUSH(); 
  
  NRF_LOG_DEBUG("evtId: %d", pSnapshot->evtId );
  
  skd_debug_printDevStatus(&pSnapshot->devStatus);
  skd_debug_printDate(&pSnapshot->timeStamp.date);
  skd_debug_printTime(&pSnapshot->timeStamp.time); 
   
  NRF_LOG_FLUSH(); 

} 

static inline void skd_debug_printBLEsettings(skd_ble_settings_t* pSettings){

  NRF_LOG_FLUSH(); 
  
  //NRF_LOG_DEBUG("mode: %d",                    *(uint8_t* )&pSettings->mode); 
  skd_ble_mode_t* pMode = &pSettings->mode; 

  NRF_LOG_DEBUG("Always on                       %s",    pMode->AO      ? "ENABLED"  : "DISABLED"); 
  NRF_LOG_DEBUG("Wait for button                 %s",    pMode->WFBT    ? "ENABLED"  : "DISABLED"); 
  NRF_LOG_DEBUG("Wait for time interval          %s",    pMode->WFTI    ? "ENABLED"  : "DISABLED"); 
  NRF_LOG_DEBUG("Wait for timezone               %s",    pMode->WFTZ    ? "ENABLED"  : "DISABLED"); 
  NRF_LOG_DEBUG("Wait for geozone                %s",    pMode->WFGZ    ? "ENABLED"  : "DISABLED"); 
  NRF_LOG_DEBUG("Wait for long stop              %s",    pMode->WFLS    ? "ENABLED"  : "DISABLED");
  NRF_LOG_DEBUG("External antenna                %s",    pMode->EXANTEN ? "ENABLED"  : "DISABLED"); 
   
  NRF_LOG_DEBUG("defaultWakeUpTimeoutSec:        %d",    pSettings->defaultWakeUpTimeoutSec);
  NRF_LOG_DEBUG("gain:                           %d",    pSettings->gain); 

  NRF_LOG_FLUSH(); 
}
 
static inline void skd_debug_printGNSSsettings(skd_gnss_settings_t* pSettings){

  NRF_LOG_FLUSH();
  //NRF_LOG_DEBUG("powerMode: %d",                          *(uint8_t* )&pSettings->powerMode);
  skd_gnss_pwr_mode_t* pwMode = &pSettings->powerMode; 
  
  NRF_LOG_DEBUG("Always on                       %s",   pwMode->AO      ? "ENABLED"  : "DISABLED");
  NRF_LOG_DEBUG("Wait for time interval          %s",   pwMode->WFTI    ? "ENABLED"  : "DISABLED");
  NRF_LOG_DEBUG("Wait for motion                 %s",   pwMode->WFMT    ? "ENABLED"  : "DISABLED"); 
  NRF_LOG_DEBUG("Satellite search interval       %s",   pwMode->SSIE    ? "ENABLED"  : "DISABLED"); 
  NRF_LOG_DEBUG("External antenna                %s",   pwMode->EXANTEN ? "ENABLED"  : "DISABLED"); 
    
  //NRF_LOG_DEBUG("DSPmode: %d",                            *(uint8_t* )&pSettings->DSPmode); 
  skd_data_processing_mode_t* dspMode = &pSettings->DSPmode; 

  NRF_LOG_DEBUG("Gezone detection                %s",      dspMode->DG  ? "ENABLED" : "DISABLED");
  NRF_LOG_DEBUG("Time threshold detection        %s",      dspMode->DTT ? "ENABLED" : "DISABLED"); 
  NRF_LOG_DEBUG("Distance threshold detection    %s",      dspMode->DDT ? "ENABLED" : "DISABLED"); 
  NRF_LOG_DEBUG("Angle threshold detection       %s",      dspMode->DAT ? "ENABLED" : "DISABLED"); 

  //NRF_LOG_DEBUG("saveFilterMode: %d",                     *(uint8_t* )&pSettings->saveFilterMode);
  skd_save_filter_mode_t* sfMode  = &pSettings->saveFilterMode; 
  
  NRF_LOG_DEBUG("Save distance filter            %s",       sfMode->DFEN  ? "ENABLED" : "DISABLED"); 
  NRF_LOG_DEBUG("Save speed filter               %s",       sfMode->SFEN  ? "ENABLED" : "DISABLED");

  NRF_LOG_DEBUG("distanceThresholdMeter:         %d",       pSettings->distanceThresholdMeter); 
  NRF_LOG_DEBUG("timeThresholdSeconds:           %d",       pSettings->timeThresholdSeconds); 
  NRF_LOG_DEBUG("angleThresholdDegree:           %d",       pSettings->angleThresholdDegree);  
  NRF_LOG_DEBUG("speedFilterThresholdKmOverHour: %d",       pSettings->speedFilterThresholdKmOverHour);
  NRF_LOG_DEBUG("distanceFilterThresholdMeter:   %d",       pSettings->distanceFilterThresholdMeter);
  NRF_LOG_DEBUG("searchTimeoutSec:               %d",       pSettings->searchTimeoutSec);

  NRF_LOG_FLUSH(); 

} 

static inline void skd_debug_printMSsettings(skd_ms_settings_t* pSettings){

  NRF_LOG_FLUSH(); 

  //NRF_LOG_DEBUG("mode: %d",                           *(uint8_t* )&pSettings->mode); 
  skd_ms_mode_t* pMode = &pSettings->mode;  

  NRF_LOG_DEBUG("Detect start of movement        %s",       pMode->DSM  ? "ENABLED" : "DISABLED");
  NRF_LOG_DEBUG("Detect end of movement          %s",       pMode->DEM  ? "ENABLED" : "DISABLED");
  NRF_LOG_DEBUG("Detect long stop                %s",       pMode->DLS  ? "ENABLED" : "DISABLED");
  NRF_LOG_FLUSH(); 
    
  NRF_LOG_DEBUG("sensitivity:                    %d",        pSettings->sensitivity); 
  NRF_LOG_DEBUG("movmentStartTimeoutSec:         %d",        pSettings->movmentStartTimeoutSec); 
  NRF_LOG_DEBUG("movmentStopTimeoutSec:          %d",        pSettings->movmentStopTimeoutSec);
  NRF_LOG_DEBUG("longStopTimeoutSec:             %d",        pSettings->longStopTimeoutSec); 

  NRF_LOG_FLUSH(); 

}
static inline void skd_debug_printTIMERsettings(skd_timer_settings_t* pSettings){

  NRF_LOG_FLUSH(); 
  
  //NRF_LOG_DEBUG("mode: %d",                    *(uint8_t* )pSettings->mode); 
  skd_time_mode_t* pMode = &pSettings->mode;
  NRF_LOG_DEBUG("BLE time interval               %s",    pMode->DBTI ? "ENABLED" : "DISABLED");
  NRF_LOG_DEBUG("BLE timezone                    %s",    pMode->DBTZ ? "ENABLED" : "DISABLED");
  NRF_LOG_DEBUG("GNSS time interval              %s",    pMode->DGTI ? "ENABLED" : "DISABLED");
  
  NRF_LOG_DEBUG("gnssOffTimeIntervalSec:         %d",    pSettings->gnssOffTimeIntervalSec); 
  NRF_LOG_DEBUG("gnssOnTimeIntervalSec:          %d",    pSettings->gnssOnTimeIntervalSec) 
  NRF_LOG_DEBUG("bleOffTimeIntervalSec:          %d",    pSettings->bleOffTimeIntervalSec); 
  NRF_LOG_DEBUG("bleOnTimeIntervalSec:           %d",    pSettings->bleOnTimeIntervalSec) 


  NRF_LOG_FLUSH(); 

} 
static inline void skd_debug_printBUTsettings(skd_button_settings_t* pSettings){

  NRF_LOG_FLUSH(); 
  
//  NRF_LOG_DEBUG("mode:                           %d",      *(uint8_t*)pSettings->mode); 
  skd_button_mode_t* pMode = &pSettings->mode; 

  NRF_LOG_DEBUG("Detect single press             %s",    pMode->DSP ? "ENABLED" : "DISABLED");
  NRF_LOG_DEBUG("Detect double press             %s",    pMode->DDP ? "ENABLED" : "DISABLED");
  NRF_LOG_DEBUG("Detect triple press             %s",    pMode->DTP ? "ENABLED" : "DISABLED");
  NRF_LOG_DEBUG("Detect quadruple press          %s",    pMode->DQP ? "ENABLED" : "DISABLED");

  NRF_LOG_DEBUG("detectionTimeoutSec:            %d",      pSettings->detectionTimeoutSec); 

  NRF_LOG_FLUSH(); 

}
static inline void skd_debug_printLEDSettings(skd_led_settings_t* pSettings){

  NRF_LOG_FLUSH(); 
  
  NRF_LOG_DEBUG("Led mode:                       %d",    pSettings->mode); 
  NRF_LOG_DEBUG("defaultTimeoutSec:              %d",    pSettings->defaultTimeoutSec); 

  NRF_LOG_FLUSH(); 


}
static inline void skd_debug_printDBsettingds(skd_db_settings_t* pSettings) {

  NRF_LOG_FLUSH(); 

  NRF_LOG_DEBUG("Overwriting:                    %s",      pSettings->mode ? "ENABLED" : "DISABLED"); 
  NRF_LOG_DEBUG("cashSize:                       %d",      pSettings->cashSize); 

  NRF_LOG_FLUSH(); 

}    


static inline void skd_debug_printSKDsettings(skd_settings_t* pSettings){

  skd_debug_printBLEsettings    (&pSettings->bleSettings);
  skd_debug_printGNSSsettings   (&pSettings->gnnsSettings); 
  skd_debug_printMSsettings     (&pSettings->msSettings);
  skd_debug_printTIMERsettings  (&pSettings->timerSettings);
  skd_debug_printLEDSettings    (&pSettings->ledSettings); 
  skd_debug_printDBsettingds    (&pSettings->dbSettings); 
} 

#endif