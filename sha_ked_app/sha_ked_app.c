#include <string.h>
#include "sha_ked_debug.h"

#include "sha_ked_app.h"
#include "skd_cmd_srv.h"
#include "skd_geo_srv.h"
#include "skd_time_srv.h"
#include "skd_led_srv.h"
#include "skd_pw_srv.h"
#include "skd_md_srv.h"
#include "skd_but_srv.h"

#include "nrf_delay.h"


#include "skd_db.h"
#include "ble_stack.h"

#include "app_timer.h"

APP_TIMER_DEF( bleReconfigTimerId );
#define     BLE_RECONFIG_TIMEOUT_SEC          30





#if SHA_KED_APP_LOG_ENABLED == 1 
    #define NRF_LOG_LEVEL               SHA_KED_APP_LOG_LEVEL
    #define NRF_LOG_INFO_COLOR          SHA_KED_APP_INFO_LOG_COLOR
    #define NRF_LOG_DEBUG_COLOR         SHA_KED_APP_DEBUG_LOG_COLOR
#else 
    #define NRF_LOG_LEVEL       0
#endif 


#define NRF_LOG_MODULE_NAME     sha_ked_app
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
NRF_LOG_MODULE_REGISTER();



/*--------------------------BLE-CMD-SUBROUTINE----------------------------------*/
static cmd_status_t     enterDFU_Cb             (void);
static cmd_status_t     getDevInfo_Cb           (skd_device_info_t*     pInfo);
static cmd_status_t     getBleSettings_Cb       (skd_ble_settings_t*    pSettings); 
static cmd_status_t     setBleSettings_Cb       (skd_ble_settings_t*    pSettings);
static cmd_status_t     getGNSSSettings_Cb      (skd_gnss_settings_t*   pSettings); 
static cmd_status_t     setGNSSSettings_Cb      (skd_gnss_settings_t*   pSettings);
static cmd_status_t     getMSSettings_Cb        (skd_ms_settings_t*     pSettings); 
static cmd_status_t     setMSSettings_Cb        (skd_ms_settings_t*     pSettings);
static cmd_status_t     getTimerSettings_Cb     (skd_timer_settings_t*  pSettings); 
static cmd_status_t     setTimerSettings_Cb     (skd_timer_settings_t*  pSettings);
static cmd_status_t     getTime_Cb              (skd_timestamp_t*       pTime );
static cmd_status_t     setTime_Cb              (skd_timestamp_t*       pTime );
static cmd_status_t     getButtonSettings_Cb    (skd_button_settings_t* pSettings); 
static cmd_status_t     setButtonSettings_Cb    (skd_button_settings_t* pSettings);
static cmd_status_t     getLedSettings_Cb       (skd_led_settings_t* pSettings); 
static cmd_status_t     setLedSettings_Cb       (skd_led_settings_t* pSettings);
static cmd_status_t     getDBSettings_Cb        (skd_db_settings_t* pSettings); 
static cmd_status_t     setDBSettings_Cb        (skd_db_settings_t* pSettings);
static cmd_status_t     getDBnumberOfRecrods_Cb (uint32_t*              DBnumberOfRecrods);
static cmd_status_t     getDBrecord_Cb          (uint32_t               recordId, 
                                                 skd_snapshot_t*        pRecord);
static cmd_status_t     getDBrecords_Cb         (uint32_t               strId, 
                                                 uint32_t               endId);
static cmd_status_t     takeSnapShot_Cb         (skd_device_status_t*   pStatus, 
                                                 skd_timestamp_t*       pTime);
static cmd_status_t     cleanDB_soft_Cb         (void);
static cmd_status_t     cleanDB_hard_Cb         (void);

static cmd_srv_app_api_t  cmd_srv_api    = { 

                                          .sendResp               = ble_stack_sendInfo,
                                          .enterDFU               = enterDFU_Cb,
                                          .getDevInfo             = getDevInfo_Cb, 
                                          .takeSnapshot           = takeSnapShot_Cb,
                                          .getBLESettings         = getBleSettings_Cb, 
                                          .setBLESettings         = setBleSettings_Cb,
                                          .getGNSSSettings        = getGNSSSettings_Cb,
                                          .setGNSSSettings        = setGNSSSettings_Cb,
                                          .getMSSettings          = getMSSettings_Cb,
                                          .setMSSettings          = setMSSettings_Cb,
                                          .getTimerSettings       = getTimerSettings_Cb,
                                          .setTimerSettings       = setTimerSettings_Cb,
                                          .getTime                = getTime_Cb, 
                                          .setTime                = setTime_Cb,
                                          .getButtonSettings      = getButtonSettings_Cb, 
                                          .setButtonSettings      = setButtonSettings_Cb,
                                          .getLedSettings         = getLedSettings_Cb, 
                                          .setLedSettings         = setLedSettings_Cb,
                                          .getDBSettings          = getDBSettings_Cb, 
                                          .setDBSettings          = setDBSettings_Cb,
                                          .getDBnumberOfRecrods   = getDBnumberOfRecrods_Cb,
                                          .getDBrecord            = getDBrecord_Cb,
                                          .getDBrecords           = getDBrecords_Cb,
                                          .cleanDB_soft           = cleanDB_soft_Cb,
                                          .cleanDB_hard           = cleanDB_hard_Cb

                                         };

/*--------------------------BLE-STACK-HANDLERS----------------------------------*/
static void skd_ble_dataReceivedHandler(uint8_t* p_data, uint8_t dataLen);
static void skd_ble_statusUpdatedHandler(ble_stack_status_evt_t* pStatus);
static void ble_reconfigTimeout(void * p_context);
static ble_stack_app_api_t  ble_api = {

      .statusUpdated_handler   = skd_ble_statusUpdatedHandler, 
      .dataReceived_handler    = skd_ble_dataReceivedHandler,
       
    };    

static ble_stack_config_t ble_config = { NULL };

/*--------------------------GEO-SERVICE-HANDLERS----------------------------------*/
static void geo_statusUpdateCb (geo_status_t    newStatus); 
static void goe_TimeThresholdDetectedCb(skd_gnss_data_t* pNewData, skd_timestamp_t* pTimeStamp);
static void goe_DistanceThresholdDetectedCb(skd_gnss_data_t* pNewData, skd_timestamp_t* pTimeStamp);  
static void goe_AngleThresholdDetectedCb(skd_gnss_data_t* pNewData, skd_timestamp_t* pTimeStamp);
static void geo_geozoneEnteredCb(uint8_t geozoneId); 
static void geo_geozoneExitedCb(uint8_t geozoneId); 
static skd_geo_api_t         geo_api    = { 
                                        .geo_statusUpdateCb              = geo_statusUpdateCb,
                                        .goe_TimeThresholdDetectedCb     = goe_TimeThresholdDetectedCb,
                                        .goe_DistanceThresholdDetectedCb = goe_DistanceThresholdDetectedCb,
                                        .goe_AngleThresholdDetectedCb    = goe_AngleThresholdDetectedCb,
                                        .geo_geozoneEnteredCb            = geo_geozoneEnteredCb,
                                        .geo_geozoneExitedCb             = geo_geozoneExitedCb

                                       }; 

/*--------------------------TIME-SERVICE-HANDLERS----------------------------------*/
static void skd_ts_bleOffIntrevalEnteredCb();
static void skd_ts_bleOnIntrevalEnteredCb();
static void skd_ts_gnssOffIntrevalEnteredCb();
static void skd_ts_gnssOnIntrevalEnteredCb();
static void skd_ts_synchronizeTimeCb(skd_timestamp_t* pCrntTime); 
static skd_time_api_t    time_srv_api     = { 
                         
                           .bleOffIntrevalEnteredCb    = skd_ts_bleOffIntrevalEnteredCb, 
                           .bleOnIntrevalEnteredCb     = skd_ts_bleOnIntrevalEnteredCb,
                           .gnssOffIntrevalEnteredCb   = skd_ts_gnssOffIntrevalEnteredCb,
                           .gnssOnIntrevalEnteredCb    = skd_ts_gnssOnIntrevalEnteredCb, 
                           .synchronizeTimeCb          = skd_ts_synchronizeTimeCb 
                          
                          };

/*--------------------------PW-SERVICE-HANDLERS-------------------------------------*/
static void updateBatterLevelCb(uint16_t batterLevelmV);
static skd_pw_srv_api_t pwApi = {

        .updateBatterLevelCb =  updateBatterLevelCb,
    };  
/*--------------------------BUTTON-SERVICE-HANDLERS---------------------------------*/
static void buttonPressinEvntCb(skd_but_msg_type evntId);
static skd_but_srv_api_t butApi = {
       
       .evntDetectedCb = buttonPressinEvntCb

    };  
/*--------------------------MOTION-DETECTION-SERVICE-HANDLERS-----------------------*/
static void startOfMotionDetectedCb();
static void endOfMotionDetectedCb();
static void longStopDetectedCb();

static skd_md_srv_api_t mdApi = {

          .startOfMotionDetectedCb = startOfMotionDetectedCb,
          .endOfMotionDetectedCb   = endOfMotionDetectedCb,
          .longStopDetectedCb      = longStopDetectedCb

    };  


/*--------------------------DB-DATA-TRANSFER-HANDLERS--------------------------------*/
static void                       skd_db_transferOverBleCompleteCb (skd_db_transfer_status_t status);
static skd_db_transfer_status_t   skd_db_sendDataOverBleCb  ( uint8_t* pData, 
                                                              uint16_t dataLen); 
 

static skd_db_transfer_api_t bleTrfApi = {
      

      .transferCompleteCb = skd_db_transferOverBleCompleteCb,
      .sendDataCb         = skd_db_sendDataOverBleCb,
  
}; 
//*/

/*--------------------------SHA-KED-CONFIG-AND-PARAMETERS---------------------------*/
static skd_device_status_t          skd_crntStatus    = {NULL}; 
static skd_device_info_t            skd_devInfo       = {NULL}; 
static skd_timestamp_t              skd_crntTime      = {NULL};  
static skd_settings_t               skd_crntSettings  = {NULL};  

static sha_ked_app_api_t crntApi = {NULL}; 



static void skd_app_keepBLESettings(){

      app_timer_stop(bleReconfigTimerId);
   
}
static void skd_app_runBLESafe(uint32_t timeoutMS){
  
    ble_stack_deInit();
    skd_ble_mode_t* pMode = &ble_config.mode;  
     
    skd_set_safe_bleSettings(&ble_config);
    ble_stack_init(&ble_config, &ble_api);
    
    app_timer_start( bleReconfigTimerId, 
                     APP_TIMER_TICKS( timeoutMS*1000 ),
                     NULL );

    

} 

static void skd_app_saveSnapshot(uint8_t evtType){
  
  skd_snapshot_t        snapshot   = {NULL};
  skd_device_status_t*  pDevStatus = &snapshot.devStatus;


  // TO DO: Add savving of battery level. 
  snapshot.evtId = evtType;
  skd_geo_srv_get_crntGEO( &pDevStatus->gnssData, NULL);
  
  skd_gnss_data_t* pGnss_data = &pDevStatus->gnssData;
  
  if(!skd_geo_srv_isSatellitesFound()) return;

  if(skd_geo_srv_isSatellitesFound() ) skd_geo_srv_get_crntTime( &snapshot.timeStamp  );
  else                                 skd_time_srv_getTime    (  &snapshot.timeStamp ); 
  
  skd_pwr_srv_getBattery(&pDevStatus->batteryLevelVolt);  
  pDevStatus->batteryLevelVolt *= 10; //Change it to mV
  
  //skd_debug_printSnapShot(&snapshot);

  skd_db_write_record(&snapshot);
  

}  


static void skd_db_transferOverBleCompleteCb(skd_db_transfer_status_t status){
  
  cmd_srv_emtpy_rsp_t rsp = {NULL};

  switch(status){
    case TRANSFER_COMPLITED_SUCCESS: {

      NRF_LOG_INFO("The database transfer over ble completed successufuly.");
      
      rsp.rsp_id      = GET_DB_RECORDS_RSP; 
      rsp.cmd_status  = CMD_DB_TRANSFER_FINISHED; 
      
      nrf_delay_ms(200); //Small delay to process last noitficaton
 
      ble_stack_sendInfo( (uint8_t *)&rsp, sizeof(rsp));

    } break; 
    case TRANSFER_ABORTED:{
    
        NRF_LOG_ERROR("The database transfer over ble aborted with error.");

        rsp.rsp_id      = GET_DB_RECORDS_RSP; 
        rsp.cmd_status  = CMD_DB_TRANSFER_FAILED; 
      
        ble_stack_sendInfo( (uint8_t *)&rsp, sizeof(rsp));
  
    }break;
    case TRANSFER_ABORTED_BY_USER:{
    
        NRF_LOG_INFO("User aborted the data transfer."); 

    }

  }  

  //Turn back goe serivce if not mode is not Motion Detected and no motion detected.
   
  skd_gnss_pwr_mode_t* pMode = &skd_crntSettings.gnnsSettings.powerMode; 
  
  if( pMode->WFMT && !skd_md_srv_isMotionDetected() ) return; 

  skd_geo_srv_init(&skd_crntSettings.gnnsSettings, &geo_api);
 
}


static skd_db_transfer_status_t   skd_db_sendDataOverBleCb  ( uint8_t* pData, 
                                                          uint16_t dataLen){
                                                        
  ble_service_error_code ret = ble_stack_sendData(pData, dataLen); 
                            
  if (ret == BS_BUSY )   return TRANSFER_BUSY; 
                                                 
  return TRANSFER_COMPLITED_SUCCESS;                                
}

void sha_ked_delay_ms(uint32_t delayMs){

  nrf_delay_ms(delayMs); 

} 

void sha_ked_app_init(sha_ked_app_config_t* pConfig, sha_ked_app_api_t* pApi){
    
    uint32_t  ret                       = NULL; 
    uint8_t   addr[BLE_MAC_ADDR_LEN]    = { NULL };

 
    if(pConfig == NULL ) {

      NRF_LOG_DEBUG("Set default device settings."); 
      skd_set_defualt_settings(&skd_crntSettings); 

    }else{
      
      memcpy(&skd_crntSettings, &pConfig->settings, sizeof(skd_settings_t)); 
        
    } 

    NRF_LOG_INFO("Initializing sha_ked application.");
    
    if(pApi != NULL) memcpy(&crntApi, pApi, sizeof(sha_ked_app_api_t)); 

    // Init ble stack
    NRF_LOG_DEBUG("---------------1---------------");
    NRF_LOG_DEBUG("Initializing ble stack");
    NRF_LOG_FLUSH(); 

    app_timer_create( &bleReconfigTimerId, 
                      APP_TIMER_MODE_SINGLE_SHOT,
                      ble_reconfigTimeout );
    
    memcpy(&ble_config, &skd_crntSettings.bleSettings, sizeof(skd_ble_settings_t));
    //ble_stack_init(&skd_crntSettings.bleSettings, &ble_api);
    skd_app_runBLESafe(BLE_RECONFIG_TIMEOUT_SEC); 
    nrf_delay_ms(2); // small delay for stack to init. 
    ble_read_MACaddr(addr); 
    memcpy(skd_devInfo.macAddr, addr, BLE_MAC_ADDR_LEN);   

    NRF_LOG_DEBUG("Initializing ble stack finished");
    NRF_LOG_DEBUG("sha_ked BLE MAC address %02x:%02x:%02x:%02x:%02x:%02x",
                   addr[0], addr[1], addr[2], addr[3], addr[4], addr[5]); 
    
    NRF_LOG_DEBUG("Set default device info");
    mempcpy(&skd_devInfo, &skd_default_devInfo, sizeof(skd_default_devInfo )); 
    memcpy(skd_devInfo.macAddr, addr, BLE_MAC_ADDR_LEN);
        
    NRF_LOG_FLUSH(); 

    // Init command service
    NRF_LOG_DEBUG("---------------2---------------");
    NRF_LOG_DEBUG("Initializing cmd service");
    NRF_LOG_FLUSH(); 
    cmd_srv_config_t   cmd_srv_config = { NULL };
       
    skd_cmd_srv_init(&cmd_srv_config, &cmd_srv_api); 
    
    NRF_LOG_DEBUG("Initializing cmd service finished");
    NRF_LOG_PROCESS(); 
 
    // Note: Don't forget about rest pin and erase all chip
    NRF_LOG_DEBUG("---------------3---------------");
    NRF_LOG_DEBUG("Initializing geo service");
    NRF_LOG_FLUSH();
    
    skd_geo_srv_init(&skd_crntSettings.gnnsSettings, &geo_api);
    
    NRF_LOG_DEBUG("Initializing geo service finished");
    NRF_LOG_PROCESS();
    
    NRF_LOG_DEBUG("---------------4---------------");
    NRF_LOG_DEBUG("Initializing sha ked database");
    NRF_LOG_FLUSH(); 

    ret = skd_db_init(&skd_crntSettings.dbSettings);
    
    NRF_LOG_DEBUG("Initializing sha ked database finished");
    NRF_LOG_PROCESS();

    
    NRF_LOG_DEBUG("---------------5---------------");
    NRF_LOG_DEBUG("Initializing sha ked time service");
    NRF_LOG_FLUSH(); 
                           
    skd_timestamp_t   crntTime         = {NULL}; 
    skd_set_default_time(&crntTime); 

    skd_time_srv_init(&skd_crntSettings.timerSettings, &time_srv_api, &crntTime); 
    
    NRF_LOG_DEBUG("Initializing sha ked time service finished");
    NRF_LOG_PROCESS();

    NRF_LOG_DEBUG("---------------6---------------");
    NRF_LOG_DEBUG("Initializing sha ked led service");
    NRF_LOG_FLUSH(); 
    
    skd_led_srv_init(&skd_crntSettings.ledSettings); 

    NRF_LOG_DEBUG("Initializing sha ked led service finished");
    NRF_LOG_PROCESS();

    
    NRF_LOG_DEBUG("---------------7---------------");
    NRF_LOG_DEBUG("Initializing sha ked button service");
    NRF_LOG_FLUSH(); 
   
    skd_but_srv_init( &skd_crntSettings.butSettings, &butApi); 
    skd_but_srv_deInit();
    skd_but_srv_init( &skd_crntSettings.butSettings, &butApi); 

    NRF_LOG_DEBUG("Initializing sha ked button service finished");
    NRF_LOG_PROCESS();

    NRF_LOG_DEBUG("---------------8---------------");
    NRF_LOG_DEBUG("Initializing sha ked power service");
    NRF_LOG_FLUSH(); 
   
    skd_pw_srv_init( NULL, &pwApi); 

    NRF_LOG_DEBUG("Initializing sha ked power service finished");
    NRF_LOG_PROCESS();


    NRF_LOG_DEBUG("---------------9---------------");
    NRF_LOG_DEBUG("Initializing sha ked motion detection service");
    NRF_LOG_FLUSH(); 
   
    skd_md_srv_init( &skd_crntSettings.msSettings, &mdApi); 
    
    NRF_LOG_DEBUG("Initializing sha ked motion detection finished");
    NRF_LOG_PROCESS();

    
    NRF_LOG_DEBUG("Data base has %d records", skd_db_read_number_of_records() );
    NRF_LOG_PROCESS();


    if(skd_db_read_number_of_records() == 0 && pConfig->SystemBootCounter == 1 ){
      
      skd_db_clean_soft();
      NRF_LOG_WARNING("First lanch and empty database detected!");

      NRF_LOG_WARNING("Set default time");
      memcpy(&skd_crntTime.date,  &skd_default_data, sizeof( skd_default_time ));  
      memcpy(&skd_crntTime.time,  &skd_default_time, sizeof( skd_default_data ));    

      skd_snapshot_t default_record = {NULL};  
      skd_set_default_record(&default_record); 
      
  
      NRF_LOG_WARNING("Writing %d test records to local database", NUMBER_OF_TEST_RECORDS_IN_DB);
      NRF_LOG_WARNING("A record size is %d byte", sizeof(skd_snapshot_t));
      NRF_LOG_FLUSH();

      for(int i = 0; i < NUMBER_OF_TEST_RECORDS_IN_DB; i++) {  

        //NRF_LOG_INFO("Write %d record", i ); 
        skd_db_write_record(&default_record);

      }
      
      NRF_LOG_DEBUG("Writting finished");
      NRF_LOG_FLUSH();
      

    }

    skd_app_saveSnapshot(SHA_KED_POWER_ON); 
} 

void sha_ked_app_deInit(){
    
    // Init ble stack
    NRF_LOG_DEBUG("---------------1---------------");
    NRF_LOG_DEBUG("Deinitializing ble stack");
    NRF_LOG_FLUSH(); 
   
    ble_stack_deInit(); 

    // Init command service
    NRF_LOG_DEBUG("---------------2---------------");
    NRF_LOG_DEBUG("Deinitializing cmd service");
    NRF_LOG_FLUSH(); 
       
    skd_cmd_srv_deInit(); // Need to be implemented
    
    NRF_LOG_DEBUG("Deinitializing cmd service finished");
    NRF_LOG_PROCESS(); 
 
    // Note: Don't forget about rest pin and erase all chip
    NRF_LOG_DEBUG("---------------3---------------");
    NRF_LOG_DEBUG("Deinitializing geo service");
    NRF_LOG_FLUSH();
    
    skd_geo_srv_deInit(); 
    
    NRF_LOG_DEBUG("Deinitializing geo service finished");
    NRF_LOG_PROCESS();
    
    NRF_LOG_DEBUG("---------------4---------------");
    NRF_LOG_DEBUG("Deinitializing sha ked database");
    NRF_LOG_FLUSH(); 

    skd_db_deInit(); 
    
    NRF_LOG_DEBUG("Deinitializing sha ked database finished");
    NRF_LOG_PROCESS();

    
    NRF_LOG_DEBUG("---------------5---------------");
    NRF_LOG_DEBUG("Deinitializing sha ked time service");
    NRF_LOG_FLUSH(); 
                           
    skd_time_srv_deInit();
    
    NRF_LOG_DEBUG("Deinitializing sha ked time service finished");
    NRF_LOG_PROCESS();

    NRF_LOG_DEBUG("---------------6---------------");
    NRF_LOG_DEBUG("Deinitializing sha ked led service");
    NRF_LOG_FLUSH(); 
    
    skd_led_srv_init(&skd_crntSettings.ledSettings); 

    NRF_LOG_DEBUG("Deinitializing sha ked led service finished");
    NRF_LOG_PROCESS();

    
    NRF_LOG_DEBUG("---------------7---------------");
    NRF_LOG_DEBUG("Deinitializing sha ked button service");
    NRF_LOG_FLUSH(); 
   
    skd_but_srv_deInit();

    NRF_LOG_DEBUG("Deinitializing sha ked button service finished");
    NRF_LOG_PROCESS();

    NRF_LOG_DEBUG("---------------8---------------");
    NRF_LOG_DEBUG("Deinitializing sha ked power service");
    NRF_LOG_FLUSH(); 
   
    skd_pw_srv_deInit(); //Need to be implemented

    NRF_LOG_DEBUG("Deinitializing sha ked power service finished");
    NRF_LOG_PROCESS();


    NRF_LOG_DEBUG("---------------9---------------");
    NRF_LOG_DEBUG("Deinitializing sha ked motion detection service");
    NRF_LOG_FLUSH(); 
   
    skd_md_srv_deInit(); 
    
    NRF_LOG_DEBUG("Deinitializing sha ked motion detection finished");
    NRF_LOG_PROCESS();

}


void sha_ked_app_goDFU(){
 
  skd_set_default_bleSettings(&ble_config);
  ble_stack_init(&ble_config, &ble_api);

}
/**@brief This function notifies application of changing in ble stack.
 *
 * @details Receive only simple event such as device connected/disconnected and 
 *          so on.
 *          
 * @param[in] pStatus       New ble status
 */

static void skd_ble_statusUpdatedHandler(ble_stack_status_evt_t* pStatus){

  switch(pStatus->type){
  case DEVICE_CONNECTED:{
    
    
    NRF_LOG_INFO("Sha_ked got connected. Keep current BLE settings."); 
    skd_app_saveSnapshot(MOBILE_APP_CONNTECTED_OVER_BT_EVNT_ID);

    skd_app_keepBLESettings();
     

  }
  break; 
  case DEVICE_DISCONNECTED:{
    NRF_LOG_INFO("Sha_ked got disconnected. Reconfig ble to safe settings ");  
  
    skd_app_saveSnapshot(MOBILE_APP_DISCONNTECTED_OVER_BT_EVNT_ID);

    //Stop db transfer if any processed
    skd_db_trf_deInit();
    
    skd_app_runBLESafe(BLE_RECONFIG_TIMEOUT_SEC); 

  }
  break; 
  default:

    //NRF_LOG_DEBUG("ble stack updated its status %d", pStatus->type);
  
  break; 
  }
   
} 

/**@brief This function handles  data  coming from extrenal ble device (mobile phone). 
 *     
 * @param[in] p_data       pointer to data buffer
 * @param[in] dataLen      data len
 */
static void skd_ble_dataReceivedHandler(uint8_t* p_data, uint8_t dataLen){

  //NRF_LOG_DEBUG("%d bytes arrived from mobile phone Application", dataLen);
  uint8_t        packLen    = dataLen;  
  cmd_srv_pack_t cmd        = {NULL}; 
  cmd_status_t   ret        =  NULL; 
   
  if( dataLen > sizeof(cmd_srv_pack_t) )  {
  
      cmd_srv_emtpy_rsp_t rsp = {NULL};

      rsp.rsp_id     = SYSTEM_CMD_RSP;
      rsp.cmd_status = CMD_TO_LONG;

      ble_stack_sendInfo( (uint8_t *)&rsp, sizeof(rsp)); 
      
      return; 
  }

  //Copy data to cmd. 
  memcpy(&cmd, p_data, packLen);
  
  cmd.cmd_len = packLen; 
  // Send message to command processor
  ret = skd_cmd_sendCmd(&cmd);


  // Send response if queue is busy
  if( ret == CMD_QUEUE_FULL){
    
      cmd_srv_emtpy_rsp_t rsp = {NULL};

      rsp.rsp_id     = SYSTEM_CMD_RSP;
      rsp.cmd_status = CMD_QUEUE_FULL;

      ble_stack_sendInfo( (uint8_t *)&rsp, sizeof(rsp)); 
      
      return; 
  
  }
   
   
} 


void sha_ked_app_process(){

    //NRF_LOG_INFO("Processing");
    skd_cmd_srv_process(); 
    skd_geo_srv_process(); 
    skd_db_trf_process();
    skd_time_srv_process();
    skd_but_srv_process();
    skd_md_srv_process();
    skd_pw_srv_process();
    ble_stack_process();

}

/** CMD LINE processing routine.  
 *  @NOTE This section contains all function that handles user commands over BLE.   
 * 
 * 
 */

static cmd_status_t enterDFU_Cb(void){
  
  NRF_LOG_INFO("Device firmware update was requested");
  NRF_LOG_PROCESS(); 
  
  cmd_status_t     status = DFU_RJECTED; 
  skd_timestamp_t  nextBootTime = {NULL};  
  
  skd_time_srv_getTime(&nextBootTime);

  if(crntApi.requestDFUCb  )      status = crntApi.requestDFUCb(&nextBootTime); 

  NRF_LOG_INFO("Device firmware update was %s", status ? "REJECTED" : "GRANTED");
  NRF_LOG_PROCESS(); 
  return status;
   
}

static cmd_status_t getDevInfo_Cb(skd_device_info_t* pInfo){

  NRF_LOG_DEBUG("User ask device Info"); 
  NRF_LOG_PROCESS(); 
  
  memcpy(pInfo, &skd_devInfo, sizeof(skd_device_info_t));

  return CMD_ACCEPTED;

} 

static cmd_status_t takeSnapShot_Cb( skd_device_status_t* pStatus, skd_timestamp_t* pTime){
  
  NRF_LOG_DEBUG("User wants to take a deviceStatus snapshot");
  NRF_LOG_PROCESS();
   
  skd_geo_srv_get_crntGEO( &pStatus->gnssData, NULL);

  skd_time_srv_getTime(pTime); 
  
  skd_pwr_srv_getBattery(&pStatus->batteryLevelVolt);  
  pStatus->batteryLevelVolt *= 10; //Change it to mV


  // memcpy(pStatus, &skd_crntStatus, sizeof(skd_device_status_t));
  //skd_debug_printDevStatus(pStatus);
  //skd_debug_printDate(&pTime->date);
  //skd_debug_printTime(&pTime->time); 
    

  return CMD_ACCEPTED;
}

static cmd_status_t getBleSettings_Cb (skd_ble_settings_t*    pSettings){

  NRF_LOG_DEBUG("User ask for BLE settings "); 
  NRF_LOG_PROCESS(); 
  

  memcpy(pSettings, &skd_crntSettings.bleSettings, sizeof(skd_ble_settings_t) );
  skd_debug_printBLEsettings(pSettings);

  return CMD_ACCEPTED;

} 

static cmd_status_t  setBleSettings_Cb (skd_ble_settings_t*    pSettings){
  
  cmd_status_t status = CMD_ACCEPTED;

  NRF_LOG_DEBUG("User wants to set BLE settings"); 
  NRF_LOG_PROCESS(); 
  
  memcpy( &skd_crntSettings.bleSettings,  pSettings, sizeof(skd_ble_settings_t));
  skd_debug_printBLEsettings(pSettings);
  

  if(crntApi.updateBLESettingsCb) status = crntApi.updateBLESettingsCb( pSettings ); 

  if(status != CMD_ACCEPTED) return status; 

  //Config set only after disconnect.
  return CMD_ACCEPTED;

}

static cmd_status_t getGNSSSettings_Cb(skd_gnss_settings_t*   pSettings){
  
  

  NRF_LOG_DEBUG("User ask for gnss settings"); 
  NRF_LOG_PROCESS(); 
  

  memcpy(pSettings, &skd_crntSettings.gnnsSettings, sizeof(skd_gnss_settings_t) ); 
  skd_debug_printGNSSsettings(pSettings); 

  
  return CMD_ACCEPTED;
  
}

static cmd_status_t  setGNSSSettings_Cb (skd_gnss_settings_t*   pSettings){
  
  cmd_status_t status = CMD_ACCEPTED;
  NRF_LOG_DEBUG("User wants to set gnss settings "); 
  NRF_LOG_PROCESS(); 

  memcpy( &skd_crntSettings.gnnsSettings,  pSettings, sizeof(skd_gnss_settings_t));
  skd_debug_printGNSSsettings(pSettings);
  
  if(crntApi.updateGNSSSettingsCb) status = crntApi.updateGNSSSettingsCb( pSettings ); 
   
  if(status != CMD_ACCEPTED) return status; 

  skd_geo_srv_deInit(); 
  skd_geo_srv_init(pSettings, &geo_api);

  return CMD_ACCEPTED;

}

static cmd_status_t  getMSSettings_Cb  (skd_ms_settings_t*     pSettings){
  
  NRF_LOG_DEBUG("User ask for a motion sensor settings "); 
  NRF_LOG_PROCESS(); 

  memcpy(pSettings, &skd_crntSettings.msSettings, sizeof(skd_ms_settings_t) );    
  skd_debug_printMSsettings(pSettings); 

  return CMD_ACCEPTED;

}

static cmd_status_t  setMSSettings_Cb (skd_ms_settings_t*     pSettings){
  
  cmd_status_t status = CMD_ACCEPTED;
  
  NRF_LOG_DEBUG("User wants to set a motion sensor settings"); 
  NRF_LOG_PROCESS(); 

  memcpy( &skd_crntSettings.msSettings,  pSettings, sizeof(skd_ms_settings_t));
  skd_debug_printMSsettings(pSettings);   
  
  if(crntApi.updateMSSettingsCb) status = crntApi.updateMSSettingsCb( pSettings ); 
   
  if(status != CMD_ACCEPTED) return status; 

  skd_md_srv_deInit();
  skd_md_srv_init(pSettings, &mdApi);  
  return CMD_ACCEPTED;
}

static cmd_status_t  getTimerSettings_Cb (skd_timer_settings_t*  pSettings){

  NRF_LOG_DEBUG("User ask for a timer settings"); 
  NRF_LOG_PROCESS(); 

  memcpy(pSettings, &skd_crntSettings.timerSettings, sizeof(skd_timer_settings_t) );
  //skd_debug_printTIMERsettings(pSettings);  
   
  return CMD_ACCEPTED;

}

static cmd_status_t  setTimerSettings_Cb( skd_timer_settings_t*  pSettings){
  
  cmd_status_t status = CMD_ACCEPTED;
  
  NRF_LOG_DEBUG("User wants to set a timer settings "); 
  NRF_LOG_PROCESS(); 

  skd_time_mode_t* pMode = &pSettings->mode; 

  //if(pMode->DBTZ) return CMD_MODE_NOT_SUPPORTED; 

  memcpy( &skd_crntSettings.timerSettings,  pSettings, sizeof(skd_timer_settings_t));
  skd_debug_printTIMERsettings(pSettings);
  
  if(crntApi.updateTimeSettingsCb) status = crntApi.updateTimeSettingsCb( pSettings ); 
   
  if(status != CMD_ACCEPTED) return status; 

  skd_timestamp_t   crntTime         = {NULL}; 
  
  skd_time_srv_getTime(&crntTime);
  
  skd_time_srv_deInit();                          
    
  //skd_set_default_time(&crntTime); 

  skd_time_srv_init(&skd_crntSettings.timerSettings, &time_srv_api, &crntTime); 

  return CMD_ACCEPTED;

}

static  cmd_status_t getTime_Cb( skd_timestamp_t* pTime ){

  NRF_LOG_DEBUG("User ask time");
  NRF_LOG_PROCESS();
  
  //memcpy(pTime, &skd_crntTime, sizeof(skd_timestamp_t));
  skd_time_srv_getTime(pTime); 
  skd_debug_printDate(&pTime->date);
  skd_debug_printTime(&pTime->time); 

  return CMD_ACCEPTED;
}

static  cmd_status_t setTime_Cb( skd_timestamp_t* pTime ){

  NRF_LOG_DEBUG("User wants to set time");
  NRF_LOG_PROCESS(); 
  
  skd_time_srv_setTime(pTime);

  skd_debug_printDate(&pTime->date);
  skd_debug_printTime(&pTime->time); 
  

  return CMD_ACCEPTED;

}

static cmd_status_t  getButtonSettings_Cb( skd_button_settings_t* pSettings){

  NRF_LOG_DEBUG("User ask for a button settings "); 
  NRF_LOG_PROCESS(); 

  memcpy(pSettings, &skd_crntSettings.butSettings, sizeof(skd_button_settings_t) );
  //skd_debug_printBUTsettings(pSettings);
  
  return CMD_ACCEPTED;

} 

static cmd_status_t  setButtonSettings_Cb( skd_button_settings_t* pSettings){
  
  cmd_status_t status = CMD_ACCEPTED;
  
  NRF_LOG_DEBUG("User wants to set a button settings "); 
  NRF_LOG_PROCESS(); 

  memcpy( &skd_crntSettings.butSettings,  pSettings, sizeof(skd_button_settings_t));
  //skd_debug_printBUTsettings(pSettings);

  if(crntApi.updateButSettingsCb) status = crntApi.updateButSettingsCb( pSettings ); 
   
  if(status != CMD_ACCEPTED) return status; 

  skd_but_srv_deInit();
  skd_but_srv_init(pSettings, &butApi); 

  return CMD_ACCEPTED;
}

static cmd_status_t  getLedSettings_Cb( skd_led_settings_t* pSettings){

  NRF_LOG_DEBUG("User ask for a LED settings "); 
  NRF_LOG_PROCESS(); 

  memcpy(pSettings, &skd_crntSettings.ledSettings, sizeof(skd_led_settings_t) );
  //skd_debug_printLEDSettings(pSettings);

  return CMD_ACCEPTED;

} 

static cmd_status_t  setLedSettings_Cb( skd_led_settings_t* pSettings){
  
  cmd_status_t status = CMD_ACCEPTED;
  
  NRF_LOG_DEBUG("User wants to set a LED settings "); 
  NRF_LOG_PROCESS(); 

  memcpy( &skd_crntSettings.ledSettings,  pSettings, sizeof(skd_led_settings_t));
  //skd_debug_printLEDSettings(pSettings);

  if(crntApi.updateLEDSettingsCb) status = crntApi.updateLEDSettingsCb( pSettings ); 
   
  if(status != CMD_ACCEPTED) return status; 
  
  skd_led_srv_deInit();
  skd_led_srv_init(&skd_crntSettings.ledSettings); 
   
  return CMD_ACCEPTED;
}

static cmd_status_t  getDBSettings_Cb( skd_db_settings_t* pSettings){

  NRF_LOG_DEBUG("User ask for a DB settings "); 
  NRF_LOG_PROCESS(); 

  memcpy(pSettings, &skd_crntSettings.dbSettings, sizeof(skd_db_settings_t) );
  //skd_debug_printDBsettingds(pSettings);
  


  return CMD_ACCEPTED;

} 

static cmd_status_t  setDBSettings_Cb( skd_db_settings_t* pSettings){
  
  cmd_status_t status = CMD_ACCEPTED; 
  
  NRF_LOG_DEBUG("User wants to set a DB settings "); 
  NRF_LOG_PROCESS(); 

  memcpy( &skd_crntSettings.dbSettings,  pSettings, sizeof(skd_db_settings_t));
  //skd_debug_printDBsettingds(pSettings);
  
  if(crntApi.updateDBSettingsCb) status = crntApi.updateDBSettingsCb( pSettings ); 
   
  if(status != CMD_ACCEPTED) return status; 

  return CMD_ACCEPTED;
}

static cmd_status_t getDBnumberOfRecrods_Cb (uint32_t* DBnumberOfRecrods){

  NRF_LOG_DEBUG("User ask databese size");
  NRF_LOG_PROCESS();
  
  *DBnumberOfRecrods = skd_db_read_number_of_records();

  //*DBnumberOfRecrods = 100000;

  return CMD_ACCEPTED;
}
 
static cmd_status_t getDBrecord_Cb(uint32_t recordId, skd_snapshot_t* pRecord){

  NRF_LOG_DEBUG("User wants to read the record %d", recordId);
  NRF_LOG_PROCESS();
  
  if(recordId  > skd_db_read_number_of_records()) return CMD_ERROR_INVALID_PARAMETERS; 

  skd_db_read_record(recordId, pRecord); 
  
  //skd_debug_printSnapShot(pRecord);

  return CMD_ACCEPTED;
} 


static cmd_status_t getDBrecords_Cb(uint32_t strId, uint32_t len){

  NRF_LOG_DEBUG("User aks data set start with %d to %d", strId,  strId + len);
  NRF_LOG_PROCESS();

  //Checking
  uint32_t db_size = skd_db_read_number_of_records();
  
  //db_size = 100000; 

  if( strId > db_size)                           return CMD_ERROR_INVALID_PARAMETERS;
  if( strId + len > db_size )                    return CMD_ERROR_INVALID_PARAMETERS; 


  skd_geo_srv_deInit(); 
  skd_db_transfer_status_t ret = skd_db_trf_init(strId, len, &bleTrfApi);   

  if( ret == TRANSFER_BUSY) return CMD_ERROR_DEVICE_BUSY;

  return CMD_ACCEPTED;
}


static cmd_status_t cleanDB_soft_Cb(){

  NRF_LOG_DEBUG("User wants to do DB soft erase");
  NRF_LOG_PROCESS();
  
  skd_db_clean_soft(); 

  return CMD_ACCEPTED;
 
}

static cmd_status_t cleanDB_hard_Cb(){

  NRF_LOG_DEBUG("User wants to do DB hard erase");
  NRF_LOG_PROCESS();
  
  skd_db_clean_hard(); 
  
  //TO DO:  do it with protection/safely!!! 
  //sd_nvic_SystemReset();

  return CMD_ACCEPTED;

} 

/** GOE service processing routine.  
 *  @NOTE This section contains all function that handles user commands over BLE.   
 *  
 */


static void geo_statusUpdateCb (geo_status_t    newStatus){
  skd_timestamp_t crntTime = {NULL};

  switch(newStatus){
    case SATELLITES_LOST: 
    
        NRF_LOG_INFO("Satellites are lost. No signal from satellites");  
        skd_app_saveSnapshot(SATELITES_SEARCHING_EVNT_ID);

    break;
    case SATELLITES_FOUND: 
        
        NRF_LOG_INFO("Satellites are found");
        skd_app_saveSnapshot(SATELITTES_FOUND_EVNT_ID);
        
        skd_time_srv_getTime(&crntTime);
        skd_ts_synchronizeTimeCb(&crntTime);
          
    break;
    case SEARCHING_FOR_SATELLITES:
       
       NRF_LOG_INFO("Searching for satellites");

    break;
    case NO_DATA_FROM_HW:
    
        NRF_LOG_INFO("Hardware malfunctioned. No data from hardware");  

    break; 
  }  
  

}

static void goe_TimeThresholdDetectedCb(skd_gnss_data_t* pNewData, skd_timestamp_t* pTimeStamp){
    
  NRF_LOG_INFO("Time threshold detected"); 
  NRF_LOG_FLUSH(); 
  
  skd_snapshot_t    record  = {NULL}; 
  
  //Update current status 
  memcpy(&skd_crntStatus.gnssData, pNewData, sizeof(skd_gnss_data_t));
  
  skd_app_saveSnapshot(TIME_THRESHOLD_DETECED_EVNT_ID);

}

static void goe_DistanceThresholdDetectedCb(skd_gnss_data_t* pNewData, skd_timestamp_t* pTimeStamp){
  
  NRF_LOG_INFO("Distance threshold detected"); 
  NRF_LOG_FLUSH(); 
  
  skd_snapshot_t    record  = {NULL}; 
  
  //Update current status 
  memcpy(&skd_crntStatus.gnssData, pNewData, sizeof(skd_gnss_data_t));
  
  skd_app_saveSnapshot(DISTANCE_THRESHOLD_DETECED_EVNT_ID);

} 

static void goe_AngleThresholdDetectedCb(skd_gnss_data_t* pNewData, skd_timestamp_t* pTimeStamp){
  
  NRF_LOG_INFO("Angle threshold detected"); 
  NRF_LOG_FLUSH(); 
  

  skd_snapshot_t    record  = {NULL}; 
  
  //Update current status 
  memcpy(&skd_crntStatus.gnssData, pNewData, sizeof(skd_gnss_data_t));
  

  skd_app_saveSnapshot(ANGLE_THRESHOLD_DETECED_EVNT_ID);


}

static void geo_geozoneEnteredCb(uint8_t geozoneId){

  NRF_LOG_INFO("Geozone %d entered", geozoneId); 
  NRF_LOG_FLUSH(); 
  
  skd_snapshot_t    record    = {NULL}; 
  skd_gnss_data_t   gnnsData  = {NULL};
  skd_timestamp_t   timestamp = {NULL}; 
   
  skd_geo_srv_get_crntGEO(&gnnsData,& timestamp); 
  //Update current status 
  memcpy(&skd_crntStatus.gnssData, &gnnsData, sizeof(skd_gnss_data_t));
  

  skd_app_saveSnapshot(GEOZONE_DETECED_EVNT_ID);

}

static void geo_geozoneExitedCb(uint8_t geozoneId){
  
  NRF_LOG_INFO("Geozone %d exited", geozoneId); 
  NRF_LOG_FLUSH(); 

  skd_snapshot_t    record    = {NULL}; 
  skd_gnss_data_t   gnnsData  = {NULL};
  skd_timestamp_t   timestamp = {NULL}; 
   
  skd_geo_srv_get_crntGEO(&gnnsData, &timestamp); 
  //Update current status 
  memcpy(&skd_crntStatus.gnssData, &gnnsData, sizeof(skd_gnss_data_t));
  
  skd_app_saveSnapshot(GEOZONE_DETECED_EVNT_ID);
  

}  

static void ble_reconfigTimeout(void * p_context){
  
    NRF_LOG_INFO("Reconfig ble to new settings");  
    NRF_LOG_PROCESS();
    
    ble_stack_deInit();

    memcpy(&ble_config, &skd_crntSettings.bleSettings, sizeof(skd_ble_settings_t));
    ble_stack_init(&ble_config, &ble_api);

} 


static void skd_ts_bleOffIntrevalEnteredCb(){

    NRF_LOG_INFO("BLE OFF interval entered.");  
    NRF_LOG_PROCESS();
    skd_ble_settings_t* pSettings = &skd_crntSettings.bleSettings; 
    skd_ble_mode_t*     pMode      = &pSettings->mode;
    
    if(ble_stack_isConnected()) return; // Do nothing if connection establsihed

    if(pMode->WFTI) {

        NRF_LOG_INFO("Shut down ble stack"); 
        skd_app_saveSnapshot(BT_TURNED_OFF_EVNT_ID);
        ble_stack_deInit();       

    }  
}

static void skd_ts_bleOnIntrevalEnteredCb(){

    NRF_LOG_INFO("BLE ON interval entered.");  
    NRF_LOG_PROCESS();
    
    skd_ble_settings_t* pSettings = &skd_crntSettings.bleSettings; 
    skd_ble_mode_t*     pMode      = &pSettings->mode;
    
    if(ble_stack_isConnected()) return; // Do nothing if connection establsihed
      
    if(pMode->WFTI) {
      
      NRF_LOG_INFO("turn on BLE statck."); 
      skd_app_saveSnapshot(BT_TURNED_ON_EVNT_ID);
      ble_stack_init(&ble_config, &ble_api);   
    
    }
}


static void skd_ts_gnssOffIntrevalEnteredCb(){

    NRF_LOG_INFO("GNNSS OFF interval entered.");  
    NRF_LOG_PROCESS();
    skd_gnss_settings_t* pSettings  = &skd_crntSettings.gnnsSettings;  
    skd_gnss_pwr_mode_t* pMode      = &pSettings->powerMode;

    if(pMode->WFTI){

       NRF_LOG_INFO("Shut down geo service"); 
       skd_geo_srv_deInit(); 
       
    }  
    
}

static void skd_ts_gnssOnIntrevalEnteredCb(){

    NRF_LOG_INFO("GNNS ON interval entered.");  
    NRF_LOG_PROCESS();

    skd_gnss_settings_t* pSettings = &skd_crntSettings.gnnsSettings;  
    skd_gnss_pwr_mode_t* pMode      = &pSettings->powerMode;
    


    if(pMode->WFTI){

       NRF_LOG_INFO("Turn on geo service"); 
       skd_geo_srv_init(pSettings, &geo_api);
       
    }  
    
}

static void skd_ts_synchronizeTimeCb(skd_timestamp_t* pCrntTime){
  
  NRF_LOG_INFO("Time servie wants synchronize time");
  NRF_LOG_PROCESS();
  int ret; 

  skd_timestamp_t goeTime = {NULL}; 
  
  if(!skd_geo_srv_isSatellitesFound()) return; 

  skd_geo_srv_get_crntTime(&goeTime);   
  
  /*
  NRF_LOG_INFO("Current time serivce time is")
  skd_debug_printDate(&pCrntTime->date);
  skd_debug_printTime(&pCrntTime->time); 
  
  NRF_LOG_INFO("Current GNSS serivce time is")
  skd_debug_printDate(&goeTime.date);
  skd_debug_printTime(&goeTime.time); 
  //*/

  ret = memcmp(&pCrntTime->date, &goeTime.date, sizeof(skd_data_t));
  
  if( ret ){

      NRF_LOG_INFO("Date desynchronized. Settings GNSS time"); 
      skd_time_srv_setTime(&goeTime);
      return; 
  } 
  
  pCrntTime->time.microseconds =   goeTime.time.microseconds; 
  ret = memcmp(&pCrntTime->time, &goeTime.time, sizeof(skd_time_t));

  if( ret ){

      NRF_LOG_INFO("Time desynchronized. Settings GNSS time"); 
      skd_time_srv_setTime(&goeTime);
      return; 
  } 
  
} 


static void updateBatterLevelCb(uint16_t batterLevelCentV){
  
    NRF_LOG_INFO("New battery level is %d centVolt", batterLevelCentV);
    skd_crntStatus.batteryLevelVolt = batterLevelCentV*10;
     
}  


static void buttonPressinEvntCb(skd_but_msg_type evntId){
  
  switch(evntId){

  case BUT_SINGLE_PRESS:{
    
    NRF_LOG_INFO("Single press event detected."); 

    skd_ble_mode_t* pMode = &skd_crntSettings.bleSettings.mode;
     
    if(!pMode->WFBT)  break;  
   
  
  }break;
  case BUT_DOUBLE_PRESS:{
  
    NRF_LOG_INFO("Double press event detected."); 
    NRF_LOG_INFO("Run ble safe mode.");
    skd_app_runBLESafe(BLE_RECONFIG_TIMEOUT_SEC);

  }break;
  case BUT_TRIPLE_PRESS:{
  
    NRF_LOG_INFO("Triple press event detected."); 
  
  }break;
  case BUT_LONG_PRESS:{

    NRF_LOG_INFO("Long press event detected."); 

  }break;

  
  }

}


static void startOfMotionDetectedCb(){

  NRF_LOG_INFO("Start of motion detected");
  skd_gnss_pwr_mode_t* pMode = &skd_crntSettings.gnnsSettings.powerMode;
  
  //skd_led_settings_t temp = { .mode = LED_ALLWAYS_ON}; 
  //skd_led_srv_init(&temp);

  skd_app_saveSnapshot(START_OF_MOVEMENT_DETECTED_EVNT_ID);

  if(pMode->WFMT) skd_geo_srv_init(&skd_crntSettings.gnnsSettings, &geo_api);
}

static void endOfMotionDetectedCb(){

  NRF_LOG_INFO("End of motion detected");
  skd_gnss_pwr_mode_t* pMode = &skd_crntSettings.gnnsSettings.powerMode;
  
  skd_app_saveSnapshot(END_OF_MOVEMENT_DETECTED_EVNT_ID);
  
  //skd_led_settings_t temp = { .mode = LED_ALLWAYS_OFF}; 
  //skd_led_srv_init(&temp);
   
  if(pMode->WFMT) skd_geo_srv_deInit();

}

static void longStopDetectedCb(){
  
  NRF_LOG_INFO("Long stop detected");

  skd_app_saveSnapshot(SATELITTES_FOUND_EVNT_ID);

  skd_ble_mode_t* pMode = &skd_crntSettings.bleSettings.mode; 
  if(pMode->WFLS) {

    ble_stack_init(&ble_config, &ble_api);

  } 

}