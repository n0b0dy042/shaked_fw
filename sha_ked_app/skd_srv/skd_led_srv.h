#ifndef SKD_LED_SRV_H
#define SKD_LED_SRV_H

#include "sha_ked_common.h"


  
typedef skd_led_settings_t skd_led_config_t; 


void skd_led_srv_init     (skd_led_config_t* pConfig);
void skd_led_srv_deInit   (void); 
void skd_led_srv_process  (void);
void skd_led_srv_blink    (uint32_t durationMs); 


#endif