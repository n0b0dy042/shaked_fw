#ifndef SKD_PW_SRV_H
#define SKD_PW_SRV_H


#include "sha_ked_common.h"



typedef void     (*updateBatterLevelCB_t) (uint16_t batterLevelCentV);

typedef struct{
  
  updateBatterLevelCB_t updateBatterLevelCb; 

}skd_pw_srv_api_t;
 
typedef struct{
  
  uint8_t futureField;

} skd_pw_srv_config_t; 


typedef enum{
  
  UPDATE_BATTER_LEVEL_TIMEOUT
   
} skd_pw_srv_msg_type_t;  


typedef struct{

  uint8_t type; 
  uint8_t context[3];

}skd_pw_srv_msg_t;  

void skd_pw_srv_init          ( skd_pw_srv_config_t* pConfig, skd_pw_srv_api_t* pApi );
void skd_pw_srv_deInit        ( void );
void skd_pw_srv_process       ( void );
void skd_pwr_srv_getBattery   ( uint16_t* pBatteryLevelCentV );


#endif