#ifndef SKD_MD_SRV_H
#define SKD_MD_SRV_H

#include <stdint.h>


#include "sha_ked_common.h"


typedef  skd_ms_settings_t skd_md_srv_config_t; 


typedef enum{
  
  START_OF_MOTION_DETECTED,
  END_OF_MOTIION_DETECTED,
  LONG_STOP_DETECTED

}skd_md_evnt_id_t; 


typedef struct{

  uint8_t evtType;

}skd_md_srv_msg_t;


typedef enum{

  IN_STATIONARY = 0, 
  IN_MOTION

} skd_md_state_t; 



 
typedef void (*startOfMotionDetectedCB_t)      (void);
typedef void (*endOfMotionDetectedCB_t)    (void);      
typedef void (*longStopDetectedCB_t)      (void);      

typedef struct{
  
  startOfMotionDetectedCB_t        startOfMotionDetectedCb;
  endOfMotionDetectedCB_t          endOfMotionDetectedCb;
  longStopDetectedCB_t             longStopDetectedCb;

}skd_md_srv_api_t;  



void skd_md_srv_init              (skd_md_srv_config_t* pConfig, skd_md_srv_api_t* pApi);
void skd_md_srv_process           (void);
void skd_md_srv_deInit            (void); 
bool skd_md_srv_isMotionDetected  (void);

#endif