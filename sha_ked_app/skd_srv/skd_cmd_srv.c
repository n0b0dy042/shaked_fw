#include "skd_cmd_srv.h"
#include "nrf_queue.h"


#include "sha_ked_debug.h" 


#if SHA_KED_CMD_SRV_LOG_ENABLED == 1 
    #define NRF_LOG_LEVEL               SHA_KED_CMD_SRV_LOG_LEVEL
    #define NRF_LOG_INFO_COLOR          SHA_KED_CMD_SRV_INFO_LOG_COLOR
    #define NRF_LOG_DEBUG_COLOR         SHA_KED_CMD_SRV_DEBUG_LOG_COLOR
#else 
    #define NRF_LOG_LEVEL       0
#endif 

#define NRF_LOG_MODULE_NAME     shd_cmd_srv
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
NRF_LOG_MODULE_REGISTER();

static cmd_srv_app_api_t app_api = {NULL};

NRF_QUEUE_DEF(  cmd_srv_pack_t, 
                m_cmd_srv_queue, 
                5, 
                NRF_QUEUE_MODE_NO_OVERFLOW );

NRF_QUEUE_INTERFACE_DEC(cmd_srv_pack_t, 
                    cmd_srv_queue);

NRF_QUEUE_INTERFACE_DEF( cmd_srv_pack_t, 
                         cmd_srv_queue, 
                         &m_cmd_srv_queue);



void skd_cmd_srv_init(cmd_srv_config_t* pConfig, cmd_srv_app_api_t* pApi){

    memcpy(&app_api, pApi, sizeof(cmd_srv_app_api_t)); 

    NRF_LOG_INFO("Initilizing service");
    NRF_LOG_PROCESS();
     
}

void skd_cmd_srv_deInit(){
  
  NRF_LOG_INFO("Deinitilizing service.");
  NRF_LOG_PROCESS();
   
  
}
 
cmd_status_t skd_cmd_sendCmd(cmd_srv_pack_t* pCmd ){
  
    ret_code_t err_code = cmd_srv_queue_push(pCmd);
    
    if(  err_code  == NRF_ERROR_NO_MEM) return CMD_QUEUE_FULL; 

    return CMD_ACCEPTED; 
  
} 

void skd_cmd_sendRsp(uint8_t* pRps, uint16_t  rspLen){

    NRF_LOG_DEBUG("Send %d byte in response", rspLen); 
    if(app_api.sendResp) app_api.sendResp(pRps, rspLen);
     
}

void skd_cmd_srv_process(){
    
    cmd_srv_pack_t new_cmd = {NULL}; 
     
    //check if new  command is  avalible
    if(cmd_srv_queue_is_empty()) return;
    
    //processing new commnad
    ret_code_t err_code = cmd_srv_queue_pop(&new_cmd);
    APP_ERROR_CHECK(err_code);
  

    NRF_LOG_INFO("Processing command 0x%02x", new_cmd.cmd_id); 
    NRF_LOG_PROCESS(); 

    switch(new_cmd.cmd_id){
        case ENTER_DFU:
        {
          rsp_enter_dfu_t  rsp     = {NULL};
          cmd_enter_dfu_t* pCmd = (cmd_enter_dfu_t* )&new_cmd;
          
          rsp.rsp_id = ENTER_DFU_RSP;
          if(app_api.enterDFU) rsp.cmd_status = app_api.enterDFU(); 

          skd_cmd_sendRsp((uint8_t*)&rsp, sizeof(rsp));           
        
        }
        break; 
        case GET_DEVICE_INFO:
        { 

          rsp_get_device_info_t  rsp= {NULL}; 
          cmd_get_device_info_t* pCmd = (cmd_get_device_info_t*)&new_cmd;
          
          rsp.rsp_id      = GET_DEVICE_INFO_RSP;
          if(app_api.getDevInfo) rsp.cmd_status = app_api.getDevInfo(&rsp.devInfo ); 

          skd_cmd_sendRsp((uint8_t*)&rsp, sizeof(rsp_get_device_info_t)); 
          
        }
        break;         
        case GET_DEVICE_STATUS:
        {
          rsp_get_device_status_t  rsp = {NULL};
          cmd_get_device_status_t* pCmd = (cmd_get_device_status_t*)&new_cmd;
           

          rsp.rsp_id = GET_DEVICE_STATUS_RSP; 
          if(app_api.takeSnapshot) rsp.cmd_status = app_api.takeSnapshot(&rsp.devStatus, &rsp.timestamp); 

          skd_cmd_sendRsp((uint8_t*)&rsp, sizeof(rsp)); 
        }
        break;


        case GET_BLE_SETTING: 
        {
           rsp_get_ble_settings_t  rsp = {NULL};
           cmd_get_ble_settings_t* pCmd = (cmd_get_ble_settings_t*)&new_cmd;            
           
           rsp.rsp_id = GET_BLE_SETTING_RSP;
           if(app_api.getBLESettings) rsp.cmd_status = app_api.getBLESettings( &rsp.settings );  

           skd_cmd_sendRsp((uint8_t*)&rsp, sizeof(rsp));  
        }  
        break;  
        case SET_BLE_SETTING: 
        {
           rsp_set_ble_settings_t  rsp = {NULL};
           cmd_set_ble_settings_t* pCmd = (cmd_set_ble_settings_t*)&new_cmd;            
                  
           rsp.rsp_id = SET_BLE_SETTING_RSP;

           if( new_cmd.cmd_len != sizeof( cmd_set_ble_settings_t )) {
              
              rsp.cmd_status  = CMD_ERROR_INVALID_PARAMETERS; 
              NRF_LOG_WARNING("Invalid ble settings"); 
              NRF_LOG_PROCESS();

           }else{

             if(app_api.setBLESettings) rsp.cmd_status = app_api.setBLESettings(&pCmd->settings);

           } 

           skd_cmd_sendRsp((uint8_t*)&rsp, sizeof(rsp));

        }  
        break;
        case GET_GNSS_SETTING: 
        {
           rsp_get_gnss_settings_t  rsp = {NULL};
           cmd_get_gnss_settings_t* pCmd = (cmd_get_gnss_settings_t*)&new_cmd;            
           
           rsp.rsp_id = GET_GNSS_SETTING_RSP;
           if(app_api.getGNSSSettings) rsp.cmd_status = app_api.getGNSSSettings( &rsp.settings );  

           skd_cmd_sendRsp((uint8_t*)&rsp, sizeof(rsp));  
        }  
        break;  
        case SET_GNSS_SETTING: 
        {
           rsp_set_gnss_settings_t  rsp = {NULL};
           cmd_set_gnss_settings_t* pCmd = (cmd_set_gnss_settings_t*)&new_cmd;            
           
           rsp.rsp_id      = SET_GNSS_SETTING_RSP;
           
           if( new_cmd.cmd_len != sizeof( cmd_set_gnss_settings_t )) {
              
              rsp.cmd_status  = CMD_ERROR_INVALID_PARAMETERS; 
              NRF_LOG_WARNING("Invalid gnns settings"); 
              NRF_LOG_PROCESS();
              

           }else{

             if(app_api.setGNSSSettings) rsp.cmd_status = app_api.setGNSSSettings(&pCmd->settings);

           }  

           skd_cmd_sendRsp((uint8_t*)&rsp, sizeof(rsp));  
        }  
        break;   
        case GET_MS_SETTING: 
        {
           rsp_get_ms_settings_t  rsp = {NULL};
           cmd_get_ms_settings_t* pCmd = (cmd_get_ms_settings_t*)&new_cmd;            
           
           rsp.rsp_id = GET_MS_SETTING_RSP;
           if(app_api.getMSSettings) rsp.cmd_status = app_api.getMSSettings( &rsp.settings );  

           skd_cmd_sendRsp((uint8_t*)&rsp, sizeof(rsp));  
        }  
        break;  
        case SET_MS_SETTING: 
        {
           rsp_set_ms_settings_t  rsp = {NULL};
           cmd_set_ms_settings_t* pCmd = (cmd_set_ms_settings_t*)&new_cmd;            
               
           rsp.rsp_id = SET_MS_SETTING_RSP;

           if( new_cmd.cmd_len != sizeof( cmd_set_ms_settings_t )) {
              
              rsp.cmd_status  = CMD_ERROR_INVALID_PARAMETERS; 
              NRF_LOG_WARNING("Invalid ms settings"); 
              NRF_LOG_PROCESS();
              
           }else{

              if(app_api.setMSSettings) rsp.cmd_status = app_api.setMSSettings( &pCmd->settings );

           }           

           skd_cmd_sendRsp((uint8_t*)&rsp, sizeof(rsp));

        }  
        break;
        case GET_TIMER_SETTING: 
        {
           rsp_get_timer_settings_t  rsp = {NULL};
           cmd_get_timer_settings_t* pCmd = (cmd_get_timer_settings_t*)&new_cmd;            
           
           rsp.rsp_id = GET_TIMER_SETTING_RSP;
           if(app_api.getTimerSettings) rsp.cmd_status = app_api.getTimerSettings( &rsp.settings );  

           skd_cmd_sendRsp((uint8_t*)&rsp, sizeof(rsp));  
        }  
        break;  
        case SET_TIMER_SETTING: 
        {
           rsp_set_timer_settings_t  rsp = {NULL};
           cmd_set_timer_settings_t* pCmd = (cmd_set_timer_settings_t*)&new_cmd;            
           
           rsp.rsp_id = SET_TIMER_SETTING_RSP;

           if( new_cmd.cmd_len != sizeof( cmd_set_timer_settings_t )) {
              
              rsp.cmd_status  = CMD_ERROR_INVALID_PARAMETERS; 
              NRF_LOG_WARNING("Invalid ms settings"); 
              NRF_LOG_PROCESS();
              
           }else{

                if(app_api.setTimerSettings) rsp.cmd_status = app_api.setTimerSettings(&pCmd->settings);

           }   

           skd_cmd_sendRsp((uint8_t*)&rsp, sizeof(rsp));  
        }  
        break;
        case GET_CRNT_TIME: 
        {
          rsp_get_time_t  rsp = {NULL};
          cmd_get_time_t* pCmd =  (cmd_get_time_t*)&new_cmd;    
          
          rsp.rsp_id = GET_CRNT_TIME_RSP; 
          if(app_api.getTime) rsp.cmd_status = app_api.getTime(&rsp.time);
          
          skd_cmd_sendRsp((uint8_t*)&rsp, sizeof(rsp)); 

        }
        break;
        case SET_CRNT_TIME: 
        {  
          rsp_set_time_t  rsp  = {NULL};
          cmd_set_time_t* pCmd =  (cmd_set_time_t*)&new_cmd;  
          
          rsp.rsp_id = SET_CRNT_TIME_RSP;

          if( new_cmd.cmd_len != sizeof( cmd_set_time_t )) {
              
              rsp.cmd_status  = CMD_ERROR_INVALID_PARAMETERS; 
              NRF_LOG_WARNING("Invalid ms settings"); 
              NRF_LOG_PROCESS();
              
           }else{

              if(app_api.setTime) rsp.cmd_status = app_api.setTime(&pCmd->time);

           }        
          
          skd_cmd_sendRsp((uint8_t*)&rsp, sizeof(rsp));  

        }  
        break;
        case GET_BUTTON_SETTING: 
        {
           rsp_get_button_settings_t  rsp = {NULL};
           cmd_get_button_settings_t* pCmd = (cmd_get_button_settings_t*)&new_cmd;            
           
           rsp.rsp_id = GET_BUTTON_SETTING_RSP;
           if(app_api.getButtonSettings) rsp.cmd_status = app_api.getButtonSettings( &rsp.settings );  

           skd_cmd_sendRsp((uint8_t*)&rsp, sizeof(rsp));  
        }  
        break;  
        case SET_BUTTON_SETTING: 
        {
           rsp_set_button_settings_t  rsp = {NULL};
           cmd_set_button_settings_t* pCmd = (cmd_set_button_settings_t*)&new_cmd;            
           
           rsp.rsp_id = SET_BUTTON_SETTING_RSP;

           if( new_cmd.cmd_len != sizeof( cmd_set_button_settings_t )) {
              
              rsp.cmd_status  = CMD_ERROR_INVALID_PARAMETERS; 
              NRF_LOG_WARNING("Invalid ms settings"); 
              NRF_LOG_PROCESS();
              
           }else{

              if(app_api.setButtonSettings) rsp.cmd_status = app_api.setButtonSettings(&pCmd->settings);

           }   

           skd_cmd_sendRsp((uint8_t*)&rsp, sizeof(rsp));  
        }  
        break;     
        case GET_LED_SETTING: 
        {
           rsp_get_led_settings_t  rsp = {NULL};
           cmd_get_led_settings_t* pCmd = (cmd_get_led_settings_t*)&new_cmd;    
           
           rsp.rsp_id = GET_LED_SETTING_RSP;
           if(app_api.getLedSettings) rsp.cmd_status = app_api.getLedSettings( &rsp.settings );  

           skd_cmd_sendRsp((uint8_t*)&rsp, sizeof(rsp));

        }
        break;
        case SET_LED_SETTING: 
        {
           rsp_set_led_settings_t  rsp = {NULL};
           cmd_set_led_settings_t* pCmd = (cmd_set_led_settings_t*)&new_cmd;            
           
           rsp.rsp_id = SET_LED_SETTING_RSP;

           if( new_cmd.cmd_len != sizeof( cmd_set_led_settings_t )) {
              
              rsp.cmd_status  = CMD_ERROR_INVALID_PARAMETERS; 
              NRF_LOG_WARNING("Invalid ms settings"); 
              NRF_LOG_PROCESS();
              
           }else{

              if(app_api.setLedSettings) rsp.cmd_status = app_api.setLedSettings(&pCmd->settings);

           }   

           skd_cmd_sendRsp((uint8_t*)&rsp, sizeof(rsp));  

        }
        break;
        case GET_DB_SETTING: 
        {
           rsp_get_db_settings_t  rsp = {NULL};
           cmd_get_db_settings_t* pCmd = (cmd_get_db_settings_t*)&new_cmd;    
           
           rsp.rsp_id = GET_DB_SETTING_RSP;
           if(app_api.getDBSettings) rsp.cmd_status = app_api.getDBSettings( &rsp.settings );  

           skd_cmd_sendRsp((uint8_t*)&rsp, sizeof(rsp));

        }
        break;
        case SET_DB_SETTING: 
        {
           rsp_set_db_settings_t  rsp = {NULL};
           cmd_set_db_settings_t* pCmd = (cmd_set_db_settings_t*)&new_cmd;            
           
           rsp.rsp_id = SET_DB_SETTING_RSP;

           if( new_cmd.cmd_len != sizeof( cmd_set_db_settings_t )) {
              
              rsp.cmd_status  = CMD_ERROR_INVALID_PARAMETERS; 
              NRF_LOG_WARNING("Invalid ms settings"); 
              NRF_LOG_PROCESS();
              
           }else{

              if(app_api.setDBSettings) rsp.cmd_status = app_api.setDBSettings(&pCmd->settings);

           }   

           skd_cmd_sendRsp((uint8_t*)&rsp, sizeof(rsp));   

        }
        break;
        case GET_DB_NUMBER_OF_RECORDS: 
        {  
          rsp_get_db_size_t  rsp  = {NULL};
          cmd_get_DB_size_t* pCmd  = (cmd_get_DB_size_t*)&new_cmd;  
          
          rsp.rsp_id  = GET_DB_NUMBER_OF_RECORDS_RSP;

          if( app_api.getDBnumberOfRecrods ) rsp.cmd_status = app_api.getDBnumberOfRecrods(&rsp.db_size);
          
          skd_cmd_sendRsp((uint8_t*)&rsp, sizeof(rsp));  

        }  
        break;
        case GET_DB_RECORD: 
        {  
          rsp_get_DB_record_t  rsp  = {NULL};
          cmd_get_DB_record_t* pCmd  = (cmd_get_DB_record_t*)&new_cmd;  

          rsp.rsp_id = GET_DB_RECORD_RSP;

          if(app_api.getDBrecord) rsp.cmd_status = app_api.getDBrecord( pCmd->sampleId, &rsp.record);
          
          skd_cmd_sendRsp((uint8_t*)&rsp, sizeof(rsp));  

        }  
        break;
        case GET_DB_RECORDS: 
        {  
          rsp_get_DB_records_t  rsp  = {NULL};
          cmd_get_DB_records_t* pCmd  = (cmd_get_DB_records_t*)&new_cmd;  
          
          rsp.rsp_id = GET_DB_RECORDS_RSP;

          if(app_api.getDBrecords) rsp.cmd_status = app_api.getDBrecords( pCmd->strId, pCmd->len );
          
          skd_cmd_sendRsp((uint8_t*)&rsp, sizeof(rsp));  

        }  
        break;
        case CLEAN_DB_SOFT: 
        {  
          rsp_clean_DB_soft_t  rsp  = {NULL};
          cmd_clean_DB_soft_t* pCmd  = (cmd_clean_DB_soft_t*)&new_cmd;  
          
          rsp.rsp_id = CLEAN_DB_SOFT_RSP;

          if(app_api.cleanDB_soft) rsp.cmd_status = app_api.cleanDB_soft();
          
          skd_cmd_sendRsp((uint8_t*)&rsp, sizeof(rsp));  

        }  
        break;
        case CLEAN_DB_HARD: 
        { 
          
          rsp_clean_DB_hard_t  rsp  = {NULL};
          cmd_clean_DB_hard_t* pCmd  = (cmd_clean_DB_hard_t*)&new_cmd;  
          
          rsp.rsp_id = CLEAN_DB_HARD_RSP;

          if(app_api.cleanDB_hard) rsp.cmd_status = app_api.cleanDB_hard();
          
          skd_cmd_sendRsp((uint8_t*)&rsp, sizeof(rsp));  

        }  
        break; 
        default:
        {
          cmd_srv_emtpy_rsp_t rsp; 

          rsp.rsp_id      = SYSTEM_CMD_RSP; 
          rsp.cmd_status  = CMD_UNKNOWN_ERROR; 
          
          skd_cmd_sendRsp((uint8_t*)&rsp, sizeof(rsp)); 

        }
        break;
       
    } 

}