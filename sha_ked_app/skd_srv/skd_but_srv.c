#include "skd_but_srv.h"
#include "sha_ked_debug.h"
#include "nrf_queue.h"
#include "app_button.h"
#include "app_timer.h"


#include "sha_ked_but.h" 

#if SHA_KED_BUT_SRV_LOG_ENABLED == 1 
    #define NRF_LOG_LEVEL               SHA_KED_BUT_SRV_LOG_LEVEL
    #define NRF_LOG_INFO_COLOR          SHA_KED_BUT_SRV_INFO_LOG_COLOR
    #define NRF_LOG_DEBUG_COLOR         SHA_KED_BUT_SRV_DEBUG_LOG_COLOR
#else 
    #define NRF_LOG_LEVEL       0
#endif 


#define NRF_LOG_MODULE_NAME     skd_but_srv
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
NRF_LOG_MODULE_REGISTER();


#define SKD_BUTTON_PIN                  16
#define SKD_LONG_PRESS_TIMEOUT_SEC      3
#define SKD_PRESS_CNTR_TIMEOUT_SEC      0.7

APP_TIMER_DEF(longPressDetectorTimerId); 
APP_TIMER_DEF(pressingCounterTimerId);

NRF_QUEUE_DEF(  skd_but_srv_msg_t, 
                m_but_srv_queue, 
                5, 
                NRF_QUEUE_MODE_OVERFLOW );


NRF_QUEUE_INTERFACE_DEC( skd_but_srv_msg_t, 
                         but_srv_queue);


NRF_QUEUE_INTERFACE_DEF( skd_but_srv_msg_t, 
                         but_srv_queue, 
                         &m_but_srv_queue);

static void  longPressTimeout(void * p_context);
static void  pressingCounterTimeout(void * p_context);
static void  buttonHandler1  (buttonEvnt_t evntType);

static skd_but_config_t     crntCofing = {NULL};
static skd_but_srv_api_t    crntApi    = {NULL};
static uint8_t              skd_but_press_cnt = NULL;    

static volatile bool  isLongPressDetected = !NULL; 

static  skd_button_cfg_t  but_config = {

          .pin_no          = SKD_BUTTON_PIN,
          .active_state    = APP_BUTTON_ACTIVE_LOW,
          .pull_cfg        = NRF_GPIO_PIN_PULLUP, 
          .debounce_timeMs  = 100,
          .isr_handler      = buttonHandler1
           
};  

static void buttonHandler (uint8_t pin_no, uint8_t button_action){
    ret_code_t err_code, ret;
    
    if( button_action == APP_BUTTON_ACTIVE_HIGH ){
    
        NRF_LOG_INFO("Button %d was pressed.", pin_no); 
        NRF_LOG_PROCESS();

        ret = app_timer_start(  longPressDetectorTimerId, 
                          APP_TIMER_TICKS( SKD_LONG_PRESS_TIMEOUT_SEC *1000),
                          NULL); 
        
        APP_ERROR_CHECK(ret);
                            
    }
    if( button_action == APP_BUTTON_ACTIVE_LOW  ){
    
        NRF_LOG_INFO("Button %d was released.", pin_no); 
        NRF_LOG_PROCESS();
        
        ret = app_timer_stop(longPressDetectorTimerId);
        
        APP_ERROR_CHECK(ret);
        
        if(skd_but_press_cnt == 0){

         ret = app_timer_start(  pressingCounterTimerId, 
                                APP_TIMER_TICKS( SKD_PRESS_CNTR_TIMEOUT_SEC *1000),
                                NULL); 
        
          APP_ERROR_CHECK(ret);

        }

        skd_but_press_cnt++;

    }
    
}
//*/


static void buttonHandler1  (buttonEvnt_t evntType){
    ret_code_t err_code, ret;
    
    if( evntType == APP_BUTTON_ACTIVE_LOW  ){
    
        NRF_LOG_INFO("Button was pressed."); 
        NRF_LOG_PROCESS();

        ret = app_timer_start(  longPressDetectorTimerId, 
                          APP_TIMER_TICKS( SKD_LONG_PRESS_TIMEOUT_SEC *1000),
                          NULL); 
        
        APP_ERROR_CHECK(ret);
                            
    }

    if( evntType ==  APP_BUTTON_ACTIVE_HIGH ){
    
        NRF_LOG_INFO("Button was released."); 
        NRF_LOG_PROCESS();
        
        ret = app_timer_stop(longPressDetectorTimerId);
        
        APP_ERROR_CHECK(ret);
        
        if(skd_but_press_cnt == 0){

         ret = app_timer_start(  pressingCounterTimerId, 
                                APP_TIMER_TICKS( SKD_PRESS_CNTR_TIMEOUT_SEC *1000),
                                NULL); 
        
         APP_ERROR_CHECK(ret);

        }

        skd_but_press_cnt++;

    }

}

void skd_but_srv_init( skd_but_config_t* pConfig, skd_but_srv_api_t* pApi) {

  ret_code_t err_code, ret;
    
  NRF_LOG_INFO("Initializing the service"); 
  // Initialize.
  
  if(pConfig == NULL) return; 
  


  err_code =  app_timer_create( &longPressDetectorTimerId, 
                               APP_TIMER_MODE_SINGLE_SHOT,
                               longPressTimeout);
  
  APP_ERROR_CHECK(err_code);

  
  err_code =  app_timer_create( &pressingCounterTimerId, 
                               APP_TIMER_MODE_SINGLE_SHOT,
                                pressingCounterTimeout);
  
  if( *(uint8_t*)&pConfig->mode == NULL){skd_but_srv_deInit(); return;}

  APP_ERROR_CHECK(err_code);

//----------------------------------------------------
   skd_but_init(&but_config); 

/*
  if(isFirstLanch){
    but_config.button_handler = buttonHandler; 
    ret = app_button_init(&but_config, 1, APP_TIMER_TICKS( 100) ); 
    APP_ERROR_CHECK(ret); 
    
    ret = app_button_enable();
    APP_ERROR_CHECK(ret);  

    isFirstLanch = NULL; 

  }

//*/

//------------------------------------------------------------------


  if(pApi != NULL) memcpy(&crntApi, pApi, sizeof(skd_but_srv_api_t));  


}

static void  longPressTimeout(void * p_context){
    
  NRF_LOG_INFO("TIMEOUT: Long Press detected");
  
  skd_but_srv_msg_t msg = { .msgType = BUT_LONG_PRESS};

  ret_code_t ret = but_srv_queue_push(&msg); 

  APP_ERROR_CHECK(ret);

  ret = app_timer_stop(  pressingCounterTimerId); 
        
  APP_ERROR_CHECK(ret);

  skd_but_press_cnt = -1; 

}

static void  pressingCounterTimeout(void * p_context){
    
  NRF_LOG_INFO("TIMEOUT: %d button multiple pressings were detected ", skd_but_press_cnt);
  
  ret_code_t ret = app_timer_stop(longPressDetectorTimerId);
        
  APP_ERROR_CHECK(ret);

  if(skd_but_press_cnt > BUT_TRIPLE_PRESS)  skd_but_press_cnt = BUT_TRIPLE_PRESS;

  skd_but_srv_msg_t msg = { .msgType = skd_but_press_cnt};

  ret = but_srv_queue_push(&msg); 
  
  APP_ERROR_CHECK(ret); 
   
  skd_but_press_cnt = -1;
}

void skd_but_srv_process(){

    skd_but_srv_msg_t newMsg = {NULL}; 
        
    //check if new  event is  avalible
   if(but_srv_queue_is_empty()) return;

   //processing new event
   ret_code_t err_code = but_srv_queue_pop(&newMsg);
   APP_ERROR_CHECK(err_code);

   //Send to process 
   if(crntApi.evntDetectedCb) crntApi.evntDetectedCb(newMsg.msgType);

}  

void skd_but_srv_deInit(){
   
    ret_code_t ret; 

    ret = app_timer_stop(longPressDetectorTimerId); 
        
    APP_ERROR_CHECK(ret);

    ret = app_timer_stop(  pressingCounterTimerId); 
        
    APP_ERROR_CHECK(ret);

//---------------------------------------------
// Need to deinit the button. 
   skd_but_deInit(&but_config);
//---------------------------------------------


}
