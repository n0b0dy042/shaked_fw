#include "skd_geo_srv.h"
#include "ZOE-M8B_drv.h"
#include "sha_ked_debug.h"
#include <math.h>

#if SHA_KED_GEO_SRV_LOG_ENABLED == 1 
    #define NRF_LOG_LEVEL               SHA_KED_GEO_SRV_LOG_LEVEL
    #define NRF_LOG_INFO_COLOR          SHA_KED_GEO_SRV_INFO_LOG_COLOR
    #define NRF_LOG_DEBUG_COLOR         SHA_KED_GEO_SRV_DEBUG_LOG_COLOR
#else 
    #define NRF_LOG_LEVEL       0
#endif 

#define NRF_LOG_MODULE_NAME     skd_geo_srv
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
NRF_LOG_MODULE_REGISTER();


#define   UNKNOWN_GEOZONE                       0xFF
#define   GEO_POSITION_ERROR_IN_METERS          3  


#define SATELLITE_FOUND           true
#define NO_SATELLITE_FOUND        !SATELLITE_FOUND

#define   GNSS_MSG_MAZ_SIZE         100 

static char             gnssMsg [ GNSS_MSG_MAZ_SIZE ]= { NULL };


static volatile bool isGeoOn = false; 

static skd_geo_config_t   crntConfig        = {NULL}; 
static zoe_sample_t       crntGEO           = {NULL};
static zoe_sample_t       lastTime          = {NULL};
static zoe_sample_t       lastPosition      = {NULL};
static zoe_sample_t       lastAngle         = {NULL};
static zoe_sample_t       newGEO            = {NULL};   
static skd_geo_api_t      crntApi           = {NULL};
static uint8_t            crntGeoZone       = UNKNOWN_GEOZONE;
static bool               isSatellitesFound = NO_SATELLITE_FOUND;

static void skd_geo_srv_processNewData          (zoe_sample_t* pNewGEO); 
static void skd_geo_srv_detectGeozone           (zoe_sample_t* pNewGEO);
static void skd_geo_srv_detectDistanceThreshold (zoe_sample_t* pNewGEO); 
static void skd_geo_srv_detectTimeThreshold     (zoe_sample_t* pNewGEO);
static void skd_geo_srv_detectAngleThreshold    (zoe_sample_t* pNewGEO); 


#include "skd_led_srv.h"

void skd_geo_srv_init (skd_geo_config_t* pConfig, skd_geo_api_t* pApi){
  
  zoe_error_code_t  ret           = NULL;
  
  if(skd_geo_srv_isOn()) {
  
    NRF_LOG_WARNING("GOE serivce is allready online.");
    NRF_LOG_PROCESS(); 

    return; 

  }


  skd_led_srv_deInit();
  skd_led_settings_t temp = { .mode = LED_ALLWAYS_ON}; 
  skd_led_srv_init(&temp);
//*/

  NRF_LOG_INFO("Initializing ZOE-M8B driver"); 
  NRF_LOG_PROCESS(); 

  ret = zoe_hw_init(pConfig);
  ret = zoe_sw_init(pConfig);
  
  memcpy( &crntApi, pApi, sizeof(skd_geo_api_t));
  memcpy( &crntConfig, pConfig, sizeof(skd_geo_config_t)); 
  
  if(crntApi.geo_statusUpdateCb) crntApi.geo_statusUpdateCb( SEARCHING_FOR_SATELLITES );  
  
  isGeoOn = true;

}

void skd_geo_srv_deInit (){
  
  zoe_error_code_t  ret           = NULL;
  
  if(!skd_geo_srv_isOn()) {
  
    NRF_LOG_WARNING("GOE serivce is already offline. Do deInit first!!");
    NRF_LOG_PROCESS(); 

    return; 

  }
  

  skd_led_srv_deInit();
  skd_led_settings_t temp = { .mode = LED_ALLWAYS_OFF}; 
  skd_led_srv_init(&temp);
//*/

  NRF_LOG_INFO("Deinitializing ZOE-M8B driver"); 
  NRF_LOG_PROCESS(); 

  ret = zoe_sw_deInit();
  ret = zoe_hw_deInit();
  
  memset(&crntApi, NULL, sizeof(skd_geo_api_t));
  memset(&crntConfig, NULL, sizeof(skd_geo_config_t)); 

  isGeoOn = false;
}


void skd_geo_srv_process(){

    zoe_error_code_t  ret     =  NULL;
    
    if(!skd_geo_srv_isOn()) return; 
    
    ret = zoe_recieveMsg( gnssMsg, GNSS_MSG_MAZ_SIZE ); 
    
    if( ret == ZOE_HW_ERROR) { 
        
        crntApi.geo_statusUpdateCb( NO_DATA_FROM_HW ); 
        return; 
    }
           
    if( ret != ZOE_NO_ERROR) return;
       
    if( zoe_verifyMsg(gnssMsg) ) return;

    zoe_msg_type_t msgType = zoe_identifyMsg( gnssMsg );        
        
    switch(msgType){
      case ZOE_NMEA_VTG_MSG:{
        
        zoe_ReadSampleFromVTG   (gnssMsg, &newGEO); 
        //gnssMsg[ strlen(gnssMsg) - 1 ] = 0;  // Delete break line sybmole
        //NRF_LOG_DEBUG("GNNS message recieved: %s", gnssMsg);

      }
      break; 
      case ZOE_NMEA_GGA_MSG:{
        
        zoe_ReadSampleFromGGA(gnssMsg, &newGEO); 
        //gnssMsg[ strlen(gnssMsg) - 1 ] = 0;  // Delete break line sybmole
        NRF_LOG_DEBUG("GNNS message recieved: %s", gnssMsg);
        NRF_LOG_DEBUG("Hdop value is %d", newGEO.gnssData.hdop);
          
      }
      break; 
      case ZOE_NMEA_GSA_MSG:{
        
        zoe_ReadSampleFromGSA(gnssMsg, &newGEO); 
        //gnssMsg[ strlen(gnssMsg) - 1 ] = 0;  // Delete break line sybmole
        //NRF_LOG_DEBUG("GNNS message recieved: %s", gnssMsg);

      }
      break;
      case ZOE_NMEA_GSV_MSG:{
         
         zoe_ReadSampleFromGSV(gnssMsg, &newGEO); 
         //gnssMsg[ strlen(gnssMsg) - 1 ] = 0;  // Delete break line sybmole
         //NRF_LOG_DEBUG("GNNS message recieved: %s", gnssMsg);


      }break;
      case ZOE_NMEA_GLL_MSG:{
        
        zoe_ReadSampleFromGLL(gnssMsg, &newGEO); 
        //gnssMsg[ strlen(gnssMsg) - 1 ] = 0;  // Delete break line sybmole
        //NRF_LOG_DEBUG("GNNS message recieved: %s", gnssMsg);


      }
      break; 
      case ZOE_NMEA_GST_MSG:{
        
        zoe_ReadSampleFromGST(gnssMsg, &newGEO); 
        //gnssMsg[ strlen(gnssMsg) - 1 ] = 0;  // Delete break line sybmole
        //NRF_LOG_DEBUG("GNNS message recieved: %s", gnssMsg);

      }
      break;
 
      case   ZOE_NMEA_RMC_MSG:{

          zoe_ReadSampleFromRMC(gnssMsg, &newGEO); 
          //gnssMsg[ strlen(gnssMsg)  - 1] = 0;  // Delete break line sybmol
          NRF_LOG_DEBUG("GNNS message recieved: %s", gnssMsg);
          //zoe_debug_printSample(&newGEO); 
          
          skd_geo_srv_processNewData(&newGEO);
              
      }
      break; 
      default: {  

        //gnssMsg[ strlen(gnssMsg) - 1 ] = 0;  // Delete break line sybmole
        NRF_LOG_WARNING("UNKNOWN ZOE MESSAGE: %s", gnssMsg);
        NRF_LOG_FLUSH();
    
      }break;

    } 
    
        


    NRF_LOG_FLUSH(); 

}   



static  void skd_geo_srv_processNewData(zoe_sample_t* pNewGEO){
  
  skd_gnss_data_t* pCrntGnss = &crntGEO.gnssData; 
  skd_gnss_data_t* pNewGnsss = &pNewGEO->gnssData;  
  
  if( pNewGnsss->latitude*pNewGnsss->longitude == 0.0  && isSatellitesFound == NO_SATELLITE_FOUND ){
  //if( pNewGnsss->satellites_count == 0 && pCrntGnss->satellites_count == 0){

    NRF_LOG_INFO("No satellites found");
    NRF_LOG_FLUSH();   
    return;
     
  } 
  
  if( pNewGnsss->latitude*pNewGnsss->longitude == 0.0  && isSatellitesFound == SATELLITE_FOUND){
  //if( pNewGnsss->satellites_count == 0 && pCrntGnss->satellites_count > 0 ){
      
    NRF_LOG_INFO("Satellites lost");
    NRF_LOG_FLUSH(); 

    if(crntApi.geo_statusUpdateCb) crntApi.geo_statusUpdateCb( SATELLITES_LOST );  
    
    isSatellitesFound = NO_SATELLITE_FOUND; 
    memcpy(&crntGEO, pNewGEO, sizeof(zoe_sample_t));  
    
    //memcpy(&lastTime, pNewGEO, sizeof(zoe_sample_t));
    //memcpy(&lastPosition, pNewGEO, sizeof(zoe_sample_t));  
    //memcpy(&lastAngle, pNewGEO, sizeof(zoe_sample_t));   

    return; 
  } 
  
  if( (pNewGnsss->latitude != 0.0 || pNewGnsss->longitude != 0.0)  && isSatellitesFound == NO_SATELLITE_FOUND ){
  //if(pNewGnsss->satellites_count > 0 && pCrntGnss->satellites_count == 0 ){
      
    NRF_LOG_INFO("Satellites found");
    isSatellitesFound = SATELLITE_FOUND; 
    memcpy(&crntGEO, pNewGEO, sizeof(zoe_sample_t));  
    //memcpy(&lastTime, pNewGEO, sizeof(zoe_sample_t));
    //memcpy(&lastPosition, pNewGEO, sizeof(zoe_sample_t));
    //memcpy(&lastAngle, pNewGEO, sizeof(zoe_sample_t));  

    if(crntApi.geo_statusUpdateCb) crntApi.geo_statusUpdateCb( SATELLITES_FOUND );  

    return; 
  }
  
  NRF_LOG_INFO("Processing valid new geodata ");  
  
  //zoe_debug_printSample(&newGEO); 
    
  skd_data_processing_mode_t* pCrntDsp = &crntConfig.DSPmode; 

  if(pCrntDsp->DG)  skd_geo_srv_detectGeozone (pNewGEO);  
  
  NRF_LOG_FLUSH(); 
  memcpy(&crntGEO, pNewGEO, sizeof(zoe_sample_t));  
    
  if(pCrntDsp->DTT) { skd_geo_srv_detectTimeThreshold     (pNewGEO);  return; }  
  if(pCrntDsp->DDT) { skd_geo_srv_detectDistanceThreshold (pNewGEO);  return; } 
  if(pCrntDsp->DAT) { skd_geo_srv_detectAngleThreshold    (pNewGEO);  return; } 
 
}  

static void skd_geo_srv_detectGeozone(zoe_sample_t* pNewGEO){
  
 skd_gnss_data_t* pNewGnssData = &pNewGEO->gnssData; 

 if(crntGeoZone != UNKNOWN_GEOZONE){
    
     skd_geozone_t* pTargetGeozone = &crntConfig.geozone[crntGeoZone];  
    
     float dst = zoe_calcDistance( pTargetGeozone->latitude, pTargetGeozone->longitude, 
                                   pNewGnssData->latitude,   pNewGnssData->longitude);  

    
     if( dst + GEO_POSITION_ERROR_IN_METERS > pTargetGeozone->radiusMeters   ) {
      
        NRF_LOG_INFO("Geozone %d exited", crntGeoZone);
        NRF_LOG_FLUSH(); 
        
        crntGeoZone = UNKNOWN_GEOZONE; 

        if(crntApi.geo_geozoneExitedCb) crntApi.geo_geozoneExitedCb(crntGeoZone); 
      
    }
    
    
  }else {  

    for(uint8_t i = 0; i < MAX_NUMBER_OF_GEO_ZONE; i++){
    
      skd_geozone_t* pTargetGeozone = &crntConfig.geozone[i];  
      
      if(!pTargetGeozone->geozoneState) continue; // if not active stop

      float dst = zoe_calcDistance( pTargetGeozone->latitude, pTargetGeozone->longitude, 
                                    pNewGnssData->latitude,   pNewGnssData->longitude);  
    
      if( dst - GEO_POSITION_ERROR_IN_METERS < pTargetGeozone->radiusMeters) {
      
        crntGeoZone = pTargetGeozone->id; 
      
        NRF_LOG_INFO("Geozone %d entered", crntGeoZone);
        NRF_LOG_FLUSH(); 

        if(crntApi.geo_geozoneEnteredCb) crntApi.geo_geozoneEnteredCb(crntGeoZone); 
      
      }
   }  
  
 }
  
} 


static void skd_geo_srv_detectTimeThreshold(zoe_sample_t* pNewGEO){
    
    int diff = NULL;  
    
    diff = zoe_calcTimeDistance( &pNewGEO->timeStamp, &lastTime.timeStamp);  
        
    if( diff <  crntConfig.timeThresholdSeconds ) return;  

    if( crntApi.goe_TimeThresholdDetectedCb) crntApi.goe_TimeThresholdDetectedCb( &pNewGEO->gnssData, 
                                                                                  &pNewGEO->timeStamp);

    memcpy(&lastTime, pNewGEO, sizeof(zoe_sample_t)); 

}


static void skd_geo_srv_detectDistanceThreshold(zoe_sample_t* pNewGEO){
     
    skd_gnss_data_t* pNewGnssData  = &pNewGEO->gnssData; 
    skd_gnss_data_t* pLastGnssData = &lastPosition.gnssData; 
    
    float diff = zoe_calcDistance( pLastGnssData->latitude, pLastGnssData->longitude, 
                                   pNewGnssData->latitude,  pNewGnssData->longitude);
    
    if( diff <  crntConfig.distanceThresholdMeter ) return;  // do nothing
    
    if( crntApi.goe_DistanceThresholdDetectedCb ) crntApi.goe_DistanceThresholdDetectedCb( &pNewGEO->gnssData, 
                                                                                           &pNewGEO->timeStamp); 
    
    memcpy(&lastPosition, pNewGEO, sizeof(zoe_sample_t));
     
}


static void skd_geo_srv_detectAngleThreshold(zoe_sample_t* pNewGEO){
     
   skd_gnss_data_t* pNewGnssData  = &pNewGEO->gnssData; 
   skd_gnss_data_t* pLastGnssData = &lastAngle.gnssData; 


   if( abs(pNewGnssData->angle - pLastGnssData->angle) < crntConfig.angleThresholdDegree) return; 
   
   if( crntApi.goe_AngleThresholdDetectedCb )   crntApi.goe_AngleThresholdDetectedCb( &pNewGEO->gnssData, 
                                                                                      &pNewGEO->timeStamp);

   memcpy(&lastAngle, pNewGEO, sizeof(zoe_sample_t)); 

} 


void  skd_geo_srv_get_crntGEO (skd_gnss_data_t* pGNNSData, skd_timestamp_t* pTimeStamp){

 if(pGNNSData != NULL) memcpy( pGNNSData, &crntGEO.gnssData,  sizeof(skd_gnss_data_t));
 if(pTimeStamp != NULL) memcpy( pTimeStamp, &crntGEO.timeStamp, sizeof(skd_timestamp_t)); 

}

void skd_geo_srv_get_crntTime      ( skd_timestamp_t* pTimeStamp){

  if(pTimeStamp != NULL) memcpy( pTimeStamp, &crntGEO.timeStamp, sizeof(skd_timestamp_t)); 

}

bool skd_geo_srv_isOn(){
  
  return isGeoOn; 

}
bool skd_geo_srv_isSatellitesFound  (void){
  
  return isSatellitesFound; 

}