#ifndef SKD_GEO_SRV_H
#define SKD_GEO_SRV_H


#include <stdint.h>
#include <stdio.h> 
#include "sha_ked_common.h"
   

typedef skd_gnss_settings_t skd_geo_config_t; 
typedef skd_gnss_data_t     skd_geo_data_t; 


typedef enum { 
 
  SATELLITES_LOST, 
  SATELLITES_FOUND,
  SEARCHING_FOR_SATELLITES, 
  NO_DATA_FROM_HW
  
} geo_status_t;



typedef void (*geo_statusUpdateCB_t)   (geo_status_t    newStatus);
typedef void (*geo_dataUpdateCB_t)     (skd_gnss_data_t* pNewData, skd_timestamp_t* pTimeStamp);
typedef void (*geo_geozoneEnteredCB_t) (uint8_t geozoneId);   
typedef void (*geo_geozoneExitedCB_t)  (uint8_t geozoneId);  


typedef struct {

   geo_statusUpdateCB_t     geo_statusUpdateCb;
   geo_dataUpdateCB_t       goe_TimeThresholdDetectedCb;
   geo_dataUpdateCB_t       goe_DistanceThresholdDetectedCb;
   geo_dataUpdateCB_t       goe_AngleThresholdDetectedCb;
   geo_geozoneEnteredCB_t   geo_geozoneEnteredCb; 
   geo_geozoneExitedCB_t    geo_geozoneExitedCb;
    
} skd_geo_api_t; 




void skd_geo_srv_init               (skd_geo_config_t* pConfig, skd_geo_api_t* pApi);
void skd_geo_srv_deInit             (void);
void skd_geo_srv_process            (void);
void skd_geo_srv_get_crntGEO        (skd_gnss_data_t* pGNNSData, skd_timestamp_t* pTimeStamp);  
void skd_geo_srv_get_crntTime       ( skd_timestamp_t* pTimeStamp);  
bool skd_geo_srv_isOn               (void);
bool skd_geo_srv_isSatellitesFound  (void);   

#endif