#ifndef ZOE_M8B_H
#define ZOE_M8B_H

#include <stdio.h>
#include <stdint.h>

#include "sha_ked_common.h"

typedef enum{
  
  ZOE_NO_ERROR = 0, 
  ZOE_INVALID_PARAMETERS, 
  ZOE_MSG_NOT_READY,
  ZOE_MSG_CORRUPTED,
  ZOE_HW_ERROR

}zoe_error_code_t;


typedef enum{

  UNKONWN_MSG = 0,
  ZOE_NMEA_RMC_MSG,
  ZOE_NMEA_GGA_MSG,
  ZOE_NMEA_GSA_MSG,
  ZOE_NMEA_GLL_MSG,
  ZOE_NMEA_GST_MSG,
  ZOE_NMEA_GSV_MSG,
  ZOE_NMEA_VTG_MSG

}zoe_msg_type_t;  


typedef  skd_gnss_settings_t        zoe_sw_config_t;
typedef  skd_gnss_settings_t        zoe_hw_config_t;


typedef struct{

  skd_gnss_data_t            gnssData; 
  skd_timestamp_t            timeStamp;

}zoe_sample_t;  


extern char const * zoe_msg_type_str[ ];
  
zoe_error_code_t    zoe_hw_init             (zoe_hw_config_t* pConfig);
zoe_error_code_t    zoe_hw_deInit();
zoe_error_code_t    zoe_sw_init             (zoe_sw_config_t* pConfig);
zoe_error_code_t    zoe_sw_deInit();
zoe_error_code_t    zoe_recieveMsg          (char*           pBuf, uint8_t bufLen);
zoe_error_code_t    zoe_verifyMsg           (const char*      pMsg);
zoe_msg_type_t      zoe_identifyMsg         (const char*      pMsg);
zoe_error_code_t    zoe_ReadSampleFromRMC   (const char* p_RMC_Msg, zoe_sample_t* pSample);
zoe_error_code_t    zoe_ReadSampleFromGGA   (const char* p_GGA_Msg, zoe_sample_t* pSample);
zoe_error_code_t    zoe_ReadSampleFromGSA   (const char* p_GSA_Msg, zoe_sample_t* pSample);
zoe_error_code_t    zoe_ReadSampleFromGLL   (const char* p_GLL_Msg, zoe_sample_t* pSample);
zoe_error_code_t    zoe_ReadSampleFromGST   (const char* p_GST_Msg, zoe_sample_t* pSample);
zoe_error_code_t    zoe_ReadSampleFromGSV   (const char* p_GSV_Msg, zoe_sample_t* pSample);
zoe_error_code_t    zoe_ReadSampleFromVTG   (const char* p_VTG_Msg, zoe_sample_t* pSample);
uint8_t             zoe_isFixed             (zoe_sample_t* pSample); 
int                 zoe_calcTimeDistance    ( skd_timestamp_t* pTime1, skd_timestamp_t* pTime2);  
uint32_t            zoe_calcDistance        (  float lat1, float long1, 
                                               float lat2, float long2 );
                                                


static  inline void  zoe_debug_printSample(zoe_sample_t* pSample ){ 
 
  skd_debug_printTime(&pSample->timeStamp.time);
  skd_debug_printDate(&pSample->timeStamp.date);
  skd_debug_printGnssData(&pSample->gnssData);
}


static inline void zoe_debug_printMsgType(zoe_msg_type_t type)  {
  
  if(type < 0 || type >  ZOE_NMEA_VTG_MSG ){
    
     NRF_LOG_WARNING("TYPE UNKNOWN"); 
     return; 

  }
   
  NRF_LOG_DEBUG("This is %s message", zoe_msg_type_str[type] ); 
  

}


void          Z0E_M8B_DEUBG();   




#endif