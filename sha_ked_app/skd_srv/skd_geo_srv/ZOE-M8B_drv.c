#include "ZOE-M8B_drv.h"
#include "miniNMEA.h"
#include "sha_ked_debug.h"

#include "nrf_gpio.h"
#include "nrf_delay.h"
#include "nrf_serial.h"



#if SKD_ZOE_M8B_DRV_LOG_ENABLED == 1 
    #define NRF_LOG_LEVEL               SKD_ZOE_M8B_DRV_LOG_LEVEL
    #define NRF_LOG_INFO_COLOR          SKD_ZOE_M8B_DRV_INFO_LOG_COLOR
    #define NRF_LOG_DEBUG_COLOR         SKD_ZOE_M8B_DRV_DEBUG_LOG_COLOR
#else 
    #define NRF_LOG_LEVEL       0
#endif 

#define NRF_LOG_MODULE_NAME     zoe_m8b_drv
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
NRF_LOG_MODULE_REGISTER();

#define _GPS_KMPH_PER_KNOT   1.852



#define  GNSS_PWR_CTRL_PIN                   7
#define  GNSS_ANT_CTRL_PIN                   11
#define  NRF_TX_GNSS_RXD_PIN                 31
#define  NRF_RX_GNSS_TXD_PIN                 21 //(RESET_PIN) 

#define RESTART_CNT_MAX_VALUE                1000

#define GNSS_UART_MODE                   NRF_SERIAL_MODE_DMA    //NRF_SERIAL_MODE_IRQ

#define SERIAL_FIFO_TX_SIZE 1
#define SERIAL_FIFO_RX_SIZE 1000

#define SERIAL_BUFF_TX_SIZE 1
#define SERIAL_BUFF_RX_SIZE 255

#define DEG_TO_RAD            0.017453292519943295769236907684886
#define radians(deg)          ((deg)*DEG_TO_RAD)

#define MAX_TEXT_LEN       100

static void nrf_serial_evt_handler(struct nrf_serial_s const * p_serial, nrf_serial_event_t event); 


NRF_SERIAL_DRV_UART_CONFIG_DEF( m_gnss_uart,
                                NRF_RX_GNSS_TXD_PIN, NRF_TX_GNSS_RXD_PIN,
                                NRF_UART_PSEL_DISCONNECTED, NRF_UART_PSEL_DISCONNECTED,
                                NRF_UART_HWFC_DISABLED, NRF_UART_PARITY_EXCLUDED,
                                NRF_UART_BAUDRATE_9600,
                                UART_DEFAULT_CONFIG_IRQ_PRIORITY);

NRF_SERIAL_QUEUES_DEF(serial_queues, SERIAL_FIFO_TX_SIZE, SERIAL_FIFO_RX_SIZE);

NRF_SERIAL_BUFFERS_DEF(serial_buffs, SERIAL_BUFF_TX_SIZE, SERIAL_BUFF_RX_SIZE);

NRF_SERIAL_CONFIG_DEF( serial_config, GNSS_UART_MODE,  
                       &serial_queues, &serial_buffs, nrf_serial_evt_handler, 
                       NULL );

NRF_SERIAL_UART_DEF(serial_uart, 0);

char const * zoe_msg_type_str[ ] ={ "UNKNOWN", "RMC", "GGA", "GSA", "GLL", "GST", "GSV", "VTG"};

static uint8_t  textMsgLen  = 0; 
static char     textMsg[ SERIAL_FIFO_RX_SIZE ] = {NULL}; 

static zoe_hw_config_t crnt_hw_config = {NULL}; 

static uint32_t  restart_cnt = NULL;  


void zoe_delay_ms(uint32_t ms_time){
 
  return nrf_delay_ms(ms_time);

} 

void zoe_power_on(){

  nrf_gpio_cfg_output(GNSS_PWR_CTRL_PIN);
  nrf_gpio_pin_clear(GNSS_PWR_CTRL_PIN);

}; 

void zoe_power_off(){

  nrf_gpio_cfg_output(GNSS_PWR_CTRL_PIN);
  nrf_gpio_pin_set(GNSS_PWR_CTRL_PIN);

}; 

void zoe_ext_ant_on(){

  nrf_gpio_cfg_output(GNSS_ANT_CTRL_PIN);
  nrf_gpio_pin_clear(GNSS_ANT_CTRL_PIN);

};

void zoe_ext_ant_off(){
  
  nrf_gpio_cfg_output(GNSS_ANT_CTRL_PIN);
  nrf_gpio_pin_set(GNSS_ANT_CTRL_PIN);
  
};


static void nrf_serial_evt_handler(struct nrf_serial_s const * p_serial, nrf_serial_event_t event)
{

  switch (event){
    case NRF_SERIAL_EVENT_DRV_ERR:

        NRF_LOG_ERROR("NRF_SERIAL_EVENT_DRV_ERR happended");

    break; 
    case NRF_SERIAL_EVENT_FIFO_ERR:

        NRF_LOG_ERROR("NRF_SERIAL_EVENT_FIFO_ERR happended");
        NRF_LOG_PROCESS(); 

    break;
    case NRF_SERIAL_EVENT_TX_DONE: 
    
        NRF_LOG_DEBUG("Processing NRF_SERIAL_EVENT_TX_DONE");

    break;
    case NRF_SERIAL_EVENT_RX_DATA:
        
        //nrf_serial_rx_drain(&serial_uart); // Flash the data, if to much 
        NRF_LOG_DEBUG("Processing NRF_SERIAL_EVENT_RX_DATA");


    break;    
   
  } 

  NRF_LOG_FLUSH();

}

zoe_error_code_t  zoe_hw_init(zoe_hw_config_t* pConfig){

  NRF_LOG_INFO("Intiliazing hardware level");
  ret_code_t ret;
  
  memcpy(&crnt_hw_config, pConfig, sizeof(zoe_hw_config_t)); 

  zoe_power_off(); 
  
  //turn on/off external antenna
  if(pConfig->powerMode.EXANTEN)    {
    
     zoe_ext_ant_on(); 
     NRF_LOG_INFO("External antenna selected"); 
     NRF_LOG_PROCESS(); 
  }
  else{

     zoe_ext_ant_off();
     NRF_LOG_INFO("No external antenna selected"); 
     NRF_LOG_PROCESS(); 
  }
  
  zoe_power_off();
  zoe_delay_ms(1);
  zoe_power_on(); 

  zoe_delay_ms(10); //Wait while zoe-m8b clear the RX line.
  ret = nrf_serial_init(&serial_uart, &m_gnss_uart, &serial_config);
  APP_ERROR_CHECK(ret);

  NRF_LOG_FLUSH(); 
    
}

zoe_error_code_t  zoe_hw_deInit(){
  
  ret_code_t ret;
  NRF_LOG_INFO("Deintiliazing hardware level"); 
  
  do{ 
      
      ret = nrf_serial_uninit(&serial_uart);
      //APP_ERROR_CHECK(ret);

  } while(ret != NRF_SUCCESS);  
  
  zoe_ext_ant_off();
  zoe_power_off();
 
}


zoe_error_code_t  zoe_sw_errorProcessign(){



} 
zoe_error_code_t  zoe_sw_init(zoe_sw_config_t* pConfig){
  
  restart_cnt = 0; 
  NRF_LOG_INFO("Intiliazing software level"); 

}

zoe_error_code_t  zoe_sw_deInit(){

  NRF_LOG_INFO("Deintiliazing software level"); 
  
}

zoe_error_code_t  zoe_recieveMsg(char* pBuf, uint8_t bufLen){
  
  ret_code_t ret;
  char byte; 

  if (restart_cnt > RESTART_CNT_MAX_VALUE) return ZOE_HW_ERROR; 

  //Set all value in buffer to zero
  memset( pBuf, NULL, bufLen);
    
  //Check if message reaedy 
  if( textMsg[ textMsgLen ] == '\n'  ){
    
   
    if( bufLen < textMsgLen ) return ZOE_INVALID_PARAMETERS; 
    
    // copy buffer
    memcpy(pBuf, textMsg, textMsgLen );
    textMsgLen = 0;
    return ZOE_NO_ERROR;  

  }

  // read untile new line or end of buffer
  do{
      
    ret = nrf_serial_read(&serial_uart, &byte, sizeof(byte), NULL, 3*1000);   

    if(ret != NRF_ERROR_TIMEOUT) APP_ERROR_CHECK(ret); 

    // if no avalible byte, stop reading
    if( ret != NRF_SUCCESS) return ZOE_MSG_NOT_READY;
     
    textMsg[ textMsgLen++ ] = byte;   
    
    // Check if buffer overflow
    if( textMsgLen == MAX_TEXT_LEN ){
            
      textMsgLen = 0; 
      memset(textMsg, NULL, MAX_TEXT_LEN);
    
    }
  
  }while( byte != '\n' );   
  
  
  
  if( bufLen < textMsgLen)    return ZOE_INVALID_PARAMETERS; 
  
  // copy buffer  
  memcpy(pBuf, textMsg, textMsgLen ); 
  textMsgLen = 0;  
  return ZOE_NO_ERROR;

}


zoe_error_code_t  zoe_verifyMsg (const char* pMsg){
    
    if(minmea_check(pMsg, false))     return ZOE_NO_ERROR; 
    else                              return ZOE_MSG_CORRUPTED; 
     
}

zoe_msg_type_t zoe_identifyMsg (const char* pMsg){
  
  zoe_msg_type_t type = minmea_sentence_id(pMsg, false); 
  
  if(type < 0 ) return ZOE_MSG_CORRUPTED; 
   
  return type;

}


zoe_error_code_t  zoe_ReadSampleFromRMC (const char* p_RMC_Msg, zoe_sample_t* pSample){
         
  struct minmea_sentence_rmc frame = {NULL};
  
  // Read data structure   
  minmea_parse_rmc(&frame, p_RMC_Msg);  
  
  //Copy time
  skd_time_t* pTime = (skd_time_t*)&pSample->timeStamp.time; 
  
  pTime->hours        = frame.time.hours;
  pTime->minutes      = frame.time.minutes;
  pTime->seconds      = frame.time.seconds;
  pTime->microseconds = frame.time.microseconds;  
      
  //Copy date
  skd_data_t*   pDate = (skd_data_t*)&pSample->timeStamp.date;  
  
  pDate->day    = frame.date.day;
  pDate->month  = frame.date.month;
  pDate->year   = frame.date.year;    //TO DO: Check the year


  //Copy gnss data
  skd_gnss_data_t*  pGnssData = (skd_gnss_data_t* )&pSample->gnssData;
  
  pGnssData->latitude             = minmea_tocoord(&frame.latitude); 
  pGnssData->longitude            = minmea_tocoord(&frame.longitude);
  pGnssData->speed                = (uint16_t)minmea_tofloat(&frame.speed)*(float)_GPS_KMPH_PER_KNOT;        
  pGnssData->angle                = (uint16_t)minmea_tofloat(&frame.course); //minmea_tofloat(&frame.course.value); 
    
}


zoe_error_code_t  zoe_ReadSampleFromGGA (const char* p_GGA_Msg, zoe_sample_t* pSample){
         
  struct minmea_sentence_gga frame = {NULL};
  
  // Read data structure   
  minmea_parse_gga(&frame, p_GGA_Msg);  
  
  //Copy time
  skd_time_t* pTime = (skd_time_t* )&pSample->timeStamp.time; 
  
  pTime->hours        = frame.time.hours;
  pTime->minutes      = frame.time.minutes;
  pTime->seconds      = frame.time.seconds;
  pTime->microseconds = frame.time.microseconds;  
      
  
  //Copy gnss data
  skd_gnss_data_t*  pGnssData = (skd_gnss_data_t* )&pSample->gnssData;
  
  pGnssData->latitude             = minmea_tocoord(&frame.latitude); 
  pGnssData->longitude            = minmea_tocoord(&frame.longitude);	
  pGnssData->hdop                 = 10*minmea_tofloat(&frame.hdop); 
  pGnssData->satellites_count     = frame.satellites_tracked;


}


zoe_error_code_t  zoe_ReadSampleFromGSA (const char* p_GSA_Msg, zoe_sample_t* pSample){

  struct minmea_sentence_gsa frame = {NULL};
  
  // Read data structure   
  minmea_parse_gsa(&frame, p_GSA_Msg);  
 
  //Copy gnss data
  skd_gnss_data_t*  pGnssData = (skd_gnss_data_t* )&pSample->gnssData;
    
  //pGnssData->hdop     = 10*minmea_tocoord(&frame.hdop); 
  pGnssData->fix_type = frame.fix_type;  

} 

zoe_error_code_t  zoe_ReadSampleFromGLL (const char* p_GLL_Msg, zoe_sample_t* pSample){
  
  struct minmea_sentence_gll frame = {NULL};

  // Read data structure   
  minmea_parse_gll(&frame, p_GLL_Msg); 
  
  //Copy time
  skd_time_t* pTime = (skd_time_t* )&pSample->timeStamp.time; 
  
  pTime->hours        = frame.time.hours;
  pTime->minutes      = frame.time.minutes;
  pTime->seconds      = frame.time.seconds;
  pTime->microseconds = frame.time.microseconds;

  //Copy gnss data
  skd_gnss_data_t*  pGnssData = (skd_gnss_data_t* )&pSample->gnssData;

  pGnssData->latitude             = minmea_tocoord(&frame.latitude); 
  pGnssData->longitude            = minmea_tocoord(&frame.longitude);	


} 

zoe_error_code_t  zoe_ReadSampleFromGST (const char* p_GST_Msg, zoe_sample_t* pSample){

  struct minmea_sentence_gst frame = {NULL};
  
  // Read data structure   
  minmea_parse_gst(&frame, p_GST_Msg);  

  //Copy gnss data
  skd_gnss_data_t*  pGnssData = (skd_gnss_data_t* )&pSample->gnssData;


}

zoe_error_code_t  zoe_ReadSampleFromGSV (const char* p_GSV_Msg, zoe_sample_t* pSample){

  struct minmea_sentence_gsv frame = {NULL};

  // Read data structure   
  minmea_parse_gsv(&frame, p_GSV_Msg);
  
  //Copy gnss data
  skd_gnss_data_t*  pGnssData = (skd_gnss_data_t* )&pSample->gnssData;

}

zoe_error_code_t  zoe_ReadSampleFromVTG (const char* p_VTG_Msg, zoe_sample_t* pSample){

  struct minmea_sentence_vtg frame = {NULL};

  // Read data structure   
  minmea_parse_vtg(&frame, p_VTG_Msg);
  

  //Copy gnss data
  skd_gnss_data_t*  pGnssData = (skd_gnss_data_t* )&pSample->gnssData;

  //pGnssData->speed =  frame.speed_kph;

}

uint8_t zoe_isFixed(zoe_sample_t* pSample){

 skd_gnss_data_t*  pGnssData = &pSample->gnssData;
 
 if( pGnssData->fix_type > MINMEA_GPGSA_FIX_NONE ) return pGnssData->fix_type; 

 return NULL; 
} 

int zoe_calcTimeDistance ( skd_timestamp_t* pTime1, skd_timestamp_t* pTime2){
  
  int time1 = NULL, time2= NULL; 

  time1 = minmea_gettime( (const struct minmea_date *)&pTime1->date,
                          (const struct minmea_time *)&pTime1->time); 

  time2 = minmea_gettime( (const struct minmea_date *)&pTime2->date, 
                          (const struct minmea_time *)&pTime2->time); 

  return time1 - time2; 

}

uint32_t zoe_calcDistance(float lat1, float long1, float lat2, float long2)
{
  // returns distance in meters between two positions, both specified
  // as signed decimal-degrees latitude and longitude. Uses great-circle
  // distance computation for hypothetical sphere of radius 6372795 meters.
  // Because Earth is no exact sphere, rounding errors may be up to 0.5%.
	
    float delta 	= radians(long1-long2);
	
    float sdlong = sin(delta);
    float cdlong = cos(delta);
	
    lat1 = radians(lat1);
    lat2 = radians(lat2);
	
    float slat1 = sin(lat1);
    float clat1 = cos(lat1);
    float slat2 = sin(lat2);
    float clat2 = cos(lat2);
    
    delta = (clat1 * slat2) - (slat1 * clat2 * cdlong);
    delta = pow(delta, 2);
    delta += pow(clat2 * sdlong, 2);
    delta = sqrt(delta);
    
    float denom = (slat1 * slat2) + (clat1 * clat2 * cdlong);
    delta = atan2(delta, denom);
	
    uint32_t out = (uint32_t) (delta * 6372795);
	
    NRF_LOG_INFO("Distance: %d M", out);

	
    return out;
}






void skd_common_debug(){

    skd_data_t data = {

        .day   = 1,
        .month = 2,
        .year  = 12
    };
     
    skd_time_t time  = {    
      .hours = 1, 
      .minutes = 2,
      .seconds = 3,
      .microseconds = 4, 
    }; 
    
    skd_geolocation_t loc = {
      .latitude  = 10, 
      .longitude = 12
    };    
    
    NRF_LOG_INFO("Starting the app"); 
    NRF_LOG_PROCESS();

    skd_debug_printDate(&data);
    skd_debug_printTime(&time); 
    skd_debug_printGeolocation(&loc);  


}
 
void miniNMEA_debug(){



  char const debugMsg [ ]= "$GPGGA,123519,4807.038,N,01131.000,E,1,08,0.9,545.4,M,46.9,M,,*47";    
    
  zoe_verifyMsg( debugMsg );
    
  zoe_msg_type_t msgType= zoe_identifyMsg(debugMsg); 
         
  char talkerId [6] = {NULL}; 
  NRF_LOG_INFO("Test message is %s",                        debugMsg);
  NRF_LOG_INFO("Calculate raw sentence checksum %d ",       minmea_checksum(debugMsg) );
  NRF_LOG_INFO("Check sentence validity and checksum. %d ", minmea_check(debugMsg, false) );
  minmea_talker_id(talkerId, debugMsg);
  NRF_LOG_INFO("Determine talker identifier. %s ",           talkerId);
  NRF_LOG_INFO("Determine sentence identifier. %d ",         minmea_sentence_id(debugMsg, false) );
    
     
  //minmea_sentence_id msgId =  minmea_sentence_id(debugMsg, false) 
    
  NRF_LOG_FLUSH(); 



} 

#define   GNSS_MSG_MAZ_SIZE     100 

static char gnssMsg [ GNSS_MSG_MAZ_SIZE ]= { NULL };

void Z0E_M8B_DEUBG(){

    zoe_sample_t      sample  = {NULL}; 
    zoe_error_code_t  zoe_ret = NULL;

    char  ggaTestMessage [ ]= "$GPGGA,123519,4807.038,N,01131.000,E,1,08,0.9,545.4,M,46.9,M,,*47";    
    char  rmcTestMessage [ ]= "$GPRMC,081836,A,3751.65,S,14507.36,E,000.0,360.0,130998,011.3,E*62"; 
    char*  debugMsg  = rmcTestMessage; 

    zoe_ret = zoe_hw_init (NULL);
    zoe_ret = zoe_sw_init (NULL);

   
    while(1 ) { 

        zoe_ret = zoe_recieveMsg( debugMsg, GNSS_MSG_MAZ_SIZE ); 
       
        if( zoe_ret != ZOE_NO_ERROR) continue;
       
        if( zoe_verifyMsg(debugMsg) ) continue;

        zoe_msg_type_t msgType = zoe_identifyMsg( debugMsg );        

        if (msgType != ZOE_NMEA_RMC_MSG)  continue; 
        
        zoe_ReadSampleFromRMC(debugMsg, &sample); 
        

        debugMsg[ strlen(debugMsg) - 1 ] = 0;  // Delete break line sybmole
        NRF_LOG_INFO("GNNS message recieved: %s", debugMsg);
        //NRF_LOG_DEBUG("The message is %s ", zoe_verifyMsg( debugMsg ) ? "CORRUPTED":  "VALID");
        //zoe_debug_printMsgType(msgType);  
        zoe_debug_printSample(&sample); 

        NRF_LOG_FLUSH(); 
    
    } 

}

