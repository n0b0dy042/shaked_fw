#ifndef SKD_TIME_SRV_H
#define SKD_TIME_SRV_H

#include "sha_ked_common.h"


typedef enum{
  
  SYNCHRONIZ_TIMEOUT,  
  GNNS_ON_INTERVAL_ENTERED,
  GNNS_OFF_INTERVAL_ENTERED,
  BLE_ON_INTERVAL_ENTERED, 
  BLE_OFF_INTERVAL_ENTERED, 
  BLE_TIMEZONE_ENTERED, 
  BLE_TIMEZONE_EXITED,
   
}skd_time_srv_timeout_type_t;  

typedef struct{

  uint8_t type;
  uint8_t context[1];
    
}skd_time_srv_msg_t;  


typedef skd_timer_settings_t skd_time_config_t;  

typedef void (*time_srv_bleOffIntrevalEnteredCB_t)    (void);  
typedef void (*time_srv_bleOnIntrevalEnteredCB_t)     (void);  
typedef void (*time_srv_gnssOffIntrevalEnteredCB_t)     (void);  
typedef void (*time_srv_gnssOnIntrevalEnteredCB_t)      (void);
typedef void (*time_srv_synchronizeTimeCB_t)  (skd_timestamp_t* pCrntTime); 


typedef struct {
  
  time_srv_bleOffIntrevalEnteredCB_t      bleOffIntrevalEnteredCb;
  time_srv_bleOnIntrevalEnteredCB_t       bleOnIntrevalEnteredCb;
  time_srv_gnssOffIntrevalEnteredCB_t     gnssOffIntrevalEnteredCb;
  time_srv_gnssOnIntrevalEnteredCB_t      gnssOnIntrevalEnteredCb; 
  time_srv_synchronizeTimeCB_t            synchronizeTimeCb; 
  
}skd_time_api_t; 


void skd_time_srv_init      ( skd_time_config_t*  pConfig, 
                              skd_time_api_t*     pApi,
                              skd_timestamp_t*    pRefTime);

void skd_time_srv_deInit    ( );
void skd_time_srv_process   ( ); 
void skd_time_srv_getTime   ( skd_timestamp_t* pTimestamp ); 
void skd_time_srv_setTime   ( skd_timestamp_t* pTimestamp ); 

#endif