#include "sha_ked_debug.h"
#include "skd_led_srv.h"


#include "app_timer.h"
#include "nrf_gpio.h"
#include "nrf_delay.h"


#define SHA_KED_LED_PIN              18 
#define DEFAULT_BLINK_TIMEOUT_SEC    2


#if SHA_KED_LED_SRV_LOG_ENABLED == 1 
    #define NRF_LOG_LEVEL               SHA_KED_LED_SRV_LOG_LEVEL
    #define NRF_LOG_INFO_COLOR          SHA_KED_LED_SRV_INFO_LOG_COLOR
    #define NRF_LOG_DEBUG_COLOR         SHA_KED_LED_SRV_DEBUG_LOG_COLOR
#else 
    #define NRF_LOG_LEVEL       0
#endif 

#define NRF_LOG_MODULE_NAME     skd_led_srv
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
NRF_LOG_MODULE_REGISTER();


APP_TIMER_DEF(ledTimerId); 

static void skd_led_srv_timeout(void * p_context);


void skd_led_srv_init( skd_led_config_t* pConfig){
    
  NRF_LOG_INFO("Starting led service"); 

  ret_code_t err_code; 
  skd_led_mode_t pMode = pConfig->mode; 
  
  nrf_gpio_cfg_output(SHA_KED_LED_PIN);
  nrf_gpio_pin_set(SHA_KED_LED_PIN);

  err_code =  app_timer_create( &ledTimerId, 
                                APP_TIMER_MODE_SINGLE_SHOT,
                                skd_led_srv_timeout);
  
  APP_ERROR_CHECK(err_code);

  if( pMode == LED_ALLWAYS_OFF) return; 
  if( pMode == LED_ALLWAYS_ON)  nrf_gpio_pin_clear(SHA_KED_LED_PIN);

  if( pMode == LED_ON_ONLY_AFTER_START) {
   

      NRF_LOG_INFO("Settings LED timer"); 

      uint16_t timeoutSec = pConfig->defaultTimeoutSec; 

      err_code =  app_timer_start(   ledTimerId, 
                                     APP_TIMER_TICKS( timeoutSec *1000),
                                     NULL);
      
      APP_ERROR_CHECK(err_code);  

      nrf_gpio_pin_clear(SHA_KED_LED_PIN);
  
  
  } 
   
}

static void skd_led_srv_timeout(void * p_context){
  
  NRF_LOG_INFO("LED TIMEOUT"); 
  nrf_gpio_pin_set( SHA_KED_LED_PIN );

}  

void skd_led_srv_process(){
  


} 

void skd_led_srv_deInit(){


  app_timer_stop(ledTimerId);
  nrf_gpio_cfg_output(SHA_KED_LED_PIN);
  nrf_gpio_pin_clear(SHA_KED_LED_PIN);

}


void skd_led_srv_blink(uint32_t durationMs){
  
  ret_code_t err_code; 

  nrf_gpio_cfg_output(SHA_KED_LED_PIN);
  nrf_gpio_pin_clear(SHA_KED_LED_PIN);

  app_timer_stop(ledTimerId);
  err_code =  app_timer_start(   ledTimerId, 
                                  APP_TIMER_TICKS( durationMs *1000),
                                  NULL);
      
  APP_ERROR_CHECK(err_code);  

}
