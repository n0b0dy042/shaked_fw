#include "skd_pw_srv.h"
#include "sha_ked_debug.h"

#include "app_timer.h"
#include "nrf_queue.h"

#include "nrf_drv_saadc.h"

#if SHA_KED_PW_SRV_LOG_ENABLED == 1 
    #define NRF_LOG_LEVEL               SHA_KED_PW_SRV_LOG_LEVEL
    #define NRF_LOG_INFO_COLOR          SHA_KED_PW_SRV_INFO_LOG_COLOR
    #define NRF_LOG_DEBUG_COLOR         SHA_KED_PW_SRV_DEBUG_LOG_COLOR
#else 
    #define NRF_LOG_LEVEL       0
#endif 

#define NRF_LOG_MODULE_NAME     skd_pw_srv
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
NRF_LOG_MODULE_REGISTER();


#define MEASURE_TIMEOUT_SEC              60

#define RES_F_VAL 4551.1111		//14B



NRF_QUEUE_DEF(  skd_pw_srv_msg_t, 
                m_pw_srv_queue, 
                5, 
                NRF_QUEUE_MODE_NO_OVERFLOW );


NRF_QUEUE_INTERFACE_DEC( skd_pw_srv_msg_t, 
                         pw_srv_queue);


NRF_QUEUE_INTERFACE_DEF( skd_pw_srv_msg_t, 
                         pw_srv_queue, 
                         &m_pw_srv_queue);


APP_TIMER_DEF( updateTimerId ); 

static skd_pw_srv_api_t   crntApi = {NULL}; 
static nrf_saadc_value_t  m_buffer_pool[2][1];
static uint16_t           crntBatteryLevel = NULL; 

static void saadc_callback     (nrf_drv_saadc_evt_t const * p_event); 
static void measureTimeout     (void * p_context);


void skd_pw_srv_init( skd_pw_srv_config_t* pConfig, skd_pw_srv_api_t* pApi){
  
  uint32_t err_code;

  NRF_LOG_INFO("Starting power service");
  
  if(pApi != NULL) memcpy(&crntApi,pApi, sizeof(skd_pw_srv_api_t));

  nrf_saadc_channel_config_t channel_config =
        NRF_DRV_SAADC_DEFAULT_CHANNEL_CONFIG_SE(NRF_SAADC_INPUT_AIN2);
	
  channel_config.acq_time = (nrf_saadc_acqtime_t)SAADC_CH_CONFIG_TACQ_40us;
       
  err_code = nrf_drv_saadc_init(NULL, saadc_callback);
  APP_ERROR_CHECK(err_code);

  err_code = nrf_drv_saadc_channel_init(NULL, &channel_config);
  APP_ERROR_CHECK(err_code);

  err_code = nrf_drv_saadc_buffer_convert(m_buffer_pool[0], 1);
  APP_ERROR_CHECK(err_code);

  err_code = nrf_drv_saadc_buffer_convert(m_buffer_pool[1], 1);
  APP_ERROR_CHECK(err_code);

  err_code =  app_timer_create( &updateTimerId, 
                                APP_TIMER_MODE_REPEATED,
                                measureTimeout);
      
  APP_ERROR_CHECK(err_code);

  err_code =  app_timer_start(   updateTimerId, 
                                 APP_TIMER_TICKS( MEASURE_TIMEOUT_SEC *1000),
                                 NULL);
      
   APP_ERROR_CHECK(err_code);  	
  
   err_code = nrf_drv_saadc_sample(); 
   APP_ERROR_CHECK(err_code);

}

void skd_pw_srv_process(){

  skd_pw_srv_msg_t newMsg = {NULL}; 
        
  //check if new  event is  avalible
  if(pw_srv_queue_is_empty()) return;

  //processing new event
  ret_code_t err_code = pw_srv_queue_pop(&newMsg);
  APP_ERROR_CHECK(err_code);
  
  switch(newMsg.type){
   case UPDATE_BATTER_LEVEL_TIMEOUT:
      
      if(crntApi.updateBatterLevelCb) crntApi.updateBatterLevelCb(crntBatteryLevel); 

      
   break;
   
  
  }

}


void skd_pw_srv_deInit( void ){
 
  NRF_LOG_INFO("Deinitilazing service");
  NRF_LOG_PROCESS();
  
  nrf_drv_saadc_channel_uninit(NULL);
  nrf_drv_saadc_uninit(); 

  app_timer_stop( updateTimerId );
 
}

static void saadc_callback(nrf_drv_saadc_evt_t const * p_event)
{
	if (p_event->type == NRF_DRV_SAADC_EVT_DONE)
	{
		ret_code_t err_code;
		double voltage_result_d;
                skd_pw_srv_msg_t newMsg = {NULL}; 

		err_code = nrf_drv_saadc_buffer_convert(p_event->data.done.p_buffer, 1);
		APP_ERROR_CHECK(err_code);

		voltage_result_d = ((double)p_event->data.done.p_buffer[0] / RES_F_VAL) * 4;
		crntBatteryLevel = (uint16_t) ((double)voltage_result_d * 100);
				
		NRF_LOG_INFO("ADC Service IT: voltage: "NRF_LOG_FLOAT_MARKER " val: %d", NRF_LOG_FLOAT(voltage_result_d), crntBatteryLevel);
                
                newMsg.type = UPDATE_BATTER_LEVEL_TIMEOUT; 
		pw_srv_queue_push(&newMsg); 
                
	}
}

static void measureTimeout(void * p_context){

    NRF_LOG_INFO("Measure timeout"); 
    ret_code_t err_code;
    
    err_code = nrf_drv_saadc_sample(); 
    APP_ERROR_CHECK(err_code);

}

void skd_pwr_srv_getBattery(uint16_t* pBatteryLevelCentV){

  memcpy(pBatteryLevelCentV, &crntBatteryLevel, sizeof(uint16_t));

}