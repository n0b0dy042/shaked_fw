#ifndef SKD_CMD_SRV_H__
#define SKD_CMD_SRV_H__ 

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

#include "sha_ked_common.h"

#define MAX_BYTE_IN_CMD      110



typedef enum{

  ENTER_DFU           = 0x00,            
  GET_DEVICE_INFO, 
  GET_DEVICE_STATUS,
  GET_BLE_SETTING,
  SET_BLE_SETTING,
  GET_GNSS_SETTING,
  SET_GNSS_SETTING,
  GET_MS_SETTING,
  SET_MS_SETTING,
  GET_TIMER_SETTING,
  SET_TIMER_SETTING,
  GET_CRNT_TIME,
  SET_CRNT_TIME,  
  GET_BUTTON_SETTING,
  SET_BUTTON_SETTING,
  GET_LED_SETTING,
  SET_LED_SETTING,
  GET_DB_SETTING,
  SET_DB_SETTING,
  GET_DB_NUMBER_OF_RECORDS,
  GET_DB_RECORD,
  GET_DB_RECORDS,
  CLEAN_DB_SOFT,
  CLEAN_DB_HARD,

} cmd_id_t;  


typedef enum{

  CMD_ACCEPTED = 0,
  CMD_REJECTED,
  CMD_ERROR_INVALID_PARAMETERS,
  CMD_TO_LONG, 
  CMD_QUEUE_FULL,
  CMD_ERROR_DEVICE_BUSY,
  CMD_DB_TRANSFER_FINISHED,
  CMD_DB_TRANSFER_FAILED,
  CMD_MODE_NOT_SUPPORTED, 
  CMD_UNKNOWN_ERROR = 0xFF 

}cmd_status_t;  



/*--------------------------------CMD--------------------------------------*/

#pragma pack(push, 1)

typedef struct{
  
  cmd_id_t      cmd_id; 
  uint8_t       cmd_body[MAX_BYTE_IN_CMD];   
  uint8_t       cmd_len;
    
}cmd_srv_pack_t;   


typedef struct{
  
  uint8_t                  cmd_id;
  skd_timestamp_t  time; 

}cmd_set_time_t;  


typedef struct{

  uint8_t                  cmd_id;
  skd_setting_legacy_t     settings; 


}cmd_set_device_settings_t;


typedef struct{

  uint8_t                  cmd_id;
  skd_ble_settings_t       settings; 

}cmd_set_ble_settings_t;

typedef struct{

  uint8_t                  cmd_id;
  skd_gnss_settings_t      settings; 

}cmd_set_gnss_settings_t;


typedef struct{

  uint8_t                  cmd_id;
  skd_ms_settings_t        settings; 

}cmd_set_ms_settings_t;


typedef struct{

  uint8_t                  cmd_id;
  skd_timer_settings_t     settings; 

}cmd_set_timer_settings_t;


typedef struct{

  uint8_t                   cmd_id;
  skd_button_settings_t     settings; 

}cmd_set_button_settings_t;


typedef struct{

  uint8_t                   cmd_id;
  skd_led_settings_t     settings; 

}cmd_set_led_settings_t;


typedef struct{

  uint8_t               cmd_id;
  skd_db_settings_t     settings; 

}cmd_set_db_settings_t;

typedef struct{

  uint8_t             cmd_id;
  uint32_t            sampleId;

}cmd_get_DB_record_t;

typedef struct{

  uint8_t             cmd_id;
  uint32_t            strId;
  uint32_t            len;

}cmd_get_DB_records_t;

#pragma pack(pop)

 
typedef struct{
  
  uint8_t      cmd_id;

}cmd_srv_emtpy_cmd_t;    



typedef  cmd_srv_emtpy_cmd_t        cmd_enter_dfu_t; 
typedef  cmd_srv_emtpy_cmd_t        cmd_get_device_info_t; 
typedef  cmd_srv_emtpy_cmd_t        cmd_get_device_status_t;
typedef  cmd_srv_emtpy_cmd_t        cmd_get_ble_settings_t;
typedef  cmd_srv_emtpy_cmd_t        cmd_get_gnss_settings_t;
typedef  cmd_srv_emtpy_cmd_t        cmd_get_ms_settings_t;   
typedef  cmd_srv_emtpy_cmd_t        cmd_get_timer_settings_t; 
typedef  cmd_srv_emtpy_cmd_t        cmd_get_button_settings_t;
typedef  cmd_srv_emtpy_cmd_t        cmd_get_led_settings_t; 
typedef  cmd_srv_emtpy_cmd_t        cmd_get_db_settings_t;   
typedef  cmd_srv_emtpy_cmd_t        cmd_get_time_t; 
typedef  cmd_srv_emtpy_cmd_t        cmd_get_DB_size_t; 
typedef  cmd_srv_emtpy_cmd_t        cmd_clean_DB_soft_t; 
typedef  cmd_srv_emtpy_cmd_t        cmd_clean_DB_hard_t; 
/*--------------------------------------------------------------------------*/
/*------------------------------------RSP-----------------------------------*/
/*--------------------------------------------------------------------------*/

typedef enum{

  ENTER_DFU_RSP           = 0x00,            
  GET_DEVICE_INFO_RSP, 
  GET_DEVICE_STATUS_RSP,
  GET_BLE_SETTING_RSP,
  SET_BLE_SETTING_RSP,
  GET_GNSS_SETTING_RSP,
  SET_GNSS_SETTING_RSP,
  GET_MS_SETTING_RSP,
  SET_MS_SETTING_RSP,
  GET_TIMER_SETTING_RSP,
  SET_TIMER_SETTING_RSP,
  GET_CRNT_TIME_RSP,
  SET_CRNT_TIME_RSP,  
  GET_BUTTON_SETTING_RSP,
  SET_BUTTON_SETTING_RSP, 
  GET_LED_SETTING_RSP,
  SET_LED_SETTING_RSP, 
  GET_DB_SETTING_RSP,
  SET_DB_SETTING_RSP, 
  GET_DB_NUMBER_OF_RECORDS_RSP,
  GET_DB_RECORD_RSP,
  GET_DB_RECORDS_RSP,
  CLEAN_DB_SOFT_RSP, 
  CLEAN_DB_HARD_RSP,
  SYSTEM_CMD_RSP = 0xFF, 
         
} rsp_id_t; 





#pragma pack(push, 1)


typedef struct{
  
  uint8_t                  rsp_id;
  uint8_t                  cmd_status; 
  skd_device_info_t        devInfo; 

}rsp_get_device_info_t; 



typedef struct{
  
  uint8_t                  rsp_id;
  uint8_t                  cmd_status; 
  skd_device_status_t      devStatus;
  skd_timestamp_t          timestamp; 

} rsp_get_device_status_t; 



typedef struct{

  uint8_t                rsp_id;
  uint8_t                cmd_status; 
  skd_ble_settings_t     settings;

} rsp_get_ble_settings_t; 

typedef struct{

  uint8_t                rsp_id;
  uint8_t                cmd_status; 
  skd_gnss_settings_t    settings;

} rsp_get_gnss_settings_t; 

typedef struct{

  uint8_t                rsp_id;
  uint8_t                cmd_status; 
  skd_ms_settings_t      settings;

} rsp_get_ms_settings_t; 


typedef struct{

  uint8_t                   rsp_id;
  uint8_t                   cmd_status; 
  skd_timer_settings_t      settings;

}rsp_get_timer_settings_t; 

typedef struct{
  
  uint8_t                  rsp_id;
  uint8_t                  cmd_status; 
  skd_timestamp_t  time; 
  
} rsp_get_time_t;  

typedef struct{

  uint8_t                   rsp_id;
  uint8_t                   cmd_status; 
  skd_button_settings_t     settings;

}rsp_get_button_settings_t; 


typedef struct{

  uint8_t                   rsp_id;
  uint8_t                   cmd_status; 
  skd_led_settings_t     settings;

}rsp_get_led_settings_t; 


typedef struct{

  uint8_t                   rsp_id;
  uint8_t                   cmd_status; 
  skd_db_settings_t         settings;

}rsp_get_db_settings_t; 


typedef struct{
  
  uint8_t                  rsp_id;
  uint8_t                  cmd_status; 
  uint32_t                 db_size; 
  
} rsp_get_db_size_t;  

typedef  struct{

  uint8_t                  rsp_id;
  uint8_t                  cmd_status; 
  skd_snapshot_t           record;

}rsp_get_DB_record_t;


#pragma pop


typedef struct{
  
  uint8_t      rsp_id;
  uint8_t      cmd_status;  

}cmd_srv_emtpy_rsp_t;    



typedef  cmd_srv_emtpy_rsp_t        rsp_enter_dfu_t; 
typedef  cmd_srv_emtpy_rsp_t        rsp_set_device_settings_t; 
typedef  cmd_srv_emtpy_rsp_t        rsp_set_ble_settings_t; 
typedef  cmd_srv_emtpy_rsp_t        rsp_set_gnss_settings_t; 
typedef  cmd_srv_emtpy_rsp_t        rsp_set_ms_settings_t; 
typedef  cmd_srv_emtpy_rsp_t        rsp_set_timer_settings_t;
typedef  cmd_srv_emtpy_rsp_t        rsp_set_time_t;  
typedef  cmd_srv_emtpy_rsp_t        rsp_set_button_settings_t;
typedef  cmd_srv_emtpy_rsp_t        rsp_set_led_settings_t;
typedef  cmd_srv_emtpy_rsp_t        rsp_set_db_settings_t;
typedef  cmd_srv_emtpy_rsp_t        rsp_get_DB_records_t;
typedef  cmd_srv_emtpy_rsp_t        rsp_clean_DB_soft_t;
typedef  cmd_srv_emtpy_rsp_t        rsp_clean_DB_hard_t;


typedef struct{

  uint8_t futureField;   

} cmd_srv_config_t ;  


typedef void             (*sendRsp_t)                     (uint8_t* p_rps, uint16_t  rspLen);
typedef cmd_status_t     (*enterDFU_cmd_t)                (void);
typedef cmd_status_t     (*getDevInfo_cmd_t)              (skd_device_info_t*       pInfo);
typedef cmd_status_t     (*takeSnapShot_cmd_t)            (skd_device_status_t*     pDevStaus, 
                                                           skd_timestamp_t*         pTimeStamp);
typedef cmd_status_t     (*getBLESettings_cmd_t)          (skd_ble_settings_t*      pSettings); 
typedef cmd_status_t     (*setBLESettings_cmd_t)          (skd_ble_settings_t*      pSettings);
typedef cmd_status_t     (*getGNSSSettings_cmd_t)         (skd_gnss_settings_t*     pSettings); 
typedef cmd_status_t     (*setGNSSSettings_cmd_t)         (skd_gnss_settings_t*     pSettings);
typedef cmd_status_t     (*getMSSettings_cmd_t)           (skd_ms_settings_t*       pSettings); 
typedef cmd_status_t     (*setMSSettings_cmd_t)           (skd_ms_settings_t*       pSettings);
typedef cmd_status_t     (*getTimerSettings_cmd_t)        (skd_timer_settings_t*    pSettings); 
typedef cmd_status_t     (*setTimerSettings_cmd_t)        (skd_timer_settings_t*    pSettings);
typedef cmd_status_t     (*setTime_cmd_t)                 (skd_timestamp_t*         time); 
typedef cmd_status_t     (*getTime_cmd_t)                 (skd_timestamp_t*         time); 
typedef cmd_status_t     (*getButtonSettings_cmd_t)       (skd_button_settings_t*   pSettings); 
typedef cmd_status_t     (*setButtonSettings_cmd_t)       (skd_button_settings_t*   pSettings);
typedef cmd_status_t     (*getLedSettings_cmd_t)          (skd_led_settings_t*      pSettings); 
typedef cmd_status_t     (*setLedSettings_cmd_t)          (skd_led_settings_t*      pSettings);
typedef cmd_status_t     (*getDBSettings_cmd_t)           (skd_db_settings_t*       pSettings); 
typedef cmd_status_t     (*setDBSettings_cmd_t)           (skd_db_settings_t*       pSettings);
typedef cmd_status_t     (*getDBnumberOfRecrods_cmd_t)    (uint32_t*                DBszie);
typedef cmd_status_t     (*getDBrecord_cmd_t )            (uint32_t                 sampleId, skd_snapshot_t* pSample);  
typedef cmd_status_t     (*getDBrecords_cmd_t )           (uint32_t                 strId, 
                                                           uint32_t                 endId);
typedef cmd_status_t     (*cleanDB_soft_cmd_t )           (void);
typedef cmd_status_t     (*cleanDB_hard_cmd_t )           (void);


typedef struct {

         sendRsp_t                     sendResp;
         enterDFU_cmd_t                enterDFU; 
         getDevInfo_cmd_t              getDevInfo;
         takeSnapShot_cmd_t            takeSnapshot;
         getBLESettings_cmd_t          getBLESettings; 
         setBLESettings_cmd_t          setBLESettings;
         getGNSSSettings_cmd_t         getGNSSSettings;
         setGNSSSettings_cmd_t         setGNSSSettings;
         getMSSettings_cmd_t           getMSSettings;
         setMSSettings_cmd_t           setMSSettings;
         getTimerSettings_cmd_t        getTimerSettings;
         setTimerSettings_cmd_t        setTimerSettings;
         getTime_cmd_t                 getTime;
         setTime_cmd_t                 setTime;
         getButtonSettings_cmd_t       getButtonSettings;
         setButtonSettings_cmd_t       setButtonSettings;
         getLedSettings_cmd_t          getLedSettings;
         setLedSettings_cmd_t          setLedSettings;
         getDBSettings_cmd_t           getDBSettings;
         setDBSettings_cmd_t           setDBSettings;
         getDBnumberOfRecrods_cmd_t    getDBnumberOfRecrods;
         getDBrecord_cmd_t             getDBrecord; 
         getDBrecords_cmd_t            getDBrecords;
         cleanDB_soft_cmd_t            cleanDB_soft;
         cleanDB_hard_cmd_t            cleanDB_hard;

} cmd_srv_app_api_t;  



void          skd_cmd_srv_init          (cmd_srv_config_t* pConfig, cmd_srv_app_api_t* pApi);
void          skd_cmd_srv_deInit        (void);
cmd_status_t  skd_cmd_sendCmd           (cmd_srv_pack_t* cmd ); 
void          skd_cmd_srv_process       (void); 
   



#endif