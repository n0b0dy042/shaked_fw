#include "skd_md_srv.h"
#include "sha_ked_debug.h"
#include "nrf_queue.h"
#include "app_button.h"
#include "app_timer.h"


#include "sha_ked_but.h"


#if SHA_KED_MD_SRV_LOG_ENABLED == 1 
    #define NRF_LOG_LEVEL               SHA_KED_MD_SRV_LOG_LEVEL
    #define NRF_LOG_INFO_COLOR          SHA_KED_MD_SRV_INFO_LOG_COLOR
    #define NRF_LOG_DEBUG_COLOR         SHA_KED_MD_SRV_DEBUG_LOG_COLOR
#else 
    #define NRF_LOG_LEVEL       0
#endif 


#define NRF_LOG_MODULE_NAME    skd_md_srv
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
NRF_LOG_MODULE_REGISTER();




APP_TIMER_DEF(jitterStopTimerId); 
APP_TIMER_DEF(montionDeteceterTimerId);
APP_TIMER_DEF(longStopTimerId); 


#define SHA_KED_MS_PIN                8
#define SKD_MAX_SENSITIVITY           10
#define SKD_JITTER_TIMEOUT_SEC        0.9




NRF_QUEUE_DEF(  skd_md_srv_msg_t, 
                m_md_srv_queue, 
                5, 
                NRF_QUEUE_MODE_OVERFLOW );


NRF_QUEUE_INTERFACE_DEC( skd_md_srv_msg_t, 
                         md_srv_queue);


NRF_QUEUE_INTERFACE_DEF( skd_md_srv_msg_t, 
                         md_srv_queue, 
                         &m_md_srv_queue);




static skd_md_srv_config_t crntConfing = {NULL};
static skd_md_srv_api_t    crntApi     = {NULL}; 
static skd_md_state_t      crntState   = IN_STATIONARY;  

static void  motionSensorHandler1 (buttonEvnt_t evntType);
static void  jitterStopTimeoutCb(void * p_context);
static void  motionDectectedTimeoutCb(void * p_context); 
static void  longstopTimeoutCb(void * p_context);

static uint32_t      jitterCounter = NULL; 
static volatile bool  isFirstLanch = !NULL; 
static void jitterProcessing();



static  skd_button_cfg_t  ms_button = {

          .pin_no           = SHA_KED_MS_PIN,
          .active_state     = APP_BUTTON_ACTIVE_LOW,
          .pull_cfg         = NRF_GPIO_PIN_PULLUP, 
          .debounce_timeMs  = 1,
          .isr_handler      = motionSensorHandler1
           
}; 

static void motionSensorHandler1 (buttonEvnt_t evntType){

   ret_code_t err_code, ret;
    
   if( evntType == APP_BUTTON_ACTIVE_HIGH ) NRF_LOG_DEBUG("Motion sensor pin is %s", \
                                                                evntType ? "high" : "low" ); 
    
   if( evntType == APP_BUTTON_ACTIVE_LOW  ) NRF_LOG_DEBUG("Motion sensor pin is %s", \
                                                                evntType ? "high" : "low" ); 
   jitterCounter++; 

   if(jitterCounter < 2*(SKD_MAX_SENSITIVITY - crntConfing.sensitivity) ) return; 
   
   jitterCounter = 0;

   jitterProcessing();  
}

static void motionSensorHandler (uint8_t pin_no, uint8_t button_action){

   ret_code_t err_code, ret;
    
   if( button_action == APP_BUTTON_ACTIVE_HIGH ) NRF_LOG_DEBUG("Motion sensor pin is %s", \
                                                                button_action ? "high" : "low" ); 
    
   if( button_action == APP_BUTTON_ACTIVE_LOW  ) NRF_LOG_DEBUG("Motion sensor pin is %s", \
                                                                button_action ? "high" : "low" ); 
   jitterCounter++; 

   if(jitterCounter < 2*(SKD_MAX_SENSITIVITY - crntConfing.sensitivity) ) return; 
   
   jitterCounter = 0;

   jitterProcessing();  
}

static void jitterProcessing(){
    
    ret_code_t ret;
    //uint32_t timeoutSec;
    float timeoutSec; 

    ret = app_timer_stop( jitterStopTimerId ); 
    APP_ERROR_CHECK(ret);   
    
    if(crntState == IN_MOTION)          timeoutSec = crntConfing.movmentStopTimeoutSec;
    if(crntState == IN_STATIONARY)      timeoutSec = SKD_JITTER_TIMEOUT_SEC;

    ret = app_timer_start( jitterStopTimerId,  APP_TIMER_TICKS( (uint32_t)( timeoutSec*1000) ), NULL);
    APP_ERROR_CHECK(ret); 
    
}

void skd_md_srv_init(skd_md_srv_config_t* pConfig, skd_md_srv_api_t* pApi){
  
  ret_code_t err_code, ret;

  NRF_LOG_INFO("Initializing the service"); 
  NRF_LOG_INFO("APP_TIMER_TICKS( 1) = %d", APP_TIMER_TICKS( 1) );
  //skd_debug_printMSsettings(pConfig);
  
  if(pConfig == NULL) return; 
  
  memcpy(&crntConfing, pConfig, sizeof( skd_md_srv_config_t ));
  if(pApi != NULL ) memcpy(&crntApi, pApi, sizeof(skd_md_srv_api_t));   
   
  err_code =  app_timer_create( &jitterStopTimerId, 
                                APP_TIMER_MODE_REPEATED,
                                jitterStopTimeoutCb);
  
  APP_ERROR_CHECK(err_code);
  
  err_code =  app_timer_create( &montionDeteceterTimerId, 
                                APP_TIMER_MODE_SINGLE_SHOT,
                                motionDectectedTimeoutCb);
  
  APP_ERROR_CHECK(err_code);
  

  err_code =  app_timer_create( &longStopTimerId, 
                                APP_TIMER_MODE_SINGLE_SHOT,
                                longstopTimeoutCb);
  
  APP_ERROR_CHECK(err_code);
  
  skd_ms_mode_t* pMode = &crntConfing.mode;

  if( *(uint8_t*)&pConfig->mode == NULL){ skd_md_srv_deInit(); } 

//----------------------------------------------------------------

  skd_but_init(&ms_button); 

/* 
  if(isFirstLanch ){ 
    
    ms_config.button_handler = motionSensorHandler; 

    ret = app_button_init(&ms_config, 1, APP_TIMER_TICKS( 1 ) ); 
    APP_ERROR_CHECK(ret); 
  

    ret = app_button_enable();
    APP_ERROR_CHECK(ret);
    
    isFirstLanch = NULL;
  }
//*/

//-----------------------------------------------------------------

  uint32_t timeoutSec = crntConfing.movmentStartTimeoutSec;
  
  crntState  = IN_STATIONARY;
  
  ret = app_timer_start( jitterStopTimerId,  APP_TIMER_TICKS( (uint32_t)( SKD_JITTER_TIMEOUT_SEC * 1000)) , NULL);

  ret = app_timer_start( montionDeteceterTimerId,  APP_TIMER_TICKS( timeoutSec*1000), NULL);
  
 
  
  APP_ERROR_CHECK(ret); 
  
  return; 
  
}
  
void skd_md_srv_process(){

  skd_md_srv_msg_t newMsg = {NULL}; 
        
  //check if new  event is  avalible
  if(md_srv_queue_is_empty()) return;

  //processing new event
  ret_code_t ret = md_srv_queue_pop(&newMsg);
  APP_ERROR_CHECK(ret);
  
  skd_ms_mode_t* pMode = &crntConfing.mode;

  switch(newMsg.evtType){
  
  case START_OF_MOTION_DETECTED:{
      
     if( pMode->DLS ) {  
        
        ret = app_timer_stop(longStopTimerId);
        APP_ERROR_CHECK(ret); 

     }

     if(crntApi.startOfMotionDetectedCb && pMode->DSM) crntApi.startOfMotionDetectedCb();
      

  }break;
  case END_OF_MOTIION_DETECTED:{ 
      

      if(crntApi.endOfMotionDetectedCb && pMode->DEM) crntApi.endOfMotionDetectedCb();
       
      if( pMode->DLS ) {

          uint32_t timeoutSec = crntConfing.longStopTimeoutSec;

          ret = app_timer_start( longStopTimerId,  APP_TIMER_TICKS( timeoutSec*1000), NULL); 
      
          APP_ERROR_CHECK(ret); 
      }

  }break;
  case LONG_STOP_DETECTED:{  
    
      if(crntApi.longStopDetectedCb && pMode->DLS ) crntApi.longStopDetectedCb();

  } break;  
  
  } 

}

void skd_md_srv_deInit(){
  
  NRF_LOG_INFO("Deinitializing the service"); 
  ret_code_t ret; 

  ret = app_timer_stop(jitterStopTimerId);
  APP_ERROR_CHECK(ret);

  ret = app_timer_stop(montionDeteceterTimerId);
  APP_ERROR_CHECK(ret);

  ret = app_timer_stop(longStopTimerId);
  APP_ERROR_CHECK(ret);


//---------------------------------------------
// Need to deinit the button. 
  skd_but_deInit(&ms_button); 
//---------------------------------------------

  return;
   
}

static void  jitterStopTimeoutCb(void * p_context){
  
  ret_code_t ret;
  
  jitterCounter = 0;  
  if( crntState == IN_STATIONARY){

    NRF_LOG_INFO("TIMEOUT: no jitter for %d sec", SKD_JITTER_TIMEOUT_SEC); 

    NRF_LOG_INFO("Restart motion detection timer");
    ret = app_timer_stop( montionDeteceterTimerId ); 
    APP_ERROR_CHECK(ret); 
    
  }

  if( crntState == IN_MOTION){
    

    NRF_LOG_INFO("TIMEOUT: no jitter for %d sec", crntConfing.movmentStopTimeoutSec);
     
    NRF_LOG_INFO("End of motion detected"); 
    skd_md_srv_msg_t msg = { .evtType = END_OF_MOTIION_DETECTED };
  
    ret_code_t ret = md_srv_queue_push(&msg);
    APP_ERROR_CHECK(ret); 
      
    crntState = IN_STATIONARY;   
    
    app_timer_stop(jitterStopTimerId);
     
    ret = app_timer_start( jitterStopTimerId,  APP_TIMER_TICKS(SKD_JITTER_TIMEOUT_SEC * 1000), NULL);
    
    APP_ERROR_CHECK(ret);   

  }
  
    uint32_t timeoutSec = crntConfing.movmentStartTimeoutSec;
     
    ret = app_timer_start( montionDeteceterTimerId, APP_TIMER_TICKS( timeoutSec *1000),
                          NULL); 
    
    APP_ERROR_CHECK(ret); 

}

static void motionDectectedTimeoutCb(void * p_context){
  
  NRF_LOG_INFO("Start of motion detected"); 
  
  skd_md_srv_msg_t msg = { .evtType = START_OF_MOTION_DETECTED };
  
  ret_code_t ret = md_srv_queue_push(&msg);
  
  crntState = IN_MOTION; 
  
  uint32_t timeoutSec = crntConfing.movmentStopTimeoutSec;

  ret = app_timer_start( jitterStopTimerId,  APP_TIMER_TICKS( timeoutSec*1000), NULL);
  

}

static void longstopTimeoutCb(void * p_context){
  
  NRF_LOG_INFO("Long stop detected");
  
  skd_md_srv_msg_t msg = { .evtType = LONG_STOP_DETECTED };
  
  ret_code_t ret = md_srv_queue_push(&msg);

}

bool skd_md_srv_isMotionDetected(){

  return crntState; 

}