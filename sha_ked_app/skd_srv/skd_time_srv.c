#include "skd_time_srv.h"
#include "sha_ked_debug.h"
#include "app_timer.h" 
#include "nrf_queue.h"

#if SHA_KED_TIME_SRV_LOG_ENABLED == 1 
    #define NRF_LOG_LEVEL               SHA_KED_TIME_SRV_LOG_LEVEL
    #define NRF_LOG_INFO_COLOR          SHA_KED_TIME_SRV_INFO_LOG_COLOR
    #define NRF_LOG_DEBUG_COLOR         SHA_KED_TIME_SRV_DEBUG_LOG_COLOR
#else 
    #define NRF_LOG_LEVEL       0
#endif 


#define NRF_LOG_MODULE_NAME     shd_time_srv
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
NRF_LOG_MODULE_REGISTER();

#define RTC_UPDATE_TIME_STEP_IN_SEC              1 //60*60//5*60 
#define SYNCHRONIZATION_TIMEOUT_SEC              60*60 


NRF_QUEUE_DEF(  skd_time_srv_msg_t, 
                m_time_srv_queue, 
                5, 
                NRF_QUEUE_MODE_OVERFLOW );


NRF_QUEUE_INTERFACE_DEC( skd_time_srv_msg_t, 
                         time_srv_queue);


NRF_QUEUE_INTERFACE_DEF( skd_time_srv_msg_t, 
                         time_srv_queue, 
                         &m_time_srv_queue);


APP_TIMER_DEF(rtcTimerId); 
APP_TIMER_DEF(syncTimerId);
APP_TIMER_DEF(bleTimerId);
APP_TIMER_DEF(gnssTimerId);

skd_time_config_t crntConfig = { NULL };
skd_time_api_t    crntApi    = { NULL }; 
skd_timestamp_t   refTime    = { NULL };
uint32_t          crntCnt    = NULL;
  


static void  addTime(skd_timestamp_t* pTimestamp, uint16_t secondsPassed);

static void  rtcTimeout               (void * p_context);
static void  syncTimeout              (void * p_context);
static void  bleOnIntrevalEntered     (void * p_context);
static void  bleOffIntrevalEntered    (void * p_context);
static void  gnssOnIntrevalEntered    (void * p_context);
static void  gnssOffIntrevalEntered   (void * p_context);


void skd_time_srv_init ( skd_time_config_t*  pConfig, 
                          skd_time_api_t*    pApi,
                          skd_timestamp_t*   pRefTime){
  
  NRF_LOG_INFO("Initializing timer service"); 
   
  //Save config
  memcpy(&crntConfig, pConfig, sizeof(skd_time_config_t) ); 
  memcpy(&crntApi, pApi, sizeof(skd_time_api_t) );
  memcpy(&refTime, pRefTime, sizeof(skd_timestamp_t));  
  
  
  ret_code_t err_code;

  //Create rtc update timeout
  
  crntCnt  = app_timer_cnt_get(); 
  
  err_code =  app_timer_create( &rtcTimerId, 
                                 APP_TIMER_MODE_REPEATED,
                                 rtcTimeout);
  
  APP_ERROR_CHECK(err_code);
  err_code =  app_timer_start( rtcTimerId, 
                               APP_TIMER_TICKS( RTC_UPDATE_TIME_STEP_IN_SEC *1000),
                               NULL);
  
  APP_ERROR_CHECK(err_code);
  //Create synchonization timeout
  err_code =  app_timer_create( &syncTimerId, 
                                 APP_TIMER_MODE_REPEATED,
                                 syncTimeout);
  
  APP_ERROR_CHECK(err_code);
  err_code =  app_timer_start( syncTimerId, 
                               APP_TIMER_TICKS( SYNCHRONIZATION_TIMEOUT_SEC *1000), //First time faster
                               NULL);
  
  APP_ERROR_CHECK(err_code);

  skd_time_mode_t* pMode =  &pConfig->mode;
 
  
  if(pMode->DBTI){
    
      NRF_LOG_INFO("Settings BLE time intervals"); 

      if(crntApi.bleOnIntrevalEnteredCb) crntApi.bleOnIntrevalEnteredCb();

      err_code =  app_timer_create( &bleTimerId, 
                                    APP_TIMER_MODE_SINGLE_SHOT,
                                    bleOffIntrevalEntered);
      
      APP_ERROR_CHECK(err_code);
      
      uint16_t timeoutSec = pConfig->bleOnTimeIntervalSec; 

      err_code =  app_timer_start(   bleTimerId, 
                                     APP_TIMER_TICKS( timeoutSec *1000),
                                     NULL);
      
      APP_ERROR_CHECK(err_code);  
    
  }
  
  
  
  if(pMode->DGTI){
      
      NRF_LOG_INFO("Settings GNNS time intervals"); 

      if(crntApi.bleOnIntrevalEnteredCb) crntApi.gnssOnIntrevalEnteredCb();

      err_code =  app_timer_create( &gnssTimerId, 
                                    APP_TIMER_MODE_SINGLE_SHOT,
                                    gnssOffIntrevalEntered);
      
      APP_ERROR_CHECK(err_code);
      
      uint16_t timeoutSec = pConfig->gnssOnTimeIntervalSec; 

      err_code =  app_timer_start(   gnssTimerId, 
                                     APP_TIMER_TICKS( timeoutSec *1000),
                                     NULL);
      
      APP_ERROR_CHECK(err_code);  

  }
  
  if(pMode->DBTZ){  } 

  NRF_LOG_FLUSH(); 

}


void skd_time_srv_process(){
   
    skd_time_srv_msg_t newMsg = {NULL}; 
        
    //check if new  event is  avalible
   if(time_srv_queue_is_empty()) return;


   //processing new event
   ret_code_t err_code = time_srv_queue_pop(&newMsg);
   APP_ERROR_CHECK(err_code);
    
   switch(newMsg.type){
      case SYNCHRONIZ_TIMEOUT:{
        
        skd_timestamp_t crntTime = {NULL}; 
        skd_time_srv_getTime(&crntTime);

        if(crntApi.synchronizeTimeCb)crntApi.synchronizeTimeCb( &crntTime); 
      
      }
      break;
      case GNNS_ON_INTERVAL_ENTERED:{
         
         if(crntApi.gnssOnIntrevalEnteredCb) crntApi.gnssOnIntrevalEnteredCb(); 
         
         err_code =  app_timer_create( &gnssTimerId, 
                                       APP_TIMER_MODE_SINGLE_SHOT,
                                       gnssOffIntrevalEntered);
      
          APP_ERROR_CHECK(err_code);
      
          uint16_t timeoutSec = crntConfig.gnssOnTimeIntervalSec; 

          err_code =  app_timer_start(   gnssTimerId, 
                                         APP_TIMER_TICKS( timeoutSec *1000),
                                         NULL);
      
          APP_ERROR_CHECK(err_code);  
      }
      break;
      case GNNS_OFF_INTERVAL_ENTERED:{
      
        if(crntApi.gnssOffIntrevalEnteredCb) crntApi.gnssOffIntrevalEnteredCb();
         
         err_code =  app_timer_create( &gnssTimerId, 
                                       APP_TIMER_MODE_SINGLE_SHOT,
                                       gnssOnIntrevalEntered);
      
          APP_ERROR_CHECK(err_code);
      
          uint16_t timeoutSec = crntConfig.gnssOffTimeIntervalSec; 

          err_code =  app_timer_start(   gnssTimerId, 
                                         APP_TIMER_TICKS( timeoutSec *1000),
                                         NULL);
      
          APP_ERROR_CHECK(err_code);  

      }
      break;
      case BLE_ON_INTERVAL_ENTERED:{
      
        if(crntApi.bleOnIntrevalEnteredCb) crntApi.bleOnIntrevalEnteredCb(); 
        
        err_code =  app_timer_create( &bleTimerId, 
                                       APP_TIMER_MODE_SINGLE_SHOT,
                                       bleOffIntrevalEntered);
      
        APP_ERROR_CHECK(err_code);
      
        uint16_t timeoutSec = crntConfig.bleOnTimeIntervalSec; 

        err_code =  app_timer_start(   bleTimerId, 
                                       APP_TIMER_TICKS( timeoutSec *1000),
                                       NULL);
      
        APP_ERROR_CHECK(err_code); 

      }
      break;   
      case BLE_OFF_INTERVAL_ENTERED:{
        
        if(crntApi.bleOffIntrevalEnteredCb) crntApi.bleOffIntrevalEnteredCb(); 
        
        err_code =  app_timer_create( &bleTimerId, 
                                       APP_TIMER_MODE_SINGLE_SHOT,
                                       bleOnIntrevalEntered);
      
        APP_ERROR_CHECK(err_code);
      
        uint16_t timeoutSec = crntConfig.bleOffTimeIntervalSec; 

        err_code =  app_timer_start(   bleTimerId, 
                                       APP_TIMER_TICKS( timeoutSec *1000),
                                       NULL);
      
        APP_ERROR_CHECK(err_code); 

      }
      break; 
        


   }   

   NRF_LOG_FLUSH(); 
}




static void  addTime(skd_timestamp_t* pTimestamp, uint16_t secondsPassed){

 uint32_t temp;
 uint8_t  maxNumberOfDays;   
   
 skd_time_t* pTime = &pTimestamp->time; 

 temp = ( pTime->seconds + secondsPassed);  
 pTime->seconds =   temp % 60; 
 
 if(temp < 60) return; 

 temp = pTime->minutes + (uint16_t)(temp/60);  
 pTime->minutes =  temp % 60; 

 if(temp < 60) return;

 temp = pTime->hours +  (uint16_t)(temp/60) ; 
 pTime->hours =   temp % 24;

 if(temp < 24) return;  
  
 skd_data_t* pData = &pTimestamp->date; 
 temp = (uint16_t)(temp/24) + pData->day;
 
 if( pData->month > 7 ){
    
    if( pData->month %2) maxNumberOfDays = 31; 
    else                 maxNumberOfDays = 30;

 
 } else if ( pData->month == 2) {
    
     if ( ( pData->year % 4 == 0 && pData->year % 100 != 0 ) || 
            (pData->year % 400 == 0))                             maxNumberOfDays = 29;
     else                                                         maxNumberOfDays = 28; 


 } else {
    
    if( pData->month %2) maxNumberOfDays = 30; 
    else                 maxNumberOfDays = 31;
 }


 if ( temp > maxNumberOfDays){
      
      pData->day    = 1; 
 
 }else{
    
    pData->day = temp; 
    return; 

 } 
 
 if( pData->month  < 11 ){
      
    pData->month += 1;
    
 }else{
    
    pData->month = 1;
    pData->year += 1; 
    
 } 

}
  
static void  rtcTimeout(void * p_context){
    
  NRF_LOG_INFO("TIMEOUT: Update reference time ");
  addTime( &refTime, RTC_UPDATE_TIME_STEP_IN_SEC);
  

  NRF_LOG_INFO("New time");
  //skd_debug_printTimeStampInLine(&refTime); 
  //skd_debug_printDate(&refTime.date);
  //skd_debug_printTime(&refTime.time);

  crntCnt = app_timer_cnt_get(); 

}

static void syncTimeout(void * p_context){
  
  NRF_LOG_INFO("TIMEOUT: Synchonization ");
  
  skd_time_srv_msg_t msg = { .type = SYNCHRONIZ_TIMEOUT }; 
  ret_code_t ret = time_srv_queue_push(&msg); 

}

static void bleOnIntrevalEntered(void * p_context){

  NRF_LOG_INFO("TIMEOUT:  BLE online  time interval entered");
  
  skd_time_srv_msg_t msg = { .type = BLE_ON_INTERVAL_ENTERED }; 
  ret_code_t ret = time_srv_queue_push(&msg); 
  
  APP_ERROR_CHECK(ret);
}

static void bleOffIntrevalEntered(void * p_context){

  NRF_LOG_INFO("TIMEOUT:  BLE offline  time interval entered");
    
  skd_time_srv_msg_t msg = { .type = BLE_OFF_INTERVAL_ENTERED }; 
  ret_code_t ret = time_srv_queue_push(&msg); 

  APP_ERROR_CHECK(ret);
}

static void gnssOnIntrevalEntered(void * p_context){

  NRF_LOG_INFO("TIMEOUT:  GNSS online  time interval entered");
  
  skd_time_srv_msg_t msg = { .type = GNNS_ON_INTERVAL_ENTERED }; 
  ret_code_t ret = time_srv_queue_push(&msg); 

  APP_ERROR_CHECK(ret);

}

static void gnssOffIntrevalEntered(void * p_context){

  NRF_LOG_INFO("TIMEOUT:  GNSS offline  time interval entered");
  
  skd_time_srv_msg_t msg = { .type = GNNS_OFF_INTERVAL_ENTERED }; 
  ret_code_t ret = time_srv_queue_push(&msg);

  APP_ERROR_CHECK(ret);
   
}

static void timezoneEnteredTimeout(void* p_context){

  NRF_LOG_INFO("TIMZE ZONE ENTERED TIMEOUT");
   
}  


static void timezoneExitedTimeout(void* p_context){

  NRF_LOG_INFO("TIMZE ZONE ENTERED TIMEOUT");
   
} 
 
  
void skd_time_srv_setTime( skd_timestamp_t* pTimestamp){
  
  ret_code_t err_code;

  err_code = app_timer_stop(rtcTimerId); 
  
  memcpy(&refTime, pTimestamp, sizeof(skd_timestamp_t)); 
  
  err_code =  app_timer_start( rtcTimerId, 
                               APP_TIMER_TICKS( RTC_UPDATE_TIME_STEP_IN_SEC *1000),
                               NULL);
  
} 

void skd_time_srv_getTime( skd_timestamp_t* pTimestamp){
  
  uint32_t newCnt = app_timer_cnt_get(); 
  
  if(newCnt < crntCnt) newCnt += APP_TIMER_MAX_CNT_VAL;  
  
  uint32_t diff = app_timer_cnt_diff_compute(newCnt, crntCnt);
  
  uint16_t secPassed =  RTC_UPDATE_TIME_STEP_IN_SEC * diff / APP_TIMER_TICKS( RTC_UPDATE_TIME_STEP_IN_SEC *1000); 
  
  memcpy(pTimestamp, &refTime, sizeof(skd_timestamp_t));

  //addTime(pTimestamp, secPassed);

} 

void skd_time_srv_deInit( ){

  app_timer_stop(rtcTimerId); 
  app_timer_stop(syncTimerId); 
  app_timer_stop(bleTimerId); 
  app_timer_stop(gnssTimerId); 
  
  memset( &crntApi, NULL, sizeof(skd_time_api_t) );
  memset( &crntConfig, NULL, sizeof(skd_time_config_t));

}   