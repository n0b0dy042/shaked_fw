#ifndef SKD_BUT_SRV_H
#define SKD_BUT_SRV_H

#include "sha_ked_common.h"


typedef enum {

  BUT_SINGLE_PRESS = 1,
  BUT_DOUBLE_PRESS, 
  BUT_TRIPLE_PRESS,
  BUT_LONG_PRESS

}skd_but_msg_type;  


typedef struct {

  uint8_t msgType;

} skd_but_srv_msg_t;

typedef void     (*eventDetected_t) (skd_but_msg_type evntId);

typedef struct{
  
  eventDetected_t evntDetectedCb; 

}skd_but_srv_api_t;
 
  
typedef skd_button_settings_t skd_but_config_t; 

void skd_but_srv_init   (skd_but_config_t* pConfig, skd_but_srv_api_t* pApi );
void skd_but_srv_deInit ();
void skd_but_srv_process();


#endif