#include "sha_ked_fds.h"
#include "fds.h" 
#include "nrf_delay.h"
#include "sha_ked_debug.h"

#if SHA_KED_FDS_LOG_ENABLED == 1 
    #define NRF_LOG_LEVEL               SHA_KED_FDS_LOG_LEVEL
    #define NRF_LOG_INFO_COLOR          SHA_KED_FDS_INFO_LOG_COLOR
    #define NRF_LOG_DEBUG_COLOR         SHA_KED_FDS_DEBUG_LOG_COLOR
#else 
    #define NRF_LOG_LEVEL       0
#endif 

#define NRF_LOG_MODULE_NAME     sha_ked_fds
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"
NRF_LOG_MODULE_REGISTER();


#define DEFAULT_WAIT_TIMEOUT_MS     1000   



// This file stores sha-ked applitcation configs 
#define SHA_KED_APP_CONFIG_FILE_ID        (0xF010)

#define BLE_SETTINGS_REC_KEY_ID           (0x1111)
#define GNSS_SETTINGS_REC_KEY_ID          (0x2222)
#define TIMER_SETTINGS_REC_KEY_ID         (0x3333)
#define MD_SETTINGS_REC_KEY_ID            (0x4444)
#define LED_SETTINGS_REC_KEY_ID           (0x5555)
#define BUT_SETTTING_REC_KEY_ID           (0x6666)
#define DB_SETTINGS_REC_KEY_ID            (0x7777)


// This file stores system configs sush as boot coutner 
// and boot time.
#define SYSTEM_CONFIG_FILE_ID             (0xF011)

#define BOOT_CNTR_RECORD_ID               (0x0001)
#define BOOT_TIME_REC_KEY_ID              (0x8888)

/* Array to map FDS events to strings. */
static char const * fds_evt_str[ ] =
{
    "FDS_EVT_INIT",
    "FDS_EVT_WRITE",
    "FDS_EVT_UPDATE",
    "FDS_EVT_DEL_RECORD",
    "FDS_EVT_DEL_FILE",
    "FDS_EVT_GC",
};

/* Array to map FDS return values to strings. */
char const * fds_err_str[ ] =
{
    "FDS_SUCCESS",
    "FDS_ERR_OPERATION_TIMEOUT",
    "FDS_ERR_NOT_INITIALIZED",
    "FDS_ERR_UNALIGNED_ADDR",
    "FDS_ERR_INVALID_ARG",
    "FDS_ERR_NULL_ARG",
    "FDS_ERR_NO_OPEN_RECORDS",
    "FDS_ERR_NO_SPACE_IN_FLASH",
    "FDS_ERR_NO_SPACE_IN_QUEUES",
    "FDS_ERR_RECORD_TOO_LARGE",
    "FDS_ERR_NOT_FOUND",
    "FDS_ERR_NO_PAGES",
    "FDS_ERR_USER_LIMIT_REACHED",
    "FDS_ERR_CRC_CHECK_FAILED",
    "FDS_ERR_BUSY",
    "FDS_ERR_INTERNAL",
};

/* Flag to check fds initialization. */
static bool volatile m_fds_initialized;



typedef  struct{
  
  uint8_t buff[200];

}__attribute__((packed, aligned(1))) alignedBuf_t;  
 

static alignedBuf_t alignedBuf = {NULL}; 
static bool volatile m_fds_gsIsFinished = true; 
static bool volatile m_fds_writeIsDone  = true;
static bool volatile m_fds_deleteIsDone  = true;

static void fds_evt_handler(fds_evt_t const * p_evt)
{
    NRF_LOG_DEBUG("Event: %s received (%s)",
                  fds_evt_str[p_evt->id],
                  fds_err_str[p_evt->result]);
    NRF_LOG_FLUSH(); 

    switch (p_evt->id)
    {
        case FDS_EVT_INIT:
            if (p_evt->result == FDS_SUCCESS)
            {
                m_fds_initialized = true;
            }
            break;

        case FDS_EVT_WRITE:
        {   
            m_fds_writeIsDone  = true;
            
            if (p_evt->result == FDS_SUCCESS)
            {
                NRF_LOG_DEBUG("Record ID:\t0x%04x",  p_evt->write.record_id);
                NRF_LOG_DEBUG("File ID:\t0x%04x",    p_evt->write.file_id);
                NRF_LOG_DEBUG("Record key:\t0x%04x", p_evt->write.record_key);
                NRF_LOG_FLUSH(); 
            }

            if (p_evt->result != FDS_SUCCESS)
            {
                NRF_LOG_ERROR("Fail to write a recrod with error %s", fds_err_str[p_evt->result] );
            }

        } break;

        case FDS_EVT_DEL_RECORD:
        {
            if (p_evt->result == FDS_SUCCESS)
            {
                NRF_LOG_DEBUG("Record ID:\t0x%04x",  p_evt->del.record_id);
                NRF_LOG_DEBUG("File ID:\t0x%04x",    p_evt->del.file_id);
                NRF_LOG_DEBUG("Record key:\t0x%04x", p_evt->del.record_key);
                NRF_LOG_FLUSH(); 
            }

            if (p_evt->result != FDS_SUCCESS)
            {
                NRF_LOG_ERROR("Fail to delete a record with error %s", fds_err_str[p_evt->result] ) ;
            }

            //m_delete_all.pending = false;
        } break;
        case FDS_EVT_GC:{

            m_fds_gsIsFinished = true;
            
            NRF_LOG_INFO("Garbage collection complete."); 
            if (p_evt->result != FDS_SUCCESS)
            {
                NRF_LOG_ERROR("Fail to do gs with error %s", fds_err_str[p_evt->result] ); 
            }
        
        }break;

        case FDS_EVT_DEL_FILE:
            
            m_fds_deleteIsDone = true;
            if (p_evt->result != FDS_SUCCESS)
            {
                NRF_LOG_ERROR("Fail to delete file with error %s", fds_err_str[p_evt->result] ); 
            }
        
        break; 
        default:
            break;
    }
}


ret_code_t skd_fds_init(){
    ret_code_t        ret; 
    fds_stat_t        stat                  = {0}; 
    
    m_fds_gsIsFinished = true; 

    /* Register first to receive an event when initialization is complete. */
    (void) fds_register(fds_evt_handler);

    ret = fds_init();
    
    if(ret != FDS_SUCCESS) {
      
      NRF_LOG_INFO("Finishing fds_init operation with error: %s", fds_err_str[ret]); 
      NRF_LOG_PROCESS();
      
      return ret;
    }

    while (!m_fds_initialized); 
      
        
    NRF_LOG_DEBUG("Reading flash usage statistics...");
    NRF_LOG_FLUSH();
    
    ret = fds_stat(&stat);
    APP_ERROR_CHECK(ret);
    
    NRF_LOG_DEBUG("FDS mumber of pages avalible: %d",                               stat.pages_available);
    NRF_LOG_DEBUG("FDS number of open records: %d",                                 stat.open_records);
    NRF_LOG_DEBUG("FDS number of valid records: %d",                                stat.valid_records);
    NRF_LOG_DEBUG("FDS number of deleted records: %d",                              stat.dirty_records);
    NRF_LOG_DEBUG("FDS number of words reserved: %d",                               stat.words_reserved);
    nrf_delay_ms(15); 
    NRF_LOG_FLUSH();
    NRF_LOG_DEBUG("FDS number of words written to flash: %d",                       stat.words_used);
    NRF_LOG_DEBUG("FDS number of free contiguous words: %d",                        stat.largest_contig);
    NRF_LOG_DEBUG("FDS The largest number of words that can be reclaimed: %d",      stat.freeable_words);
    NRF_LOG_DEBUG("Filesystem corruption has been detected.: %s",                   stat.corruption? "DETECTED" : "NOT_DETECTED");
    nrf_delay_ms(15); 
    NRF_LOG_FLUSH();
    

    return NULL; 

}

ret_code_t skd_fds_deInit(){
  
  ret_code_t ret; 
  m_fds_deleteIsDone = false; 

  //Delete sha ked settings file
  ret = fds_file_delete(SHA_KED_APP_CONFIG_FILE_ID); 
  
  if(ret != FDS_SUCCESS){
         
    NRF_LOG_ERROR("Finishing fds_file_delete operation with error: %s", fds_err_str[ret]); 
    NRF_LOG_PROCESS();
    return ret; 

  }  

  //Wait until deleting done
  uint32_t cnrtMs = DEFAULT_WAIT_TIMEOUT_MS;
  while( !m_fds_deleteIsDone && cnrtMs-- ){cnrtMs--; nrf_delay_ms(1);}; 

  m_fds_gsIsFinished = false;

  NRF_LOG_INFO("Running a garbage collector.");
  ret = fds_gc();
    
  if(ret != FDS_SUCCESS){
         
    NRF_LOG_ERROR("Finishing fds_file_delete operation with error: %s", fds_err_str[ret]); 
    NRF_LOG_PROCESS();
    return ret; 
  }  
 
  NRF_LOG_INFO("Waiting for a garbage collector to finish.");
  NRF_LOG_PROCESS();
  
  cnrtMs = 1000;
  while( !m_fds_gsIsFinished || cnrtMs-- ){cnrtMs--; nrf_delay_ms(1);} ; 

  return FDS_SUCCESS;

}

static  ret_code_t skd_fds_writeRecord( fds_record_desc_t       * const p_desc,
                                        fds_record_t      const * const p_record){
  
  ret_code_t      ret;
   
  m_fds_writeIsDone = false;
  ret = fds_record_write(p_desc, p_record);
       
  if(ret == FDS_ERR_NO_SPACE_IN_FLASH){
  
    NRF_LOG_WARNING("No space to write into the flash. Trying to lanch garbage collector");
    
    m_fds_gsIsFinished = false;
    ret = fds_gc();
    

    //Wait until garbage collector is done.
    uint32_t cnrtMs = DEFAULT_WAIT_TIMEOUT_MS;
    while( !m_fds_gsIsFinished && cnrtMs-- ){cnrtMs--; nrf_delay_ms(1);} ; 
         
    if(cnrtMs == NULL ) {
          
              NRF_LOG_ERROR("Fail to clean flash");

              m_fds_gsIsFinished = true;
              m_fds_writeIsDone  = true;
              return FDS_ERR_NO_SPACE_IN_FLASH; 
    } 
         
    ret = fds_record_write(p_desc, p_record);
  } 
  
  if(ret != FDS_SUCCESS){
         
    NRF_LOG_ERROR("Finishing fds_record_write operation with error: %s", fds_err_str[ret]); 
    NRF_LOG_PROCESS();
    return ret; 
  }  
        
 
  //Wait until writting done
  uint32_t cnrtMs = DEFAULT_WAIT_TIMEOUT_MS;
  while( !m_fds_writeIsDone && cnrtMs-- ){cnrtMs--; nrf_delay_ms(1);}; 
  

  if(cnrtMs == NULL ) return FDS_ERR_OPERATION_TIMEOUT; 

  return FDS_SUCCESS; 

}

static ret_code_t skd_fds_updateRecord( fds_record_desc_t       * const p_desc,
                                 fds_record_t      const * const p_record){
  ret_code_t ret;
  m_fds_writeIsDone  = false;

  ret = fds_record_update(p_desc, p_record); 
      
  if(ret == FDS_ERR_NO_SPACE_IN_FLASH){

    NRF_LOG_WARNING("No space to write into the flash. Trying to lanch garbage collector");
    
    m_fds_gsIsFinished = false;

    ret = fds_gc();
    
    uint32_t cnrtMs = 1000;
    while( !m_fds_gsIsFinished || cnrtMs-- ){cnrtMs--; nrf_delay_ms(1);} ; 
         
    if(cnrtMs == NULL ) {
          
          NRF_LOG_ERROR("Fail to clean flash");

              m_fds_gsIsFinished = true;
              m_fds_writeIsDone  = true;
         
          return FDS_ERR_NO_SPACE_IN_FLASH; 
        }
     
      
    ret = fds_record_update(p_desc, p_record); 

  } 
   

  if(ret != FDS_SUCCESS){
         
    NRF_LOG_ERROR("Finishing fds_record_write operation with error: %s", fds_err_str[ret]); 
    NRF_LOG_PROCESS();
    return ret; 
  }  

  //Wait until writting done
  uint32_t cnrtMs = DEFAULT_WAIT_TIMEOUT_MS;
  while( !m_fds_writeIsDone && cnrtMs-- ){cnrtMs--; nrf_delay_ms(1);}; 

  if(cnrtMs == NULL ) return FDS_ERR_OPERATION_TIMEOUT; 
  
  return FDS_SUCCESS; 

}

static  ret_code_t skd_fds_writeData (uint16_t fileId, uint16_t recordId, uint8_t* pData, uint16_t dataLen){

  ret_code_t        ret;
  fds_record_desc_t desc                  = {0};
  fds_find_token_t  tok                   = {0};
  fds_record_t      record                = {0};
  
  memcpy(&alignedBuf,  pData, dataLen);


  //Set record
  record.file_id           = fileId;
  record.key               = recordId;
  record.data.p_data       = (uint8_t*)&alignedBuf; 
  record.data.length_words = (dataLen + 3) / sizeof(uint32_t);; 
  
  ret = fds_record_find(fileId, recordId, &desc, &tok); 
  
  switch(ret){
    case FDS_SUCCESS:{
      
      NRF_LOG_INFO("Found the record. Try to update if");
      
      ret =   skd_fds_updateRecord(&desc, &record);  

      if(ret != FDS_SUCCESS){
         
            NRF_LOG_ERROR("fail to update record with error: %s", fds_err_str[ret]); 
            NRF_LOG_PROCESS();
            return ret; 
      } 

    
    }break;
    case FDS_ERR_NOT_FOUND:{
   
       NRF_LOG_WARNING("No records found. Create new one");
       
       ret =   skd_fds_writeRecord(&desc, &record);  
      
       if(ret != FDS_SUCCESS){
         
            NRF_LOG_ERROR("fail to update record with error: %s", fds_err_str[ret]); 
            NRF_LOG_PROCESS();
            return ret; 
       } 
      
    }break;  
    default:{
    
      NRF_LOG_ERROR("Finishing fds_record_find operation with error: %s", fds_err_str[ret]); 
      NRF_LOG_PROCESS();
      return ret;  
    
    } break;

  }
  
  return FDS_SUCCESS; 

} 

static  ret_code_t skd_fds_readData (uint16_t fileId, uint16_t recordId, uint8_t* pData, uint16_t dataLen){
  
  ret_code_t          ret; 
  fds_record_desc_t   desc                  = {NULL};
  fds_find_token_t    tok                   = {NULL};
  fds_flash_record_t  record                = {NULL};
    

  ret = fds_record_find(fileId, recordId, &desc, &tok); 
  
  if(ret != FDS_SUCCESS){ 
  
    NRF_LOG_ERROR("Finishing fds_record_find operation with error: %s", fds_err_str[ret]); 
    NRF_LOG_PROCESS();
    return ret; 

  } 
  
  ret = fds_record_open(&desc, &record); 
  
  if(ret != FDS_SUCCESS){ 
  
    NRF_LOG_ERROR("Finishing fds_record_open operation with error: %s", fds_err_str[ret]); 
    NRF_LOG_PROCESS();
    return ret; 

  } 
  
  memcpy(pData, record.p_data, dataLen); 

  ret = fds_record_close(&desc); 
  
  if(ret != FDS_SUCCESS){ 
  
    NRF_LOG_ERROR("Finishing fds_record_close operation with error: %s", fds_err_str[ret]); 
    NRF_LOG_PROCESS();
    return ret; 

  } 

  return FDS_SUCCESS;



}

ret_code_t skd_fds_readBleSettings(skd_ble_settings_t* pSettings){
  return skd_fds_readData(  SHA_KED_APP_CONFIG_FILE_ID, 
                            BLE_SETTINGS_REC_KEY_ID, 
                            (uint8_t*)pSettings,
                            sizeof(skd_ble_settings_t));

}
ret_code_t skd_fds_writeBleSettings(skd_ble_settings_t* pSettings){

    return skd_fds_writeData( SHA_KED_APP_CONFIG_FILE_ID, 
                              BLE_SETTINGS_REC_KEY_ID, 
                             (uint8_t*)pSettings,
                             sizeof(skd_ble_settings_t));
}

ret_code_t skd_fds_readGNSSSettings(skd_gnss_settings_t* pSettings){
  
  return skd_fds_readData(  SHA_KED_APP_CONFIG_FILE_ID, 
                            GNSS_SETTINGS_REC_KEY_ID, 
                            (uint8_t*)pSettings,
                            sizeof(skd_gnss_settings_t));
}
ret_code_t skd_fds_writeGNSSSettings(skd_gnss_settings_t* pSettings){

    return skd_fds_writeData( SHA_KED_APP_CONFIG_FILE_ID, 
                             GNSS_SETTINGS_REC_KEY_ID, 
                             (uint8_t*)pSettings,
                             sizeof(skd_gnss_settings_t));
}


ret_code_t skd_fds_readMSSettings(skd_ms_settings_t* pSettings){

  return skd_fds_readData(  SHA_KED_APP_CONFIG_FILE_ID, 
                            MD_SETTINGS_REC_KEY_ID, 
                            (uint8_t*)pSettings,
                            sizeof(skd_ms_settings_t));
}


ret_code_t skd_fds_writeMSSettings(skd_ms_settings_t* pSettings){

   return skd_fds_writeData( SHA_KED_APP_CONFIG_FILE_ID, 
                             MD_SETTINGS_REC_KEY_ID, 
                             (uint8_t*)pSettings,
                             sizeof(skd_ms_settings_t));
}


ret_code_t skd_fds_readTimerSettings(skd_timer_settings_t* pSettings){

  return skd_fds_readData(   SHA_KED_APP_CONFIG_FILE_ID, 
                            TIMER_SETTINGS_REC_KEY_ID, 
                            (uint8_t*)pSettings,
                            sizeof(skd_timer_settings_t));
}

ret_code_t skd_fds_writeTimerSettings(skd_timer_settings_t* pSettings){

   return skd_fds_writeData( SHA_KED_APP_CONFIG_FILE_ID, 
                             TIMER_SETTINGS_REC_KEY_ID, 
                             (uint8_t*)pSettings,
                             sizeof(skd_timer_settings_t));
}


ret_code_t skd_fds_readDBSettings(skd_db_settings_t* pSettings){

 return skd_fds_readData(   SHA_KED_APP_CONFIG_FILE_ID, 
                            DB_SETTINGS_REC_KEY_ID, 
                            (uint8_t*)pSettings,
                            sizeof(skd_db_settings_t));

}

ret_code_t skd_fds_writeDBSettings(skd_db_settings_t* pSettings){

  return skd_fds_writeData( SHA_KED_APP_CONFIG_FILE_ID, 
                             DB_SETTINGS_REC_KEY_ID, 
                             (uint8_t*)pSettings,
                             sizeof(skd_db_settings_t));
}


ret_code_t skd_fds_readLEDSettings(skd_led_settings_t* pSettings){
 
 return skd_fds_readData(   SHA_KED_APP_CONFIG_FILE_ID, 
                            LED_SETTINGS_REC_KEY_ID, 
                            (uint8_t*)pSettings,
                            sizeof(skd_led_settings_t));
}

ret_code_t skd_fds_writeLEDSettings(skd_led_settings_t* pSettings){

   return skd_fds_writeData( SHA_KED_APP_CONFIG_FILE_ID, 
                             LED_SETTINGS_REC_KEY_ID, 
                             (uint8_t*)pSettings,
                             sizeof(skd_led_settings_t));

}


ret_code_t skd_fds_readButSettings(skd_button_settings_t* pSettings){

   return skd_fds_readData( SHA_KED_APP_CONFIG_FILE_ID, 
                            BLE_SETTINGS_REC_KEY_ID, 
                            (uint8_t*)pSettings,
                            sizeof(skd_button_settings_t));
}

ret_code_t skd_fds_writeButSettings(skd_button_settings_t* pSettings){

   return skd_fds_writeData( SHA_KED_APP_CONFIG_FILE_ID, 
                             BLE_SETTINGS_REC_KEY_ID, 
                             (uint8_t*)pSettings,
                             sizeof(skd_button_settings_t));
}



ret_code_t  skd_fds_readSKDSettings     (skd_settings_t*        pSettings){
  
   skd_fds_readBleSettings   (&pSettings->bleSettings);
   skd_fds_readGNSSSettings  (&pSettings->gnnsSettings);
   skd_fds_readTimerSettings (&pSettings->timerSettings);
   skd_fds_readDBSettings    (&pSettings->dbSettings);
   skd_fds_readMSSettings    (&pSettings->msSettings);
   skd_fds_readLEDSettings   (&pSettings->ledSettings);
   skd_fds_readButSettings   (&pSettings->butSettings);
}

ret_code_t  skd_fds_writeSKDSettings    (skd_settings_t*        pSettings){

   skd_fds_writeBleSettings   (&pSettings->bleSettings);
   skd_fds_writeGNSSSettings  (&pSettings->gnnsSettings);
   skd_fds_writeTimerSettings (&pSettings->timerSettings);
   skd_fds_writeDBSettings    (&pSettings->dbSettings);
   skd_fds_writeMSSettings    (&pSettings->msSettings);
   skd_fds_writeLEDSettings   (&pSettings->ledSettings);
   skd_fds_writeButSettings   (&pSettings->butSettings);


}


ret_code_t skd_fds_readBootCnt(uint32_t* pBootCnt){

  return skd_fds_readData(  SYSTEM_CONFIG_FILE_ID, 
                            BOOT_CNTR_RECORD_ID, 
                            (uint8_t*)pBootCnt,
                            sizeof(uint32_t));
 
}


ret_code_t     skd_fds_writeBootCnt(uint32_t* pBootCnt){
  
  return skd_fds_writeData( SYSTEM_CONFIG_FILE_ID, 
                            BOOT_CNTR_RECORD_ID, 
                            (uint8_t*)pBootCnt,
                            sizeof(uint32_t));
   
}

  
ret_code_t  skd_fds_readBootTime  (skd_timestamp_t*       pTimeStamp){

  return skd_fds_readData(  SYSTEM_CONFIG_FILE_ID, 
                            BOOT_TIME_REC_KEY_ID, 
                            (uint8_t*)pTimeStamp,
                            sizeof(skd_timestamp_t));
} 

ret_code_t  skd_fds_writeBootTime (skd_timestamp_t*       pTimeStamp){

  return skd_fds_writeData(  SYSTEM_CONFIG_FILE_ID, 
                             BOOT_TIME_REC_KEY_ID, 
                             (uint8_t*)pTimeStamp,
                             sizeof(skd_timestamp_t));
}  