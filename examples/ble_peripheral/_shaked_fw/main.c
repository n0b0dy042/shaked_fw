/**
 * Copyright (c) 2014 - 2018, Nordic Semiconductor ASA
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form, except as embedded into a Nordic
 *    Semiconductor ASA integrated circuit in a product or a software update for
 *    such product, must reproduce the above copyright notice, this list of
 *    conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 *
 * 3. Neither the name of Nordic Semiconductor ASA nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * 4. This software, with or without modification, must only be used with a
 *    Nordic Semiconductor ASA integrated circuit.
 *
 * 5. Any software provided in binary form under this license must not be reverse
 *    engineered, decompiled, modified and/or disassembled.
 *
 * THIS SOFTWARE IS PROVIDED BY NORDIC SEMICONDUCTOR ASA "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NORDIC SEMICONDUCTOR ASA OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
/** @file
 *
 * @defgroup ble_sdk_uart_over_ble_main main.c
 * @{
 * @ingroup  ble_sdk_app_nus_eval
 * @brief    UART over BLE application main file.
 *
 * This file contains the source code for a sample application that uses the Nordic UART service.
 * This application uses the @ref srvlib_conn_params module.
 */


#include <stdint.h>
#include <string.h>
#include "nordic_common.h"
#include "nrf.h"
#include "app_util_platform.h"

#include "nrf_pwr_mgmt.h"
#include "nrf_delay.h"
#include "app_timer.h"
#include "nrf_drv_clock.h"


#include "skd_time_srv.h" 
#include "sha_ked_debug.h"
#include "sha_ked_app.h"
#include "sha_ked_fds.h"



#if SHA_KED_MAIN_LOG_ENABLED == 1 
    #define NRF_LOG_LEVEL               SHA_KED_MAIN_LOG_LEVEL
    #define NRF_LOG_INFO_COLOR          SHA_KED_MAIN_INFO_LOG_COLOR
    #define NRF_LOG_DEBUG_COLOR         SHA_KED_MAIN_DEBUG_LOG_COLOR
#else 
    #define NRF_LOG_LEVEL       0
#endif 

#define NRF_LOG_MODULE_NAME     sha_ked_main
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"
NRF_LOG_MODULE_REGISTER();


static skd_app_requestStatus_t updateBLESettingsCb    (skd_ble_settings_t*      pSettings);
static skd_app_requestStatus_t updateGNSSSettingsCb   (skd_gnss_settings_t*     pSettings);
static skd_app_requestStatus_t updateMSSettingsCb     (skd_ms_settings_t*       pSettings);
static skd_app_requestStatus_t updateLEDSettingsCb    (skd_led_settings_t*      pSettings);
static skd_app_requestStatus_t updateDBSettingsCb     (skd_db_settings_t*       pSettings);
static skd_app_requestStatus_t updateTimeSettingsCb   (skd_timer_settings_t*    pSettings);
static skd_app_requestStatus_t updateButSettingsCb    (skd_button_settings_t*   pSettings);


static skd_app_requestStatus_t  requestDFUCb();

static uint8_t isNeedDFU  = NULL; 



uint32_t nrf_log_getTimeStamp(){
  
  uint64_t          crntTimeMs;
  skd_timestamp_t   crntTimeStamp; 
  skd_time_t*       pCrntTime = &crntTimeStamp.time; 

  skd_time_srv_getTime(&crntTimeStamp); 
  
   
  crntTimeMs = pCrntTime->microseconds + pCrntTime->seconds*1000 + pCrntTime->minutes*60*1000 
             + pCrntTime->hours*60*60*1000; 
              
  return APP_TIMER_TICKS(crntTimeMs); 
}
 
/**@brief Function for initializing the nrf log module.
 */
static void log_init(void)
{
    //ret_code_t err_code = NRF_LOG_INIT(NULL);
    ret_code_t err_code = NRF_LOG_INIT(nrf_log_getTimeStamp);
    APP_ERROR_CHECK(err_code);

    NRF_LOG_DEFAULT_BACKENDS_INIT();
}

/**@brief Function for initializing power management.
 */
static void power_management_init(void)
{
    ret_code_t err_code;
    err_code = nrf_pwr_mgmt_init();
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for handling the idle state (main loop).
 *
 * @details If there is no pending log operation, then sleep until next the next event occurs.
 */
static void idle_state_handle(void)
{
    UNUSED_RETURN_VALUE(NRF_LOG_PROCESS());
    //nrf_pwr_mgmt_run();
}



skd_app_requestStatus_t enterDFU(){
  uint32_t newBootCntr; 
  NRF_LOG_INFO("Preparing to enter DFU mode");
  NRF_LOG_FLUSH(); 
  
  nrf_delay_ms(200); // Give  ble stack a time to finish its routine
  sha_ked_app_deInit();
  
  // Set boot conter to Zero
  newBootCntr = NULL; 
  skd_fds_writeBootCnt(&newBootCntr);

  // Erase every setting before
  skd_fds_deInit();  // Clean last settings 


  NRF_LOG_INFO("Entering DFU mode");
  NRF_LOG_PROCESS();
   
  isNeedDFU = NULL;
  sha_ked_app_goDFU(); //Need to enable softdevice in order to lanch bootloader!!
  sd_power_gpregret_set(0, 0x01);
  sd_nvic_SystemReset();
  
  return DFU_GRANTED;

} 


void clock_init(){

    ret_code_t err_code; 

    err_code = nrf_drv_clock_init();

    APP_ERROR_CHECK(err_code);
    
    nrf_drv_clock_lfclk_request(NULL); 

    err_code = app_timer_init(); 
    
    APP_ERROR_CHECK(err_code);

} 



/**@brief Application main function.
 */

int main(void)
{
    ret_code_t                ret          = NULL;     
    sha_ked_app_config_t      config       = { NULL };
    uint32_t*                 pBootCounter = NULL;                  
    skd_timestamp_t*          pBootTime    = { NULL };
    skd_settings_t*           pSettings    = NULL;        
    

    log_init();
    clock_init();
    
    NRF_LOG_INFO("Starting sha_ked_app") 
    NRF_LOG_PROCESS();

    ret = skd_fds_init();
    
    APP_ERROR_CHECK(ret);

    //Check system boot coutner
    pBootCounter = &config.SystemBootCounter;  
    ret = skd_fds_readBootCnt(pBootCounter); 

    if(ret != NRF_SUCCESS){ 
     
      NRF_LOG_WARNING("Fail to read boot counter with error %d. Set bootcounter to zero", ret );
      *pBootCounter = NULL;
       
    }else{
        
        NRF_LOG_INFO("Read the system boot counter. The boot counter value is %d ", *pBootCounter);
        
    
    } 
    

    //Check system boot time
    pBootTime = &config.SystemBootTime;
    ret       = skd_fds_readBootTime(pBootTime);
    
    if(ret != NRF_SUCCESS){

      NRF_LOG_WARNING("Fail to read boot time with error %d. Set default boot time: ", ret );
      
      skd_set_default_time(pBootTime);
      ret  = skd_fds_writeBootTime(pBootTime);
      APP_ERROR_CHECK(ret); 
       
    }else {

       NRF_LOG_INFO("Read the system boot time from FLASH. ");
        
    }  
    
    skd_data_t* pData = &pBootTime->date;
    skd_time_t* pTime = &pBootTime->time; 

    NRF_LOG_DEBUG("The system boot time is: %d:%d:%d|%d:%d:%d",  pTime->hours,       pTime->minutes, pTime->seconds, 
                                                                 (pData->year +2000), pData->month,   pData->day );  

    skd_debug_printTimeStampInLine(pBootTime); 

    //Check sha ked settings
    pSettings = &config.settings; 
    if(*pBootCounter == NULL) {
        
        NRF_LOG_WARNING("First lanch detected. Set default sha ked settings!");

        skd_set_defualt_settings(pSettings);  
        skd_fds_writeSKDSettings(pSettings);
         
    } else{
      

      ret = skd_fds_readSKDSettings(pSettings); 
            
      APP_ERROR_CHECK(ret); 

      NRF_LOG_INFO("Read SHA-KED app settings from FLASH: ");
      
    } 
    
    skd_debug_printSKDsettings(&config.settings); 
    
    NRF_LOG_INFO("Update and save boot counter to FLASH.");
    
    config.SystemBootCounter++;
    ret = skd_fds_writeBootCnt(&config.SystemBootCounter);  

    APP_ERROR_CHECK(ret);

    sha_ked_app_api_t api = { 
              
              .requestDFUCb          = requestDFUCb,
              .updateBLESettingsCb   = updateBLESettingsCb,
              .updateGNSSSettingsCb  = updateGNSSSettingsCb,
              .updateMSSettingsCb    = updateMSSettingsCb,
              .updateLEDSettingsCb   = updateLEDSettingsCb,
              .updateButSettingsCb   = updateButSettingsCb,
              .updateDBSettingsCb    = updateDBSettingsCb,
              .updateTimeSettingsCb  = updateTimeSettingsCb        
              
    };


    sha_ked_app_init(&config, &api);
    
    // Enter main loop.
    for (;;)
    {
        
        sha_ked_app_process();         
        if(isNeedDFU)   enterDFU();
        idle_state_handle();

    }

}




static skd_app_requestStatus_t  requestDFUCb(skd_timestamp_t* pNextBootTime){

    NRF_LOG_INFO("Application ask to enter DFU mode"); 
    NRF_LOG_INFO("DFU granted");
    NRF_LOG_FLUSH();
    
    isNeedDFU = !NULL;
    
    if(pNextBootTime != NULL) skd_fds_writeBootTime(pNextBootTime); 

    return DFU_GRANTED;
} 

static skd_app_requestStatus_t  updateBLESettingsCb(skd_ble_settings_t* pSettings){
  
  NRF_LOG_INFO("Application wants to update BLE settings");  
  
  skd_fds_writeBleSettings(pSettings);

  return DFU_GRANTED;
}

static skd_app_requestStatus_t updateGNSSSettingsCb(skd_gnss_settings_t* pSettings){
  
  NRF_LOG_INFO("Application wants to update GNSS settings");  
  
  skd_fds_writeGNSSSettings(pSettings);

  return  DFU_GRANTED;
}

static skd_app_requestStatus_t updateMSSettingsCb(skd_ms_settings_t* pSettings){
  
  NRF_LOG_INFO("Application wants to update MS settings");  
  
  skd_fds_writeMSSettings(pSettings);

  return  DFU_GRANTED;
}

static skd_app_requestStatus_t updateLEDSettingsCb(skd_led_settings_t* pSettings){

  NRF_LOG_INFO("Application wants to update LED settings");  
  
  skd_fds_writeLEDSettings(pSettings); 

  return  DFU_GRANTED;
}

static skd_app_requestStatus_t updateDBSettingsCb(skd_db_settings_t* pSettings){

  NRF_LOG_INFO("Application wants to update DB settings");  
  
  skd_fds_writeDBSettings(pSettings);

  return  DFU_GRANTED;
}

static skd_app_requestStatus_t updateTimeSettingsCb(skd_timer_settings_t* pSettings){

  NRF_LOG_INFO("Application wants to update Timer settings");  
  
  ret_code_t ret = skd_fds_writeTimerSettings(pSettings);
  
  if(ret = NRF_SUCCESS){

    NRF_LOG_ERROR("Fail to save timer settings");

  } 

  return  DFU_GRANTED;
}

static skd_app_requestStatus_t updateButSettingsCb(skd_button_settings_t* pSettings){

  NRF_LOG_INFO("Application wants to update Button settings");
  
  skd_fds_writeButSettings(pSettings);

  return  DFU_GRANTED;
}

/**
 * @}
 */
